<?php 
	$blockRedirect = ($_SERVER['SERVER_ADDR'] == '192.168.2.22'|| $_SERVER['SERVER_ADDR'] == '70.32.79.141');
	require 'share/inc_share.php'; // all share values set here 
	$anticache = microtime();//'1.0';
  // DAVINDER use version when ready to go live / online demo

  $fbTest = ($_SERVER['SERVER_ADDR'] == '192.168.2.22' || $_SERVER['SERVER_ADDR'] == '70.32.79.141' || $_SERVER['SERVER_ADDR'] == 'localhost');
	$fbTest = false;
	$fbAppId = $fbTest ? '1722643848004587' : '1606787219590251';
?>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? require 'share/inc_metatags.php'; ?>
<style type="text/css">body { margin: 0px; overflow:hidden; }</style>
<link rel="stylesheet" href="style.css" />

    <!-- 3rd party libraries -->
	<script src="js/lib/ext/underscore.min.js"></script>
    <script src="js/lib/ext/pixi.js"></script>
	<script src="js/lib/koko/pixi_ext.js"></script>
    <script src="js/lib/ext/tweenmax.min.js"></script>
    <script src="js/lib/ext/soundjs-0.6.0.min.js"></script>
    <script src="js/lib/ext/jquery-2.1.3.min.js"></script>
    <script src="js/lib/ext/md5.js"></script>
    <!-- koko framework -->
    <script>var koko = {};</script>
    <script src="js/lib/koko/app.js?v=<?=$anticache;?>"></script>
    <script src="js/lib/koko/assets.js?v=<?=$anticache;?>"></script>
    <script src="js/lib/koko/audio.js?v=<?=$anticache;?>"></script>
    <script src="js/lib/koko/client.js?v=<?=$anticache;?>"></script>
    <script src="js/lib/koko/view.js?v=<?=$anticache;?>"></script>
    <script src="js/lib/koko/display.js?v=<?=$anticache;?>"></script>
    <script src="js/lib/koko/math.js?v=<?=$anticache;?>"></script>
    <script src="js/lib/koko/cookies.js?v=<?=$anticache;?>"></script>
	<!-- <script src="js/lib/koko/facebook.js?v=<?=$anticache;?>"></script> -->
    <script src="js/lib/koko/form.js?v=<?=$anticache;?>"></script>
    <script src="js/lib/koko/shorthand.js?v=<?=$anticache;?>"></script>
	<script src="js/lib/koko/userinput.js?v=<?=$anticache;?>"></script>
	<script src="js/lib/koko/extpixi/clipManager.js?v=<?=$anticache;?>"></script>
    <!-- framework ui -->
    <script src="js/lib/koko/ui/snapslider.js?v=<?=$anticache;?>"></script>
	<script src="js/lib/koko/ui/button.js?v=<?=$anticache;?>"></script>
    <script src="js/lib/koko/ui/panview.js?v=<?=$anticache;?>"></script>
    <script src="js/lib/koko/ui/toggle.js?v=<?=$anticache;?>"></script>
    <script src="js/lib/koko/ui/input.js?v=<?=$anticache;?>"></script>
    <!-- this app -->
    <script src="js/vm.js?v=<?=$anticache;?>"></script>
    <script src="js/controller/game.js?v=<?=$anticache;?>"></script>
	<script src="js/controller/collectables.js?v=<?=$anticache;?>"></script>
	<script src="js/controller/character.js?v=<?=$anticache;?>"></script>
	<script src="js/model/user.js?v=<?=$anticache;?>"></script>
	<script src="js/model/achievements.js?v=<?=$anticache;?>"></script>
	<script src="js/model/levels.js?v=<?=$anticache;?>"></script>
    <!-- views -->
    <script src="js/view/preloader.js?v=<?=$anticache;?>"></script>
	<script src="js/view/menubg.js?v=<?=$anticache;?>"></script>
	<script src="js/view/mainmenu.js?v=<?=$anticache;?>"></script>
	<script src="js/view/menuhud.js?v=<?=$anticache;?>"></script>
	<script src="js/view/leaderboard.js?v=<?=$anticache;?>"></script>
    <script src="js/view/levelselect.js?v=<?=$anticache;?>"></script>
	<script src="js/view/gameover.js?v=<?=$anticache;?>"></script>
	<script src="js/view/congratulations.js?v=<?=$anticache;?>"></script>
	<script src="js/view/ingamebg.js?v=<?=$anticache;?>"></script>
    <script src="js/view/char_select.js?v=<?=$anticache;?>"></script>
    <script src="js/view/settings.js?v=<?=$anticache;?>"></script>
    <script src="js/view/win_prizes.js?v=<?=$anticache;?>"></script>
	<script src="js/view/ingame.js?v=<?=$anticache;?>"></script>
	<script src="js/view/ingamecountdown.js?v=<?=$anticache;?>"></script>
	<script src="js/view/howtoplay.js?v=<?=$anticache;?>"></script>
	<script src="js/view/loader.js?v=<?=$anticache;?>"></script>
	<script src="js/view/connect_popup.js?v=<?=$anticache;?>"></script>
	<script src="js/view/connect_popup_connect.js?v=<?=$anticache;?>"></script>
	<script src="js/view/loaduser.js?v=<?=$anticache;?>"></script>
	<script src="js/view/game_complete.js?v=<?=$anticache;?>"></script>
	<script>
		document.addEventListener("visibilitychange", function() {
			if(document.hidden) 
			{
				window.currentMute = !!au.mute;
				koko.audio.setMute(true);
			}
			else koko.audio.setMute(window.currentMute);
		});
	</script>
</head>
<body>

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NKL9JQ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NKL9JQ');</script>
<!-- End Google Tag Manager -->


<a href="" target="_blank" class="hide" id="link"></a>
<div id="fb-root"></div>
<script>
  facebook.enabled = false;
 //  window.fbAsyncInit = function() {
 //    FB.init({
 //      appId      : '<?=$fbAppId;?>',
 //      status     : true,
 //      xfbml  	 : true
 //    });
	// facebook.enabled = false;
	// FB.Canvas.setSize({ height: 610 });
 //  };
 //  (function(){
 //     if (document.getElementById('facebook-jssdk')) return;
 //     var firstScriptElement = document.getElementsByTagName('script')[0];
 //     var facebookJS = document.createElement('script'); 
 //     facebookJS.id = 'facebook-jssdk';
 //     facebookJS.src = '//connect.facebook.net/en_US/all.js';
 //     firstScriptElement.parentNode.insertBefore(facebookJS, firstScriptElement);
 //   }());
</script>


<h1 itemprop="name" style="display:none;"><?=$title;?></h1>
<img itemprop="image" src="<?=$img;?>" style="display:none;" />
<p itemprop="description" style="display:none;"><?=$description;?></p>
<? 
// redirection code here 
echo '<!-- '.$blockRedirect.'||'.$_REQUEST['tab'].'||'.$deviceType.'-->';

if(!$blockRedirect && !$_REQUEST['tab'] && $deviceType != 'phone'){
// if not in a tab and not a mobile device redirect to facebook page
	echo '<script>top.location=\''.$url.'\'</script><body><html>';
	die();
}
?>
<script>
	app.init();
	
	app.setupComplete = function(){
        user.load();
		vm.init();
		app.resize();
	}
	window.addEventListener("load", app.loaded, false);
	
</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  //ga('create', 'UA-45466828-21', 'auto');
  //ga('send', 'pageview');
  // DAVINDER uncomment when ready to go live
</script>
    
<div id="fb-like" class="DOMscaleable hide">
    <a id="facebook_notconnected_like" href="https://www.facebook.com/VirginMoneyUK" target="_blank" class="fb-like-link"></a>
    <div id="facebook_connected_like" class="fb-like hide" data-href="https://www.facebook.com/VirginMoneyUK" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div>    
    </div>
<div id="fb-like-prizes" class="DOMscaleable hide">
    <a id="facebook_notconnected_like_prizes" href="https://www.facebook.com/VirginMoneyUK" target="_blank" class="fb-like-link"></a>
    <div id="facebook_connected_like_prizes" class="fb-like hide" data-href="https://www.facebook.com/VirginMoneyUK" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div>  
</div>
<div id="rotate_msg" class="rotate-device" style="display:none"></div>
    
</body>
</html>