// JavaScript Document
var EndGameView = koko.view({
	assets: [
        { id:'win_prizes_btn_desktop', src:'img/win_prizes_btn.png' }
	],
	isFinalThrow: false,
	like: null,
	buildView: function() {
		var g = app.guiSize, vp = app.viewPort, _t = this;
		
		this.like = document.getElementById('fb-like');
        
    
        var panelScale = 1;
        if(koko.client.system.isMobile) {
            panelScale = vp.height/750;
            if(panelScale > 1.2) panelScale = 1.2;
        }
        
        
		
		this.children = d.buildLayout([
			{ name:'panel_cont', type:'container', regX:226*as, regY:600*as, x: vp.centerX, y: vp.bottom-(20*as), s:panelScale },
				
				{ name: 'panel_bg', type:'sprite', id:'game_complete_panel', parent:'panel_cont' },
					{ name:'pb_cont', type:'container', parent:'panel_cont' },
						{ name: 'pb_lei', type:'sprite', id:'lei', parent:'pb_cont', y: -5*as, x: -5*as },
						{ name: 'pb_title', type:'sprite', id:'newPersonalBest', parent:'pb_cont', x: 308*as, y:20*as, regX:108*as, regY:30*as },
						
				{ name:'distance_txt', parent:'panel_cont', y: 110*as, type:'bmptext', text:'1027m', font:Math.floor(85*as)+'px ARMTB', tint:0x111111 },
			
				{ name: 'facebook_btn', type:'sprite', id:'facebook_share', parent:'panel_cont', y: 270*as, regX:46*as, x:156*as, regY:46*as, s:0.7 },
				{ name: 'twitter_btn', type:'sprite', id:'twitter_share', parent:'panel_cont', y: 270*as, regX:46*as, x:226*as, regY:46*as, s:0.7 },
				{ name: 'google_plus_btn', type:'sprite', id:'google_plus_share', parent:'panel_cont', y: 270*as, regX:46*as, x:296*as, regY:46*as, s:0.7 },
			
				{ name: 'home_btn', type:'sprite', id:'home_btn', parent:'panel_cont', y: 370*as, x:108*as, regX:42*as, regY:42*as },
				{ name: 'continue_btn', type:'sprite', id:'play_again_btn', parent:'panel_cont', y: 370*as, x:271*as, regX:115*as, regY:42*as },
				
				{ name:'toplink_cont', type:'container', x: vp.right - (20*as), y: vp.yop + (20*as)},
			
				{ name: 'settings_btn', type:'sprite', id:'settings_btn', x: -35*as, y: 36*as, regX:35*as, regY:36*as, parent:'toplink_cont' },
				{ name: 'settings_line', type:'sprite', id:'settings_line', x: -75*as, y: 6*as,  parent:'toplink_cont' },
				{ name: 'challengefriend_btn', type:'sprite', id:'challengefriend_btn', x: -155*as, y: 36*as, regY:36*as, regX:67*as, parent:'toplink_cont' },
	
				
				{ name: 'vm_header', type:'sprite', id:'virgin_money_header', x: vp.left, y:v.top },
		], this.container);

            
		this.facebook_btn = new button();
		this.facebook_btn.setContent(this.children.facebook_btn).over({s:0.65}).callback(function(b, e){
			au.play("pop_"+m.randomInt(1,2));
			
			facebook.checkLogin(function(){
				ga('send', 'event', 'Game', 'Share Score Facebook - Connected');
				facebook.share(app.shareURL+'&player_share=1&player_score='+_t.score);
			}, function(){
				ga('send', 'event', 'Game', 'Share Score Facebook - Not Connected');
				app.openLink('https://suitcasesling.kokogames.com/api/share/facebook?player_score='+_t.score, 'share', e);
			}, false);
			
		});
		
		this.twitter_btn = new button();
		this.twitter_btn.setContent(this.children.twitter_btn).over({s:0.65}).callback(function(b, e){
			au.play("pop_"+m.randomInt(1,2));
			ga('send', 'event', 'Game', 'Share Score Twitter');
			app.openLink('https://suitcasesling.kokogames.com/api/share/twitter?player_score='+_t.score, 'share', e);
		});
		
		this.google_plus_btn = new button();
		this.google_plus_btn.setContent(this.children.google_plus_btn).over({s:0.65}).callback(function(b, e){
			au.play("pop_"+m.randomInt(1,2));
			ga('send', 'event', 'Game', 'Share Score Google+');
			app.openLink('https://suitcasesling.kokogames.com/api/share/googleplus?player_score='+_t.score, 'share', e);
		});
		
		this.continue_btn = new button();
		this.continue_btn.setContent(this.children.continue_btn).over({s:0.95}).callback(function(){
			au.play("pop_"+m.randomInt(1,2));
			ga('send', 'event', 'Game', 'Play Again From Game Over');
			_t.playAgain();
		});		
		
		this.home_btn = new button();
		this.home_btn.setContent(this.children.home_btn).over({s:0.95}).callback(function(){
			au.play("pop_"+m.randomInt(1,2));
			ga('send', 'event', 'Game', 'Go Home From Game Over');
			_t.transitionOut();
		});		

	
		this.settings_btn = new button();
		this.settings_btn.setContent(this.children.settings_btn).over({s:0.95}).callback(function(){
			au.play("pop_"+m.randomInt(1,2));
			v.get('settings').transitionIn();
		});
		
		this.challengefriend_btn = new button();
		this.challengefriend_btn.setContent(this.children.challengefriend_btn).over({s:0.95}).callback(function(b, e){
			au.play("pop_"+m.randomInt(1,2));
			facebook.checkLogin(function(){
				ga('send', 'event', 'Game', 'Challenge Friends - Connected');
				facebook.share(app.shareURL);
			}, function(){
				ga('send', 'event', 'Game', 'Challenge Friends - Not Connected');
				app.openLink('https://suitcasesling.kokogames.com/api/share/facebook', 'share', e);
			}, false);
		});
		
		this.vm_header_btn = new button();
		this.vm_header_btn.setContent(this.children.vm_header).over({s:0.95}).callback(function(b, e){
			au.play("pop_"+m.randomInt(1,2));
			ga('send', 'event', 'Game', 'CTA Virgin Money');
			app.openLink('https://suitcasesling.kokogames.com/api/link/client1', 'link', e);
		});
				
	},
	pulsateTimer: null,
	transitionIn: function(){
		if(_.size(this.children)==0) this.buildView();
		
		//enable
		var _t = this;
		
		game.tweenPosition(1, 700, Back.easeOut);
		this.container.x = 700;
	
		
		d.animateObject(this.container, 1, { x: 0, ease:Back.easeOut});
		
		var bestAttempt = game.calculateScore();
		this.score = Math.floor(bestAttempt*game.distanceMult);
		this.children.distance_txt.setText(this.score + "m");
		this.children.distance_txt.x = (226*as) - ((this.score + "m").length * (25*as));
		
		
		
		//show hide pb
		this.children.pb_cont.visible = true;
		var lei = this.children.pb_lei;
		var title = this.children.pb_title;
		title.visible = false;
		
		//new pb
		if(game.score == game.pb) {

			title.scale.x = title.scale.y = 0;
			title.visible = true;
			d.animateObject(title, .3, { delay: 1.5, s:1.2, onComplete:function(){
				d.animateObject(title, .2, { delay:.5, s:1 });	
			}});
			
			setTimeout(function(){
				au.play('new_pb');
			}, 1600);
			
			ga('send', 'event', 'Game', 'New Personal Best Scored');
		}
            
        this.transitionedOut = false;
            
        facebook.checkLogin(function(){
            app.submitScore(function(){
                if(!_t.transitionedOut) {
                    v.get('leaderboard').transitionIn();	
                }
            });
        }, function(){
            v.get('leaderboard').transitionIn();
        }, false);
				
		//show
		this.children.toplink_cont.alpha = 0;
		this.children.vm_header.alpha = 0;
		d.animateObject(this.children.toplink_cont, .5, { delay:1, alpha: 1});
		d.animateObject(this.children.vm_header, .5, { delay:1, alpha: 1});
		
		setTimeout(function(){
			_t.enable();
		}, 1500);
		
		this.show();
		
		app.addResizeListener('endgame', function(){
				_t.resize();
		});
	},
	resize: function(){
		var c = this.children, vp = app.viewPort;
		c.vm_header.position.x = vp.left;
		c.vm_header.position.y = vp.top;
		
		c.toplink_cont.position.x = vp.right - (20*as);
		c.toplink_cont.position.y = vp.top + (20*as);
		
		var left = ((vp.x >0) ? vp.x : 0) + (vp.width * vp.s);
		var top = ((vp.y >0) ? vp.y : 0) + (vp.height * vp.s);
		
		this.like.style.left = (left-(110*vp.s*as)) + "px";
		this.like.style.top = (top-(50*vp.s*as)) + "px";
		
		this.like.style[koko.client.cssprops.transform] = "scale("+vp.s*2*as+")";		
	},
	enable: function(){
		this.facebook_btn.enable();
		this.twitter_btn.enable();
		this.google_plus_btn.enable();
		this.continue_btn.enable();
		this.home_btn.enable();
		this.vm_header_btn.enable();
		this.settings_btn.enable();
		this.challengefriend_btn.enable();
		this.like.className = this.like.className.replace(/hide/g, '').replace(/\s{2,}/g, ' ');
		this.resize();
		v.get('leaderboard').enable();		
	},
	disable: function(){
		this.facebook_btn.disable();
		this.twitter_btn.disable();
		this.google_plus_btn.disable();
		this.continue_btn.disable();
		this.home_btn.disable();
		this.vm_header_btn.disable();
		this.vm_header_btn.disable();
		this.settings_btn.disable();
		this.challengefriend_btn.disable();
		this.like.className = this.like.className + ' hide';
		v.get('leaderboard').disable();
	},
	transitionOut: function(){
        
        this.transitionedOut = true;
        
        //hide
		v.get('ingame').fadeOutCharacters();
		
		var _t = this;
		
		d.animateObject(this.children.toplink_cont, .2, { alpha: 0});
		d.animateObject(this.children.vm_header, .2, { alpha: 0, onComplete: function(){
			
			game.tweenPosition(1, game.xpos*-1, Sine.easeOut);
			var time = 800/game.xpos;
			
			d.animateObject(_t.container, time, { x: 800, onComplete:function(){
				_t.hide();	
			}});	
		}});
		
		setTimeout(function(){
			v.get('mainmenu').transitionIn();
		}, 1200);
		
		v.get('leaderboard').transitionOut();
		
		//disable
		this.disable();
		
		app.removeResizeListener('endgame');
	},
	playAgain: function(){
		clearInterval(this.pulsateTimer);
		
		v.get('ingame').fadeOutCharacters();
		var _t = this;
		d.animateObject(this.children.toplink_cont, .2, { alpha: 0});
		d.animateObject(this.children.vm_header, .2, { alpha: 0, onComplete: function(){
			
			game.tweenPosition(1, game.xpos*-1, Sine.easeOut);
			var time = 800/game.xpos;
			
			d.animateObject(_t.container, time, { x: 800, onComplete:function(){
				_t.hide();	
			}});	
		}});
		
		
		setTimeout(function(){
			v.get('ingamehud').transitionIn(); 
		}, 1200);
		
		v.get('leaderboard').transitionOut();
		
		//disable
		this.disable();
		//remove
		app.removeResizeListener('endgame');
	}
});