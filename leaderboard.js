// JavaScript Document
var LeaderboardView = koko.view({
	assets: [
		{ name:'leaderboard_json', src:'img/leaderboard.json?v=2' },
	],
	initialLoad:true,
	buildView: function() {
		var g = app.guiSize, vp = app.viewPort, _t = this;
		
		this.children = d.buildLayout([
			{ name:'bg', type:'shape', fill:0x000000, alpha:0, visible: false, w: g.width, h:g.height },
			//ze buttons
			{ name: 'lb_cont', type:'container',  x: vp.centerX, regX:320, y: vp.bottom-212},
			{ name: 'lb_main_btn_cont', type:'container',  x:320, regX:135*as, y:0, parent:'lb_cont'},
            
            { name: 'lb_main_connect', type:'sprite', id:'lb_main_btn_connect', x:-37*as, y:0, parent:'lb_main_btn_cont' },
			{ name: 'lb_main_btn_up', type:'sprite', id:'lb_main_btn_up', x:0, y:0, parent:'lb_main_btn_cont', visible:false },
			{ name: 'lb_main_btn_down', type:'sprite', id:'lb_main_btn_down', x:0 , y:0, parent:'lb_main_btn_cont', visible:false},
			
			{ name: 'lb_wrapper_cont', type:'container',  x:0, y:(53*as), parent:'lb_cont'},
			{ name: 'lb_panel', type:'sprite', id:'lb_panel', x: 0, y:0, parent:'lb_wrapper_cont'},
			
			{ name: 'lb_loader', type:'sprite', id:'lb_loader', regX:27.5, regY:27.5, x: 320, y: 100, parent:'lb_wrapper_cont', visible: false },
			
			
			{ name: 'lb_players_cont', type:'container', id:'lb_players_cont', x:55 , y:48, parent:'lb_wrapper_cont'},
			{ name: 'lb_players_mask', type:'mask', id:'lb_players_mask', width:533, height:120, parent:'lb_players_cont'},
			
						
			{ name: 'lb_arrowleft_btn', type:'container', x:26 , y:106, regY:46, regX:16, parent:'lb_wrapper_cont'},
				{ name: 'lb_arrowleft_active', type:'sprite', id:'lb_arrowleft_active', parent:'lb_arrowleft_btn'},
				{ name: 'lb_arrowleft_inactive', type:'sprite', id:'lb_arrowleft_inactive',  parent:'lb_arrowleft_btn', visible:false},
				
			{ name: 'lb_arrowright_btn', type:'container', x:613 , y:106, regY:46, regX:16, parent:'lb_wrapper_cont'},
				{ name: 'lb_arrowright_active', type:'sprite', id:'lb_arrowright_active', parent:'lb_arrowright_btn'},
				{ name: 'lb_arrowright_inactive', type:'sprite', id:'lb_arrowright_inactive',  parent:'lb_arrowright_btn', visible:false},
			
			{ name: 'lb_myfriends_cont', type:'container', id:'lb_myfriends_btn', x:110 , y:25, regY:16, regX:55, parent:'lb_wrapper_cont'},
			{ name: 'lb_friends_active', type:'sprite', id:'lb_friends_active', x:0, y:0, parent:'lb_myfriends_cont'},
			{ name: 'lb_friends_inactive', type:'sprite', id:'lb_friends_inactive', x:0, y:0, parent:'lb_myfriends_cont', visible:false},
			
			{ name: 'lb_everyone_cont', type:'container', id:'lb_everyone_btn', x:230 , y:25, regY:16, regX:55,  parent:'lb_wrapper_cont'},
			{ name: 'lb_everyone_active', type:'sprite', id:'lb_everyone_active', x:0, y:0, parent:'lb_everyone_cont'},
			{ name: 'lb_everyone_inactive', type:'sprite', id:'lb_everyone_inactive', x:0, y:0, parent:'lb_everyone_cont', visible:false},
			
			{ name: 'lb_invitefriends_btn', type:'sprite', id:'lb_invitefriends_btn', x:540 , y:25, regY:16, regX:45, parent:'lb_wrapper_cont'}
			
			
			
		], this.container);
		
		this.children.bg.hitArea = new PIXI.Rectangle(0, 0, g.width, g.height-200);
		
		_t.lb_main_btn_cont = new button();
		_t.lb_main_btn_cont.setContent(_t.children.lb_main_btn_cont).callback(function(){
			
			if(_t.isOpen){
				_t.closeLeaderboard();
			} else {
				//console.log('try login');
				facebook.checkLogin(function(){
					//console.log('open leaderboard');
					_t.openLeaderboard();					
				}, function(){}, true);
			}			
		});
		
		//firends button
		_t.lb_myfriends_cont = new button();
		_t.lb_myfriends_cont.setContent(_t.children.lb_myfriends_cont).over({s:0.95}).callback(function(){
			_t.loadLeaderboard('friends');
		});
		
		this.screen_btn = new button();
		this.screen_btn.setContent(this.children.bg).callback(function(){
			_t.closeLeaderboard();
		});
		
		//my friends button
		_t.lb_everyone_cont = new button();
		_t.lb_everyone_cont.setContent(_t.children.lb_everyone_cont).over({s:0.95}).callback(function(){
			_t.loadLeaderboard('everyone');
		});
		
		//invite button
		_t.lb_invitefriends_btn = new button();
		_t.lb_invitefriends_btn.setContent(_t.children.lb_invitefriends_btn).over({s:0.95}).callback(function(){
			facebook.inviteFriends('');
		});
		
		//left button
		_t.lb_arrowleft_btn = new button();
		_t.lb_arrowleft_btn.setContent(_t.children.lb_arrowleft_btn).over({s:0.95}).callback(function(){
			_t.slider.gotoSlide(_t.slider.currentSlideIndex-6,.2);
			
		});
		
	
		
		//right button
		_t.lb_arrowright_btn = new button();
		_t.lb_arrowright_btn.setContent(_t.children.lb_arrowright_btn).over({s:0.95}).callback(function(){
			_t.slider.gotoSlide(_t.slider.currentSlideIndex+6,.2);
			
		});
			
		//slider
		_t.slider = new snapslider();
		_t.container.addChild(_t.slider.container);
		_t.children.lb_wrapper_cont.addChild(_t.slider.container);
		_t.slider.container.position.x = 0;
		_t.slider.container.position.y = 0;
		
		_t.slider.setupChangeHandler(function(){
			if(_t.slider.currentSlideIndex == 0) {
				_t.lb_arrowleft_btn.disable();
				_t.children.lb_arrowleft_active.visible = false;
				_t.children.lb_arrowleft_inactive.visible = true;
			} else {
				_t.lb_arrowleft_btn.enable();
				_t.children.lb_arrowleft_active.visible = true;
				_t.children.lb_arrowleft_inactive.visible = false;
			}
			
			if(_t.slider.currentSlideIndex == _t.slider.totalSlides) {
				_t.lb_arrowright_btn.disable();
				_t.children.lb_arrowright_active.visible = false;
				_t.children.lb_arrowright_inactive.visible = true;
			} else {
				_t.lb_arrowright_btn.enable();
				_t.children.lb_arrowright_active.visible = true;
				_t.children.lb_arrowright_inactive.visible = false;
			}
			
			for( var i = 0; i < _t.slotbuttons.length; i++ ) {
				var b = _t.slotbuttons[i];
				if(b.data.index < _t.slider.currentSlideIndex || b.data.index > _t.slider.currentSlideIndex + 5) {
					b.disable();	
				} else {
					b.enable();	
				}
			}
		});
        
        
        facebook.checkLogin(function(){
            _t.children.lb_main_connect.visible = false;
            _t.children.lb_main_btn_up.visible = false;
            _t.children.lb_main_btn_down.visible = true;
        }, function(){
            
        }, false);
		
	},
	isMyFriends: false,
	isOpen: false,
	players:[],
	fbfriendids:false,
	slotbuttons:[],
	formatName:function(str){
		if(str.indexOf(' ')>-1){
			var array = str.split(' ');
			return array[0] + ' ' + array[(array.length-1)].substr(0,1) + '.';
		} else{
			return str;
		}
	},
	updateFriendsScores: function(complete){
		var url = 'https://suitcasesling.kokogames.com/api/scores/v2/facebook', _t = this;
		facebook.getFriends(function(){
			var urlEXT = '?friends=' + facebook.userID;
			for( var i = 0; i < facebook.installedFriendCount; i++) {
				urlEXT += "," + facebook.friends[i].id;
			}
			url += urlEXT;
			
			$.get(url).done(function( data ) {
				//get ya friends
				for( var i = 0; i < facebook.installedFriendCount; i++ ) {
					var friend = facebook.friends[i];
					for( var j = 0; j < data.response.scores.length; j++ ){
						var score = data.response.scores[j];
						if(friend.id == score.player_fb_uid) {
							friend.score = parseInt(score.player_score);
							facebook.friends[i].score = friend.score;
						}
					}
					friend.image = facebook.images[friend.id];
					_t.players.push(friend);
				}
				complete();
			});
		});
	},
	apiCall:function(url){
		var _t = this;
		_t.players = [];
		//console.log('api call');
		$.get(url).done(function( data ) {
			//console.log(data);
			if(_t.isMyFriends) {
				//get ya friends
				for( var i = 0; i < facebook.installedFriendCount; i++ ) {
					var friend = _.clone(facebook.friends[i]);
					for( var j = 0; j < data.response.scores.length; j++ ){
						var score = data.response.scores[j];
						if(friend.id == score.player_fb_uid) {
							friend.score = parseInt(score.player_score);
							facebook.friends[i].score = friend.score;
						}
					}
					if(friend.score == 2147483647) continue;
					friend.image = facebook.images[friend.id];
					
					_t.players.push(friend);
				}
				//set all to type friend
				for( var i = 0; i < _t.players.length; i++ ){
					_t.players[i].type = 'friend';	
				}
				
				//get a random set of friends
				var slots = (_t.players.length>6) ?(_t.players.length % 6) : (6-_t.players.length); 
				var arr = [];
				var chosen = [];
				var attempts = 0;
				for( var i = 0; i < slots; i++ ){
					if(attempts > 4) break;
					var index = m.randomInt( facebook.installedFriendCount, facebook.friends.length);
					var foundMatch = false;
					for( var j = 0; j < chosen.length; j++ ){
						if(chosen[j] == index) foundMatch = true;	
					}
					if(foundMatch) {
						attempts++;
						continue;	
					}
					chosen.push(index);
					attempts = 0;
					var player = _.clone(facebook.friends[index]);
					if(player == null) continue;
					player.image = facebook.images[player.id];
					_t.players.push(player);
				}
				//set to invite
				for( var i = facebook.installedFriendCount; i < _t.players.length; i++) {
					_t.players[i].type = 'invite';	
					_t.players[i].score = -1;
				}
				
				_t.renderLeaderboard();
			} else {
				var ids = [];
				for( var i = 0; i < data.response.scores.length; i++ ){
					var score = data.response.scores[i];
					var player = {};
					
					player.score = parseInt(score.player_score);
					player.type = 'friend';
					player.id = score.player_fb_uid;
					player.name = score.player_full_name;
					player.image = score.player_image;
					
					if(player.score == 2147483647) continue;
					
					ids.push(player.id);
					
					_t.players.push(player);
				}
				_t.renderLeaderboard();
			}
			
		});
	},
	renderLeaderboard: function(){
		var _t = this, c = this.children, vp = app.viewPort,layout=[],panelX = 0;
		
		this.hideLoader();
		
		
		for( var i = 0, l = c.lb_players_cont.children.length; i < l; i++){
			c.lb_players_cont.removeChildAt(0);
		}
		
		var manifest = [];
		
		//clear all buttons
		for(var i = 0; i<_t.slotbuttons.length;i++){
			_t.slotbuttons[i].disable();
			_t.slotbuttons[i] = null;
		}
		_t.slotbuttons = [];
		
		
		
		//order the players by score
		_t.players = _.sortBy( _t.players, 'score').reverse();
		
		
		for( var i = 0; i < _t.players.length; i++ ) {
			var player = _t.players[i];
			var playername = (player.name.length>8)?player.name.substr(0,8)+'..': player.name;
			
			if (player.type=='friend'){
				// friend slot
				layout = _.union(layout, [
					{ name: 'lb_player_cont'+i, type:'container', x:panelX+40, y:60, regX:40, regY:60 },
					{ name: 'lb_playerpanel'+i, type:'sprite', id:'lb_playerpanel', x:0 , y:0, parent:'lb_player_cont'+i}
				])
				
				if(player.image != 'none') {
					layout = _.union(layout, [{ name: 'lb_player_img'+i, type:'bitmap', id:'profilephoto_'+i, x:10 , y:10, parent:'lb_player_cont'+i}]);
				}
				layout = _.union(layout, [{ name:'player_title'+i, type:'bmptext', text:(i+1)+'. '+playername, font:'14px ARMTB', y: 75, x: 3,  tint:0xfd0000 , parent:'lb_player_cont'+i},
					{ name: 'player_score'+i, type:'bmptext', font:'18px ARMTB', tint:0x111111, text: Math.floor(player.score*game.distanceMult)+'m',  align:'center', y: 95, x: 3, parent:'lb_player_cont'+i}
				]);
				panelX = (i+1)*90;
			} else{
				layout = _.union(layout, [
					{ name: 'lb_player_cont'+i, type:'container', x:panelX+40, y:60, regX:40, regY:60 },
					{ name: 'lb_playerpanel'+i, type:'sprite', id:'lb_playerpanel', x:0 , y:0, parent:'lb_player_cont'+i},
					{ name: 'lb_player_img'+i, type:'bitmap', id:'profilephoto_'+i, x:10 , y:10, parent:'lb_player_cont'+i, s:1.2},
					{ name:'player_title'+i, type:'bmptext', text:playername, font:'14px ARMTB', x: 3, y: 75,  tint:0xfd0000 , parent:'lb_player_cont'+i},
					{ name: 'lb_invite_btn'+i, type:'sprite', id:'lb_invite_btn', x:40 , y:104, regY:16, regX:33, parent:'lb_player_cont'+i, s:0.8},
				]);
				panelX = (i+1)*90;
			}
		
			if(player.image != 'none') manifest.push({ id:'profilephoto_'+i, src:  player.image });
		}
		
		a.addToManifest(manifest);
		a.loadManifest(function(){},function(){}); 
		
		var child = d.buildLayout(layout, c.lb_players_cont),text='';
		
		for( var i = 0; i < _t.players.length; i++ ) {
		
			var player = _t.players[i];
		
			if (player.type=='invite') {
				
				var b = new button();
				
				b.setContent(child['lb_player_cont'+i]).callback(function(e, b){
					facebook.inviteFriend(this.data.id);
				});
				
				b.data = {id:player.id,name:player.player_full_name, index:i};
				
				_t.slotbuttons.push(b);
			}
		
			if(child['player_score'+i]){
				text = child['player_score'+i];
				text.x = 40 - ( text.text.length * 5);
			}
			if(child['player_title'+i]){
				text = child['player_title'+i];
				text.x = 40 - ( text.text.length * 2.8);
			}
                
             if(child['lb_player_img'+i]) {
                child['lb_player_img'+i].width = child['lb_player_img'+i].height = 60;
            }
		}
		
		 var snaps = _t.players.length-5;
        if(snaps < 0) snaps = 0;
		_t.slider.setContent(_t.children.lb_players_cont);
		_t.slider.setSnapping(90, snaps);
		_t.slider.gotoSlide(0);
		//_t.slider.enable();
		_t.slider.changeCallback();
		
	},
	loadLeaderboard:function(type){
		var url = 'https://suitcasesling.kokogames.com/api/scores/v2/facebook', _t = this;
		
		this.showLoader();
		
		for( var i = 0, l = this.children.lb_players_cont.children.length; i < l; i++){
			this.children.lb_players_cont.removeChildAt(0);
		}
		
		if(type == 'friends'){
			
			this.isMyFriends = true;
			this.children.lb_everyone_inactive.visible = true;
			this.children.lb_everyone_active.visible = false;
			this.children.lb_friends_inactive.visible = false;
			this.children.lb_friends_active.visible = true;
			
			this.lb_myfriends_cont.disable();
			this.lb_everyone_cont.enable();
			
			facebook.getFriends(function(){
				var urlEXT = '?friends=' + facebook.userID;
				for( var i = 0; i < facebook.installedFriendCount; i++) {
					urlEXT += "," + facebook.friends[i].id;
				}
				url += urlEXT;
				
				_t.apiCall(url);
			});
			
		} else{
			
			this.isMyFriends = false;
			this.children.lb_everyone_inactive.visible = false;
			this.children.lb_everyone_active.visible = true;
			this.children.lb_friends_inactive.visible = true;
			this.children.lb_friends_active.visible = false;
			
			this.lb_myfriends_cont.enable();
			this.lb_everyone_cont.disable();
		  
			this.apiCall(url);
		}
	},
	openLeaderboard:function(){
		var _t = this, c = this.children, vp = app.viewPort;
		
		var mainmenu = v.get('mainmenu');
		if(mainmenu.container.visible) {
			mainmenu.disable();
		}
		else
		{
			var endgame = v.get('endgame');	
			if(endgame.container.visible) {
				endgame.disable();
			}
		}
		
		_t.children.lb_main_connect.visible = false;
		_t.children.lb_main_btn_up.visible = false;
		_t.children.lb_main_btn_down.visible = true;
		
		//console.log('open leaderboard');		
		c.lb_wrapper_cont.visible = true;
		d.animateObject(c.lb_cont, 0.2, { y: vp.bottom-172-(53*as) , ease: Power0.easeIn});
		_t.isOpen = true;
		
		_t.enable();
		_t.loadLeaderboard('friends');
		
		this.children.bg.visible = true;
		this.children.bg.alpha = 0;
		d.animateObject(this.children.bg, .3, { alpha: 0.3 });
	},
	closeLeaderboard:function(){
		var _t = this, c = this.children, vp = app.viewPort;
		
		var mainmenu = v.get('mainmenu');
		if(mainmenu.container.visible) {
			mainmenu.enable();
		}
		else
		{
			var endgame = v.get('endgame');	
			if(endgame.container.visible) {
				endgame.enable();
			}
		}
		
		_t.children.lb_main_btn_up.visible = true;
		_t.children.lb_main_btn_down.visible = false;
				
				
		c.lb_wrapper_cont.visible = true;
		d.animateObject(c.lb_cont, 0.2, { y: vp.bottom-(53*as), ease: Power0.easeIn});
		this.isOpen = false;
		this.disable();
		this.lb_main_btn_cont.enable();
		
		this.hideLoader();
		
		this.children.bg.alpha = 0;
		d.animateObject(this.children.bg, .1, { alpha: 0, onComplete: function(){
			_t.children.bg.visible = false;	
		}});		
	},
	showLoader: function(){
		var _t = this;
		this.children.lb_loader.visible = true;
		this.children.lb_loader.rotation = 0;
		app.addUpdateListener('lb_loader', function(){
			var rot = m.radiansToAngle(_t.children.lb_loader.rotation)+5;
			_t.children.lb_loader.rotation = m.angleToRadians(rot);
		});
	},
	hideLoader: function() {
		app.removeUpdateListener('lb_loader');
		this.children.lb_loader.visible = false;
	},
	transitionIn: function() {
		if(!facebook.enabled) return;
		
		if(_.size(this.children)==0) this.buildView();
		
		var _t = this, c = this.children, vp = app.viewPort;	
		
		c.lb_cont.position.y = vp.bottom+(100*as);
		c.lb_wrapper_cont.visible = false;
		
		this.lb_main_btn_cont.enable();
		
		d.animateObject(c.lb_cont, 1, { delay:1.5, y: vp.bottom-(53*as), ease: Power1.easeIn});
		this.enable();
		this.show();
		this.container.visible = true;
	},
	transitionOut: function() {
		if(!facebook.enabled) return;
		
		
		this.disable();
		this.container.visible = false;
	},
	enable:function(){
		this.lb_myfriends_cont.enable();
		this.lb_everyone_cont.enable();
		this.lb_invitefriends_btn.enable();
		this.lb_arrowleft_btn.enable();
		this.lb_arrowright_btn.enable();
		this.screen_btn.enable();
		this.lb_main_btn_cont.enable();
	},
	disable:function(){
		this.lb_myfriends_cont.disable();
		this.lb_everyone_cont.disable();
		this.lb_invitefriends_btn.disable();
		this.lb_arrowright_btn.disable();
		this.screen_btn.disable();
		this.lb_main_btn_cont.disable();
		//buttons
		for(var i = 0; i<this.slotbuttons.length;i++){
			this.slotbuttons[i].disable();
		}
	}
});// JavaScript Document