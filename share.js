var sharegame = function(share,score, isGeneric, e){
                
                var shareurl = 'https://forcesrun.co.uk/?share=1';
                var sharetxt = 'I am currently playing Forces Run. See if you can top the scoreboard? http://goo.gl/zxIC3s ';
                
                if(score > 0 && !isGeneric){
					// add score to shareurl
				    shareurl  += '&player_score='+score;
                    
                    var forceText = ''
                    switch(user.forceSelected)
                    {
                        case koko.views.get('levelselectview').SCENE_ARMY:
                            forceText = 'Army';
                        break;
                        case  koko.views.get('levelselectview').SCENE_RAF:
                             forceText = 'RAF';
                        break; 
                        case koko.views.get('levelselectview').SCENE_MARINES:
                             forceText = 'Marines';
                        break;
                        case koko.views.get('levelselectview').SCENE_NAVY:
                            forceText = 'Navy';
                        break;
                        case koko.views.get('levelselectview').SCENE_CIV:
                             forceText = 'Civilians';
                        break;              
                    }
                    
                    
                    shareurl  += '&player_force='+forceText;
					sharetxt = "I've just scored " +score+' for the '+forceText+' on Forces Run. See if you can beat my high score! #ForcesRun http://goo.gl/zxIC3s  ';
                }    
    
                switch(share) 
                {
                 case 'facebook':
                    koko.audio.play('sfx_click', 1, false);
                    FB.ui(
                      {
                        method: 'share',
                        href: shareurl
                      },
                      // callback
                      function(response) {}
                    );
                    if(score > 0)  ga('send', 'event', 'Share', 'Facebook - Score');
                    else ga('send', 'event', 'Share', 'Facebook - Generic');
                 break;
                 case 'twitter':  
                    app.openLink('http://twitter.com/intent/tweet?text='+encodeURIComponent(sharetxt), 'share',e);
                    if(score > 0)  ga('send', 'event', 'Share', 'Twitter - Score');
                    else ga('send', 'event', 'Share', 'Twitter - Generic');
                 break;

                 case 'google':
                    app.openLink('https://plus.google.com/share?url='+encodeURIComponent(shareurl), 'share',e);
                 break;
                }
}


koko.class('ShareView', 'view', {
    ShareView: function(){
        this.superr.view.call(this);  
    },
    assets: [],
    prevMenu:null,
    
    buildView: function(){
    
        var g = app.guiSize, vp = app.viewPort;
    
        this.children = koko.display.buildLayout([
        { name:'bg', type:'shape', alpha:0.7, fill:0x0, width:g.width, height:g.height },
        { name:'sh_menuContainer', type:'container', id:'sh_menuContainer', x:480, y:320 },
        { name:'sh_ssmlpopUpBgnd', type:'sprite', id:'sh_ssmlpopUpBgnd', parent:'sh_menuContainer', x:-352, y:-243 },
        { name:'sh_colourCarrier', type:'container', id:'sh_colourCarrier', parent:'sh_menuContainer', x:-319, y:-214 },
        { name:'sh_babyBgnd3', type:'sprite', id:'sh_babyBgnd3', parent:'sh_colourCarrier', x:0, y:0, visible:false },
        { name:'sh_chiefBgnd3', type:'sprite', id:'sh_chiefBgnd3', parent:'sh_colourCarrier', x:0, y:0, visible:false },
        { name:'sh_cleverBgnd3', type:'sprite', id:'sh_cleverBgnd3', parent:'sh_colourCarrier', x:0, y:0, visible:false },
        { name:'sh_stuntBgnd3', type:'sprite', id:'sh_stuntBgnd3', parent:'sh_colourCarrier', x:0, y:0, visible:false },
        { name:'sh_missBgnd3', type:'sprite', id:'sh_missBgnd3', parent:'sh_colourCarrier', x:0, y:0 },
        { name:'sh_scloseBtn', type:'sprite', id:'sh_scloseBtn', parent:'sh_menuContainer', x:320, y:-230, regX:32, regY:36 },
        { name:'sh_sshareTwitterBtn', type:'sprite', id:'sh_sshareTwitterBtn', parent:'sh_menuContainer', x:143, y:171, regX:141, regY:64 },
        { name:'sh_sshareFacebookBtn', type:'sprite', id:'sh_sshareFacebookBtn', parent:'sh_menuContainer', x:-158, y:171, regX:142, regY:64 },
        { name:'sh_sshareCTA', type:'sprite', id:'sh_sshareCTA', parent:'sh_menuContainer', x:-29, y:-134 },
        { name:'sh_sshareCTA2', type:'sprite', id:'sh_sshareCTA2', parent:'sh_menuContainer', x:-29, y:-144, visible:false},
        { name:'sh_shareTitle', type:'sprite', id:'sh_shareTitle', parent:'sh_menuContainer', x:-10, y:-278, regX:158 },
        { name:'sh_shareHerbs', type:'container', id:'sh_shareHerbs', parent:'sh_menuContainer', x:-295, y:68 },
        { name:'shareChief', type:'sprite', id:'shareChief', parent:'sh_shareHerbs', x:129, y:-124, visible:false, regX:129, regY:125 },
        { name:'sh_shareClever', type:'sprite', id:'sh_shareClever', parent:'sh_shareHerbs', x:177, y:-124, visible:false, regX:129, regY:125 },
        { name:'sh_shareBaby', type:'sprite', id:'sh_shareBaby', parent:'sh_shareHerbs', x:174, y:-124, visible:false, regX:129, regY:125 },
        { name:'sh_shareStunt', type:'sprite', id:'sh_shareStunt', parent:'sh_shareHerbs', x:148, y:-124, visible:false, regX:129, regY:125 },
        { name:'sh_shareMiss', type:'sprite', id:'sh_shareMiss', parent:'sh_shareHerbs', x:151, y:-124, visible:false,regX:129, regY:125 }
        ], this.container);
		
		this.children.sh_menuContainer.x = app.viewPort.centerX;
		this.children.sh_menuContainer.y = app.viewPort.centerY;
        
        if(this.prevMenu == 'mainmenu' || user.highScore == 0)
        {
            this.children.sh_sshareCTA.visible = false;   
            this.children.sh_sshareCTA2.visible = true;   
            console.log("HIUDE SHAREES")
                
        }
        
        
        switch(user.selectedCharacter)
        {
            case 0:
              this.children.sh_shareStunt.visible = true;   
            break;
            case 1:
              this.children.sh_shareMiss.visible = true;    
            break;
            case 2:
              this.children.sh_shareClever.visible = true;  
            break;
            case 3:
              this.children.shareChief.visible = true;  
            break;
            case 4:
              this.children.sh_shareBaby.visible = true;   
            break;
        }
        app.addResizeListener('pause', this.resize.bind(this));
        
	   var closeButton = new Button(this.children.sh_scloseBtn, { scale:1.1 }, {
        click: function(event, button){ 
            //this is drag and button is Button
            this.transitionOut();
              koko.audio.play('sfx_click', 1, false);
            
        }.bind(this)});
        var shareFBButton = new Button(this.children.sh_sshareFacebookBtn, { scale:1.1 }, {
        click: function(event, button){ 
            
            if(koko.client.system.isMobile)
            {
                main.FBShare = true;
                main.FBEvent = event;
                koko.views.get('orientation').transitionIn();
                
             }
            else
            {
                var isGeneric = false;
                if(this.prevMenu == 'mainmenu') isGeneric = true;
                sharegame('facebook', user.highScore, user.highScoreHerbert,isGeneric, event);  
            }
            
        }.bind(this)});
        var shareTwitButton = new Button(this.children.sh_sshareTwitterBtn, { scale:1.1 }, {
        click: function(event, button){ 
            var isGeneric =  this.prevMenu == 'mainmenu'
            sharegame('twitter',  user.highScore, user.highScoreHerbert,isGeneric, event);   
             koko.audio.play('sfx_click', 1, false);
            //ga('send', 'event', 'Share', 'Twitter - Score');
            event.target.scale.x = 1.5;
            event.target.scale.y = 1.5;
        }.bind(this)});

		this.elements = [ closeButton, shareFBButton, shareTwitButton ];
    },
    resize:function()
    {
        var c = this.children;
        var vp = app.viewPort;
        //c.ps_popupOverlay_shape.x = vp.left ;
        //c.ps_popupOverlay_shape.y = vp.top; 
    },
    transitionIn: function($menu){
        this.prevMenu = $menu;
        ga('send', 'event', 'View', 'Share'); 
        this.buildView();
		this.enable();
        this.show();
         koko.audio.play('sfx_transition', 1, false);
        d.applyThenAnimate(this.children.sh_menuContainer, { scale:0 }, .2, { ease:Back.easeOut, scale:1 });
        
        
    },
    transitionOut: function(){
        v.get(this.prevMenu).enable();
		this.hide();
        this.destroy();  
    }
});