var HUDView = (function() {
	//extends view
	HUDView.prototype = new View();
    //constructor
    function HUDView() { 
		this.container  = new createjs.Container();
		this.children = {};
		this.ready = false;
		this.guideComplete = false;
		this.showInput = false;
		//add assets to the manifest
		AssetManager.addToManifest([
			{ id: 'ask_button', src: 'img/ingame/green_btn.png' },
			{ id: 'ask_button_disabled', src: 'img/ingame/green_btn_disabled.png' },
			//select
			{ id: 'select_popup', src: 'img/ingame/select_answer_popup.png' },
			{ id: 'select_popup_active', src: 'img/ingame/select_answer_popup_selected.png' },
			{ id: 'select_btn', src: 'img/ingame/select_btn.png' },
		]);
	};
	
	HUDView.prototype.buildView = function() {
		var _t = this;
		
		var bound = CanvasViewport.canvasMaxSize;
		
		var assetManifest = [
			//ASK BRIAN
			{ name: 'ask_btn', type:'container', x:bound.w*0.5, y: bound.h-100 },
			{ name: 'ask_btn_back_disabled', type:'bitmap', bitmap: { id: 'ask_button_disabled' }, parent: 'ask_btn', regX:304, regY: 40 },
			{ name: 'ask_btn_back', type: 'bitmap', bitmap: { id: 'ask_button' }, parent:'ask_btn', regX:304, regY: 40 },
			{ name: 'ask_btn_title', type: 'text', y:10, text: { style:'34px Trebuchet MS, Verdana', color:'#FFFFFF', val:'Ask BRIAN', align:'center' }, parent:'ask_btn' },
			{ name: 'select_container', type: 'container', regX: 304, x: bound.w*0.5, y:bound.h-220 },
			{ name: 'select', type:'bitmap', bitmap: { id: 'select_btn' }, parent: 'select_container' },
			{ name: 'select_txt', type: 'text', x: 20, y: 45, text: { style:'24px Trebuchet MS, Verdana', color:'#000000', val:defaultInput }, parent: 'select_container'},
			{ name: 'input_container', type: 'container', regX: 304, x: bound.w*0.5, y:bound.h-220, visible:false },
			{ name: 'input_back', type: 'shape', shape: { fill: 'rgba(0,0,0,0.1)', x:0, y:0, w: 350, h:57 }, parent: 'input_container', y:10},
			{ name: 'select_typed', type: 'text', x: 200, y: 45, text: { style:'24px Trebuchet MS, Verdana', color:'#000000', val:'Type here ...'}, parent: 'input_container'},
			{ name: 'options', regX: 304, x: (bound.w*0.5)-5, regY:-210, visible:false },
			{ name: 'options_back', parent:'options', type:'bitmap', bitmap: { id: 'select_popup' } },
			{ name: 'select_questionmark', type: 'text', x: 875, y: 595, text: { style:'24px Trebuchet MS, Verdana', color:'#333333', val:'?'}, visible:true }
		];
		
		var def = [
			{ y: 3, h:70 },	
			{ y: 74, h:73 },	
			{ y: 148, h:72 },	
			{ y: 221, h:73 },	
			{ y: 295, h:71 }
		];
		
		for( var i = 0; i < questions.length; i++) {
			var cont_name = 'question'+i;
			var d = def[i];
			assetManifest.push({ name: cont_name, type:'container', parent:'options', y: d.y, x:7 });
			assetManifest.push({ name: cont_name + "_txt", type:'text', text: { style:'26px Trebuchet MS, Verdana', color:'#409dc8', val:questions[i].txt + " ___________ ?" }, parent:cont_name, y: 45, x:20 });
		}
		
		//build layout
		_t.children = AssetManager.buildLayout(assetManifest, _t.container);
		
		for( var i = 0; i < questions.length; i++) {
			var d = def[i];
			var shape = new createjs.Shape();
			
			var r = { tl:0, tr:0, br:0, bl:0 };
			if( i == 0 ) r.tl = r.tr = 5;
			if( i == (questions.length-1) ) r.bl = r.br = 5;
			
			shape.graphics.beginFill('#eaf9ff').drawRoundRectComplex( 0, 0, 604, d.h, r.tl, r.tr, r.br, r.bl );
			shape.alpha = 0.01;
			_t.children['question'+i].addChild(shape);
			_t.children['question'+i].setChildIndex(shape, 0);
			_t.children['question'+i+'_bg'] = shape;
		}
		
		_t.children.ask_btn_title.shadow = new createjs.Shadow("rgba(0,0,0,0.3)", 1.5, 1.5, 3);
		
		//loop over options
		for( i = 0; i < questions.length; i++ ){
			//create buttons and attach click event
			new Button(_t.children['question'+i]).setData({questionIndex:i}).over({children:[{id:0, alpha:1}]}).out({children:[{id:0, alpha:0.01}]}).click(function(b){
				//close menu
				_t.dropdown.trigger('click');
				//update info
				game.questionType = b.data.questionIndex;
				game.questionText = defaultInput;
				//update select
				_t.showInput = true;
				_t.updateSelect(false);
				//next?
				if(Client.isMobile()) {
					//auto fire popup to type question
					setTimeout(function(){
						_t.input_container_btn.trigger('click');
					}, 150);
				} else {
					//focus input field
					var inp = document.getElementById('desktop_input');
					inp.focus();	
				}
				
			});
		}
		
		_t.ask_btn = new Button(_t.children.ask_btn).over({children:[{id:0, scaleX:1.02, scaleY:1.02}] }).out({children:[{id:0, scaleX:1, scaleY:1}] }).click(function(){
			_t.showBrianResponse();
			AudioManager.playSound('button_sfx');
		});
		
		_t.ask_btn.mouseEnabled = false;
		
		_t.input_container_btn = new Button(_t.children.input_container).over({}).out({}).click(function(){
			_t.tryCloseMenu();
			var c = prompt(questions[game.questionType].txt + "...", '');
			if(c) {
				game.questionText = c.substr(0,20);
				_t.updateSelect(false);
				setTimeout(function(){
					_t.showBrianResponse();
				}, 250);
			} else {
				game.questionText = defaultInput;
				_t.updateSelect(false);
			}
			AudioManager.playSound('button_sfx');
		});
		
		_t.dropdown = new Button(_t.children.select_container).setData({open:false}).over({}).out({}).click(function(b) {
			if(b.data.open) {
				_t.children.options.y = -30;
				createjs.Tween.get(_t.children.options).to({alpha:0, y:-10 }, 100).call(function(){
					_t.children.options.visible = false;																				  
				});
			} else {
				_t.children.options.y = -10;
				_t.children.options.alpha = 0;
				_t.children.options.visible = true;
				createjs.Tween.get(_t.children.options).to({alpha:1, y:-30 }, 200);
			}
			b.data.open = !b.data.open;
			AudioManager.playSound('button_sfx');
			_t.showInput = false;
			_t.updateSelect(false);
		});
		
		_t.ready = true;
		_t.resize();
		_t.updateSelect(false);
	};
	
	HUDView.prototype.tryCloseMenu = function(){
		var _t = this;
		if(_t.dropdown.data.open) {
			_t.dropdown.trigger('click');
		}
	}
	
	HUDView.prototype.updateSelect = function(resize){
		var _t = this;
		//to remove the select going back to Ask A Question... uncomment next line
		_t.showInput = true;
		if(Client.isMobile()) {
			_t.updateSelectMobile(resize);
		} else {
			_t.updateSelectDesktop(resize);
		}
	};
	
	HUDView.prototype.updateSelectDesktop = function(resize) {
		var _t = this;
		//update
		if(_t.showInput) {
			_t.children.select_txt.text = questions[ game.questionType  ].txt;
		} else {
			_t.children.select_txt.text = 'Choose A Question...';
		}
		
		var inp = document.getElementById('desktop_input');
		var canvasPosition = CanvasViewport.canvas.getBoundingClientRect();
		
		var inputLeftPos = 336 + (40 + _t.children.select_txt.getMeasuredWidth() - 10);
		var inputTopPos = 558;
		var inputWidth = 600-(inputLeftPos-336)-65-30;

		inp.style.left = (inputLeftPos*CanvasViewport.scale) + "px";	
		inp.style.top = (inputTopPos*CanvasViewport.scale) + "px";
		
		inp.style.width = inputWidth+"px";
		
		if(!resize) inp.style.display = _t.showInput ? 'block' : 'none';
		
		_t.children.select_questionmark.visible = _t.showInput;
		
		inp.style.color = game.questionText == defaultInput ? '#999999' : '#000000';
		
		inp.style[CanvasViewport.transform] = "scale("+CanvasViewport.scale+")";
		inp.value = game.questionText;
		
		//button
		_t.updateButtonEnabled();
	};
	
	HUDView.prototype.updateSelectMobile = function(resize) {
		var _t = this;
		//update
		
		if(_t.showInput) {
			_t.children.select_txt.text = questions[ game.questionType  ].txt;
		} else {
			_t.children.select_txt.text = 'Choose A Question...';
		}
		
		_t.children.select_questionmark.visible = _t.showInput;
		_t.children.input_container.visible = _t.showInput;
		
		_t.children.select_typed.text = game.questionText;
		
		
		_t.children.select_typed.color = game.questionText == defaultInput ? '#999999' : '#000000';
		
		
		_t.children.select_typed.x = 40 + _t.children.select_txt.getMeasuredWidth();
		_t.children.input_back.x = _t.children.select_typed.x - 10;
		var inputBackWidth = 600-_t.children.input_back.x-50-30;
		//alert(inputBackWidth);
		for( var i = 0; i < game.questionText.length; i++)
		{
			var txt = game.questionText.substr(0, game.questionText.length-i);
			_t.children.select_typed.text = txt;
			if(_t.children.select_typed.getMeasuredWidth() < (inputBackWidth-60)) {
				if(_t.children.select_typed.text != game.questionText) {
					_t.children.select_typed.text = _t.children.select_typed.text + '...';	
				}
				break;
			}
		}
		//build input back               .beginStroke .beginFill                 .drawRect                    .endFill .endStroke 
		_t.children.input_back.graphics.clear().s('#999999').f('rgba(255,255,255,0.5)').dr(0, 0, inputBackWidth, 55).ef().es();
		//sort ask button
		_t.updateButtonEnabled();
	}
	
	HUDView.prototype.updateValue = function(val) {
		var _t = this;
		
		var inp = document.getElementById('desktop_input');
		game.questionText = val.substr(0,20);
		inp.value = game.questionText;
		
		_t.updateSelect(false);
	}
	
	HUDView.prototype.keyPress = function(e) {
		var _t = this;
		//hit enter
		if(e.keyCode == 13) {
			var inp = document.getElementById('desktop_input');
			inp.blur();
			setTimeout(function(){
				_t.showBrianResponse();
			}, 100);
		}
	}
	
	
	
	HUDView.prototype.updateButtonEnabled = function(){
		var _t = this;
		//update
		var enabled = (game.questionText != defaultInput && game.questionText != '');
		
		_t.children.ask_btn_back_disabled.visible = !enabled;
		_t.children.ask_btn_back.visible = enabled;
		_t.ask_btn.mouseEnabled = enabled;
		_t.children.ask_btn_title.color = enabled ? '#FFFFFF' : '#dfdfdf';
	}
	
	HUDView.prototype.showBrianResponse = function() {
		var _t = this;
		
		ViewManager.getView( 'sharehud' ).transitionOut();
		createjs.Tween.get(_t.container).to({alpha:0}, 200);		
		ViewManager.getView( "screen" ).transitionIn();
		//close the select if open
		if(_t.dropdown.data.open) {
			_t.dropdown.trigger('click');	
		}
		if(!Client.isMobile()) {
			var inp = document.getElementById('desktop_input');
			inp.blur();
			inp.style.display = 'none';
		}
	}
	
	HUDView.prototype.returnFromBrianResponse = function() {
		var _t = this;
		createjs.Tween.get(_t.container).to({alpha:1}, 500);
		ViewManager.getView( 'sharehud' ).transitionIn();
		_t.updateSelect(false);
		if(!Client.isMobile()) {
			var inp = document.getElementById('desktop_input');
			inp.style.display = 'block';
			inp.focus();
		}
	}
	
	HUDView.prototype.resize = function() {
		var _t = this;
		if(!_t.ready) return;
		_t.updateSelect(true);
	}
	
	HUDView.prototype.transitionIn = function() {
		var _t = this;
		if(_.size(_t.children) == 0) _t.buildView();
		_t.container.alpha = 0;
		createjs.Tween.get(_t.container).to({alpha:1}, 500);
	};
	
	//return constructor
    return HUDView;
})();