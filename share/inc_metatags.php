<link rel="apple-touch-icon" sizes="57x57" href="share/favicon/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="114x114" href="share/favicon/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="72x72" href="share/favicon/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="144x144" href="share/favicon/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="60x60" href="share/favicon/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="120x120" href="share/favicon/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="76x76" href="share/favicon/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="152x152" href="share/favicon/apple-touch-icon-152x152.png">
<link rel="icon" type="image/png" href="share/favicon/favicon-196x196.png" sizes="196x196">
<link rel="icon" type="image/png" href="share/favicon/favicon-160x160.png" sizes="160x160">
<link rel="icon" type="image/png" href="share/favicon/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="share/favicon/favicon-16x16.png" sizes="16x16">
<link rel="icon" type="image/png" href="share/favicon/favicon-32x32.png" sizes="32x32">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="msapplication-TileColor" content="#cc0000">
<meta name="msapplication-TileImage" content="share/favicon/mstile-144x144.png">


<meta property="og:site_name" content="<?=$siteName;?>" />
<meta property="og:image" content="<?=$img;?>" />
<meta property="og:title" content="<?=$title;?>" />
<meta property="og:description" content="<?=$description;?>" />
<meta property="fb:app_id" content="<?=$fbappId;?>" />
<meta property="og:type" content="game" />
<meta property="og:url" content="<?=$targetURL;?>" />


<title><?=$title;?></title>
<meta name="title" content="<?=$title;?>" />
<meta name="description" content="<?=$description;?>"/>
<meta name="keywords" content="<?=$keywords?>"/>

<link rel="image_src" href="<?=$img;?>" />

<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="<?=$twitterAccount?>">
<meta name="twitter:creator" content="<?=$twitterAccount?>">
<meta name="twitter:title" content="<?=$title?>">
<meta name="twitter:image:src" content="<?=$img?>">
<meta name="twitter:domain" content="<?=$targetURL?>">
