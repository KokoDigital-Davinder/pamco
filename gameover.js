koko.class('GameOverView', 'view', {
    GameOverView: function(){
        this.superr.view.call(this);  
    },
    assets: [],
    name_input:null,
    gamePlays:0,
    formStr:'',
    buildView: function(){
         var g = app.guiSize, vp = app.viewPort;
        this.children = koko.display.buildLayout([
        { name:'bg', type:'shape', alpha:0.7, fill:0x0, width:g.width, height:g.height },
        { name:'go_skipBtn', type:'sprite', id:'go_skipBtn', x:932, y:607, regX:64, regY:38 },
        { name:'go_menuContainer', type:'container', id:'go_menuContainer', x:480, y:320 },
        { name:'smlpopUpBgnd', type:'sprite', id:'smlpopUpBgnd', parent:'go_menuContainer', x:-362, y:-261 },
        { name:'go_colorBgnd', type:'container', id:'go_colorBgnd', parent:'go_menuContainer', x:-328, y:-235 },
        { name:'go_missBgnd', type:'sprite', id:'missBgnd', parent:'go_colorBgnd', x:0, y:0, visible:false },
        { name:'go_chiefBgnd', type:'sprite', id:'go_chiefBgnd', parent:'go_colorBgnd', x:0, y:0, visible:false },
        { name:'go_babyBgnd', type:'sprite', id:'go_babyBgnd', parent:'go_colorBgnd', x:0, y:0, visible:false },
        { name:'go_cleverBgnd', type:'sprite', id:'go_cleverBgnd', parent:'go_colorBgnd', x:0, y:0, visible:false },
        { name:'go_stuntBgnd', type:'sprite', id:'go_stuntBgnd', parent:'go_colorBgnd', x:0, y:0, visible:false },
        { name:'go_wellDoneTitle', type:'sprite', id:'go_wellDoneTitle', parent:'go_menuContainer', x:-176, y:-300 },
        { name:'go_newBest', type:'sprite', id:'go_newBest', parent:'go_menuContainer', x:286, y:-121, regX:76, regY:95 },
        { name:'scoreTitles', type:'sprite', id:'scoreTitles', parent:'go_menuContainer', x:-5, y:-173 },
        { name:'go_submitScore', type:'sprite', id:'go_submitScore', parent:'go_menuContainer', x:-285, y:40 },
        { name:'go_submitBtn', type:'sprite', id:'go_submitBtn', parent:'go_menuContainer', x:-20, y:258, regX:146, regY:42 },
        { name:'go_nameInput_txt', type:'bmptext', parent:'go_menuContainer', x:-226, y:109, text:'Name', size:'48px', font:'FSJoey', lw:425.1, tint:0x999999 },
        { name:'go_gameOverHerbs', type:'container', id:'go_gameOverHerbs', parent:'go_menuContainer', x:-271, y:30 },
        { name:'go_missHerb', type:'sprite', id:'go_missHerb', parent:'go_gameOverHerbs', x:115, y:0, regX:115, regY:236, visible:false },
        { name:'go_stuntHerb', type:'sprite', id:'go_stuntHerb', parent:'go_gameOverHerbs', x:115, y:0, visible:false, regX:152, regY:242 },
        { name:'go_babyHerb', type:'sprite', id:'go_babyHerb', parent:'go_gameOverHerbs', x:115, y:0, visible:false, regX:111, regY:253 },
        { name:'go_chiefHerb', type:'sprite', id:'go_chiefHerb', parent:'go_gameOverHerbs', x:135, y:1, visible:false, regX:190, regY:243 },
        { name:'go_cleverHerb', type:'sprite', id:'go_cleverHerb', parent:'go_gameOverHerbs', x:115, y:0, visible:false, regX:117, regY:255 },
            
        { name:'go_score_txt', type:'bmptext', parent:'go_menuContainer', x:90, y:-182, text:user.gameScore, size:'55px', font:'TremendousTight', lw:104, tint:0x19b3fc },
        { name:'go_bonusScore_txt', type:'bmptext', parent:'go_menuContainer', x:90, y:-144, text:user.gameCollected * 10, size:'55px', font:'TremendousTight', lw:104, tint:0x19b3fc },
        { name:'go_scoreTotal_txt', type:'bmptext', parent:'go_menuContainer', x:90, y:-94, text:user.score, size:'96px', font:'TremendousTight', lw:172.05, tint:0xeb5fb6 }
        ], this.container);
		
        
      
        this.children.go_newBest.scale.x = 0;
        this.children.go_newBest.scale.y = 0;
		
        
       
        switch(user.selectedCharacter)
        {
            case 0:
              this.children.go_stuntBgnd.visible = true;  
              this.children.go_stuntHerb.visible = true;  
            break;
            case 1:
              this.children.go_missBgnd.visible = true;  
              this.children.go_missHerb.visible = true;  
            break;
            case 2:
              this.children.go_cleverBgnd.visible = true;  
              this.children.go_cleverHerb.visible = true;  
            break;
            case 3:
              this.children.go_chiefBgnd.visible = true;  
              this.children.go_chiefHerb.visible = true;  
            break;
            case 4:
              this.children.go_babyBgnd.visible = true;  
              this.children.go_babyHerb.visible = true;  
            break;
        }
        
        //console.log('SCORES', user.score, user.highScore)
        
        if(user.score == user.highScore) d.animate(  this.children.go_newBest, .3, {scale:1, delay:.8, ease:Back.easeOut})
        
       this.children.go_nameInput_txt.hitArea = new PIXI.Rectangle(-10, -10, 320, 55);
		this.name_input = new InputText(this.children.go_nameInput_txt, this, 'Name', -200, {
			change: function(inp){
                /*this.formStr = inp.val;
                this.formStr = this.formStr.trim();
                
                
				if(this.formStr == inp.defaultVal || this.formStr == '') {
					this.children.name_input.tint = 0xbababa;
					return;
				}
				
				this.name_valid = formStr != 'Name' && formStr.length > 1 && formStr.length < 50;
				this.children.go_nameInput_txt.tint = !this.name_valid ? 0xff0000 : 0xbababa;
				//this.children.name_error.visible = !inp.valid
				*/
			}.bind(this),
			tab: function(){
				//this.email_input.focus();	
			}.bind(this),
			focus: function(){
				//this.email_input.blur();
			}.bind(this),
            blur: function(){
                console.log('blur');
            }.bind(this)
		});
		
        
        
        
	var skipButton = new Button(this.children.go_skipBtn, { scale:1.1 }, {
			click: function(event, button){ 
				//this is drag and button is Button
                koko.audio.play('sfx_click', 1, false);
				this.transitionOut(); 
                ga('send', 'event', 'Score Submitted', 'Skipped'); 
                v.get('shareview').transitionIn('highscores');
			}.bind(this)});

        var submitButton = new Button(this.children.go_submitBtn, { scale:1.1 }, {
			click: function(event, button){ 
                this.formStr = this.name_input.val;
                this.formStr = this.formStr.trim();
                
                  koko.audio.play('sfx_click', 1, false);
                if( this.formStr != '')
                {
                    //this is drag and button is Button
                    this.submit(); 
                    ga('send', 'event', 'Score Submitted', 'True'); 
                   // v.get('shareview').transitionIn('highscores');
                }
                //v.get('highscores').transitionIn();
			}.bind(this)});
		
		this.elements = [ skipButton, submitButton, this.name_input   ];
        app.addResizeListener('gameover', this.resize.bind(this));
        
        this.gamePlays ++;
        ga('send', 'event', 'Games Played', this.gamePlays); 
        
    },
    
    submit: function(){
		this.name_input.disable();
		WebAPICall.submitScore(function(){
			this.transitionOut(); 
             v.get('shareview').transitionIn('highscores')
		}.bind(this),this.formStr, user.score );
	},
    
    resize:function()
    {
        var c = this.children;
        var vp = app.viewPort;
        
        c.go_skipBtn.x = vp.right - (c.go_skipBtn.width/2) - 10;
        c.go_skipBtn.y = vp.bottom - (c.go_skipBtn.height/2) - 10;      
        
        c.go_menuContainer.x = vp.centerX;
        c.go_menuContainer.y = vp.centerY;
        
    },
    transitionIn: function(){
        this.buildView();
		this.enable();
        this.show();
         koko.audio.play('sfx_transition', 1, false);
        d.applyThenAnimate(this.children.go_menuContainer, { scale:0 }, .2, { ease:Back.easeOut, scale:1 });
        ga('send', 'event', 'View', 'Game Over'); 
    },
    transitionOut: function(){
        this.name_input.disable();
        
        this.disable();
		this.hide();
        this.destroy();  
       
        if(user.charUnlocked != 0)
        {
            v.get('unlockcharacter').transitionIn();
        }else
        {
            game.destroyGame();
            v.get('ingame').transitionOut();
            v.get('highscores').transitionIn();
        }
        
    }
});