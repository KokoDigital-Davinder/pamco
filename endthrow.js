// JavaScript Document
var EndThrowView = koko.view({
	assets: [
	],
	isFinalThrow: false,
	buildView: function() {
		var g = app.guiSize, vp = app.viewPort, _t = this;
		
		this.children = d.buildLayout([
			{ name:'panel_cont', type:'container', regX:(179*as), regY:(283*as), x: vp.centerX, y: vp.bottom-(240*as) },
				{ name:'endThrow_cont', type:'container', parent:'panel_cont' },
					{ name: 'panel_bg', type:'sprite', id:'throw_complete_panel', parent:'endThrow_cont' },
					{ name:'distance_txt', parent:'endThrow_cont', y: 55*as, type:'bmptext', text:'105m', font:Math.floor(72*as)+'px ARMTB', tint:0x717073 },
			
				{ name:'foulThrow_cont', type:'container', parent:'panel_cont' },
					{ name: 'panel_bg', type:'sprite', id:'foulthrow', parent:'foulThrow_cont', y: 40*as },
					
					
			{ name: 'continue_btn', type:'sprite', id:'continue_btn', parent:'panel_cont', y: 180*as, x:179*as, regX:(142*as), regY:(48*as) },
					{ name: 'next_attempt_btn', type:'sprite', id:'next_attempt_btn', parent:'panel_cont', y: 180*as, x:179*as, regX:140*as, regY:46*as },		
		], this.container);
		
		
		this.continue_btn = new button();
		this.continue_btn.setContent(this.children.continue_btn).over({s:0.95}).callback(function(){
			au.play("pop_"+m.randomInt(1,2));
			_t.transitionOut();
		});		
		
		this.next_btn = new button();
		this.next_btn.setContent(this.children.next_attempt_btn).over({s:0.95}).callback(function(){
			au.play("pop_"+m.randomInt(1,2));
			_t.transitionOut();
		});	
		
	},
	transitionIn: function(isFinalThrow){
		if(_.size(this.children)==0) this.buildView();
		
		
		var vp = app.viewPort;
		
		//enable
		this.continue_btn.enable();
		this.next_btn.enable();
		
		//boom
		this.isFinalThrow = isFinalThrow;
		
		this.children.continue_btn.visible = this.isFinalThrow;
		this.children.next_attempt_btn.visible = !this.isFinalThrow;
		
		if(game.distance == 0) {
			this.children.panel_cont.position.y = vp.bottom-(490*as);
			this.children.endThrow_cont.visible = false;
			this.children.foulThrow_cont.visible = true;
			
		} else {
			this.children.panel_cont.position.y = vp.bottom-(240*as);
			this.children.endThrow_cont.visible = true;
			this.children.foulThrow_cont.visible = false;
			
			//show distance
			var distance = Math.floor(game.distance*game.distanceMult).toString()+"m";
			this.children.distance_txt.setText(distance);
			this.children.distance_txt.position.x = (179*as) - (22 *as * distance.length);
		}		
		
		this.container.alpha = 0;
		this.container.position.y = 50;
		d.animateObject(this.container, .5, { alpha:1, y: 0});
		
		//show
		this.show();
	},
	transitionOut: function(){
		var _t = this;
		
		//enable
		this.continue_btn.disable();
		this.next_btn.disable();
		
		v.get('ingamehud').resetHUD();

		//animate container
		d.animateObject(this.container, .2, { alpha:0, y: -50, onComplete:function(){
			_t.hide();
			
			if(_t.isFinalThrow) {
				v.get('ingamehud').transitionOut();
				v.get('endgame').transitionIn();
			}
			else
			{
				game.setupNextThrow();
			}
			
		}});
	}
});