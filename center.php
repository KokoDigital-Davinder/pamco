<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Lounge Leap</title>
	<style>
		#frame {
			width:810px;
			height:610px;
			margin: 0 auto;
			display:block;
			border: 5px solid #ffffff;
		}
		body {
			background:#371119;
		}
		
	</style>
</head>
<body>

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NKL9JQ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NKL9JQ');</script>
<!-- End Google Tag Manager -->

<iframe src="index.php?tab=1" id="frame" frameborder="0"></iframe>

<script>
		
		//window.onload = function(){
			var frame = document.getElementById('frame');
			
			window.onresize = function(){
				frame.style.marginTop = Math.round((window.innerHeight-610) *0.5 ) +"px";
			}
			window.onresize();
		//}
	</script>
</body>
</html>