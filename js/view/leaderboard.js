// JavaScript Document
var LeaderboardView = koko.view({
	assets: [ { id:'leaderboard_json', src:'img/leaderboard.json?v=10' } ],
    open: false,
	buildView: function(){
        var g = app.guiSize, _t = this;
        
        if(app.leaderboardAlwaysOpen) this.open = true;
        
		this.children = d.buildLayout([
            { name:'cover', v:false, alpha:0.5, type:'shape', w:g.width, h:g.height, fill:'#000000' },
            
            { name:'container', type:'container' },
              
			  
			  
			   
			   
			   
			   
            { name:'wrap', type:'container', parent:'container' },
                { name:'handle_btn', type:'sprite', id:'leaderboard_handle', x:-54, y: 5, parent:'wrap', visible:false },
                { name:'content', type:'container', parent:'wrap' },
				
				
				
				
				
             { name:'bg', type:'sprite', id:'lbpanel', parent:'content' },
					
					
					{ name:'connected_cont', type:'container', parent:'content' },
              	
			{ name: 'players_cont', type:'container', parent:'connected_cont', x: 8, y:77 },
                { name:'player_slider_container', type:'container', parent:'players_cont' },
                { name:'leaderboard_load', type:'sprite', id:'ls_loader', parent:'player_slider_container', regX:15, regY:15, x:67, y:100 },
				
				{ name:'topcover', type:'sprite', id:'lbpanel', parent:'connected_cont' },
				{ name:'bottomcover', type:'sprite', id:'lbpanel', parent:'connected_cont' },
				
				
            { name:'friends_btn', type:'container', parent:'content', y: 10, x: 5 },
                { name:'f_btn_bg', type:'sprite', id:'friends_btn', parent:'friends_btn' },
                { name:'f_btn_selected', type:'sprite', id:'friends_selected', parent:'friends_btn', visible:false },
            { name:'everyone_btn', type:'container', parent:'content', y: 10, x: 77 },
                { name:'e_btn_bg', type:'sprite', id:'everyone_btn', parent:'everyone_btn' },
                { name:'e_btn_selected', type:'sprite', id:'everyone_selected', parent:'everyone_btn', visible:false },
            
            { name:'notconnected_cont', type:'container', parent:'content' },
                { name:'nc_title', type:'sprite', parent:'notconnected_cont', id:'leaderboardconnectinfo', y: 200, x: 24 },
                { name:'nc_connect_btn', type:'sprite', parent:'notconnected_cont', id:'leaderboardfbconnect_btn', y: 290, x: 76, regX:53, regY:16 },
            
            
           
            { name:'lbinvitelarge_btn', type:'sprite', id:'lbinvitelarge_btn', y: 546, x:8, parent:'connected_cont' },
            
            { name:'up_arrow_btn', type:'sprite', id:'lbarrow_up', x: 8, y: 49, parent:'connected_cont' },
            { name:'down_arrow_btn', type:'sprite', id:'lbarrow_down', x: 8, y: 508, parent:'connected_cont' },
            
            { name:'title', parent:'connected_cont', type:'sprite', id:'lbtitle', y: 49, x: 8 },
            
            
                
		], this.container);
            
	
		this.children.topcover.height = 77;
		this.children.bottomcover.height = 102;
		this.children.bottomcover.y = app.viewPort.bottom - 102;
			
			
        this.invite_btn = new button(this.children.lbinvitelarge_btn, { y:543 }, function(){
            au.play('button'+m.randomInt(1,2), 1);
            facebook.checkLogin(function(){ 
				facebook.inviteFriends('');  
			}, function() {
                v.get('connect').transitionIn(function(){
                    facebook.inviteFriends('');  
                });
            }, false);
        });
            
            
        this.nc_connect_btn = new button(this.children.nc_connect_btn, { y: 287 }, function(){
            facebook.checkLogin(function(){}, function(){}, true);  
        });
            
        this.up_arrow = new button(this.children.up_arrow_btn, { y: 46 }, function(){
           au.play('button'+m.randomInt(1,2), 1);
            for( var i = 1; i< _t.children.player_slider_container.children.length; i++ ){
                _t.children.player_slider_container.children[i].visible = true;
            }
            _t.slider.gotoSlide(_t.slider.currentSlideIndex - 6, .5);
        });
        
        this.down_arrow = new button(this.children.down_arrow_btn, { y: 505 }, function(){
            au.play('button'+m.randomInt(1,2), 1);
            for( var i = 1; i< _t.children.player_slider_container.children.length; i++ ){
                _t.children.player_slider_container.children[i].visible = true;
            }
            _t.slider.gotoSlide(_t.slider.currentSlideIndex + 6, .5);
        });
            
        this.friends_btn = new button(this.children.friends_btn, { y: 7 }, function(){
            au.play('button'+m.randomInt(1,2), 1);
            
            v.get('connect').transitionIn(function(){
                _t.cancelLoadingScores();
                _t.loadScores(0);
            });
           
        });
        
        this.everyone_btn = new button(this.children.everyone_btn, { y: 7 }, function(){
            au.play('button'+m.randomInt(1,2), 1);
            v.get('connect').transitionIn(function(){
                _t.cancelLoadingScores();
                _t.loadScores(1);
            });
        });
            
        this.slider = new snapslider(this.children.player_slider_container, 'y');
        this.slider.setSnapping(0, 0);
        this.slider.setupChangeCompleteHandler(function(){
            var index = this.currentSlideIndex;
            for( var i = 1; i< _t.children.player_slider_container.children.length; i++ ){
                _t.children.player_slider_container.children[i].visible = i > index && i < index+8;
            }
            
            if(this.atStart()){
                d.animateObject(_t.children.up_arrow_btn, .1, { alpha:0 });
                d.animateObject(_t.children.title, .1, { alpha:1 });
                _t.up_arrow.disable();
            } else {
                d.animateObject(_t.children.up_arrow_btn, .1, { alpha:1 });
                d.animateObject(_t.children.title, .1, { alpha:0 });
                _t.up_arrow.enable();
            }
            if(this.atEnd()){
                _t.children.down_arrow_btn.visible = false;
                _t.down_arrow.disable();
            } else {
                _t.children.down_arrow_btn.visible = true;
                _t.down_arrow.enable();
            }
        });

        this.children.up_arrow_btn.alpha = 0;
        this.up_arrow.disable();
        
        this.slider.setupChangeHandler(function(){
            for( var i = 1; i< _t.children.player_slider_container.children.length; i++ ){
                _t.children.player_slider_container.children[i].visible = true;
            }
        });
            
		this.children.container.position.x = app.viewPort.right;
        this.children.content.visible = this.open;

        this.hideConnectMessage();
        
        if(!app.leaderboardAlwaysOpen) {
            this.handle_btn = new button(this.children.handle_btn, { y: 2 }, function(){
                au.play('button'+m.randomInt(1,2), 1);
                _t.toggleShow();    
            });
        } else {
            /*facebook.checkLogin(function(){
            }, function(){
                _t.loadScores(1);
            }, false);*/
        }
    }, 
    hideHandle: function(){
        d.animateObject(this.children.handle_btn, .2, { y: -100 });
    },
    showHandle: function(){
        d.animateObject(this.children.handle_btn, .2, { y: 5 }); 
    },
    destroyBoard: function(){
        for( var i = 0; i < this.invitebuttons.length; i++){
            this.invitebuttons[i].disable();
        }
        this.invitebuttons = [];
        for( var i = 1, l = this.children.player_slider_container.children.length; i < l; i++) {
            this.children.player_slider_container.removeChildAt(1);
        }
    },
    showLoader: function(){
        var loader = this.children.leaderboard_load;
        loader.visible = true;
        app.addUpdateListener('leaderboardloader', function(){
            loader.rotation = m.angleToRadians(m.radiansToAngle(loader.rotation) + 5);
        });
    },
    hideLoader: function(){
        this.children.leaderboard_load.visible = false;
        app.removeUpdateListener('leaderboardloader');
    },
    shouldShowScores: false,
    scoreLoadAjax: null,
    activeBoard: null,
    invitebuttons: [],
    showConnectMessage: function() {
        this.children.connected_cont.visible = false;
        this.children.notconnected_cont.visible = true;
        this.nc_connect_btn.enable();
    },
    hideConnectMessage: function(){
        this.children.connected_cont.visible = true;
        this.children.notconnected_cont.visible = false;
        this.nc_connect_btn.disable();
    },
    loadScores: function(board, alwaysLoad){
        this.shouldShowScores = true;
        var _t = this, c = this.children;
        //console.log('attempt load scores');
        // if(!facebook.loggedin) {
            //console.log('lb: show connect message');
            this.showConnectMessage();
            return;
        // }
        //console.log('hide connect message');
        this.hideConnectMessage();  
        
        if(this.activeBoard == board && !alwaysLoad) return;
        this.activeBoard = board;
        //toggle states
        c.e_btn_bg.visible = board == 0;
        c.e_btn_selected.visible = board == 1;
        c.f_btn_bg.visible = board == 1;
        c.f_btn_selected.visible = board == 0;
        //disable buttons
        this.everyone_btn.disable();
        this.friends_btn.disable();
                
        //reset slide
        this.destroyBoard();
        this.showLoader();
        this.slider.gotoSlide(0);
        //disable buttons
        this.up_arrow.disable();
        this.down_arrow.disable();
        
        setTimeout(function(){
            if(_t.activeBoard == 0) {
                _t.loadFacebookFriendScores(function(scores){
                    _t.populateLeaderboard(scores);
                    _t.everyone_btn.enable();   
                    _t.friends_btn.disable();   
                });   
            } else {
                _t.loadEveryoneScores(function(scores){
                    _t.populateLeaderboard(scores);
                    _t.friends_btn.enable();
                    _t.everyone_btn.disable();
                }); 
            }
        }, 1000);
    },
   //  loadFacebookFriendScores: function(success){
   //      var urlEXT = '', _t = this;
   //      if(facebook.loggedin && facebook.gotfriends) {
   //          //loggedin and everything is cool
   //          urlEXT = '?friends=' + facebook.getFriendIds().join();
   //      } 
   //      else if(facebook.loggedin && !facebook.gotfriends) {
   //          facebook.getFriends(function(){
   //             _t.loadFacebookFriendScores(success); 
   //          });
   //          return;
   //      } else {
   //          facebook.checkLogin(function(){
   //              facebook.getFriends(function(){
   //                 _t.loadFacebookFriendScores(success);  
   //              });
   //          }, function(){
   //              _t.loadScores(1);
   //          }, true);
   //          return;
   //      }
                
   //      this.scoreLoadAjax = $.ajax({
   //          dataType:'json', 
			// type: 'GET',
			// url: app.url + "api/scores/v2/facebook"+urlEXT, 
   //          crossDomain: true,
   //          xhrFields: { withCredentials: true },
   //          success: function(data){
   //              if(success) success(data.response.scores);
   //          }
   //      });
        
   //      return this.scoreLoadAjax;
   //  },
    loadEveryoneScores: function(success){
        var _t = this;
        this.scoreLoadAjax = $.ajax({
            dataType:'json', 
			type: 'GET',
			url: app.url + "api/scores/v2/facebook", 
            crossDomain: true,
            xhrFields: { withCredentials: true },
            success: function(data){
               if(success) success(data.response.scores);
            }
        });
    },
    cancelLoadingScores: function(){
        this.shouldShowScores = false;
        if(this.scoreLoadAjax) {
            this.scoreLoadAjax.abort();
            this.scoreLoadAjax = null;
        }
        this.hideLoader();
    },
    populateLeaderboard: function(scores) {
        
        scores.sort(function(a, b){
            var x = parseInt(a['player_score']); var y = parseInt(b['player_score']);
            return ((x > y) ? -1 : ((x < y) ? 1 : 0));
        });
        
        var imgs = [];
        var layout = [];
        for( var i = 0; i < scores.length; i++ ){
            var imgObj = { name:'bg', type:'sprite', id:'lbprofilepic_empty', parent:'friend'+i, x: 1, y: 1 };
            
            var img = scores[i].player_image;
            if(img !== null && img != 'none') {
                var ref = img.substr(img.length-30, 30);
                
                //not found before
                if(!facebook.images[ref]) {
                    facebook.images[ ref ] = true;
                    imgs.push({id:'friends_img_'+ref, src: img.replace('http:', document.location.protocol) });
                }
                
                //set to img
                imgObj.id = 'friends_img_'+ref;
                imgObj.type = 'bitmap';
                imgObj.s = 49/60;
                imgObj.x = imgObj.y = 0;
            }
           
            layout = layout.concat([
                { name:'friend'+i, type:'container', y: (62 * i)+5, x: 8 },
                    imgObj,
                    { name:'cover', type:'sprite', id:'friends_mask', parent:'friend'+i },
                    { name:'pipet', type:'sprite', id:'leaderboard_number', parent:'friend'+i, x:-6, y:-5 },
                    { name:'pipet_title_'+i, type:'bmptext', text:(i+1).toString(), font:'11px ARMTB', x: 3, y: -1, tint:0xffffff, parent:'friend'+i },
                    { name:'name', type:'bmptext', text:scores[i].player_full_name, font:'14px ARMTB', x: 54, y: 8, tint:0x717073, parent:'friend'+i },
                    { name:'score', type:'bmptext', text:scores[i].player_score+'m', font:'22px ARMTB', x: 54, y: 20, tint:0x333333, parent:'friend'+i },
            ]);

        }
    
        var slotCount = 8 - this.children.player_slider_container.children.length;
        if(slotCount < 3) slotCount = 3;
            
        var inviteFriends = _.sample(facebook.friends, slotCount+4);
        var iterator = 0;
        var invButtons = [];

        for( var j = 0; j < inviteFriends.length; j++ ) {
            var fri = inviteFriends[j];
            if(fri.installed) continue;
            
            var index = i+iterator;
            
            var imgObj = { name:'bg', type:'sprite', id:'lbprofilepic_empty', parent:'friend'+index, x: 1, y: 1 };
            
            var img = fri.img;
            if(img !== null && img != 'none') {
                var ref = img.substr(img.length-30, 30);
                
                //not found before
                if(!facebook.images[ref]) {
                    facebook.images[ ref ] = true;
                    imgs.push({id:'friends_img_'+ref, src: img.replace('http:', document.location.protocol)});
                }
                
                //set to img
                imgObj.id = 'friends_img_'+ref;
                imgObj.type = 'bitmap';
                imgObj.s = 59/60;
                imgObj.x = imgObj.y = 0;
            }
            
            layout = layout.concat([
                { name:'friend'+index, type:'container', y: (62 * index)+5, x: 8 },
                    imgObj,
                    { name:'cover', type:'sprite', id:'friends_mask', parent:'friend'+index },
                    { name:'invite_btn'+index, type:'sprite', x: 54, y: 19, id:'lbinvitesmall_btn', parent:'friend'+index },
					{ name:'name', type:'bmptext', text:((fri.name.split(' '))[0]), font:'14px ARMTB', x: 56, y: 5, tint:0x717073, parent:'friend'+index },
            ]);
			
            invButtons.push({id: fri.id, name: 'invite_btn'+index});
                
            iterator++;
        }
        
        //load images here
        if(imgs.length > 0) {
            a.clearManifest();
            a.addToManifest(imgs);
            a.loadManifest(function(){}, function(){});
        }

        //build layout
        var c = d.buildLayout(layout, this.children.player_slider_container);
        //center text
        for( var k = 0; k < scores.length; k++ ){
            c['pipet_title_'+k].centerText(true, false);   
        }
        
        for(k = 0; k < invButtons.length; k++ ) {
            var inv = invButtons[k];
                
            var invbut = new button(c[inv.name], { y: 18 }, function(){
                facebook.inviteFriend(this.data.id);    
            }, { id: inv.id });
                
            invbut.enable();
            this.invitebuttons.push(invbut);        
        }

        this.slider.setSnapping(62, (i+iterator)-6);
        this.hideLoader();

        //update buttons
        if(this.activeBoard == 0) this.everyone_btn.enable();
        if(this.activeBoard == 1) this.friends_btn.enable();  
        
    },
    toggleShow: function(){
        this.open = !this.open;
            
        var _t = this;
        
        if(this.open) {
            this.children.content.visible = true;
            this.children.cover.visible = true;
            d.animateObject(this.children.wrap, .5, { x: -150 });
            this.disableMenus();
            this.activeBoard = null;
            this.loadScores(1);
        } else {
            this.children.cover.visible = false;
            this.cancelLoadingScores();
            d.animateObject(this.children.wrap, .5, { x: 0, onComplete:function(){
                _t.children.content.visible = false;
                _t.enableMenus();
                _t.destroyBoard();
            }});
        }
    
        
    },
    enableMenus: function(){
        //disbale other menus
        v.each(['mainmenu', 'charselect', 'gameover', 'menuhud', 'levelselect'], function(view){
            if(view.container.visible) view.enable();
        });
    }, 
    disableMenus: function(){
        //disbale other menus
        v.each(['mainmenu', 'charselect', 'gameover', 'menuhud', 'levelselect'], function(view){
            if(view.container.visible) view.disable();
        });
    },
    resize: function(){
        this.children.container.position.x = app.viewPort.right;
    },
	transitionIn: function(){
        //debugger;
		if(_.size(this.children) == 0) {
            this.buildView();
        } else if(app.leaderboardAlwaysOpen) {
            return;   
        }
        this.show();
        this.enable();
        
        this.container.position.x = 300;
        d.animateObject(this.container, .3, { x: 0 });

        var _t = this;
        app.addResizeListener('leaderboard', function(){
           _t.resize(); 
        });
	}, 
	transitionOut: function() {
        if(app.leaderboardAlwaysOpen) return; 
        
        this.hide();
        this.disable();
        
        app.removeResizeListener('leaderboard');
	},
    enable: function(){
        if(!app.leaderboardAlwaysOpen) this.handle_btn.enable();
        this.invite_btn.enable();
        this.up_arrow.enable();
        this.down_arrow.enable();
        this.friends_btn.enable();
        this.everyone_btn.enable();
        this.slider.enable();
        this.nc_connect_btn.enable();
    },
    disable: function(){
        if(!app.leaderboardAlwaysOpen) this.handle_btn.disable();
        this.invite_btn.disable();
        this.up_arrow.disable();
        this.down_arrow.disable();
        this.friends_btn.disable();
        this.everyone_btn.disable();
        this.slider.disable();
        this.nc_connect_btn.disable();
    }
});