var ConnectView = koko.view({
    assets: [
    ],
    buildView: function(){
        var vp = app.viewPort, g = app.guiSize;
        this.children = d.buildLayout([
            { name:'bg', type:'shape', alpha:0.5, w: g.width, h:g.height },
            
            { name:'con', type:'container', regX:169, x: vp.centerX, y: 50 },
            
            { name:'bg', type:'sprite', id:'sorry_panel', parent:'con' },
                { name:'close_btn', type:'sprite', id:'puclose_btn', parent:'con', x:310, y: -15 },
                
            { name:'connect_btn', type:'sprite', id:'sfbconnect_btnl', parent:'con', x:23, y: 135 }
        ], this.container);
        
        var _t = this;
        this.close_btn = new button(this.children.close_btn, { y: -18 }, function() {
            au.play('button'+m.randomInt(1,2), 1);
            ga('send', 'event', 'Game', 'UI - Must Connect Popup - Close');
            _t.transitionOut(false);
        });
        
        this.connect_btn = new button(this.children.connect_btn, { y: 132 }, function(){
            au.play('button'+m.randomInt(1,2), 1);
            ga('send', 'event', 'Game', 'UI - Must Connect Popup - Attempt Connect');
            facebook.checkLogin(function(){
                _t.transitionOut(true);
                ga('send', 'event', 'Game', 'UI - Must Connect Popup - Connected');
            }, function() { }, true);
        });
    },
    transitionIn: function(success){
        var _t = this;
        this.success = success;
        
        // facebook.checkLogin(function(){
        //    success();
        // }, function() {
            _t.doTransitionIn();
        // }, false);
    },
    doTransitionIn: function() {
        if(_.size(this.children)==0) this.buildView();
        this.enable();
        this.show();
        
        ga('send', 'event', 'Game', 'UI - Must Connect Popup');
        
        
        this.container.alpha = 0;
        d.animateObject(this.container, .3, { alpha: 1 });
            
        this.children.con.position.y = 70;
        this.children.con.alpha = 0;
        d.animateObject(this.children.con, .3, { alpha: 1, y:50 });
        
        //disbale other menus
        if(v.get('leaderboard').open && !app.leaderboardAlwaysOpen) {
            v.get('leaderboard').disable();
        } else {
            v.each(['mainmenu', 'charselect', 'gameover', 'menuhud', 'leaderboard'], function(view){
                if(view.container.visible) view.disable();
            });
        }
    },
    transitionOut: function(playSuccess){
        var _t = this;
        
        this.disable();
        d.animateObject(this.container, .2, { alpha:0, onComplete: function(){
            _t.hide();
            
            if(playSuccess) _t.success();
            
            if(v.get('leaderboard').open && !app.leaderboardAlwaysOpen) {
                v.get('leaderboard').enable();
            } else {
                v.each(['mainmenu', 'charselect', 'gameover', 'menuhud', 'leaderboard'], function(view){
                    if(view.container.visible) view.enable();
                });
            }
        }});
        //reenable other menus
    },
    enable: function(){
        this.close_btn.enable();
        this.connect_btn.enable();
    },
    disable:  function(){
        this.close_btn.disable();
        this.connect_btn.disable();
    }
});