var ScoreboardView = koko.view({
    assets: [ { id:'char_select_json', src:'img/character_select.json' },
            { id:'char_select_json', src:'img/characterSelectIcons.json' },
            { id:'char_select_json', src:'img/scoreboardIcons.json' },
            { id:'ingame2_json', src:'img/howToPlayIcons.json?v=4' } 
            ],

    chosenCharacter: 'male',

    buildView: function(){
        var vp = app.viewPort;
        
        this.children = d.buildLayout([
            { name:'title', type:'sprite', id:'title_top10.png', regX: 141, x: vp.centerX, y: 100 },
            { name: 'panelContainer', type:'container', x:0, y:0, visible:true },
                { name:'backing', type:'sprite', parent:'panelContainer', id:'scoreboard_backing_all.png', regX: 0, x: vp.centerX, y: vp.centerY ,visible:true},
                { name: 'positionsContainer', parent:'panelContainer', type:'container', x:0, y:0, visible:true },
                    { name:'positionText1', position:1, dataField:'position',type:'bmptext',  parent:'positionsContainer', text:"1",font:'40px ARMTB_LARGE', x:0, y:0, tint:0xD84A94, align:'center', visible:true},
                    { name:'positionText2', position:2, dataField:'position',type:'bmptext',  parent:'positionsContainer', text:"2",font:'40px ARMTB_LARGE', x:0, y:0, tint:0xD84A94, align:'center', visible:true},
                    { name:'positionText3', position:3, dataField:'position',type:'bmptext',  parent:'positionsContainer', text:"3",font:'40px ARMTB_LARGE', x:0, y:0, tint:0xD84A94, align:'center', visible:true},
                    { name:'positionText4', position:4, dataField:'position',type:'bmptext',  parent:'positionsContainer', text:"4",font:'40px ARMTB_LARGE', x:0, y:0, tint:0xD84A94, align:'center', visible:true},
                    { name:'positionText5', position:5, dataField:'position',type:'bmptext',  parent:'positionsContainer', text:"5",font:'40px ARMTB_LARGE', x:0, y:0, tint:0xD84A94, align:'center', visible:true},
                    { name:'positionText6', position:6, dataField:'position',type:'bmptext',  parent:'positionsContainer', text:"6",font:'40px ARMTB_LARGE', x:0, y:0, tint:0xD84A94, align:'center', visible:true},
                    { name:'positionText7', position:7, dataField:'position',type:'bmptext',  parent:'positionsContainer', text:"7",font:'40px ARMTB_LARGE', x:0, y:0, tint:0xD84A94, align:'center', visible:true},
                    { name:'positionText8', position:8, dataField:'position',type:'bmptext',  parent:'positionsContainer', text:"8",font:'40px ARMTB_LARGE', x:0, y:0, tint:0xD84A94, align:'center', visible:true},
                    { name:'positionText9', position:9, dataField:'position',type:'bmptext',  parent:'positionsContainer', text:"9",font:'40px ARMTB_LARGE', x:0, y:0, tint:0xD84A94, align:'center', visible:true},
                    { name:'positionText10', position:10, dataField:'position',type:'bmptext',  parent:'positionsContainer', text:"10",font:'30px ARMTB_LARGE', x:0, y:0, tint:0xD84A94, align:'center', visible:true},
                
                { name: 'namesContainer', parent:'panelContainer', type:'container', x:0, y:0, visible:true },
                    { name:'nameText1', position:1, dataField:'name',type:'bmptext',  parent:'namesContainer', text:"---",font:'25px ARMTB', x:0, y:0, tint:0xB7B7B7, align:'left', visible:true},
                    { name:'nameText2', position:2, dataField:'name',type:'bmptext',  parent:'namesContainer', text:"---",font:'25px ARMTB', x:0, y:0, tint:0xB7B7B7, align:'left', visible:true},
                    { name:'nameText3', position:3, dataField:'name',type:'bmptext',  parent:'namesContainer', text:"---",font:'25px ARMTB', x:0, y:0, tint:0xB7B7B7, align:'left', visible:true},
                    { name:'nameText4', position:4, dataField:'name',type:'bmptext',  parent:'namesContainer', text:"---",font:'25px ARMTB', x:0, y:0, tint:0xB7B7B7, align:'left', visible:true},
                    { name:'nameText5', position:5, dataField:'name',type:'bmptext',  parent:'namesContainer', text:"---",font:'25px ARMTB', x:0, y:0, tint:0xB7B7B7, align:'left', visible:true},
                    { name:'nameText6', position:6, dataField:'name',type:'bmptext',  parent:'namesContainer', text:"---",font:'25px ARMTB', x:0, y:0, tint:0xB7B7B7, align:'left', visible:true},
                    { name:'nameText7', position:7, dataField:'name',type:'bmptext',  parent:'namesContainer', text:"---",font:'25px ARMTB', x:0, y:0, tint:0xB7B7B7, align:'left', visible:true},
                    { name:'nameText8', position:8, dataField:'name',type:'bmptext',  parent:'namesContainer', text:"---",font:'25px ARMTB', x:0, y:0, tint:0xB7B7B7, align:'left', visible:true},
                    { name:'nameText9', position:9, dataField:'name',type:'bmptext',  parent:'namesContainer', text:"---",font:'25px ARMTB', x:0, y:0, tint:0xB7B7B7, align:'left', visible:true},
                    { name:'nameText10', position:10, dataField:'name',type:'bmptext',  parent:'namesContainer', text:"---",font:'25px ARMTB', x:0, y:0, tint:0xB7B7B7, align:'left', visible:true},
                
                { name: 'companiesContainer', parent:'panelContainer', type:'container', x:0, y:0, visible:true },
                    { name:'companyText1', position:1, dataField:'company',type:'bmptext',  parent:'companiesContainer', text:"---",font:'15px ARMTB', x:0, y:0, tint:0x70C04F, align:'left', visible:true},
                    { name:'companyText2', position:2, dataField:'company',type:'bmptext',  parent:'companiesContainer', text:"---",font:'15px ARMTB', x:0, y:0, tint:0x70C04F, align:'left', visible:true},
                    { name:'companyText3', position:3, dataField:'company',type:'bmptext',  parent:'companiesContainer', text:"---",font:'15px ARMTB', x:0, y:0, tint:0x70C04F, align:'left', visible:true},
                    { name:'companyText4', position:4, dataField:'company',type:'bmptext',  parent:'companiesContainer', text:"---",font:'15px ARMTB', x:0, y:0, tint:0x70C04F, align:'left', visible:true},
                    { name:'companyText5', position:5, dataField:'company',type:'bmptext',  parent:'companiesContainer', text:"---",font:'15px ARMTB', x:0, y:0, tint:0x70C04F, align:'left', visible:true},
                    { name:'companyText6', position:6, dataField:'company',type:'bmptext',  parent:'companiesContainer', text:"---",font:'15px ARMTB', x:0, y:0, tint:0x70C04F, align:'left', visible:true},
                    { name:'companyText7', position:7, dataField:'company',type:'bmptext',  parent:'companiesContainer', text:"---",font:'15px ARMTB', x:0, y:0, tint:0x70C04F, align:'left', visible:true},
                    { name:'companyText8', position:8, dataField:'company',type:'bmptext',  parent:'companiesContainer', text:"---",font:'15px ARMTB', x:0, y:0, tint:0x70C04F, align:'left', visible:true},
                    { name:'companyText9', position:9, dataField:'company',type:'bmptext',  parent:'companiesContainer', text:"---",font:'15px ARMTB', x:0, y:0, tint:0x70C04F, align:'left', visible:true},
                    { name:'companyText10', position:10, dataField:'company',type:'bmptext',  parent:'companiesContainer', text:"---",font:'15px ARMTB', x:0, y:0, tint:0x70C04F, align:'left', visible:true},
                
                { name: 'scoresContainer', parent:'panelContainer', type:'container', x:0, y:0, visible:true },
                    { name:'scoreText1', position:1, dataField:'score',type:'bmptext',  parent:'scoresContainer', text:"0m",font:'30px ARMTB_LARGE', x:0, y:0, tint:0xD84A94, align:'right', visible:true},
                    { name:'scoreText2', position:2, dataField:'score',type:'bmptext',  parent:'scoresContainer', text:"0m",font:'30px ARMTB_LARGE', x:0, y:0, tint:0xD84A94, align:'right', visible:true},
                    { name:'scoreText3', position:3, dataField:'score',type:'bmptext',  parent:'scoresContainer', text:"0m",font:'30px ARMTB_LARGE', x:0, y:0, tint:0xD84A94, align:'right', visible:true},
                    { name:'scoreText4', position:4, dataField:'score',type:'bmptext',  parent:'scoresContainer', text:"0m",font:'30px ARMTB_LARGE', x:0, y:0, tint:0xD84A94, align:'right', visible:true},
                    { name:'scoreText5', position:5, dataField:'score',type:'bmptext',  parent:'scoresContainer', text:"0",font:'30px ARMTB_LARGE', x:0, y:0, tint:0xD84A94, align:'right', visible:true},
                    { name:'scoreText6', position:6, dataField:'score',type:'bmptext',  parent:'scoresContainer', text:"0",font:'30px ARMTB_LARGE', x:0, y:0, tint:0xD84A94, align:'right', visible:true},
                    { name:'scoreText7', position:7, dataField:'score',type:'bmptext',  parent:'scoresContainer', text:"0",font:'30px ARMTB_LARGE', x:0, y:0, tint:0xD84A94, align:'right', visible:true},
                    { name:'scoreText8', position:8, dataField:'score',type:'bmptext',  parent:'scoresContainer', text:"0",font:'30px ARMTB_LARGE', x:0, y:0, tint:0xD84A94, align:'right', visible:true},
                    { name:'scoreText9', position:9, dataField:'score',type:'bmptext',  parent:'scoresContainer', text:"0",font:'30px ARMTB_LARGE', x:0, y:0, tint:0xD84A94, align:'right', visible:true},
                    { name:'scoreText10', position:10, dataField:'score',type:'bmptext',  parent:'scoresContainer', text:"0",font:'30px ARMTB_LARGE', x:0, y:0, tint:0xD84A94, align:'right', visible:true},
                







            //=================================================================================

            { name:'femaleSilouette', type:'sprite', id:'female_selectedNew.png', regX:179, x: vp.centerX-50, y:520, regY:336, visible:false },
            { name:'female', type:'sprite', id:'female_not_selectedNew.png', regX:179, x: vp.centerX-50, y:520, regY:323, visible:false},
            { name:'maleSilouette', type:'sprite', id:'male_selectedNew.png', x:vp.centerX+50, y: 520, regY:323, visible:false},
            { name:'male', type:'sprite', id:'male_not_selectedNew.png', x:vp.centerX+50, y: 520, regY:323, visible:false },
            { name:'continue', type:'sprite', id:'confirm_button.png', regX: 141, x: vp.centerX, y: 100, visible:false }
            ], this.container);

        console.log("children: ", this.children);

        var relativeUnit = app.viewPort.height/100;
        this.children.title.position.y -= relativeUnit*5;


        this.children.backing.anchor = new PIXI.Point(0.5, 0.5);
        this.children.title.anchor = new PIXI.Point(0.5, 0.5);
        this.children.positionText1.anchor = new PIXI.Point(0.5, 0.5);
        this.children.positionText2.anchor = new PIXI.Point(0.5, 0.5);
        this.children.positionText3.anchor = new PIXI.Point(0.5, 0.5);
        this.children.positionText4.anchor = new PIXI.Point(0.5, 0.5);
        this.children.positionText5.anchor = new PIXI.Point(0.5, 0.5);
        this.children.positionText6.anchor = new PIXI.Point(0.5, 0.5);
        this.children.positionText7.anchor = new PIXI.Point(0.5, 0.5);
        this.children.positionText8.anchor = new PIXI.Point(0.5, 0.5);
        this.children.positionText9.anchor = new PIXI.Point(0.5, 0.5);
        this.children.positionText10.anchor = new PIXI.Point(0.5, 0.5);
        this.children.scoreText1.anchor = new PIXI.Point(0.5, 0.5);
        this.children.nameText1.anchor = new PIXI.Point(0.5, 0.5);
        this.children.companyText1.anchor = new PIXI.Point(0.5, 0.5);

        this.children.title.scale.x = 0.5;
        this.children.title.scale.y = 0.5;
        this.children.backing.scale.x = 0.5;
        this.children.backing.scale.y = 0.5;
        this.children.backing.position.y +=  relativeUnit*6;
        this.children.backing.position.x +=  relativeUnit*0;
        var spaceBetweenEntries = relativeUnit*8.15;

        //===================================================================
        this.children.positionText1.position.y +=  relativeUnit*15.5;
        this.children.positionText1.position.x +=  relativeUnit*10.7;
        this.children.positionText2.position.y +=  relativeUnit*15.5 + spaceBetweenEntries*1;
        this.children.positionText2.position.x +=  relativeUnit*10.7;
        this.children.positionText3.position.y +=  relativeUnit*15.5+ spaceBetweenEntries*2;
        this.children.positionText3.position.x +=  relativeUnit*10.7;
        this.children.positionText4.position.y +=  relativeUnit*15.5+ spaceBetweenEntries*3;
        this.children.positionText4.position.x +=  relativeUnit*10.7;
        this.children.positionText5.position.y +=  relativeUnit*15.5+ spaceBetweenEntries*4;
        this.children.positionText5.position.x +=  relativeUnit*10.7;
        this.children.positionText6.position.y +=  relativeUnit*15.5+ spaceBetweenEntries*5;
        this.children.positionText6.position.x +=  relativeUnit*10.7;
        this.children.positionText7.position.y +=  relativeUnit*15.5+ spaceBetweenEntries*6;
        this.children.positionText7.position.x +=  relativeUnit*10.7;
        this.children.positionText8.position.y +=  relativeUnit*15.5+ spaceBetweenEntries*7;
        this.children.positionText8.position.x +=  relativeUnit*10.7;
        this.children.positionText9.position.y +=  relativeUnit*15.5+ spaceBetweenEntries*8;
        this.children.positionText9.position.x +=  relativeUnit*10.7;
        this.children.positionText10.position.y +=  relativeUnit*15.5+ spaceBetweenEntries*9 +relativeUnit*0.8;
        this.children.positionText10.position.x +=  relativeUnit*10;
        //==================================================================================
        
        
        this.children.nameText1.position.y +=  relativeUnit*16;
        this.children.nameText1.position.x +=  relativeUnit*16;

        var xPosition = this.children.nameText1.position.x;
        var yPosition = this.children.nameText1.position.y;
        var c =this.children;
        var multiplier = 1;

        for(var k = 2; k < 11; ++k) {
            c["nameText" + (k)].position.y = yPosition + spaceBetweenEntries*multiplier; 
            c["nameText" + (k)].position.x = xPosition;  
            multiplier++;        
        }
        //==================================================================================
        
        this.children.companyText1.position.y +=  relativeUnit*19;
        this.children.companyText1.position.x +=  relativeUnit*16.1;

        var xPosition = this.children.companyText1.position.x;
        var yPosition = this.children.companyText1.position.y;
        var c =this.children;
        var multiplier = 1;

        for(var k = 2; k < 11; ++k) {
            c["companyText" + (k)].position.y = yPosition + spaceBetweenEntries*multiplier; 
            c["companyText" + (k)].position.x = xPosition;  
            multiplier++;        
        }

        //====================================================================================
        this.children.scoreText1.position.y +=  relativeUnit*16;
        this.children.scoreText1.position.x +=  relativeUnit*49;

        var xPosition = this.children.scoreText1.position.x;
        var yPosition = this.children.scoreText1.position.y;
        var c =this.children;
        var multiplier = 1;

        for(var k = 2; k < 11; ++k) {
            c["scoreText" + (k)].position.y = yPosition + spaceBetweenEntries*multiplier; 
            c["scoreText" + (k)].position.x = xPosition;  
            multiplier++;        
        }
        
        //====================================================================================

        
        this.children.female.anchor = new PIXI.Point(0.5, 0.5);
        this.children.femaleSilouette.anchor = new PIXI.Point(0.5, 0.5);
        this.children.male.anchor = new PIXI.Point(0.5, 0.5);
        this.children.maleSilouette.anchor = new PIXI.Point(0.5, 0.5);
        this.children.continue.anchor = new PIXI.Point(0.5, 0.5);

        
        this.children.female.scale.x = 0.45;
        this.children.female.scale.y = 0.45;
        this.children.femaleSilouette.scale.x = 0.45;
        this.children.femaleSilouette.scale.y = 0.45;
        this.children.male.scale.x = 0.45;
        this.children.male.scale.y = 0.45;
        this.children.maleSilouette.scale.x = 0.45;
        this.children.maleSilouette.scale.y = 0.45;
        this.children.continue.scale.x = 0.5;
        this.children.continue.scale.y = 0.5;

        this.children.title.position.y -=  relativeUnit*0;
        this.children.title.position.x +=  relativeUnit*12;
        this.children.continue.position.y +=  relativeUnit*76;
        this.children.continue.position.x +=  relativeUnit*11.5;
        this.children.male.position.y +=  relativeUnit*0;
        this.children.male.position.x -=  relativeUnit*24;
        this.children.maleSilouette.position.y +=  relativeUnit*0;
        this.children.maleSilouette.position.x -=  relativeUnit*24;
        this.children.female.position.y +=  relativeUnit*0;
        this.children.female.position.x +=  relativeUnit*38;
        this.children.femaleSilouette.position.y +=  relativeUnit*1;
        this.children.femaleSilouette.position.x +=  relativeUnit*38;


        
        var _t = this;
        var yUp = this.children.female.position.y - 2.5;
        this.female_btn = new button(this.children.female, {}, function(){
            //_t.select('female'); 
            //ga('send', 'event', 'Game', 'Choose Female');
            au.play('button'+m.randomInt(1,2), 1);
            user.gender = 'female';
            _t.children.maleSilouette.visible = false;
            _t.children.femaleSilouette.visible = true;
            
        });
        
        yUp = this.children.male.position.y - 2.5;
        this.male_btn = new button(this.children.male, {}, function(){
            //_t.select('male'); 
            //ga('send', 'event', 'Game', 'Choose Male');
            au.play('button'+m.randomInt(1,2), 1);

            user.gender = 'male';
            _t.children.maleSilouette.visible = true;
            _t.children.femaleSilouette.visible = false;
            
        });
        
        yUp = this.children.continue.position.y - 2.5;
        this.continue_btn = new button(this.children.continue, {y:yUp}, function(){
            //_t.select('male'); 
            //ga('send', 'event', 'Game', 'Choose Male');
            au.play('button'+m.randomInt(1,2), 1);
            _t.select(_t.chosenCharacter);
        });

    WebAPICall.getScores(function(){
        //console.log(WebAPICall.scoreList)
            this.setData(WebAPICall.scoreList);
        }.bind(this));
        

    },
    select: function(g) {
        //user.gender = g;
        user.save();
        this.transitionOut();
        game.setLevel(0);

        v.get('ingame').transitionIn();
    },

    setData: function($data){
        var c =this.children;
        console.log($data)
        for(var i = 0; i<$data.length; i++)
        {
             var score = $data[i].player_score;
             var company = $data[i].player_company;
             var playername = $data[i].player_full_name;



            c["scoreText" + (i+1)].setText(score); 
            c["scoreText" + (i+1)].updateText();
            //.......
            //c["positionText" + (k)].setText("3"); 
            //c["positionText" + (k)].updateText();

            c["nameText" + (i+1)].setText(playername); 
            c["nameText" + (i+1)].updateText();

            c["companyText" + (i+1)].setText(company); 
            c["companyText" + (i+1)].updateText();

        }
    },

    transitionIn: function(){
        //only show this if the gender had not been selected
        currentView = 'scoreboard';
        //DAVINDER access character select here 
            if(_.size(this.children)==0) this.buildView();   
            v.get('menubg').switchToBg2();
            v.get('menuhud').switchToCharacterSelect();
            this.show();
            this.enable();
            //this.setData();


        

        


    },
    transitionOut: function(){
        this.hide();
        v.get('menubg').switchToBg();
        this.disable();
    },
    enable: function(){
        this.male_btn.enable();
        this.female_btn.enable();
        this.continue_btn.enable();
    },
    disable: function(){
       this.male_btn.disable();
       this.female_btn.disable(); 
    }
});