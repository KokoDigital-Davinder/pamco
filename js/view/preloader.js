// JavaScript Document
var PreloaderView = koko.view({
	assets: [
		{ id:'preloader_json', src:'img/preloaderIcons.json' }
	],
	mobileTapToStart: true,
	buildView: function() {
		var g = app.guiSize, vp = app.viewPort;

		this.children = d.buildLayout([
			{ name:'title', type:'sprite', id:'game_logo.png', regX:220, x: 405, y: 180, visible:true },
			//{ name:'tagline', type:'sprite', id:'tagline', regX: 186, x: 405, y:275 , visible:false },
			{ name:'pamcoLogo', type:'sprite', id:'pamco_logo.png', regX: 0, x: 405, y:275 , visible:false},

			
		
			{ name:'loader', type:'container', regX:173, regY:51, x: 405, y: 420, visible:true },
			{ name:'loading_back', type:'sprite', id:'loading_pink.png', parent:'loader', x: 15, y:40, visible:false }, //black bar
			{ name:'loading_front', type:'sprite', id:'loading_bar.png', parent:'loader', visible:true }, //fancy box to "frame" the loading bar
            { name:'loading_bar', type:'sprite', id:'loading_pink.png', parent:'loader', x: 0, y:0, sX:0, visible:false },//red bar
			{ name:'loadingText', type:'sprite', id:'loading_title.png', parent:'loader', x: 0, y:0, visible:true },
		{ name:'tapToPlay', type:'sprite', id:'tap_to_play.png',  x: 0, y:0, visible:false}
			
			
            //{ name:'taptostart', type:'sprite', id:'taptostart', x:405, regX:78, y: 400, alpha:0, visible:false },
        ], this.container);

	var relativeUnit = app.viewPort.height/100;


	//===============================================================================================


	//this.children.title.scale.x = 0.5;
	//this.children.title.scale.y = 0.5;
	this.children.loading_front.scale.x = 0.5;
	this.children.loading_front.scale.y = 0.5;
	this.children.loading_bar.scale.x = 0;
	this.children.loading_bar.scale.y = 0.5;
	this.children.loadingText.scale.x = 0.5;
	this.children.loadingText.scale.y = 0.5;
	this.children.title.scale.x = 0.5;
	this.children.title.scale.y = 0.5;
	this.children.tapToPlay.scale.x = 0.5;
	this.children.tapToPlay.scale.y = 0.5;
	this.children.pamcoLogo.scale.x = 0.47;
	this.children.pamcoLogo.scale.y = 0.47;

	this.children.loader.anchor = new PIXI.Point(0.5, 0.5);
	this.children.loading_front.anchor = new PIXI.Point(0.5, 0.5);
	this.children.loadingText.anchor = new PIXI.Point(0.5, 0.5);
	this.children.title.anchor = new PIXI.Point(0.5, 0.5);
	this.children.pamcoLogo.anchor = new PIXI.Point(0.5, 0.5);
	this.children.tapToPlay.anchor = new PIXI.Point(0.5, 0.5);


	this.children.loader.position.y = app.viewPort.centerY + relativeUnit*30;
	this.children.loadingText.position.y -=  relativeUnit*0.5;
	this.children.loading_bar.position.y -=  relativeUnit*3.4;
	this.children.loading_bar.position.x -=  relativeUnit*15;
	this.children.pamcoLogo.position.y =  relativeUnit*3.8;
	this.children.pamcoLogo.position.x =  relativeUnit*25;
	this.children.tapToPlay.position.y =  vp.centerY + relativeUnit*22;
	this.children.tapToPlay.position.x =  vp.centerX;

	this.children.title.position.x =  app.viewPort.centerX + relativeUnit*18;
	//this.children.loadingText.position.x = app.viewPort.centerX;*/
    //this.children.mainMenuTitle.position.y = app.viewPort.centerY - relativeUnit*15;
    //this.children.mainMenuTitle.position.x = app.viewPort.centerX;


},

    soundPerc: 0,
    updateSoundPerc: function(perc) {
        this.soundPerc = perc;
    },
	transitionIn: function(){
		
		currentView = 'preloader';

		if(_.size(this.children)==0) this.buildView();
		//load the assets
		var _t = this;
		
		var loadedGraphics = false;
		var loadedSounds = false;
		var loadedTimeWait = false;
		
        //load the graphics for the rest of the game
        a.loadManifest(function(perc) {

        	var loadingBarFitsInsideLoadingFront = _t.children.loading_bar.width < _t.children.loading_front.width*0.9;
			
			_t.children.loading_bar.visible = true;

            if(loadingBarFitsInsideLoadingFront){

	            _t.children.loading_bar.scale.x = perc/2;
            }

            //console.log('preload', perc);
        }, function(){
            loadedGraphics = true;
            //console.log('gfx done');
            if(loadedSounds && loadedTimeWait) _t.finishLoading();
        });
		//audio lading
		au.loadedHandler = function(){
			loadedSounds = true;
            //console.log('sfx done');
			if(loadedGraphics && loadedTimeWait) _t.finishLoading();
		}
		
		setTimeout(function(){
			loadedTimeWait = true;
            //console.log('timer done');
			if(loadedGraphics && loadedSounds) _t.finishLoading();
		}, 1000);
		
		this.show();
	},
	finishLoading: function(){
		console.log("inside finish loading in preloader");
		//tap to start
		if(c.system.isMobile && this.mobileTapToStart) {
			var _t = this;
			var tappedToStart = function(e){
                au.play('button'+m.randomInt(1,2), 1);
				_t.transitionOut();
				_t.startMusicLoop();
				app.renderer.view.removeEventListener(c.eventName('down'), tappedToStart);
			}
			
			app.resize();
            //DAVINDR
            //d.animateObject(this.children.loader, .2, { alpha:0 });
            //d.animateObject(this.children.taptostart, .2, { alpha:1, delay:0.1 });
            
			app.renderer.view.addEventListener(c.eventName('down'), tappedToStart);
		} else {
			this.transitionOut();	
			this.startMusicLoop();	
		}
	},
	startMusicLoop: function(){
		vm.playMusic();
	},
	transitionOut: function() {
		console.log("begin transition out inside preloader");
		var _t = this;
        d.animateObject(this.container, .2, { alpha: 0, onComplete: function(){ 
            _t.gotoMainMenu();
        }});
	},
    gotoMainMenu: function(){
        
        /*this.children.title.position.y = 90;
        this.children.title.position.x = app.viewPort.centerX;
		this.children.tagline.position.y = 175;
		this.children.tagline.position.x = app.viewPort.centerX;*/
		//remove the loader and
		this.container.removeChild(this.children.loader);
		/*this.container.removeChild(this.children.taptostart);
		this.children.taptostart = this.children.loader = this.children.loading_back = this.children.loading_bar = this.children.loading_front = null;
		*/
		//show these guys
        v.get('mainmenu').container.addChild(this.container);
        this.container.alpha = 1;
        
        //.do it
        switch(1){
            case 1:
                v.each(['menubg', 'menuhud', 'scoreboard', 'leaderboard'], 'transitionIn');
		    break;
            case 2:
                game.setLevel(0);
		       v.each(['menubg', 'menuhud', 'gameover', 'leaderboard'], 'transitionIn');
            break;
        }   
    }
});