// JavaScript Document
var InGameCountdownView = koko.view({
	assets: [
        //{ id:'ingamecountdown_json', src:'img/ingamecountdown.json' }

       // { id:'ingame_json', src:'img/inGameIcons.json?v=4' },
	],
	buildView: function() {
		var g = app.guiSize, vp = app.viewPort, _t = this;		
		
		this.children = d.buildLayout([
            { name:'getready', type:'sprite', id:'getready_text.png', regX: 179, regY:42, x:vp.centerX, y:vp.centerY, s:0.6, alpha:0 },
            { name:'go', type:'sprite', id:'go_text.png', regX: 64, regY:35, x:vp.centerX, y:vp.centerY, s:0.6, alpha:0 },
            
		], this.container);
            
	},
	transitionIn: function() {
        if(_.size(this.children)==0) this.buildView();
        
        var _t = this;
        
        d.animateObject(this.children.getready, 0.5, { s:1, alpha:1, yoyo:true, repeat:1 });
            
        d.animateObject(this.children.go, 1, { delay:1, s:1, alpha:1, yoyo:true, repeat:1, onComplete:function(){
            _t.transitionOut();
        }});
        
        this.show();
    },
	transitionOut: function() {
		this.hide();
        game.allowMovement = true;
	}
});