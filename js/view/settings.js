var SettingsView = koko.view({
    assets: [
        { id:'popup_json', src:'img/popups.json?v=11' },
        { id:'settings_json', src:'img/settings.json' },
        { id:'settings2_json', src:'img/settingsIcons.json' }
    ],
    buildView: function(){
        var vp = app.viewPort, g = app.guiSize;
        this.children = d.buildLayout([
            { name:'bg', type:'shape', alpha:0.5, w: g.width, h:g.height },
            
            { name:'con', type:'container', regX:186, x: vp.centerX, y: vp.centerY },
            
            { name:'panel', type:'sprite', id:'settings_panel.png', parent:'con' },
                { name:'close_btn', type:'sprite', id:'settings_closebutton.png', parent:'con', x:320, y: 5, visible:true},
            { name: 'fb_btn', type:'sprite', id:'share_facebook_button.png', x: 70, y: 400, parent:'con' },
            { name: 'gplus_btn', type:'sprite', id:'sgplus_btn', x: 150, y: 400, parent:'con', visible:false },
            { name: 'twitter_btn', type:'sprite', id:'share_twitter_button.png', x: 230, y: 400, parent:'con' },
            
            { name:'female', type:'container', parent:'con', x: 295, y: 250, visible:false },
                { name:'female_active', type:'sprite', id:'female_selected', parent:'female', visible:false  },
                { name:'female_inactive', type:'sprite', id:'female_deselected', parent:'female', visible:false },
             { name:'male', type:'container', parent:'con', x: 240, y: 250, visible:false },
                { name:'male_active', type:'sprite', id:'male_selected', parent:'male', visible:false  },
                { name:'male_inactive', type:'sprite', id:'male_deselected', parent:'male', visible:false },
            
            { name: 'switch1', type:'sprite', id:'settings_slider.png', parent:'con', x: 228, y: 97, visible:false },
            { name: 'switch2', type:'sprite', id:'settings_slider.png', parent:'con', x: 228, y: 176, visible:false },


            { name: 'sfx_switch', type:'container', parent:'con', x: 228, y: 97, visible:true},
					{ name: 'on1', type:'sprite', id:'settings_slider.png', parent:'sfx_switch', visible:true},
					{ name: 'off1', type:'sprite', id:'settings_slider.png', parent:'sfx_switch' },

            { name: 'sfx_switchTwo', type:'container', parent:'con', x: 228, y: 97, visible:true},
                    { name: 'onTwo', type:'sprite', id:'settings_slider.png',  parent:'sfx_switchTwo', visible:true},
                    { name: 'offTwo', type:'sprite', id:'settings_slider.png', parent:'sfx_switchTwo' },

					
				{ name: 'music_switch', type:'container', parent:'con', x: 228, y: 176, visible:false },
					{ name: 'on', type:'sprite', id:'slider_on', parent:'music_switch', visible:false },
					{ name: 'off', type:'sprite', id:'slider_off', parent:'music_switch' },
        ], this.container);
             

        var relativeUnit = app.viewPort.height/100;
        this.children.panel.anchor = new PIXI.Point(0.5, 0.5);
        this.children.fb_btn.anchor = new PIXI.Point(0.5, 0.5);
        this.children.twitter_btn.anchor = new PIXI.Point(0.5, 0.5);
        this.children.switch1.anchor = new PIXI.Point(0.5, 0.5);
        this.children.switch2.anchor = new PIXI.Point(0.5, 0.5);
        this.children.on1.anchor = new PIXI.Point(0.5, 0.5);
        this.children.off1.anchor = new PIXI.Point(0.5, 0.5);
        this.children.onTwo.anchor = new PIXI.Point(0.5, 0.5);
        this.children.offTwo.anchor = new PIXI.Point(0.5, 0.5);
        this.children.close_btn.anchor = new PIXI.Point(0.5, 0.5);

        this.children.panel.scale.x = 0.5;
        this.children.panel.scale.y = 0.5;
        this.children.fb_btn.scale.x = 0.5;
        this.children.fb_btn.scale.y = 0.5;
        this.children.twitter_btn.scale.x = 0.5;
        this.children.twitter_btn.scale.y = 0.5;
        this.children.switch1.scale.x = 0.5;
        this.children.switch1.scale.y = 0.5;
        this.children.switch2.scale.x = 0.5;
        this.children.switch2.scale.y = 0.5;
        this.children.on1.scale.x = 0.5;
        this.children.on1.scale.y = 0.5;
        this.children.off1.scale.x = 0.5;
        this.children.off1.scale.y = 0.5;
        this.children.onTwo.scale.x = 0.5;
        this.children.onTwo.scale.y = 0.5;
        this.children.offTwo.scale.x = 0.5;
        this.children.offTwo.scale.y = 0.5;
        this.children.close_btn.scale.x = 0.45;
        this.children.close_btn.scale.y = 0.45;

        this.children.con.position.x -= relativeUnit*2;
        this.children.panel.position.x = vp.centerX - relativeUnit*5;
        this.children.panel.position.y = vp.centerY - relativeUnit*15;
        this.children.switch1.position.x += relativeUnit*10;
        this.children.switch1.position.y += relativeUnit*10.3;
        this.children.switch2.position.x += relativeUnit*10;
        this.children.switch2.position.y += relativeUnit*6;
        this.children.fb_btn.position.x += relativeUnit*23;
        this.children.fb_btn.position.y -= relativeUnit*14.5;
        this.children.twitter_btn.position.x += relativeUnit*9;
        this.children.twitter_btn.position.y -= relativeUnit*14.5;
        this.children.sfx_switch.position.x += relativeUnit*3;
        this.children.sfx_switch.position.y += relativeUnit*10;
        this.children.sfx_switchTwo.position.x += relativeUnit*3;
        this.children.sfx_switchTwo.position.y += relativeUnit*18;
        this.children.close_btn.position.x += relativeUnit*3.5;
        this.children.close_btn.position.y += relativeUnit*11;
        /*this.children.panel.switch1.x = vp.centerX - relativeUnit*5;
        this.children.panel.switch1.y = vp.centerY - relativeUnit*15;*/




        var _t = this;
        var yUp = this.children.close_btn.position.y - 2.5;
        this.close_btn = new button(this.children.close_btn, { y: yUp }, function(){
            au.play('button'+m.randomInt(1,2), 1);
            _t.transitionOut();
        });
        
        var yUp = this.children.fb_btn.position.y - 2.5;
        this.fb_btn = new button(this.children.fb_btn, { y: yUp }, function(e){
            au.play('button'+m.randomInt(1,2), 1);
            sharegame('facebook', 0, true, event); 
        });
        this.gplus_btn = new button(this.children.gplus_btn, { y: 397 }, function(e){
            au.play('button'+m.randomInt(1,2), 1);
            app.openLink(app.url + 'api/share/googleplus', 'link', e);
            ga('send', 'event', 'Game', 'CTA - Share Google+');
        });

        var yUp = this.children.twitter_btn.position.y - 2.5;
        this.twitter_btn = new button(this.children.twitter_btn, { y: yUp }, function(e){
            au.play('button'+m.randomInt(1,2), 1);
            sharegame('twitter', 0, true, event); 
            ga('send', 'event', 'Game', 'CTA - Share Twitter');
        });
        
        this.female_btn = new button(this.children.female, { y: 247 }, function(){
           au.play('button'+m.randomInt(1,2), 1);
            _t.changeGender('female', true); 
        });
        this.male_btn = new button(this.children.male, { y: 247 }, function(){
           au.play('button'+m.randomInt(1,2), 1);
            _t.changeGender('male', true); 
        });
            
            
        this.sfx_switch = new toggle(this.children.sfx_switchTwo, 48, function(on){
            
            //--------------------------------
            user.sfx = on ? 1 : 0;
            au.setSoundGroupVolume('sfx', user.sfx);
            user.save();
            console.log("sfx_switch");
		});
		this.sfx_switch.setToggle(user.sfx == 1); //0 sets it to the right position
		
		this.music_switch = new toggle(this.children.sfx_switch, 48, function(on){
            user.music = on ? 1 : 0;
            au.setSoundGroupVolume('music', user.music);
            user.save();
            console.log("sfx_switchTwo");
		});
		this.music_switch.setToggle(user.music == 1);
        
    },
    transitionIn: function(){
        if(_.size(this.children)==0)this.buildView();
        this.enable();
        this.show();
            
        this.container.alpha = 0;
        d.animateObject(this.container, .3, { alpha: 1 });
            
        this.children.con.position.y = 70;
        this.children.con.alpha = 0;
        d.animateObject(this.children.con, .3, { alpha: 1, y:50 });
        
        //disbale other menus
        v.each(['mainmenu', 'charselect', 'gameover', 'menuhud', 'leaderboard'], function(view){
            if(view.container.visible) view.disable();
        });
        
        //select it
        this.changeGender(user.gender, false);
    },
    changeGender: function(g, save) {
        
        if(save) {
            user.gender = g;
            user.save();
        }
        
        switch(g) {
            case 'male':
            case 'unknown':
                this.children.male_active.visible = true;
                this.children.male_inactive.visible = false;
                this.children.female_active.visible = false;
                this.children.female_inactive.visible = true;
            break;
            case 'female':
                this.children.male_active.visible = false;
                this.children.male_inactive.visible = true;
                this.children.female_active.visible = true;
                this.children.female_inactive.visible = false;
            break;
        }
        
        
    },
    transitionOut: function(){
        var _t = this;
        
        this.disable();
        d.animateObject(this.container, .2, { alpha:0, onComplete: function(){
            _t.hide();
            
            v.each(['mainmenu', 'charselect', 'gameover', 'menuhud', 'leaderboard'], function(view){
                if(view.container.visible) view.enable();
            });
        }});
        //reenable other menus
        
    },
    enable: function(){
        this.close_btn.enable();
        this.fb_btn.enable();
        this.gplus_btn.enable();
        this.twitter_btn.enable();
        this.male_btn.enable();
        this.female_btn.enable();
        this.sfx_switch.enable();
        this.music_switch.enable();
    },
    disable:  function(){
        this.close_btn.disable();
        this.fb_btn.disable();
        this.gplus_btn.disable();
        this.twitter_btn.disable();
        this.male_btn.disable();
        this.female_btn.disable();
        this.sfx_switch.disable();
        this.music_switch.disable();
    }
});