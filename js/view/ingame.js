// JavaScript Document
var InGameView = koko.view({
	assets: [
		{ id:'ingame_json', src:'img/ingame.json?v=4' },
        { id:'ingame_json', src:'img/coins.json?v=4' },
        { id:'ingame_json', src:'img/collectablesIcons.json?v=4' },
        { id:'ingame_json', src:'img/boostsAndBaddies0.json?v=4' },
        { id:'ingame_json', src:'img/boostsAndBaddies1.json?v=4' },
        { id:'ingame_json', src:'img/inGameIcons.json?v=4' },
        { id:'ingame_json', src:'img/backgroundSky0.json?v=4' },
        { id:'ingame_json', src:'img/backgroundSky1.json?v=4' },
		{ id:'ar_font', src:'img/arial_rounded_mt_bold.fnt' },
        { id:'ar_large_font', src:'img/arial_rounded_mt_bold_large.fnt' }
	],

    maxHeight:0,

	buildView: function() {
		var g = app.guiSize, vp = app.viewPort, _t = this;		
		
		//add the relative containers
        var gamebg = v.get('ingamebg').container;
		this.container.addChild(gamebg);
		this.container.addChild(game.container);
        
        game.container.pivot.y = game.container.position.y = 610;
        gamebg.pivot.y = gamebg.position.y = 610;
		
		//children
		this.children = d.buildLayout([
            
            { name:'boost_cont', type:'container', x:5, y: 6 },
                { name:'boost_back', type:'sprite', id:'boost_back', parent:'boost_cont', visible:true},
                { name:'boost_fill', type:'sprite', id:'boost_fill_0', parent:'boost_cont', visible:true},
                { name:'boost_overlay', type:'sprite', id:'boost_overlay', parent:'boost_cont', visible:true},
            
            { name:'star_container', type:'container', x: vp.centerX, y: 49, visible:false },
            { name:'gostar1', type:'container', x:-90, regX:41, regY:43, parent:'star_container', visible:false },
                { name:'gostar1_empty', type:'sprite', id:'gostar1_empty', parent:'gostar1', visible:false },
                { name:'ach_1_title', type:'bmptext', text:"Reach\n500m", font:'16px ARMTB', x:42, y:44, tint:0x868686, align:'center', parent:'gostar1', visible:false},
                { name: 'icon_cont_1a', type:'container', x:37, y:41, s:0.8, parent:'gostar1' , visible:false},
                    { name:'gostar1_full', type:'container', parent:'gostar1', visible:false },
                        { name:'gostar1_fullbg', type:'sprite', id:'gostar1_full', parent:'gostar1_full'  , visible:false},
                        { name:'ach_1_title_above', type:'bmptext', text:"Reach\n500m", font:'16px ARMTB', x:42, y:44, tint:0xcc6600, align:'center', parent:'gostar1_full', visible:false},
                        { name: 'icon_cont_1b', type:'container', x:38, y:41, s:0.8, parent:'gostar1_full' , visible:false},
            
            { name:'gostar2', type:'container', x:0, regX:41, regY:43, parent:'star_container' , visible:false},
                { name:'gostar2_empty', type:'sprite', id:'gostar2_empty', parent:'gostar2' , visible:false},
                { name:'ach_2_title', type:'bmptext', text:"Reach\n500m", font:'16px ARMTB', x:42, y:44, tint:0x868686, align:'center', parent:'gostar2', visible:false},
                { name: 'icon_cont_2a', type:'container', x:37, y:41, s:0.8, parent:'gostar2' , visible:false},
                    { name:'gostar2_full', type:'container', parent:'gostar2' },
                        { name:'gostar2_fullbg', type:'sprite', id:'gostar2_full', parent:'gostar2_full'  , visible:false},
                        { name:'ach_2_title_above', type:'bmptext', text:"Reach\n500m", font:'16px ARMTB', x:42, y:44, tint:0xcc6600, align:'center', parent:'gostar2_full', visible:false},
                        { name: 'icon_cont_2b', type:'container', x:38, y:41, s:0.8, parent:'gostar2_full' , visible:false},
           
            { name:'gostar3', type:'container', x:90, regX:41, regY:43, parent:'star_container' , visible:false},
                { name:'gostar3_empty', type:'sprite', id:'gostar3_empty', parent:'gostar3' , visible:false},
                { name:'ach_3_title', type:'bmptext', text:"Reach\n500m", font:'16px ARMTB', x:44, y:44, tint:0x868686, align:'center', parent:'gostar3', visible:false},
                { name: 'icon_cont_3a', type:'container', x:37, y:41, s:0.8, parent:'gostar3' , visible:false},
                    { name:'gostar3_full', type:'container', parent:'gostar3' },
                        { name:'gostar3_fullbg', type:'sprite', id:'gostar3_full', parent:'gostar3_full', visible:false  },
                        { name:'ach_3_title_above', type:'bmptext', text:"Reach\n500m", font:'16px ARMTB', x:44, y:44, tint:0xcc6600, align:'center', parent:'gostar3_full', visible:false},
                        { name: 'icon_cont_3b', type:'container', x:38, y:41, s:0.8, parent:'gostar3_full' , visible:false},
		
                
            { name:'height_cont', type:'container', regX:139, x: vp.right - 7, y: 10 },
                { name:'distance_txt', type:'bmptext', parent:'height_cont', text:'DISTANCE', font:'15px ARMTB', x:24, y: 27, visible:true },
                { name:'height_txt', type:'bmptext', parent:'height_cont', text:'0m', font:'40px ARMTB_LARGE', x:124, y: 27, tint:0x4db8ff },
            
		], this.container);
            
        var relativeUnit = app.viewPort.height/100;

        this.children.distance_txt.position.x += relativeUnit*5;
        this.children.distance_txt.position.y -= relativeUnit*1.2; 


        
        this.achievements = [];
                                      
		for( var i = 0; i < game.level.achievements.length; i++ ) {
			var ach = game.level.achievements[i];
            
            var title = this.children['ach_'+ (i+1) +'_title'];
            var above = this.children['ach_'+ (i+1) +'_title_above']; 
            
            var titleText = ach.title.replace(' ', '\n').replace(' x ', '      ');
            
            title.setText(titleText).centerText(true, true)
            above.setText(titleText).centerText(true, true)
            
            var fullStar = this.children['gostar'+(i+1)+'_full'];
            var fullText = this.children['ach_'+(i+1)+'_title_above'];
            
            //remove icons
            if(this.children['icon_cont_'+(i+1)+'a'].children.length == 1) this.children['icon_cont_'+(i+1)+'a'].removeChildAt(0);
            if(this.children['icon_cont_'+(i+1)+'b'].children.length == 1) this.children['icon_cont_'+(i+1)+'b'].removeChildAt(0);
            
            //add icons
            if(ach.data && ach.data.icon) {
                 d.buildLayout([{type:'sprite', id:'goc_' + ach.data.icon }], this.children['icon_cont_'+(i+1)+'a']);
                 d.buildLayout([{type:'sprite', id:'goc_' + ach.data.icon }], this.children['icon_cont_'+(i+1)+'b']);
            }   
			//ach.title
			if(!ach.completed) {
				
                var icon = ach.data.id ? ach.data.id : 'none';
                this.achievements.push({type:ach.data.type, id:i, val: ach.data.value, icon: icon, gfx: this.children['gostar'+(i+1)+'_fullbg']});
                
			} else {
                this.achievements.push(false);   
            }
		}
		//pos
        this.container.position.x = 0;
		
	},
    resize: function(){
        var vp = app.viewPort;
        var gamebg = v.get('ingamebg').container;
        
        game.scale = vp.width / 660;
    
        game.container.scale.x = game.container.scale.y = game.scale;
        gamebg.scale.x = gamebg.scale.y = game.scale;
        
        gamebg.position.x = game.container.position.x = vp.left;
            
        this.children.boost_cont.position.x = vp.left + 5;
        this.children.height_cont.position.x = vp.right - 7;
        
       /*
        1     0.7
        580 - 460
        */
        //this.children.star_container.scale.x = this.children.star_container.scale.y = m.clamp((vp.width - 460)/ 120, 0.7, 1);
       // this.children.star_container.position.x = vp.centerX + (30-(37/this.children.star_container.scale.x));
        //console.log(vp.centerX, this.children.star_container.position.x )
        
    },
	showGame: function(){
		this.buildView();
		//enable this ting
		this.enable();
		this.show();
		//init
       // vm.playLevelMusic(game.level.id);
        
        game.allowMovement = false;
        //v.get('howtoplay').transitionIn();
       if(!v.get('howtoplay').hasShown && game.level.id == 0) {
            v.get('howtoplay').transitionIn();
        } else {
            v.get('howtoplay').hasShown = true;
            v.get('ingamecountdown').transitionIn();
        }      
            
        game.init();
		game.start();
        
        var _t = this;
        app.addResizeListener('ingame', function(){
           _t.resize(); 
        });
            
        this.container.alpha = 0;
        d.animateObject(this.container, .3, { alpha: 1 });
            
	},
    loadedCharFile: null,
	transitionIn: function() {
        currentView = 'ingame';
        v.get('menuhud').switchToNoIcons();
		//load graphics	
		var assets = [];
        if(user.gender=="male")
        {
            var charFile = 'maleChar'
        }
        else
            {
                 var charFile = 'femaleChar'
            }
		//var charFile = user.gender.substr(0,1) + game.level.assetRef;


		if(this.loadedCharFile == charFile) {
            v.get('ingame').showGame();
        } else {
            //DAVINDER Background sky set here
            assets.push({ id:'char_gfx', src:'img/characters/' + charFile + '.json'});
            assets.push({ id:'bg_gfx0', src:'img/' + "backgroundSky0" + '.json'});
		    assets.push({ id:'bg_gfx1', src:'img/' + "backgroundSky1" + '.json'});
		    //hard code the background image above      
            this.loadedCharFile = charFile;
        
            //load em
            v.get('loader').transitionIn(assets, function(){
                v.get('ingame').showGame();
            });     
        }
        
        v.get('leaderboard').hideHandle();
	},
    lastBoost: null,
	updateHUD: function(height, streak, boost) {
		
        this.children.height_txt.setText(Math.floor(height).toString()+"m").pivotText(1, 0);
        
        
        //update boost
		var boostlevel = Math.round(boost * 10) * 10;
        if(this.lastBoost != boostlevel) {
            this.lastBoost = boostlevel;
		    //temp
            this.children.boost_fill.texture = new PIXI.Texture.fromFrame('boost_fill_' + boostlevel + ".png");
        }
	},
    destroy: function(){
        for( var i = 0, l = this.container.children.length; i < l; i++ ) this.container.removeChildAt(0); 
        this.children = null;
    },
	transitionOut: function() {
		//vars
        //console.log("this.children.height_txt.text",this.children.height_txt.text);
       
       //step1
        var heightReached = this.children.height_txt.text;
        var _t = this, c = this.children, vp = app.viewPort;
        v.get('menubg').transitionIn();
        _t.hide();
        _t.destroy();
        
        v.get('gameover').transitionIn(heightReached);
        /*d.animateObject(this.container, .2, { alpha: 0, onComplete: function(){
            v.each(['gameover', 'leaderboard', 'menuhud'], 'transitionIn');
            
            v.get('leaderboard').showHandle();
        }});*/
        
        //vm.playMusic();
        
        this.disable();                             
		game.stop();
        app.removeResizeListener('ingame');
	},
    updateStars: function( height, collected) {
        for( var i = 0; i < this.achievements.length; i++ ){
            var a = this.achievements[i];
            if(!a) continue;
			
            var perc = 0;
			
            switch(a.type) {
                case 'height':
                    perc = height / a.val;
                break;
                case 'collect':
                    perc = collected[a.icon]/a.val;
                break;
            }
			
            if(perc > 1) perc = 1;
            if(perc == 0) perc = 0.01;
			
			//update boost
			var starlevel = Math.round(perc * 10) * 10;
			if( isNaN(starlevel) ) starlevel = 0;
			
			//console.log(a.laststarlevel , starlevel);
			if(a.laststarlevel != starlevel) {
				a.laststarlevel = starlevel;
				
				if(starlevel == 0 ) {
					a.gfx.visible = false; 
				} else {
					a.gfx.visible = true;
					a.gfx.texture = new PIXI.Texture.fromFrame("gostar"+(a.id+1)+"_full_" + starlevel + ".png");
				}
			}
			
           
            if(perc == 1) {
                var itm = this.children['gostar'+(i+1)];
                
                au.play('star1', 1);
                
                d.animateObject(itm, .15, { s:1.1, ease:Back.easeOut, onComplete:function(){
                    d.animateObject(itm, .05, { s:1, onComplete:function(){
                        d.animateObject(itm, .15, { s:1.1, ease:Back.easeOut, onComplete:function(){
                            d.animateObject(itm, .05, { s:1 });
                        }});
                    }});
                }});
                
                switch(a.type) {
                    case 'collect':
                        if(game.level.specialCollectableTypes.indexOf(a.icon) > -1) {
                            game.level.specialCollectableTypes.splice(game.level.specialCollectableTypes.indexOf(a.icon), 1);
                        }
                    break;
                }
                
                this.achievements[i] = false;
            }
        }
    },
	enable:function(){
		
	},
	disable:function(){
		
	}
});