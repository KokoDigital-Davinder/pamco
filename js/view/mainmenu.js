// JavaScript Document register changes
var MainMenuView = koko.view({
	assets:[ {id:'mainmenu_json', src:'img/mainmenu.json'},
    {id:'mainmenu_json', src:'img/mainMenuIcons.json'}],
    like: null,
	buildView: function(){
        var vp = app.viewPort;
        
        this.like = document.getElementById('fb-like');
        
		this.children = d.buildLayout([
            { name:'content_cont', type:'container', x: vp.centerX, visible:true},
            { name:'play_btn', type:'sprite', id:'play_button.png', x:0, y:0, parent:'content_cont', visible:true},
			{ name:'highScoresButton', type:'sprite', id:'highscores_button.png', x:0, y:0, parent:'content_cont', visible:true},
            { name:'winprizes_btn', type:'sprite', id:'winprizes_btn', x:-165, y:426, parent:'content_cont', visible:false },
			{ name:'creditcard', type:'sprite', id:'creditcard', x:-162, y:206, parent:'content_cont', visible:false },
			{ name:'male_jump', type:'sprite', id:'male_jump', x:-246, y:211, parent:'content_cont', visible:false },
			{ name:'female_jump', type:'sprite', id:'female_jump', x:133, y:193, parent:'content_cont', visible:false },
		], this.container);	
		
        /*this.children.loading_front.scale.x = 0.5;
        this.children.loading_front.scale.y = 0.5;
        this.children.loader.anchor = new PIXI.Point(0.5, 0.5);
        this.children.pamcoLogo.position.y =  relativeUnit*3.8;
        this.children.pamcoLogo.position.x =  relativeUnit*28;*/
        
        var relativeUnit = app.viewPort.height/100;

        this.children.play_btn.scale.x = 0.5;
        this.children.play_btn.scale.y = 0.5;
        this.children.highScoresButton.scale.x = 0.5;
        this.children.highScoresButton.scale.y = 0.5;

        this.children.play_btn.anchor = new PIXI.Point(0.5, 0.5);
        this.children.highScoresButton.anchor = new PIXI.Point(0.5, 0.5);

        console.log("vp", vp);

        this.children.play_btn.position.y =  relativeUnit*65;
        //this.children.play_btn.position.x =  relativeUnit*20;
        this.children.highScoresButton.position.y +=  relativeUnit*78;
        //this.children.highScoresButton.position.x =  relativeUnit*28;

        

		var _t = this;
        var yUp = this.children.play_btn.position.y - 2.5;
        console.log("play button", this.children.play_btn); 
		this.play_btn = new button(this.children.play_btn, {y: yUp }, function(){
            au.play('button'+m.randomInt(1,2), 1);
            
            // v.get('connect2').transitionIn(function(){
            //     v.get('mainmenu').play();
            // });
            _t.play();

		});

       
        var yUp2 = this.children.highScoresButton.position.y - 5;
        console.log("play button", this.children.play_btn); 
        this.highScoresButton = new button(this.children.highScoresButton, {y: yUp2 }, function(){
            au.play('button'+m.randomInt(1,2), 1);
            
            // v.get('connect2').transitionIn(function(){
            //     v.get('mainmenu').play();
            // });
            //_t.play();

            _t.transitionOut(function(){
            //v.get('ingame').transitionIn();
            v.get('scoreboard').transitionIn();
            });

        });
	},
    play: function(){
        this.transitionOut(function(){
            console.log('charselect');
            //v.get('ingame').transitionIn();
            v.get('charselect').transitionIn();
        });
    },
	transitionIn: function() {
        console.log("inside transitionIN mainMenur");
        currentView = 'mainmenu';
		v.get('menuhud').switchToMainMenu();
        if(_.size(this.children) == 0) this.buildView();
		
		this.show();
		this.enable();
            
        // if(facebook.loggedin) {
        //     $("#facebook_notconnected_like").addClass('hide');  
        //     $("#facebook_connected_like").removeClass('hide');  
        // }
		
		app.resize();
            
        this.container.alpha = 0;
        d.animateObject(this.container, .3, { alpha: 1 });
		
		//v.get('menuhud').toggleCardVisibility(true);
            
        // facebook.checkLogin(function() {}, function() {}, false);
        
        var _t = this;
        app.addResizeListener('mainmenu', function(){ _t.resize(); });
		
		//v.get('winprizes').hideEnterNow = false;
		//v.get('winprizes').transitionIn(true);
        console.log("inside main menu");
    },
    resize:function(){
        var vp = app.viewPort;
        
        //shazinga
        var fbLikeScale = 1.2;
        var left = ((vp.x >0) ? vp.x : 0) + (vp.width * vp.s);
        var top = ((vp.y >0) ? vp.y : 0) + (vp.height * vp.s);

        left = (left-(55*vp.s*fbLikeScale));
        top = (top-(25*vp.s*fbLikeScale));

        this.like.style[koko.client.cssprops.transform] = "translate("+left+"px,"+top+"px) scale("+vp.s*fbLikeScale+")";
    },
	transitionOut: function(complete) {
	
		
		
        var _t = this;
            
        d.animateObject(this.container, .2, { alpha:0, onComplete: function(){
            _t.hide();
            if(complete) complete();
        }});
		this.disable();
            
        
        app.removeResizeListener('mainmenu');
	},
	enable: function() {
		this.play_btn.enable();	
		//this.winprizes_btn.enable();
        this.like.className = this.like.className.replace(/hide/g, '').replace(/\s{2,}/g, ' ');
        this.highScoresButton.enable();
	}, 
	disable: function() {
		this.play_btn.disable();
		//this.winprizes_btn.disable();
        this.like.className = this.like.className + ' hide';
        this.highScoresButton.disable();
	}
});