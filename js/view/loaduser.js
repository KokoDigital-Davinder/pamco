
    // JavaScript Document
var LoadUserView = koko.view({
	assets: [
        { id:'preloader_json', src:'img/preloaderIcons.json' }
    ],
    loaded: false,
	buildView: function(){
		var g = app.guiSize, vp = app.viewPort;
		this.children = d.buildLayout([
            { name:'loader', type:'container', regX:173, regY:51, x: 405, y: 420, visible:true },
            { name:'loading_back', type:'sprite', id:'loading_pink.png', parent:'loader', x: 15, y:40, visible:false }, //black bar
            { name:'loading_front', type:'sprite', id:'loading_bar.png', parent:'loader', visible:true }, //fancy box to "frame" the loading bar
            { name:'loading_bar', type:'sprite', id:'loading_pink.png', parent:'loader', x: 0, y:0, sX:0, visible:false }//red bar
            
		], this.container);
	},
    skip: function(){
        v.get('charselect').transitionIn();  
    },
	transitionIn: function(){
		if(app.userLoaded) {
            console.log('here - userLoaded');
            this.skip();
            return;
        }
        
        if(_.size(this.children) == 0) this.buildView();
		
		var _t = this;        
        //IF USER IS LOADED AND STUFF THEN SKIP
        app.getToken(function(){
            app.loadUser(function(){
               _t.transitionOut(); 
            });
        });
		
        this.container.alpha = 0;
        d.animateObject(this.container, .3, { alpha:1});
        
        _t.perc = 0.01;
        this.children.loading_bar.scale.x = 0;
        app.addUpdateListener('loaduser', function(){
            _t.children.loading_bar.scale.x = _t.perc > 1 ? 1 : _t.perc;
            _t.perc += 0.005;
        });
		
        this.show();
	},
	transitionOut: function(){
        var _t = this;
        this.loaded =  true;
        app.removeUpdateListener('loaduser');
        d.animateObject(this.children.loading_bar, .1, { sX:1, onComplete: function(){
            d.animateObject(_t.container, .2, {alpha:0, onComplete:function(){
              v.get('charselect').transitionIn();  
                _t.hide();
            }});
        }});
	}
});	