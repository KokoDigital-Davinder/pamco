// JavaScript Document
var LevelSelectView = koko.view({
	assets: [
		{ id:'levelselect_json', src:'img/levelselect.json' },
	],
	panels: [],
    hasLoadedScores: false,
    scores: [],
	buildView: function() {
		var g = app.guiSize, vp = app.viewPort, _t = this;
		
        		//children
		this.children = d.buildLayout([
            
            {name:'panel_container_wrap', type:'container', x: vp.centerX, y: 80 },
			{ name:'panel_container', type:'container', parent:'panel_container_wrap' },
			
			{ name:'arrow_right', type:'sprite', id:'arrow_right', x:vp.right-50, y:221, regX:36 },
			{ name:'arrow_left', type:'sprite', id:'arrow_left', x:vp.left+50, y:221 },
			
			{ name:'pippet_container', x:vp.centerX, y: 580-20, regX:50 },
            { name:'pippet_inactive', type:'sprite', id:'pippet_inactive', parent:'pippet_container' },
            { name:'pippet_inactive', type:'sprite', id:'pippet_inactive', x:28, parent:'pippet_container' },
            { name:'pippet_inactive', type:'sprite', id:'pippet_inactive', x:56, parent:'pippet_container' },
            { name:'pippet_inactive', type:'sprite', id:'pippet_inactive', x:84, parent:'pippet_container' },
            { name:'pippet_active', type:'sprite', id:'pippet_active', x:0, parent:'pippet_container' },
			
			{ name:'info_panel', type:'container', id:'info_panel', x:vp.centerX, regX:107, y:430-26 },
			{ name:'copyLine', type:'sprite', id:'levelselect_tagline', x:vp.centerX, y:365-14, regX:204, regy:16 },
			
			{ name:'achievements_btn', type:'container', id:'achievements_btn', parent:'info_panel', x:0, y:94 },
            { name:'achievements_idle', type:'sprite', id:'achievements_btn', parent:'achievements_btn', x:0, y:0 },
            { name:'achievements_selected', type:'sprite', id:'achievements_selected', parent:'achievements_btn', x:0, y:0 },	
            
			{ name:'leaderboard_btn', type:'container', id:'leaderboard_btn', parent:'info_panel', x:107, y:94 },
				{ name:'leaderboard_idle', type:'sprite', id:'leaderboard_btn', parent:'leaderboard_btn', x:0, y:0 },
				{ name:'leaderboard_selected', type:'sprite', id:'leaderboard_selected', parent:'leaderboard_btn', x:0, y:0 },	
			
			{ name:'infopanel_top', type:'sprite', id:'infopanel_top', parent:'info_panel', x:0, y:0 },
            { name: 'notconnected_cont', parent:'info_panel', visible: false },
            { name: 'lb_connectinfo', parent:'notconnected_cont', type:'sprite', id:'lb_connectinfo', x:16, y: 12 },
            { name: 'friendpanelfbconnect_btn', parent:'notconnected_cont', type:'sprite', id:'friendpanelfbconnect_btn', x:50, y:52 },
                
				{ name:'leaderboard_container', parent:'info_panel', visible:false },
                
                {name:'leaderboard_load', type:'sprite', id:'ls_loader', parent:'leaderboard_container', regX:15, regY:15, x:107, y:40 },
                
                { name:'l_1', type:'container', parent:'leaderboard_container', x: 11, y: 10 },
                    //{ name:'l_1_noimg', type:'sprite', id:'friends_empty', parent:'l_1' },
                    { name:'l_1_imgwrap', type:'container', parent:'l_1'},
                    { name:'l_1_cover', type:'sprite', id:'friends_mask', parent:'l_1' },
                    { name:'l_1_pipet', type:'sprite', id:'leaderboard_number', parent:'l_1', x:-6, y:-5 },
                    { name:'l_1_pipet_title', type:'bmptext', text:'1', font:'11px ARMTB', x: 0, y: -1, tint:0xffffff, parent:'l_1' },
                    { name:'l_1_name', type:'bmptext', text:'Ben A.', font:'12px ARMTB', x: 25, y: 51, tint:0x717073, parent:'l_1' },
                    { name:'l_1_score', type:'bmptext', text:'183m', font:'18px ARMTB', x: 24, y: 62, tint:0xcc0000, parent:'l_1' },
                    { name:'l_1_invite', type:'sprite', id:'friends_invite', parent:'l_1', y: 53, x: -5 },
                
                { name:'l_2', type:'container', parent:'leaderboard_container', x: 82, y: 10 },
                    //{ name:'l_2_noimg', type:'sprite', id:'friends_empty', parent:'l_2' },
                    { name:'l_2_imgwrap', type:'container', parent:'l_2'},
                    {name :'l_2_cover', type:'sprite', id:'friends_mask', parent:'l_2' },
                    { name:'l_2_pipet', type:'sprite', id:'leaderboard_number', parent:'l_2', x:-6, y:-5 },
                    { name:'l_2_pipet_title', type:'bmptext', text:'2', font:'11px ARMTB', x: 0, y: -1, tint:0xffffff, parent:'l_2' },
					{ name:'l_2_name', type:'bmptext', text:'Ben A.', font:'12px ARMTB', x: 25, y: 51, tint:0x717073, parent:'l_2' },
                    { name:'l_2_score', type:'bmptext', text:'183m', font:'18px ARMTB', x: 24, y: 62, tint:0xcc0000, parent:'l_2' },
                    { name:'l_2_invite', type:'sprite', id:'friends_invite', parent:'l_2', y: 53, x: -5 },
            
                { name:'l_3', type:'container', parent:'leaderboard_container', x: 153, y: 10 },
                    //{ name:'l_3_noimg', type:'sprite', id:'friends_empty', parent:'l_3' },
                    { name:'l_3_imgwrap', type:'container', parent:'l_3'},
                    {name :'l_3_cover', type:'sprite', id:'friends_mask', parent:'l_3' },
                    { name:'l_3_pipet', type:'sprite', id:'leaderboard_number', parent:'l_3', x:-6, y:-5 },
                    { name:'l_3_pipet_title', type:'bmptext', text:'3', font:'11px ARMTB', x: 0, y: -1, tint:0xffffff, parent:'l_3' },
					{ name:'l_3_name', type:'bmptext', text:'Ben A.', font:'12px ARMTB', x: 25, y: 51, tint:0x717073, parent:'l_3' },
                    { name:'l_3_score', type:'bmptext', text:'183m', font:'18px ARMTB', x: 24, y: 62, tint:0xcc0000, parent:'l_3' },
                    { name:'l_3_invite', type:'sprite', id:'friends_invite', parent:'l_3', y: 53, x: -5 },
            
				{ name:'achievements_container', parent:'info_panel' },
					{ name:'achievement_details', parent:'achievements_container' },
						{ name:'checkbox_tick_1', type:'sprite', id:'checkbox_tick', parent:'achievement_details', x:187, y:9, visible:false },
						{ name:'checkbox', type:'sprite', id:'checkbox', parent:'achievement_details', x:183, y:4 },
						{ name:'achievement_1_title', type:'bmptext', text:'acheivement title here', font:'18px ARMTB', x: 10, y: 7, tint:0xcc0000, parent:'achievement_details' },
						
						{ name:'checkbox_tick_2', type:'sprite', id:'checkbox_tick', parent:'achievement_details', x:187, y:38, visible:false },
						{ name:'checkbox', type:'sprite', id:'checkbox', parent:'achievement_details', x:183, y:33 },
						{ name:'achievement_2_title', type:'bmptext', text:'acheivement title here', font:'18px ARMTB', x: 10, y: 36, tint:0xcc0000, parent:'achievement_details' },
						
						{ name:'checkbox_tick_3', type:'sprite', id:'checkbox_tick', parent:'achievement_details', x:187, y:67, visible:false },
						{ name:'checkbox', type:'sprite', id:'checkbox', parent:'achievement_details', x:183, y:62 },
						{ name:'achievement_3_title', type:'bmptext', text:'acheivement title here', font:'18px ARMTB', x: 10, y: 65, tint:0xcc0000, parent:'achievement_details' },
					   
                    {name:'icon_cont', type:'container', parent:'achievement_details' },
                    { name:'achievements_locked', type:'sprite', id:'locked_text', parent:'achievements_container', regX:86, regY:25, x:107, y:45 },
					
			
			
			{ name:'home_btn', type:'sprite', id:'home_btn', x: vp.left+10, y: vp.bottom - 10, regY:58 },
		], this.container);	
            
            
		//buttons
		this.right_arrow_btn = new button(this.children.arrow_right, { y: 218 }, function(e){
			au.play('button'+m.randomInt(1,2), 1);
            _t.slider.gotoPreviousSlide(.2);
		});
            
		this.left_arrow_btn = new button(this.children.arrow_left, { y: 218 }, function(e){
			au.play('button'+m.randomInt(1,2), 1);
            _t.slider.gotoNextSlide(.2);
		});
            
		this.pippet_btn = new button(this.children.pippet_container, null, function(e){
			au.play('button'+m.randomInt(1,2), 1);
            _t.slider.gotoSlide(Math.floor(e.getLocalPosition(_t.children.pippet_container).x/28), .2);
		});
            
		this.leaderboard_btn = new button(this.children.leaderboard_btn, { y: 91 }, function(){
			au.play('button'+m.randomInt(1,2), 1);
            _t.showTab('leaderboard', _t.slider.currentSlideIndex, true);
		});
            
		this.achievements_btn = new button(this.children.achievements_btn, { y: 91 }, function(){
			au.play('button'+m.randomInt(1,2), 1);
            _t.showTab('achievements', _t.slider.currentSlideIndex, true);
		});
            
        this.friendpanelfbconnect_btn = new button(this.children.friendpanelfbconnect_btn, { y: 49 }, function(){
            facebook.checkLogin(function(){
                _t.showTab('leaderboard', _t.slider.currentSlideIndex, true);
            }, function(){}, true);  
        });
            
		this.home_btn = new button(this.children.home_btn, { y: vp.bottom - 13 }, function(){
			au.play('button'+m.randomInt(1,2), 1);
            _t.transitionOut(function(){
			 v.get('mainmenu').transitionIn();
            });
		});
            
        var l1inv = new button(this.children.l_1_invite, { y: 51 }, function(){
            au.play('button'+m.randomInt(1,2), 1);
            facebook.inviteFriend(this.data.id);
        });
        
        var l2inv = new button(this.children.l_2_invite, { y: 51 }, function(){
            au.play('button'+m.randomInt(1,2), 1);
            facebook.inviteFriend(this.data.id);
        });
        
        var l3inv = new button(this.children.l_3_invite, { y: 51 }, function(){
            au.play('button'+m.randomInt(1,2), 1);
            facebook.inviteFriend(this.data.id);
        });
        
        this.inviteButtons.push(l1inv);
        this.inviteButtons.push(l2inv);
        this.inviteButtons.push(l3inv);
		
		//slider
		this.slider = new snapslider(_t.children.panel_container, 'x');
		this.slider.setSnapping(365 + 100, 4);
		//setup change complete handler
		this.slider.setupChangeCompleteHandler(function(){
			_t.children.pippet_active.x = 28 * this.currentSlideIndex;
			_t.showTab('achievements', this.currentSlideIndex, true);
			
			//buttons
			_t.children.arrow_left.alpha = this.atStart() ? 0.5 : 1;
			_t.children.arrow_right.alpha = this.atEnd() ? 0.5 : 1;
		});
	},
    inviteButtons: [],
    showLoader: function(){
        var loader = this.children.leaderboard_load;
        loader.visible = true;
        app.addUpdateListener('lslbloader', function(){
            loader.rotation = m.angleToRadians(m.radiansToAngle(loader.rotation) + 5);
        });
    },
    hideLoader: function(){
        this.children.leaderboard_load.visible = false;
        app.removeUpdateListener('lslbloader');
    },
	activeTab: { type: '', level:'' },
	showTab: function(type, level, animate){
		if(this.activeTab.type == type && this.activeTab.level == level) return;
		//vars
		var info = levels[level], c = this.children, completeFunc, _t = this;
        this.cancelLoadingScores();
        this.activeTab.type = type;
		this.activeTab.level = level;
        
		//buttons
		switch(type){
			case 'achievements':
				this.children.leaderboard_idle.visible = true;
				this.children.leaderboard_selected.visible = false;
				this.children.achievements_idle.visible = false;
				this.children.achievements_selected.visible = true;
                
				//boom
				completeFunc = function(){
                    for( var i =0, l = c.icon_cont.children.length; i < l; i++ ){
                        c.icon_cont.removeChildAt(0);   
                    }
                    
					//animate the panel in
					c.achievements_container.visible = true;
					c.achievements_container.alpha = animate ? 0 : 1;
                    
					if(animate) {
						d.animateObject(c.achievements_container, .1, { alpha:1 });
					}
					if(info.locked) {
						c.achievement_details.visible = false;
						c.achievements_locked.visible = true;
					} else {
						c.achievement_details.visible = true;
						c.achievements_locked.visible = false;
						//show details
						var iconLayout = [];
                        for(var i = 0; i < info.achievements.length; i++ ){
							var a =  info.achievements[i];

                            c['achievement_'+ (i+1) +'_title'].setText(a.title).centerText(false, false);
							c['checkbox_tick_'+ (i+1)].visible = a.completed;
                            if(a.data && a.data.icon) {
                                iconLayout.push({ x: c['achievement_'+ (i+1) +'_title'].textWidth+15, y: 3 + (29*i), type:'sprite', id:'a_' + a.data.icon });
                            }
						}
                        d.buildLayout(iconLayout, c.icon_cont);
					}
				}
			break;
			case 'leaderboard':
				this.children.leaderboard_idle.visible = false;
				this.children.leaderboard_selected.visible = true;
				this.children.achievements_idle.visible = true;
				this.children.achievements_selected.visible = false;
				//boom
				completeFunc = function(){
                    // if(!facebook.loggedin) {
                        _t.activeTab.type = 'connect';
                         //animate the panel in
                        c.notconnected_cont.visible = true;
                        c.notconnected_cont.alpha = 0;
                        d.animateObject(c.notconnected_cont, .1, { alpha:1 });
                    // } else {
                    //     //animate the panel in
                    //     c.leaderboard_container.visible = true;
                    //     c.leaderboard_container.alpha = 0;
                    //     d.animateObject(c.leaderboard_container, .1, { alpha:1 });


                    //     c.l_1.visible = c.l_2.visible = c.l_3.visible = false;
                    //     _t.showLoader();

                    //     _t.loadScores(_t.slider.currentSlideIndex+1, true);
                    // }
				}
			break;	
		}
		//fade out current panel
		var itm = c.leaderboard_container.visible ? c.leaderboard_container : c.achievements_container;
        if(c.notconnected_cont.visible) itm = c.notconnected_cont;
        
		if(animate) {
			d.animateObject(itm, .1, { alpha:0, onComplete: function(){
				itm.visible = false;
				completeFunc();
			}});
		} else {
			itm.alpha = 0;
			itm.visible = false;
			completeFunc();
		}
		//set active
		
	},
    shouldShowScores: false,
    scoreLoadAjax: null,
    loadScores: function(level, populate){
        this.shouldShowScores = true;
        var _t = this, c = this.children;
        // if(!this.hasLoadedScores) {
        //     this.scoreLoadAjax = v.get('leaderboard').loadFacebookFriendScores(function(scores){
        //         _t.hasLoadedScores = true;
        //         _t.scores = scores;
                
        //         if(populate) _t.populateScores(scores, level);
        //     });
        // } else {
           if(populate) _t.populateScores(_t.scores, level); 
        // }
    },
    populateScores: function(scores, level){
        var _t = this, c = this.children;
        //this returns here just incase the ajax success is called even after its been cancelled
        if(!_t.shouldShowScores) return;
        
        var scoresname = 'player_score_level' + level;
        
        //clear images from the imgwrap containers
        for( var i = 1; i < 4; i++ ) {
            if(c['l_' + i + '_imgwrap'].children.length != 0) c['l_' + i + '_imgwrap'].removeChildAt(0);
        }
        
        //ordered by the level
        scores.sort(function(a, b){
            var x = parseInt(a[scoresname]); var y = parseInt(b[scoresname]);
            return ((x > y) ? -1 : ((x < y) ? 1 : 0));
        });
        
        //setup
        var imgs = [];
        var layout = [];
        //get imgs
        for( var i = 0; i < (scores.length > 3 ? 3 : scores.length); i++ ){
            var p = scores[i];
            if(p[scoresname] == 0) break;
            
            var imgObj = { name:'bg', type:'sprite', id:'lbprofilepic_empty', parent:c['l_'+(i+1)+'_imgwrap'], x: 1, y: 1 };
            
            var img = p.player_image;
            if(img !== null && img != 'none') {
                var ref = img.substr(img.length-30, 30);
                
                //not found before
                if(!facebook.images[ref]) {
                    facebook.images[ ref ] = true;
                    imgs.push({id:'friends_img_'+ref, src: img.replace('http:', document.location.protocol)});
                }
                
                //set to img
                imgObj.id = 'friends_img_'+ref;
                imgObj.type = 'bitmap';
                imgObj.s = 49/60;
                imgObj.x = imgObj.y = 0;
            }
            //layout
            layout.push(imgObj);
            
            c['l_'+(i+1)+'_name'].setText(p.player_full_name).centerText(true, false).visible = true;
            c['l_'+(i+1)+'_score'].setText(p[scoresname] + "m").centerText(true, false).visible = true;

            c['l_'+(i+1)+'_pipet_title'].visible = true;
            c['l_'+(i+1)+'_pipet'].visible = true;

            c['l_'+(i+1)+'_invite'].visible = false;
            _t.inviteButtons[i].disable();
        }
        
        var inviteFriends = _.sample(facebook.friends, 5);        
        for( var j = 0; j < inviteFriends.length; j++ ) {
            var fri = inviteFriends[j];
            if(fri.installed) continue;
            
            if(i > 2) break;
            
            var imgObj = { name:'bg', type:'sprite', id:'lbprofilepic_empty', parent:c['l_'+(i+1)+'_imgwrap'], x: 1, y: 1 };
            
            var img = fri.img;
            if(img !== null && img != 'none') {
                var ref = img.substr(img.length-30, 30);
                
                //not found before
                if(!facebook.images[ref]) {
                    facebook.images[ ref ] = true;
                    imgs.push({id:'friends_img_'+ref, src: img.replace('http:', document.location.protocol)});
                }
                
                //set to img
                imgObj.id = 'friends_img_'+ref;
                imgObj.type = 'bitmap';
                imgObj.s = 59/60;
                imgObj.x = imgObj.y = 0;
            }
            //layout
            layout.push(imgObj);

            c['l_'+(i+1)+'_name'].visible = false;
            c['l_'+(i+1)+'_score'].visible = false;

            c['l_'+(i+1)+'_pipet_title'].visible = false;
            c['l_'+(i+1)+'_pipet'].visible = false;

            //invite button
            c['l_'+(i+1)+'_invite'].visible = true;
            _t.inviteButtons[i].enable();
            _t.inviteButtons[i].data.id = fri.id;
            
            i++;
        }
        
        //load images here
        if(imgs.length > 0) {
            a.clearManifest();
            a.addToManifest(imgs);
            a.loadManifest(function(){}, function(){});
        }
        
        //build layout
        d.buildLayout(layout, this.container);
        
        c.l_1.visible = c.l_2.visible = c.l_3.visible = true;
        _t.hideLoader();
    },
    cancelLoadingScores: function(){
        this.shouldShowScores = false;
        if(this.scoreLoadAjax) {
            this.scoreLoadAjax.abort();
            this.scoreLoadAjax = null;
        }
        this.hideLoader();
    },
	transitionIn: function() {
        var _t = this;
        console.log('here - ..');
        //facebook.checkLogin(function(){ 
            if(v.get('loaduser').loaded) {
                _t.transitionInAnimation();
            } else {
                console.log('here - loaduser');
                v.get('loaduser').transitionIn();

            };
        //}, function() {
            _t.transitionInAnimation();
        //}, false);    

    },
    playButtonTimer: null,
    transitionInAnimation: function(){
        
		if(_.size(this.children)==0) this.buildView();
		//vars
		var _t = this, c = this.children, vp = app.viewPort;
		
		//creat level panels
		for( var i = 0; i < levels.length; i++ ){
			var panel = new levelSelectPanel();
			panel.build(i);
			panel.container.position.x = (365 + 100) * i;
            this.children.panel_container.addChild(panel.container);
			this.panels.push(panel);
		}
		
		this.activeTab.type = '';
		this.showTab('achievements', 0, false);
        this.slider.gotoSlide(0);
		this.children.arrow_left.alpha = 0.5;
        
        //animate
        var animatePlayButton = function(){
            if(_t.panels.length == 0) return;
            if(_t.panels[_t.slider.currentSlideIndex].children.play_btn) d.animateObject(_t.panels[_t.slider.currentSlideIndex].children.play_btn, .2, { s:1.1, yoyo:true, repeat:3 });
        }
        setTimeout(function(){
            this.playButtonTimer = setInterval(function(){
                animatePlayButton();
            }, 3000);
            animatePlayButton();
        }, 100);
		//enable / show
		this.enable();
		this.show();
        
        this.container.alpha = 0;
        d.animateObject(this.container, .3, { alpha: 1});
		
		v.get('menuhud').toggleCardVisibility(false);
        
        app.addResizeListener('levelselect', function(){
           _t.resize(); 
        })
        
        this.hasLoadedScores = false;
        this.scores = [];
        
        //if(facebook.loggedin) this.loadScores(0, false);
        this.loadScores(0, false);
	},
	transitionOut: function(complete) {
        
        clearInterval(this.playButtonTimer);
		//hide disable
        var _t = this;
        d.animateObject(this.container, .3, { alpha: 0, onComplete:function(){
            _t.hide();
            //destroy panels
            for( var i = 0; i < levels.length; i++ ) {
                var p = _t.panels[0];
                //remove the panel
                _t.children.panel_container.removeChild(p.container);
                //remove it from array
                _t.panels.splice(0, 1);
            }
            //clear array
            _t.panels = [];
            //done
            if(complete) complete();
        }});
        
		this.disable();
        this.hideLoader();
        app.removeResizeListener('levelselect');
	},
    resize: function(){
        var g = app.guiSize, vp = app.viewPort;
        
        var padding = vp.width * 0.015;
        
        this.children.arrow_right.position.x = vp.right - padding;
        this.children.arrow_left.position.x = vp.left + padding;
        
        this.children.home_btn.position.x = vp.left + 10;
			  
    },
	enable:function(){
		for( var i = 0; i < this.panels.length; i++) {
			this.panels[i].enable();	
		}
		this.slider.enable();
		this.right_arrow_btn.enable();
		this.left_arrow_btn.enable();
		this.pippet_btn.enable();
		this.leaderboard_btn.enable();
		this.achievements_btn.enable();
		this.home_btn.enable();
        this.friendpanelfbconnect_btn.enable();
	},
	disable:function(){
		for( var i = 0; i < this.panels.length; i++) {
			this.panels[i].disable();	
		}
		this.slider.disable();
		this.right_arrow_btn.disable();
		this.left_arrow_btn.disable();
		this.pippet_btn.disable();
		this.leaderboard_btn.disable();
		this.achievements_btn.disable();
		this.home_btn.disable();
        this.friendpanelfbconnect_btn.disable();
	},
	gotoLevel: function(id) {
		//go away
		v.each(['menuhud', 'leaderboard'], 'transitionOut');
        this.transitionOut(function(){
            //set the level that is being played
            game.setLevel( id );
            //transition the game in
		    v.get('ingame').transitionIn();
        });
	}
});
var levelSelectPanel = koko.view({
	info: null,
	build: function(level) {
		//get level definition
		this.info = levels[level];
		
		//layour
		var layout = [
			{ name:'preview', type:'sprite', id:'preview_' + (this.info.id+1), x: 21, y: 47 }	
		];
		
		///main frame
		if(!this.info.locked) {
			layout = layout.concat([
				{ name:'frame_unlocked', type:'sprite', id:'frame_unlocked' },
					{ name:'play_btn', type:'sprite', id:'play_btn', x:188, y:115, regX:30, regY:39 },
				{ name:'pb_txt', type:'bmptext', text:Math.floor(this.info.highscore * CONST.PIX_M_MULT)+"m", font:'32px ARMTB', x: 180, y: 185, tint:0xFFFFFF },
			]);
		} else {
			layout = layout.concat([
				{ name:'frame_locked', type:'sprite', id:'frame_locked' },
					{ name:'lock', type:'sprite', id:'lock', x:158, y:123-25 },
			]);
		}
		
		//title text
		layout = layout.concat([
			{ name:'title_shadow', type:'bmptext', text:this.info.name, font:'32px ARMTB', x: 182, y: 12, tint:0x333333, alpha:0.5 },
			{ name:'title', type:'bmptext', text:this.info.name, font:'32px ARMTB', x: 180, y: 10, tint:0xFFFFFF },
		]);
		//stars
		var star1 = this.info.achievements[0].completed ? 'on' : 'off';
		var star2 = this.info.achievements[1].completed ? 'on' : 'off';
		var star3 = this.info.achievements[2].completed ? 'on' : 'off';
		//add to build
		layout = layout.concat([
			{ name:'one_star', type:'sprite', id:'one_star_'+star1, scale:.78, x:112, y:215 },
			{ name:'two_star', type:'sprite', id:'two_star_'+star2,  scale:.78, x:160, y:215 },
			{ name:'three_star', type:'sprite', id:'three_star_'+star3,  scale:.78, x:208, y:215 },
		]);
				
		//vars
		this.container.pivot.x = 184;
		this.container.hitArea = new PIXI.Rectangle(0, 0, 365, 335);
		
		//build
		this.children = d.buildLayout(layout, this.container);
		
		if(!this.info.locked) {
			this.play_btn = new button(this.children.play_btn, { y:115-5 }, function(){
				au.play('button'+m.randomInt(1,2), 1);
                v.get('levelselect').gotoLevel(this.data.id);
                ga('send', 'event', 'Game', 'Action - Play Level ' + (this.data.id+1));
			});
			this.play_btn.data.id = this.info.id;
		}
		
		//update pivot
		this.children.title_shadow.centerText(true, false);
        this.children.title.centerText(true, false);
		if(!this.info.locked) {
			this.children.pb_txt.centerText(true, false);
		}
        
        //show this
        this.show();
	},
	enable: function(){
		if(this.play_btn) this.play_btn.enable();	
	},
	disable: function(){
		if(this.play_btn) this.play_btn.disable();	
	}
});