var GameOverView = koko.view({
	assets: [ { id:'gameover_json', src:'img/gameover.json' },
                { id:'gameover_json2', src:'img/gameOverIcons.json' }
            ],

    gameOverHeightReached:null,

	buildView: function(){
        var vp = app.viewPort;
		
		this.openedNewLevel = -1;
		var diff = koko.client.system.isMobile ? 10 : 40;
		
		this.children = d.buildLayout([
			
            
			{ name:'overlay', type:'sprite', id:'pop-up_dark_overlay.png', x:0, y:0, visible:true },
            { name:'panel', type:'sprite', id:'gameover_panel.png', x:0, y:0, visible:true },
            { name:'continue', type:'sprite', id:'gameover_continue_button.png', y: 0, x: 0 },
            { name:'messageTitle', type:'bmptext', text:"Brand Message", font:'32px ARMTB', x:0, y:0, tint:0x4d4d4d, align:'center', visible:false},
            { name:'messageText', type:'bmptext', text:"Lorem ipsum dolor sit amet",font:'16px ARMTB', x:0, y:0, tint:0x4d4d4d, align:'center', maxWidth:50, wordWrap:true, wordWrapWidth:50, visible:false},

            
            { name:'scorepanel', type:'sprite', id:'scorepanel', x:-148, y:75-diff, visible:false },
			{ name:'npb', type:'sprite', id:'npb', x:483-330, y:179, visible:false, regX:52, regY:52 },
			{ name:'nlu', type:'sprite', id:'nlu', x:483-330, y:179, visible:false, regX:52, regY:52 },
			{ name:'enterdraw_btn', type:'sprite', id:'enterdraw_btn', x:182-330, y:323-diff-10, visible:false },
            
			{ name:'gostar1_empty', type:'sprite', id:'gostar1_empty', x:204-330, y:59-diff, visible:false },
            { name:'ach_1_title', type:'bmptext', text:"Reach\n500m", font:'16px ARMTB', x:246-330, y:103-diff, tint:0x868686, align:'center', visible:false},
            { name:'gostar1_full', type:'sprite', id:'gostar1_full', x:247-330, y:100-diff, regX:43, regY:41, s:0, visible:false  },
            { name:'ach_1_title_above', type:'bmptext', text:"Reach\n500m", font:'16px ARMTB', x:246-330, y:103-diff, tint:0xcc6600, align:'center', alpha:0, visible:false},
            
			{ name:'gostar2_empty', type:'sprite', id:'gostar2_empty', x:286-330, y:41-diff, visible:false },
            { name:'ach_2_title', type:'bmptext', text:"Reach\n500m", font:'16px ARMTB', x:328-330, y:86-diff, tint:0x868686, align:'center', visible:false },
			{ name:'gostar2_full', type:'sprite', id:'gostar2_full', x:329-330, y:82-diff, regX:43, regY:41, s:0, visible:false  },
            { name:'ach_2_title_above', type:'bmptext', text:"Reach\n500m", font:'16px ARMTB', x:328-330, y:86-diff, tint:0xcc6600, align:'center', alpha:0, visible:false},
            
			{ name:'gostar3_empty', type:'sprite', id:'gostar3_empty', x:368-330, y:59-diff, visible:false },
            { name:'ach_3_title', type:'bmptext', text:"Reach\n500m", font:'16px ARMTB', x:412-330, y:103-diff, tint:0x868686, align:'center', visible:false },
			{ name:'gostar3_full', type:'sprite', id:'gostar3_full', x:411-330, y:100-diff, regX:43, regY:41, s:0, visible:false },
            { name:'ach_3_title_above', type:'bmptext', text:"Reach\n500m", font:'16px ARMTB', x:412-330, y:103-diff, tint:0xcc6600, align:'center', alpha:0, visible:false},
            
            { name: 'icon_cont_0', type:'container', x:246-330-3, y:101+2-diff, s:0.8, visible:false },
            { name: 'icon_cont_1', type:'container', x:328-330-3, y:83+2-diff, s:0.8, visible:false },
            { name: 'icon_cont_2', type:'container', x:410-330-3, y:101+2-diff, s:0.8, visible:false },
            
            { name:'score_txt', type:'bmptext', text:"", font:'80px ARMTB_LARGE', x:327-330, y:140-diff, tint:0x333333, visible:false },
			{ name:'gplus_btn', type:'sprite', id:'gplus_btn', x:364-330, y:223-diff, visible:false },
			{ name:'twitter_btn', type:'sprite', id:'twitter_btn', x:300-330, y:223-diff, visible:false },
			{ name:'fb_btn', type:'sprite', id:'fb_btn', x:236-330, y:223-diff, visible:false },
            
            { name:'levelselect_btn', type:'sprite', id:'levelselect_btn', x:186-330, y:484-diff-10, visible:false },
			{ name:'playagain_btn', type:'sprite', id:'playagain_btn', x:287-330, y:484-diff-10, visible:false },
			{ name:'nextlevel_btn', type:'sprite', id:'nextlevel_btn', x:388-330+45, y:484+44-diff-10,  regX:45, regY:44, visible:false },
            { name:'nextlevel_lock', type:'sprite', id:'nextlevel_lock', x: 445-330, y: 475-diff-10, visible:false },
            { name:'gohome_btn', type:'sprite', id:'gohome_btn', x:388-330+45, y:484+44-diff-10,  regX:45, regY:44, visible:false },
            
			
			{name:'goc_findoutmore_btn', type:'sprite', id:'goc_findoutmore_btn', x: -185*.78, y: 580-diff-10, visible:false}
            
		], this.container);
            
		var relativeUnit = app.viewPort.height/100;

        this.children.panel.anchor = new PIXI.Point(0.5, 0.5);
        this.children.continue.anchor = new PIXI.Point(0.5, 0.5);
        this.children.messageTitle.anchor = new PIXI.Point(0.5, 0.5);
        this.children.messageText.anchor = new PIXI.Point(0.5, 0.5);

        this.children.panel.scale.x = 0.55;
        this.children.panel.scale.y = 0.55;
        this.children.continue.scale.x = 0.55;
        this.children.continue.scale.y = 0.55;

		this.children.panel.position.y = vp.centerY - relativeUnit*3;
        this.children.panel.position.x = vp.centerX - relativeUnit*35;
        this.children.overlay.position.y -= 50;
        this.children.overlay.position.x -= 300;
        this.children.continue.position.y = vp.centerY + relativeUnit*32;
        this.children.continue.position.x = vp.centerX - relativeUnit*35;
        this.children.messageTitle.position.y = vp.centerY - relativeUnit*23;
        this.children.messageTitle.position.x = vp.centerX - relativeUnit*50;
        this.children.messageText.position.y = vp.centerY - relativeUnit*15;
        this.children.messageText.position.x = vp.centerX - relativeUnit*100;

        this.children.messageText.position.x = vp.centerX - relativeUnit*100;

        this.children.goc_findoutmore_btn.scale.x = this.children.goc_findoutmore_btn.scale.y = 0.78;
        
        this.container.position.x = vp.centerX;
        this.container.position.y = 25;
        
        var _t = this;

        var yUp = this.children.continue.position.y-2.5;
        this.continueButton = new button(this.children.continue, {y:yUp}, function(){
            au.play('button'+m.randomInt(1,2), 1);
            //step 3
            _t.transitionOut(function(){
                v.get('congratulations').transitionIn(_t.gameOverHeightReached);
            });
        });
	},	
       
	transitionIn: function(heightReached){
        //step 2
        this.gameOverHeightReached = heightReached;
        if(_.size(this.children)==0) this.buildView();
        var _t = this;
		this.show();
        this.enable();
        this.container.alpha = 1;
        console.log(this.children);
        //console.log("heightReached: ", heightReached)
        

	},
	transitionOut: function(complete){
        var _t = this;
        d.animateObject(this.container, .2, { alpha:0, onComplete:function(){
            _t.hide();
            if(complete) complete();
        }});
		this.disable();
	},
	enable: function(){
        this.continueButton.enable();
	}, 
	disable:function(){
        this.continueButton.disable();
	}
});