var WinPrizesView = koko.view({
    assets: [{ id:'winprizes_json', src:'img/prizedetails.json?v=4' }],
    hideEnterNow: false,
    like:null,
    buildView: function(){
        var vp = app.viewPort, g = app.guiSize;
        
        this.like = document.getElementById('fb-like-prizes');
        

        this.children = d.buildLayout([
            { name:'bg', type:'shape', alpha:0.5, w: g.width, h:g.height },
                { name:'con', type:'container', regX:241, x: vp.centerX, y: 15 },
                { name:'con_bg', type:'sprite', id:'pu_panel', parent:'con' },
            
            { name:'stage_1', type:'container', parent:'con' },
            
            { name:'stage_2', type:'container', parent:'con', visibility:false, x:5, y: 20 },
            { name:'stage_3', type:'container', parent:'con', visibility:false, x:5, y: 20 },
                { name:'close_btn', type:'sprite', id:'puclose_btn', parent:'con', x:429, y: 5 },
                
            { name:'s_1_bg', type:'sprite', id:'prizedetailsimage', parent:'stage_1', x: 20, y:20 },
            { name:'s_1_enter', type:'sprite', id:'enterhere_btn', parent:'stage_1', x: 25, y: 480, visible:!this.hideEnterNow } ,
            { name:'s_1_play', type:'sprite', id:'popupPlayNow_btn', parent:'stage_1', x: 25, y: 480, visible:this.hideEnterNow } ,
            { name:'terms_1_hit', type:'shape', w:130, h:25, x: 17, y: 437, fill:'#000000', parent:'stage_1', alpha:0 },
			
			{ name:'s_2_bg', type:'sprite', id:'formcontent', parent:'stage_2', x: 20, y:20 }, 
            { name:'s_2_submit', type:'sprite', id:'submitdetails_btn', parent:'stage_2', x: 20, y: 460 },
            { name:'terms_2_hit', type:'shape', w:1, h:1, x: 45, y: 345, fill:'#000000', parent:'stage_2', alpha:0 },
            
            { name: 'terms_opt_toggle', type:'container', parent:'stage_2', x: 22, y: 335, s:0.8 },
                { name: 'on', type:'sprite', id:'check_on', parent:'terms_opt_toggle', visible:false },
                { name: 'off', type:'sprite', id:'check_off', parent:'terms_opt_toggle' },
            { name:'terms_err', type:'sprite', id:'error_asterix', parent:'stage_2', x: 20, y:330, s:0.7 },
            
            { name:'fname_txt', type:'bmptext', font:'28px ARMTB', text:'First name', parent:'stage_2', x:30, y: 81, tint:0xbababa },
                { name:'fname_err', type:'sprite', id:'error_asterix', parent:'stage_2', x: 20, y: 70, s:0.7 },
            { name:'sname_txt', type:'bmptext', font:'28px ARMTB', text:'Surname', parent:'stage_2', x:30, y: 144, tint:0xbababa },
                { name:'sname_err', type:'sprite', id:'error_asterix', parent:'stage_2', x: 20, y: 135, s:0.7 },
            
           // { name:'firstline_txt', type:'bmptext', font:'28px ARMTB', text:'First line of address', parent:'stage_2', x:30, y: 130, tint:0xbababa },
            //    { name:'firstline_err', type:'sprite', id:'error_asterix', parent:'stage_2', x: 20, y: 120, s:0.7 },
            //{ name:'secondline_txt', type:'bmptext', font:'28px ARMTB', text:'Second line of address', parent:'stage_2', x:30, y: 183, tint:0xbababa },
            
            //{ name:'town_txt', type:'bmptext', font:'28px ARMTB', text:'Town / City', parent:'stage_2', x:30, y: 236, tint:0xbababa },
             //   { name:'town_err', type:'sprite', id:'error_asterix', parent:'stage_2', x: 20, y: 226, s:0.7 },
            //{ name:'postcode_txt', type:'bmptext', font:'28px ARMTB', text:'Postcode', parent:'stage_2', x:245, y: 236, tint:0xbababa },
              //  { name:'postcode_err', type:'sprite', id:'error_asterix', parent:'stage_2', x: 235, y: 226, s:0.7 },
            
            { name:'emailaddress_txt', type:'bmptext', font:'28px ARMTB', text:'Email address', parent:'stage_2', x:30, y: 207, tint:0xbababa },
                { name:'emailaddress_err', type:'sprite', id:'error_asterix', parent:'stage_2', x: 20, y: 200, s:0.7 },
            { name:'contactnumber_txt', type:'bmptext', font:'28px ARMTB', text:'Contact Number', parent:'stage_2', x:30, y: 273, tint:0xbababa },
                { name:'contactnumber_err', type:'sprite', id:'error_asterix', parent:'stage_2', x: 20, y:263, s:0.7 }, 
            
            //{ name:'likeButton', type:'sprite', id:'pd_like_button', parent:'stage_2', x: 20, y:400, s:0.7 }, 
            { name:'likeCopy', type:'sprite', id:'pd_like_copy', parent:'stage_2', x: 110, y:402, s:0.7},
            
            { name:'s_3_bg', type:'sprite', id:'thankyoutext', parent:'stage_3', x: 20, y:20 }, 
            
            { name:'s_3_virgincta_btn', type:'sprite', id:'virgincta_btn', parent:'stage_3', x: 20, y: 260 },
            { name:'s_3_invitecta_btn', type:'sprite', id:'invitecta_btn', parent:'stage_3', x: 20, y: 360 },
            { name:'s_3_playagaincta_btn', type:'sprite', id:'playagaincta_btn', parent:'stage_3', x: 20, y: 460 },
        ], this.container);
        
       
        var _t = this;
        this.close_btn = new button(this.children.close_btn, { y: 2 }, function(){
            au.play('button'+m.randomInt(1,2), 1);
            _t.transitionOut();
        });
        
        this.s_1_enter_btn = new button(this.children.s_1_play, { y: 477 }, function(){
           au.play('button'+m.randomInt(1,2), 1);
              _t.gotoStage(2); 
        }); 
        
        this.s_1_play_btn = new button(this.children.s_1_play, { y: 477 }, function(){
           au.play('button'+m.randomInt(1,2), 1);
        _t.playAgain(); 
          
        });
        
         /*this.like_btn = new button(this.children.likeButton, { y: 397 }, function(){
           au.play('button'+m.randomInt(1,2), 1);
        _t.playAgain(); 
          
        });*/
      
        this.children.terms_1_hit.hitArea = new PIXI.Rectangle(0, 0, 130, 25);
        this.terms_1_hit_btn = new button(this.children.terms_1_hit, null, function(e){
            au.play('button'+m.randomInt(1,2), 1);
            app.openLink(app.url + 'api/link/terms', 'link', e);
        });
        
		
        this.children.terms_2_hit.hitArea = new PIXI.Rectangle(0, 0, 170, 25);
        this.terms_2_hit_btn = new button(this.children.terms_2_hit, null, function(e){
            au.play('button'+m.randomInt(1,2), 1);
            app.openLink(app.url + 'api/link/terms', 'link', e);
        });
            
            
        this.s_2_submit_btn = new button(this.children.s_2_submit, { y: 457 }, function(){
           au.play('button'+m.randomInt(1,2), 1);
            _t.submitForm(); 
        });
            
        this.terms_opt = new toggle(this.children.terms_opt_toggle, 0, function(on){});
		this.terms_opt.setToggle(false);
        
        this.s_3_virgincta_btn = new button(this.children.s_3_virgincta_btn, { y: 257 }, function(e){
            au.play('button'+m.randomInt(1,2), 1);
            app.openLink(app.url + 'api/link/client0', 'link', e);    
        });
        this.s_3_invitecta_btn = new button(this.children.s_3_invitecta_btn, { y: 357 }, function(){
            au.play('button'+m.randomInt(1,2), 1);
            facebook.checkLogin(function(){ 
				facebook.inviteFriends('');  
			}, function() {
                v.get('connect').transitionIn(function(){
                    facebook.inviteFriends('');  
                });
            }, false);
        });
        this.s_3_playagaincta_btn = new button(this.children.s_3_playagaincta_btn, { y: 457 }, function(){
            au.play('button'+m.randomInt(1,2), 1);
            _t.playAgain();    
        });
        
            
        this.fname_input = new input(this.children.fname_txt, 400, 30, -10, 0);
        this.fname_input.setColors(0xbababa, 0x333333).setDesktopClasses('winPrizesInput', 'idle', 'active').setDisplayLength(23, "...");
            
        this.sname_input = new input(this.children.sname_txt, 400, 30, -10, 0);
        this.sname_input.setColors(0xbababa, 0x333333).setDesktopClasses('winPrizesInput', 'idle', 'active').setDisplayLength(23, "...");
    
        //this.firstline_input = new input(this.children.firstline_txt, 400, 30, -10, -3);
        //this.firstline_input.setColors(0xbababa, 0x333333).setDesktopClasses('winPrizesInput', 'idle', 'active').setDisplayLength(23, "...");
        
        //this.secondline_input = new input(this.children.secondline_txt, 400, 30, -10, -3);
        //this.secondline_input.setColors(0xbababa, 0x333333).setDesktopClasses('winPrizesInput', 'idle', 'active').setDisplayLength(23, "...");
            
        //this.town_input = new input(this.children.town_txt, 185, 30, -10, -3);
        //this.town_input.setColors(0xbababa, 0x333333).setDesktopClasses('winPrizesInput', 'idle', 'active').setDisplayLength(23, "...");
            
        //this.pcode_input = new input(this.children.postcode_txt, 185, 30, -10, -3);
        //this.pcode_input.setColors(0xbababa, 0x333333).setDesktopClasses('winPrizesInput', 'idle', 'active').setDisplayLength(23, "...");
            
        this.email_input = new input(this.children.emailaddress_txt, 400, 30, -10, 0);
        this.email_input.setColors(0xbababa, 0x333333).setDesktopClasses('winPrizesInput', 'idle', 'active').setDisplayLength(23, "...");
        
        this.contact_input = new input(this.children.contactnumber_txt, 400, 30, -10, 0);
        this.contact_input.setColors(0xbababa, 0x333333).setDesktopClasses('winPrizesInput', 'idle', 'active').setDisplayLength(23, "...");
        
         
    },
    submitForm: function(){
        /*this.gotoStage(3);
    },
    realSubmitForm: function(){*/
        var _t = this;
        
        var fname = this.fname_input.val;
        var sname = this.sname_input.val;
        //var firstline = this.firstline_input.val;
       // var secondline = this.secondline_input.val;
       // var town = this.town_input.val;
        //var pcode = this.pcode_input.val;
        var email = this.email_input.val;
        var contact = this.contact_input.val;
    
        var c = this.children;
        var errors = [c.fname_err, c.sname_err, c.contactnumber_err, c.terms_err];
        for( var i = 0; i < errors.length; i++ ){
            errors[i].visible = false;   
        }
        
        //name
        c.fname_err.visible = !f.withinLength(fname, 3, 30);
        c.sname_err.visible = !f.withinLength(sname, 3, 30);
        //address
        //c.firstline_err.visible = !f.withinLength(firstline, 3, 50);
        //town
        //c.town_err.visible = !f.withinLength(town, 2, 40);
        //c.postcode_err.visible = !f.withinLength(pcode, 4, 8);
        //contact
        c.emailaddress_err.visible = !f.validEmail(email);
        c.contactnumber_err.visible = !f.phoneNumber(contact);
        //terms
        c.terms_err.visible = !this.terms_opt.on;
        //check
        var foundError = false;
        for( var i = 0; i < errors.length; i++ ) {
            if(errors[i].visible) foundError = true;   
        }
        
        if(!foundError) {
            
            ga('send', 'event', 'Game', 'Action - Submit Competition Success');
            
            $.ajax({
				dataType:'json', 
				type: 'POST',
				url: app.url + "api/competition", 
				data: JSON.stringify({
					player_forename: fname,
                    player_surname: sname,
                    //player_address1: firstline,
                    //player_address2: secondline,
                    //player_city: town,
                    //player_postcode: pcode,
                    player_email: email,
                    player_telephone: contact
				}), 
				success: function( data ) {
					
					setTimeout(function(){
						if(!koko.client.system.isMobile) {
							facebook.share('http://www.loungeleap.co.uk/');
						}
					}, 1000);
					
					_t.gotoStage(3);
				}
			});
        } else {
            ga('send', 'event', 'Game', 'Action - Submit Competition From Validation Error');   
        }
    },
    gotoStage: function(stage) {
        var _t = this;
        switch(stage) {
            case 1:
                 this.hideLike();
                this.children.stage_1.visible = true;
                this.children.stage_1.alpha = 1;
                this.children.stage_2.visible = false;
                this.children.stage_3.visible = false;
                
                this.hideEnterNow ?  this.s_1_play_btn.enable() : this.s_1_enter_btn.enable();  
                
                this.terms_1_hit_btn.enable();
            break;
            case 2:
                ga('send', 'event', 'Game', 'Action - Competition View Form');
                
                this.s_1_enter_btn.disable();
                this.terms_1_hit_btn.disable();
                this.s_2_submit_btn.enable();
                this.terms_2_hit_btn.enable();
                this.terms_opt.enable();
                this.children.stage_1.visible = false;
                this.children.stage_2.visible = true;
                this.children.stage_3.visible = false;
            
                var inputs = [this.fname_input, this.sname_input, this.email_input, this.contact_input];
                                      
                for( var i =0; i < inputs.length; i++){
                    inputs[i].enable(true);
                }
                
                var resizeTryCount = 0;
                var resizing = setInterval(function(){
                     for( var i =0; i < inputs.length; i++){
                        inputs[i].resize();
                     }
                    resizeTryCount++;
                    if(resizeTryCount > 10) clearInterval(resizing);
                }, 10);
                
                var c = this.children;
                var errors = [c.fname_err, c.sname_err, c.emailaddress_err, c.contactnumber_err, c.terms_err];
                for( var i = 0; i < errors.length; i++ ){
                    errors[i].visible = false;   
                }
            break;
            case 3:
                this.hideLike();
                var inputs = [this.fname_input, this.sname_input, this.email_input, this.contact_input];
                                      
                for( var i =0; i < inputs.length; i++){
                    inputs[i].disable();
                }
                this.s_2_submit_btn.disable();
                this.terms_2_hit_btn.disable();
                this.terms_opt.disable();
                this.children.stage_1.visible = false;
                this.children.stage_2.visible = false;
                this.children.stage_3.visible = true;
                
                this.s_3_virgincta_btn.enable();
                this.s_3_invitecta_btn.enable();
                this.s_3_playagaincta_btn.enable();
            break;
        }
    },
      resize:function(){
        var vp = app.viewPort;
        
        //shazinga
        var fbLikeScale = 1;
        var left = 0;
        var top = vp.top == 0 ? vp.y : vp.top;
		top += (vp.centerY + 135) * vp.s;
		
		if(vp.left > 0) {
			left = (vp.centerX - vp.left - 200) * vp.s
		} else {
			left = (vp.x + vp.centerX - 200) * vp.s
		}
		
        this.like.style[koko.client.cssprops.transform] = "translate("+left+"px,"+top+"px) scale("+vp.s*fbLikeScale+")";
    },
    transitionIn: function(){
        if(_.size(this.children)==0) this.buildView();
       //show/enable
        var _t = this
        this.show();
        this.enable();
        
        //  if(facebook.loggedin) {
        //     $("#facebook_notconnected_like_prizes").addClass('hide');  
        //     $("#facebook_connected_like_prizes").removeClass('hide');  
        // }
        
        this.container.alpha = 0;
        d.animateObject(this.container, .3, { alpha: 1 });
            
        this.children.con.position.y = 35;
        this.children.con.alpha = 0;
		var inputs = [this.fname_input, this.sname_input, this.email_input, this.contact_input];                       
		
        d.animateObject(this.children.con, .3, { alpha: 1, y:15, onComplete:function(){
			
			for( var i =0; i < inputs.length; i++){
				inputs[i].resize();
			}
		} });
            
        ga('send', 'event', 'Game', 'Action - Open Competition');
        
        //disbale other menus
        v.each(['mainmenu', 'charselect', 'gameover', 'menuhud', 'leaderboard'], function(view){
            if(view.container.visible) view.disable();
        });
        
         this.hideEnterNow ?  this.gotoStage(1) : this.gotoStage(2);
		 
         app.addResizeListener('winprizes', function(){ _t.resize(); });
       
    },
    playAgain: function() {
        this.transitionOut();
        
        setTimeout(function(){
            v.each(['mainmenu', 'charselect', 'gameover'], function(view){
                if(view.container.visible) view.transitionOut();
                
                setTimeout(function(){
                    facebook.checkLogin(function() {
                        v.get('charselect').transitionIn();
                    }, function() {
                       v.get('charselect').transitionIn();
                    }, false);
                }, 210);
            });
        }, 210);
    },
    transitionOut: function(){
        //hide/disable
        app.removeResizeListener('winprizes');
        var _t = this;
        
        this.disable();
        d.animateObject(this.container, .2, { alpha:0, onComplete: function(){
            _t.hide();
            
            v.each(['mainmenu', 'charselect', 'gameover', 'menuhud', 'leaderboard'], function(view){
                if(view.container.visible) view.enable();
            });
        }});
        
        //input
        var inputs = [this.fname_input, this.sname_input, this.email_input, this.contact_input];                       
        for( var i =0; i < inputs.length; i++){
            inputs[i].disable();
        }
    },
    hideLike:function()
    {
         this.like.className = this.like.className + ' hide';
    },
    enable: function(){
         this.close_btn.enable();  
        this.like.className = this.like.className.replace(/hide/g, '').replace(/\s{2,}/g, ' ');
    },
    disable: function(){
        this.close_btn.disable()
        this.s_1_enter_btn.disable();
        this.terms_1_hit_btn.disable();
        this.terms_2_hit_btn.disable();
        this.terms_opt.disable();
        this.s_3_virgincta_btn.disable();
        this.s_3_invitecta_btn.disable();
        this.s_3_playagaincta_btn.disable();
        this.s_1_play_btn.disable();
        this.hideLike();
    }
});