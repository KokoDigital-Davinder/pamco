// JavaScript Document
var InGameBackgroundView = koko.view({
	pool: {},
	itms: [],
	objIndex: 0,
	objNames: [],
	buildView: function() {

		var bgScale = 1;
        
        var layout = [];
        var bg = game.level.bg;
        
        layout.push({name:bg.bottom, type:'sprite', id:bg.bottom, visible:false});
        
        for( var i = 0; i < bg.itms.length; i++){
            layout.push({name:bg.itms[i], type:'sprite', id:bg.itms[i], visible:false});
        }
        // layout.push({name:'Menus_backgroundstatic', type:'sprite', id:'Menus_backgroundstatic', visible:false});
         //layout.push({name:'backgroundClouds1', type:'sprite', id:'backgroundClouds1', visible:false});
         //layout.push({name:'backgroundClouds2', type:'sprite', id:'backgroundClouds2', visible:false});
        // layout.push({name:'backgroundClouds3', type:'sprite', id:'backgroundClouds3', visible:false});
        console.log('LAYOUT = ',layout)
        layout.push({name:'wp_cont', type:'container'});
        
		this.pool = d.buildLayout(layout, this.container);

        /*var relativeUnit = app.viewPort.height/100;
        this.children.bg.bottom.scale.x = 0.615;
        this.children.bg.bottom.scale.y = 0.615;*/

		//DAVINDER reposition background here
		this.itms.push({ ref:bg.bottom, y:660 - this.pool[bg.bottom].height});
		
		this.objNames = bg.itms;
        
        this.setupWaypoints();
	},
    setupWaypoints: function() {
        //clear them
        for( var i = 0, l = this.pool.wp_cont.children.length; i < l; i++) {
            this.pool.wp_cont.removeChildAt(0);        
        }
        
        var imgs = [];
        var layout = [];
        
        //start de waypoints
        var scores = v.get('levelselect').scores;
        if(scores.length > 0) {
            for( var i = 0; i < scores.length; i++ ){
                var score = scores[i]['player_score_level' + (game.level.id+1)];
                
                if(score == 0) continue;
                
                var imgObj = { name:'bg', type:'sprite', id:'lbprofilepic_empty', parent:'friend'+i, x: 568, y: 11 };
                var img = scores[i].player_image;
                
                if(img !== null && img != 'none') {
                    var ref = img.substr(img.length-30, 30);

                    //not found before
                    if(!facebook.images[ref]) {
                        facebook.images[ ref ] = true;
                        imgs.push({id:'friends_img_'+ref, src: img.replace('http:', document.location.protocol) });
                    }

                    //set to img
                    imgObj.id = 'friends_img_'+ref;
                    imgObj.type = 'bitmap';
                    imgObj.s = 49/60;
                }

                layout = layout.concat([
                    { name:'friend'+i, type:'container', y: CONST.FLOOR - (score/CONST.PIX_M_MULT), regY: 115 },
                    { name:'bg', type:'sprite', id:'waypoint_bg', parent:'friend'+i },
                    imgObj,
                    { name:'name'+i, type:'bmptext', text:scores[i].player_full_name, font:'14px ARMTB', x: 593, y: 70, tint:0x717073, parent:'friend'+i },
                ]);   
            }
        }
                    
        //loop achievemtns
        for( var i = 0; i < game.level.achievements.length; i++ ){
            var ach = game.level.achievements[i];
            if(ach.title.indexOf('Reach ') == -1 || ach.completed) continue;
            
            var dist = ach.title.replace('Reach ', '');
            dist = parseInt(dist.replace('m', ''));
            
            layout = layout.concat([
                { name:'ach'+i, type:'container', y: CONST.FLOOR - (dist/CONST.PIX_M_MULT),  regY: 115 },
                { name:'bg', type:'sprite', id:'waypoint_bg', parent:'ach'+i, visible:false },
                { name:'star'+ach.id, id:'wp_star_full', type:'sprite', parent:'ach'+i, x: 592, y: 34, regX:27, regY:27, s:0.5, alpha:0, visible:false},
                { name:'achname'+i, type:'bmptext', text:dist+"m", font:'14px ARMTB', x: 593, y: 70, tint:0x717073, parent:'ach'+i, visible:false  }
            ]); 
        }
                    
        //load images here
        if(imgs.length > 0) {
            a.clearManifest();
            a.addToManifest(imgs);
            a.loadManifest(function(){}, function(){});
        }

        //build layout
        var c = d.buildLayout(layout, this.pool.wp_cont);
                    
        for( var i = 0; i < scores.length; i++ ){
            if(c.hasOwnProperty('name'+i)) c['name'+i].centerText(true, false);    
        }
        for( var i = 0; i < game.level.achievements.length; i++ ){
            if(c.hasOwnProperty('achname'+i)) c['achname'+i].centerText(true, false); 
        }
        
        this.wp_children = c;
    },
    unlockDistAch: function(id){
        d.animateObject(this.wp_children['star' + id], .3, { s:1, alpha:1 });
    },
    clear: function(){
        for( var i =0, l = this.container.children.length; i < l; i++) {
            this.container.removeChildAt(0);
        }
        this.pool = {};
        this.itms = [];
        this.objName = [];
        this.objIndex = 0;
    },
	transitionIn: function(){
        this.clear();
		this.buildView();
        this.show();
	},
	update: function(position) {
        
        this.pool.wp_cont.position.y = position*-1;
        
		//check the last one, create new if fell below 
		position *= 0.8;
        
        var ceilingHeight = 610 - ( 610 / game.scale );
		
		var topY = this.itms[this.itms.length-1].y - position;
        
        
		if(topY > ceilingHeight) {
			var nextImgRef = this.objNames[ this.objIndex % this.objNames.length ];
			this.objIndex++;
			this.itms.push({ ref: nextImgRef, y: this.itms[this.itms.length-1].y - (this.pool[nextImgRef].height-5) });
		}
        //console.log(this.itms)
		//look backwards over the itms
		for( var i = this.itms.length-1; i >= 0; i-- ){
			var itm = this.itms[i];
			var img = this.pool[itm.ref];
			//position
			var y = img.position.y = itm.y - position;
			//610=height of document	
			img.visible = y < 650 && (y+img.height) > ceilingHeight;
			//check break loop
			if(y + img.height > 650) {
				break;
			}
		}
		
	},
	transitionOut: function(){ 
		this.hide();
	}
});