var sharegame = function(share,score, isGeneric, e){
                
                var shareurl = 'https://powerupyourplanning.com/?share=1';
                var sharetxt = "I just played PAMCo's Power-Up Your Planning game.Play here... https://bit.ly/2EXKblV";
                console.log(score)
                if(score > 0 && !isGeneric){
                    // add score to shareurl
                    shareurl  += '&player_score='+score;
                    sharetxt = "I've just scored " +score+'m on @PAMCo_Ltd #powerupyourplanning game. Can you do better! https://bit.ly/2EXKblV ';
                }    
    
                switch(share) 
                {
                 case 'facebook':
                    //koko.audio.play('sfx_click', 1, false);
                    FB.ui(
                      {
                        method: 'share',
                        href: shareurl
                      },
                      // callback
                      function(response) {}
                    );
                    //if(score > 0)  ga('send', 'event', 'Share', 'Facebook - Score');
                    //else ga('send', 'event', 'Share', 'Facebook - Generic');
                 break;
                 case 'twitter':  
                    app.openLink('http://twitter.com/intent/tweet?text='+encodeURIComponent(sharetxt), 'link',e);
                    //if(score > 0)  ga('send', 'event', 'Share', 'Twitter - Score');
                    //else ga('send', 'event', 'Share', 'Twitter - Generic');
                 break;

                 case 'google':
                    app.openLink('https://plus.google.com/share?url='+encodeURIComponent(shareurl), 'link',e);
                 break;
                }
}
var CongratulationsView = koko.view({
    assets: [ { id:'char_select_json', src:'img/character_select.json' },
            { id:'char_select2_json', src:'img/congratulationsIcons.json' } 
            ],
    congratulationsHeightReached:"bbblank",
    name_input:null,
    company_input:null,
    email_input:null,

    buildView: function(){
        var vp = app.viewPort;
        
        this.children = d.buildLayout([
            { name:'title', type:'sprite', id:'title_congratulations.png', regX: 141, x: vp.centerX, y: 100 },
            { name: 'panelContainer', type:'container', x:0, y:0, visible:true },
                { name:'panel', type:'sprite', parent:'panelContainer', id:'Submit_panel_background.png', regX: 141, x: vp.centerX, y: 100 },
                { name:'distance', type:'bmptext', parent:'panelContainer', text:this.congratulationsHeightReached,font:'80px ARMTB_LARGE', x:0, y:0, tint:0x4db8ff, align:'center', visible:true},
                //{ name:'privacyPolicy', type:'bmptext', parent:'panelContainer', text:"PRIVACY POLICY",font:'16px ARMTB', x:0, y:0, tint:0x4d4d4d, align:'center', visible:true},
                { name:'facebookIcon', type:'sprite', parent:'panelContainer', id:'share_facebook_button.png', regX: 141, x: vp.centerX, y: 100 },
                { name:'twitterIcon', type:'sprite', parent:'panelContainer', id:'share_twitter_button.png', regX: 141, x: vp.centerX, y: 100 },
                { name: 'congratulationsPart1', parent:'panelContainer', type:'container', x:0, y:0, visible:true },
                    { name:'submitScore', type:'sprite', id:'titls_submitscore.png', parent:'congratulationsPart1', x:0, y:0 },
                    { name:'submitButton', type:'sprite', id:'submit_button.png', x:0, y:0 },
                    { name:'enterButton', type:'sprite', id:'enter_button.png',  x:0, y:0, visible:false},
                    { name:'textField', type:'sprite', id:'submitform_textfield.png', parent:'congratulationsPart1', x:0, y:0 },
                    { name:'textField2', type:'sprite', id:'submitform_textfield.png', parent:'congratulationsPart1', x:0, y:0 },
                    { name:'textField3', type:'sprite', id:'submitform_textfield.png', parent:'congratulationsPart1', x:0, y:0 },
                    { name:'nameField', type:'bmptext', parent:'panelContainer', text:"NAME",font:'25px ARMTB', x:0, y:0, tint:0xcccccc, align:'left', visible:true},
                    { name:'companyField', type:'bmptext', parent:'panelContainer', text:"COMPANY",font:'25px ARMTB', x:0, y:0, tint:0xcccccc, align:'left', visible:true},
                    { name:'emailField', type:'bmptext', parent:'panelContainer', text:"EMAIL",font:'25px ARMTB', x:0, y:0, tint:0xcccccc, align:'left', visible:true},
                    { name:'termsAndConditions', type:'sprite', id:'terms_text', parent:'panelContainer', x:0, y:0, visible:true},
                    { name:'privacyPolicy', type:'sprite', id:'privacypolicy_text.png', parent:'panelContainer', x:0, y:0, visible:true},
                    { name:'divider', type:'sprite', id:'divide_text.png', parent:'panelContainer', x:0, y:0, visible:true},

                { name: 'congratulationsPart2', parent:'panelContainer', type:'container', x:0, y:0, visible:false },
                    { name:'win50000', type:'sprite', id:'title_win50.png', parent:'congratulationsPart2', x:0, y:0 , visible:true},
                    { name:'textWin', type:'sprite', id:'text_win100.png', parent:'congratulationsPart2', x:0, y:0 , visible:true},
                   
                //{ name:'submitScore', type:'sprite', id:'submitform_textfield.png', parent:'congratulationsPart1', x:0, y:0 },
           { name:'replayButton', type:'sprite', id:'replay_button.png', x: 0, y: 100 },
            { name:'top10Button', type:'sprite', id:'top10_button.png', x: 0, y: 100 },
            { name:'female', type:'sprite', id:'female_selectedNew.png', regX:179, x: vp.centerX-50, y:520, regY:336, visible:false },
            { name:'male', type:'sprite', id:'male_selectedNew.png', x:vp.centerX+50, y: 520, regY:323, visible:false }
        ], this.container);


        var relativeUnit = app.viewPort.height/100;

        this.children.title.anchor = new PIXI.Point(0.5, 0.5);
        this.children.panel.anchor = new PIXI.Point(0.5, 0.5);
        this.children.facebookIcon.anchor = new PIXI.Point(0.5, 0.5);
        this.children.twitterIcon.anchor = new PIXI.Point(0.5, 0.5);
        this.children.textField.anchor = new PIXI.Point(0.5, 0.5);
        this.children.textField2.anchor = new PIXI.Point(0.5, 0.5);
        this.children.textField3.anchor = new PIXI.Point(0.5, 0.5);
        this.children.replayButton.anchor = new PIXI.Point(0.5, 0.5);
        this.children.top10Button.anchor = new PIXI.Point(0.5, 0.5);
        this.children.termsAndConditions.anchor = new PIXI.Point(0.5, 0.5);
        this.children.privacyPolicy.anchor = new PIXI.Point(0.5, 0.5);
        this.children.distance.anchor = new PIXI.Point(0.5, 0.5);
        this.children.nameField.anchor = new PIXI.Point(0.5, 0.5);
        this.children.companyField.anchor = new PIXI.Point(0.5, 0.5);
        this.children.emailField.anchor = new PIXI.Point(0.5, 0.5);
        this.children.win50000.anchor = new PIXI.Point(0.5, 0.5);
        this.children.enterButton.anchor = new PIXI.Point(0.5, 0.5);
        this.children.submitButton.anchor = new PIXI.Point(0.5, 0.5);
        this.children.textWin.anchor = new PIXI.Point(0.5, 0.5);
        this.children.privacyPolicy.anchor = new PIXI.Point(0.5, 0.5);
        this.children.termsAndConditions.anchor = new PIXI.Point(0.5, 0.5);
        this.children.divider.anchor = new PIXI.Point(0.5, 0.5);

       // this.children.panelContainer.scale.y = 0.97;
        this.children.panel.scale.x = 0.50;
        this.children.panel.scale.y = 0.50;
        this.children.twitterIcon.scale.x = 0.50;
        this.children.twitterIcon.scale.y = 0.50;
        this.children.facebookIcon.scale.x = 0.50;
        this.children.facebookIcon.scale.y = 0.50;
        this.children.submitScore.scale.x = 0.50;
        this.children.submitScore.scale.y = 0.50;
        this.children.submitButton.scale.x = 0.45;
        this.children.submitButton.scale.y = 0.45;
        this.children.textField.scale.x = 0.50;
        this.children.textField.scale.y = 0.50;
        this.children.textField2.scale.x = 0.50;
        this.children.textField2.scale.y = 0.50;
        this.children.textField3.scale.x = 0.50;
        this.children.textField3.scale.y = 0.50;
        this.children.replayButton.scale.x = 0.50;
        this.children.replayButton.scale.y = 0.50;
        this.children.replayButton.scale.x = 0.50;
        this.children.replayButton.scale.y = 0.50;
        this.children.top10Button.scale.x = 0.50;
        this.children.top10Button.scale.y = 0.50;
        this.children.win50000.scale.x = 0.50;
        this.children.win50000.scale.y = 0.50;
        this.children.enterButton.scale.x = 0.45;
        this.children.enterButton.scale.y = 0.45;
        this.children.textWin.scale.x = 0.50;
        this.children.textWin.scale.y = 0.50;
        this.children.termsAndConditions.scale.x = 0.50;
        this.children.termsAndConditions.scale.y = 0.50;
        this.children.privacyPolicy.scale.x = 0.50;
        this.children.privacyPolicy.scale.y = 0.50;
        this.children.divider.scale.x = 0.5;
        this.children.divider.scale.y = 0.5;

        this.children.panelContainer.position.y -= 48;
        this.children.title.position.y -= relativeUnit*4;
        //this.children.title.position.x = vp.centerX - relativeUnit*35;
        this.children.panel.position.y = vp.centerY + relativeUnit*7;
        this.children.panel.position.x = vp.centerX + relativeUnit*11;
        this.children.distance.position.y += relativeUnit*26;
        this.children.distance.position.x += relativeUnit*22;
        this.children.facebookIcon.position.y += relativeUnit*29;
        this.children.facebookIcon.position.x += relativeUnit*12 + 15;
        this.children.twitterIcon.position.y += relativeUnit*29;
        this.children.twitterIcon.position.x += relativeUnit*23 + 14;
        this.children.congratulationsPart1.position.y = vp.centerY + relativeUnit*7;
        this.children.congratulationsPart1.position.x = vp.centerX + relativeUnit*12;
            this.children.submitScore.position.y -= relativeUnit*5;
            this.children.submitScore.position.x -= relativeUnit*25;
            this.children.submitButton.position.y += relativeUnit*74.5;
            this.children.submitButton.position.x += relativeUnit*35.5;
            this.children.textField.position.y += relativeUnit*2;
            this.children.textField.position.x -= relativeUnit*11.7;
            //this.children.nameField.position.y += relativeUnit*2;
            //this.children.nameField.position.x -= relativeUnit*11.7;
            this.children.textField2.position.y += relativeUnit*10;
            this.children.textField2.position.x -= relativeUnit*11.7;
            this.children.textField3.position.y += relativeUnit*18;
            this.children.textField3.position.x -= relativeUnit*11.7;
            this.children.termsAndConditions.position.y += relativeUnit*88;
            this.children.termsAndConditions.position.x += relativeUnit*11.7;
            this.children.privacyPolicy.position.y += relativeUnit*88;
            this.children.privacyPolicy.position.x += relativeUnit*40;
        this.children.replayButton.position.y = vp.centerY + relativeUnit*42;
        this.children.replayButton.position.x = vp.centerX - relativeUnit*14;
        this.children.top10Button.position.y = vp.centerY + relativeUnit*42;
        this.children.top10Button.position.x = vp.centerX + relativeUnit*14;

        this.children.congratulationsPart2.position.y = vp.centerY + relativeUnit*7;
        this.children.congratulationsPart2.position.x = vp.centerX + relativeUnit*12;
            

        this.children.female.anchor = new PIXI.Point(0.5, 0.5);
        this.children.male.anchor = new PIXI.Point(0.5, 0.5);

        this.children.title.scale.x = 0.5;
        this.children.title.scale.y = 0.5;
        this.children.female.scale.x = 0.5;
        this.children.female.scale.y = 0.5;
        this.children.male.scale.x = 0.5;
        this.children.male.scale.y = 0.5;

        this.children.title.position.y -=  relativeUnit*2;
        this.children.title.position.x +=  relativeUnit*9;
        this.children.male.position.y +=  relativeUnit*5;
        this.children.male.position.x +=  relativeUnit*6;
        this.children.female.position.y +=  relativeUnit*6;
        this.children.female.position.x +=  relativeUnit*4;

        this.children.nameField.position.y = this.children.replayButton.position.y - relativeUnit*35;
        this.children.nameField.position.x = this.children.replayButton.position.x - relativeUnit*6;
        this.children.companyField.position.y = this.children.replayButton.position.y - relativeUnit*27;
        this.children.companyField.position.x = this.children.replayButton.position.x - relativeUnit*6;
        this.children.emailField.position.y = this.children.replayButton.position.y - relativeUnit*19;
        this.children.emailField.position.x = this.children.replayButton.position.x - relativeUnit*6;

        this.children.win50000.position.y -= relativeUnit*0.5;
        this.children.win50000.position.x -= relativeUnit*12;
        this.children.enterButton.position.y += relativeUnit*74.5;
        this.children.enterButton.position.x += relativeUnit*35.5;

        this.children.textWin.position.y += relativeUnit*13;
        this.children.textWin.position.x -= relativeUnit*12;
        this.children.termsAndConditions.position.y += relativeUnit*1;
        this.children.termsAndConditions.position.x += relativeUnit*13.5;
        this.children.privacyPolicy.position.y += relativeUnit*1;
        this.children.privacyPolicy.position.x += relativeUnit*7.5;
        this.children.divider.position.y += relativeUnit*89;
        this.children.divider.position.x += relativeUnit*37;


        this.name_input = new input(this.children.nameField, 260,20,0,0)
        this.name_input.setDesktopClasses('textInputActive', 'textInputActive', 'textInputActive')
        
        this.name_input.enable();


         this.company_input = new input(this.children.companyField, 260,20,0,0)
        
        this.company_input.enable();


         this.email_input = new input(this.children.emailField, 260,20,0,0)
        
        this.email_input.enable();











        
        var _t = this;

        var yUp = this.children.submitButton.position.y - 2.5;
        this.submitButton = new button(this.children.submitButton, {y: yUp }, function(){
            au.play('button'+m.randomInt(1,2), 1);
            WebAPICall.submitScore(null, this.name_input.val,Math.floor(game.highestHeightActual) , this.company_input.val, this.email_input.val)
            _t.switchToPart2();
        }.bind(this));

        var yUp = this.children.replayButton.position.y - 2.5;
        this.replayButton = new button(this.children.replayButton, {y: yUp }, function(){
            au.play('button'+m.randomInt(1,2), 1);
            _t.play();
        });

        var yUp = this.children.top10Button.position.y - 2.5;
        this.top10Button = new button(this.children.top10Button, {y: yUp }, function(){
            au.play('button'+m.randomInt(1,2), 1);

            _t.transitionOut(function(){
            //v.get('ingame').transitionIn();
            v.get('scoreboard').transitionIn();
            });
        });

        var yUp = this.children.twitterIcon.position.y - 2.5;
        this.twitterIcon = new button(this.children.twitterIcon, {y: yUp }, function(){
            au.play('button'+m.randomInt(1,2), 1);
            sharegame('twitter', Math.floor(game.highestHeightActual), false, event);   
            
        }.bind(this));

        var yUp = this.children.facebookIcon.position.y - 2.5;
        this.facebookIcon = new button(this.children.facebookIcon, {y: yUp }, function(){
            au.play('button'+m.randomInt(1,2), 1);
             sharegame('facebook', Math.floor(game.highestHeightActual), false, event);   
            
        }.bind(this));

        var yUp = this.children.enterButton.position.y - 2.5;
        this.enterButton = new button(this.children.enterButton, {y: yUp }, function(){
            au.play('button'+m.randomInt(1,2), 1);
        });

         var yUp = this.children.termsAndConditions.position.y - 2.5;
        this.termsAndConditions = new button(this.children.termsAndConditions, { }, function(){
            au.play('button'+m.randomInt(1,2), 1);
        });

        var yUp = this.children.privacyPolicy.position.y - 2.5;
        this.privacyPolicy = new button(this.children.privacyPolicy, { }, function(){
            au.play('button'+m.randomInt(1,2), 1);
        });

        var yUp = this.children.textField.position.y - 2.5;
        this.textField = new button(this.children.textField, { }, function(){
            au.play('button'+m.randomInt(1,2), 1);
        });

        var yUp = this.children.textField2.position.y - 2.5;
        this.textField2 = new button(this.children.textField2, { }, function(){
            au.play('button'+m.randomInt(1,2), 1);
        });

        var yUp = this.children.textField3.position.y - 2.5;
        this.textField3 = new button(this.children.textField3, { }, function(){
            au.play('button'+m.randomInt(1,2), 1);
        });

        var yUp = this.children.female.position.y - 2.5;
        this.female_btn = new button(this.children.female, { y:yUp }, function(){
            _t.select('female'); 
            ga('send', 'event', 'Game', 'Choose Female');
            au.play('button'+m.randomInt(1,2), 1);
        });
        
        yUp = this.children.male.position.y - 2.5;
        this.male_btn = new button(this.children.male, { y:yUp }, function(){
            _t.select('male'); 
            ga('send', 'event', 'Game', 'Choose Male');
            au.play('button'+m.randomInt(1,2), 1);
        });
        
    },
    
    play: function(g) {
        console.log("press play button");
        this.transitionOut();
        game.setLevel(0);
        v.get('ingame').transitionIn();
    },

    select: function(g) {
        user.gender = g;
        user.save();
        this.transitionOut();
        game.setLevel(0);
        v.get('ingame').transitionIn();
    },

    switchToPart2: function(g) {
        this.children.congratulationsPart1.visible = false;
        this.children.nameField.visible = false;
        this.children.companyField.visible = false;
        this.children.emailField.visible = false;
        this.children.textField.visible = false;
        this.children.textField2.visible = false;
        this.children.textField3.visible = false;
        this.children.enterButton.visible = true;
        //this.children.enterButton.position.x = this.children.submitButton.position.x;
        //this.children.enterButton.position.y += this.children.submitButton.position.y;
        //this.children.submitButton.visible = false;
        this.children.congratulationsPart2.visible = true;
        this.children.win50000.visible = true;
        //this.children.enterButton.visible = true;
        //this.children.enterButton.enable();

    },


    transitionIn: function(heightReached){
        //only show this if the gender had not been selected
        //step 4
        this.congratulationsHeightReached = heightReached;
        currentView = 'congratulations';
        this.buildView();   
        v.get('menubg').switchToBg2();
        v.get('menuhud').switchToCongratulations();
        this.show();
        this.enable();
        this.children.distance.text = this.congratulationsHeightReached;
        this.children.distance.visible = false;
        this.children.distance.visible = true;

setTimeout(function(){
               app.resize();
        }, 20);

          
            //this.switchToPart2();
        
    },
    transitionOut: function(){
        this.hide();
        v.get('menubg').switchToBg();
        this.disable();
    },
    enable: function(){
        this.male_btn.enable();
        this.female_btn.enable();
        this.submitButton.enable();
        this.replayButton.enable();
        this.top10Button.enable();
        this.termsAndConditions.enable();
        this.privacyPolicy.enable();
        this.textField.enable();
        this.textField2.enable();
        this.textField3.enable();
        this.twitterIcon.enable();
        this.facebookIcon.enable();
        this.enterButton.enable();
    },
    disable: function(){
       this.male_btn.disable();
       this.female_btn.disable(); 
       this.name_input.disable();
       this.email_input.disable();
       this.company_input.disable();
    }
});