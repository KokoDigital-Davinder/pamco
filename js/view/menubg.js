// JavaScript Document
var MenuBGView = koko.view({
	assets: [{id:'menubg', src:'img/Menus_backgroundstatic.jpg'},
			{id:'menubg2', src:'img/Menus_background_gameover.jpg'}
			],
	buildView: function(){
		this.children = d.buildLayout([
			{ name:'bg', type:'bitmap', id:'menubg',visible:true },
			{ name:'bg2', type:'bitmap', id:'menubg2',visible:false }
		], this.container);

		var relativeUnit = app.viewPort.height/100;
		this.children.bg.scale.x = 0.615;
		this.children.bg.scale.y = 0.615;
		this.children.bg.position.y -= 170;
		

		this.children.bg2.scale.x = 0.615;
		this.children.bg2.scale.y = 0.615;
		this.children.bg2.position.y -= 0;
		this.children.bg2.position.x -= 0;


	},

	switchToBg: function(){
		this.children.bg.visible = true;
		this.children.bg2.visible = false;
        
	},

	switchToBg2: function(){
		this.children.bg.visible = false;
		this.children.bg2.visible = true;
        
	},

	transitionIn: function(){
		if(_.size(this.children)==0) this.buildView();
		this.show();
        
	},
	transitionOut: function(){
		this.hide();
	}
});