var HowToPlayView = koko.view({
    assets: [
        { id:'howtoplayjson', src:'img/howtoplay.json' },
        { id:'ingame2_json', src:'img/howToPlayIcons.json?v=4' }
    ],
    hasShown: false,
    buildView: function(){
        var vp = app.viewPort, g = app.guiSize, _t = this;
        this.children = d.buildLayout([
            { name:'bg', type:'shape', alpha:0.5, w: g.width, h:g.height },
            { name:'con', type:'container', regX:194, x: vp.centerX, y: 50 },
            { name:'bg', type:'sprite', id:'how_to_play.png', parent:'con' },
            { name:'play', type:'sprite', id:'gameover_continue_button.png', parent:'con', y: 395, x: 18 },
        ], this.container);
        
        var relativeUnit = app.viewPort.height/100;

       //this.children.con.y += relativeUnit*50;

       this.children.bg.anchor = new PIXI.Point(0.5, 0.5);
       this.children.play.anchor = new PIXI.Point(0.5, 0.5);

       this.children.bg.scale.x = 0.54;
       this.children.bg.scale.y = 0.54;

       this.children.play.scale.x = 0.54;
       this.children.play.scale.y = 0.54;

       this.children.bg.position.y =  vp.centerY - relativeUnit*7;
       this.children.bg.position.x =  vp.centerX - relativeUnit*5; 

       this.children.play.position.y =  vp.centerY + relativeUnit*30;
       this.children.play.position.x =  vp.centerX - relativeUnit*5; 

       var yUp = this.children.play.position.y-2.5;
        this.play_btn = new button(this.children.play, {y:yUp}, function(){
            au.play('button'+m.randomInt(1,2), 1);
            _t.transitionOut(function(){
                v.get('ingamecountdown').transitionIn();
            });
        });
          
    },
    transitionIn: function(){
        if(_.size(this.children)==0)this.buildView();
        this.show();
        
        var _t = this;
            
        this.container.alpha = 0;
        this.children.con.position.y = 70;
        this.children.con.alpha = 0;
        d.animateObject(this.container, .3, { alpha:1});
        d.animateObject(this.children.con, .3, { alpha:1, y: 50, onComplete:function(){
            _t.enable();
        }});
            
        this.hasShown = true;
    },
    transitionOut: function(complete){
        var _t = this;
        this.disable();
        
        d.animateObject(this.container, .2, { alpha:0, onComplete:function(){
            _t.hide();
            complete();
        }});
    },
    enable: function(){
       this.play_btn.enable();
    },
    disable:  function(){
        this.play_btn.disable();
    }
});