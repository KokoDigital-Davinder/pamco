var GameCompleteView = koko.view({
    assets: [
		{ id:'gcpanel', src:'img/gamecomplete_popup.png' },
		{ id:'gcContinue_btn', src:'img/gcContinue_btn.png' },
    ],
    buildView: function(){
        var vp = app.viewPort, g = app.guiSize;
        this.children = d.buildLayout([
            { name:'bg', type:'shape', alpha:0.5, w: g.width, h:g.height },
            
            { name:'con', type:'container', regX:182, x: vp.centerX, y: 50 },
            
            { name:'bg', type:'bitmap', id:'gcpanel', parent:'con' },
				
			{ name:'close_btn', type:'bitmap', id:'gcContinue_btn', parent:'con', x:182, y: 320+35, regX:171}
				
        ], this.container);
        
        var _t = this;
        this.close_btn = new button(this.children.close_btn, { y: 320-5+35 }, function() {
            au.play('button'+m.randomInt(1,2), 1);
            _t.transitionOut();
        });
		
		this.panel_btn = new button(this.children.bg, { }, function(e){
			app.openLink("https://uk.virginmoney.com/virgin/credit-cards/", "link");
		})
    },
    transitionIn: function(next){
        var _t = this;
        this.next = next;
        
         _t.doTransitionIn();
    },
    doTransitionIn: function() {
        if(_.size(this.children)==0) this.buildView();
        this.enable();
        this.show();
        
        ga('send', 'event', 'Game', 'UI - Game End');
        
        
        this.container.alpha = 0;
        d.animateObject(this.container, .3, { alpha: 1 });
            
        this.children.con.position.y = 90;
        this.children.con.alpha = 0;
        d.animateObject(this.children.con, .3, { alpha: 1, y:70 });
    },
    transitionOut: function(){
        var _t = this;
        
        this.disable();
        d.animateObject(this.container, .2, { alpha:0, onComplete: function(){
            _t.hide();
            v.get(_t.next).transitionIn();
        }});
        //reenable other menus
    },
    enable: function(){
        this.close_btn.enable();
		this.panel_btn.enable();
		
    },
    disable:  function(){
        this.close_btn.disable();
		this.panel_btn.disable();
    }
});