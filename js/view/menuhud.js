// JavaScript Document
var MenuHUD = koko.view({
	assets: [ { id:'generic_hud_json', src:'img/generic_hud.json' },
	 	{id:'generic_hud2_json', src:'img/hudIcons.json'}
	 ],
	buildView: function(){
		var vp = app.viewPort;
		
		//build this yo
		this.children = d.buildLayout([
			{ name:'virginlogo', type:'sprite', id:'virginlogo', x:vp.left, visible:false },
            
            { name:'virgincardswebsite', type:'sprite', id:'virgincardswebsite', x: vp.left + 10, y: vp.bottom - 24, visible:false },
           
            { name:'pamcoLogo', type:'sprite', id:'pamco_logo.png', regX: 186, x: 405, y:275 , visible:true},
            { name:'settingsButton', type:'sprite', id:'hud_settings.png', x: 100, y: 18, visible:true},
            { name:'homeButton', type:'sprite', id:'hud_home.png', x: 100, y: 18, visible:true},
            { name:'backButton', type:'sprite', id:'hud_back.png', x: 100, y: 18, visible:true},

            { name:'right_cont', type:'container', x: vp.right, visible:false },
			
			
			{ name:'seperator', type:'sprite', id:'seperator', x: -119, y: 4, parent:'right_cont', visible:false },
			{ name:'challengefriends_btn', type:'sprite', id:'challengefriends_btn', x: -226, y: 19, parent:'right_cont', visible:false }
		], this.container);


		var relativeUnit = app.viewPort.height/100;

		this.children.pamcoLogo.anchor = new PIXI.Point(0.5, 0.5);
		this.children.settingsButton.anchor = new PIXI.Point(0.5, 0.5);
		this.children.homeButton.anchor = new PIXI.Point(0.5, 0.5);
		this.children.backButton.anchor = new PIXI.Point(0.5, 0.5);

		this.children.pamcoLogo.scale.x = 0.47;
		this.children.pamcoLogo.scale.y = 0.47;
		this.children.settingsButton.scale.x = 0.5;
		this.children.settingsButton.scale.y = 0.5;
		this.children.homeButton.scale.x = 0.5;
		this.children.homeButton.scale.y = 0.5;
		this.children.backButton.scale.x = 0.5;
		this.children.backButton.scale.y = 0.5;

		this.children.pamcoLogo.position.y =  relativeUnit*3.8;
		this.children.pamcoLogo.position.x =  relativeUnit*25;
		this.children.settingsButton.position.y =  vp.top + relativeUnit*4;
		this.children.settingsButton.position.x =  vp.right - relativeUnit*5;   //Actual position
		//this.children.settingsButton.position.x =  vp.right - relativeUnit*10;
		this.children.homeButton.position.y =  vp.top + relativeUnit*4;
		this.children.homeButton.position.x =  vp.right - relativeUnit*12;
		//this.children.homeButton.position.x =  vp.right - relativeUnit*17;
		this.children.backButton.position.y =  vp.top + relativeUnit*4;
		this.children.backButton.position.x =  vp.left + relativeUnit*4;
		
		this.children.challengefriends_btn.hitArea = new PIXI.Rectangle(-10, -10, 113, 49);
		this.challengefriends_btn = new button(this.children.challengefriends_btn, { y: 16 }, function(e){
			au.play('button'+m.randomInt(1,2), 1);
            facebook.checkLogin(function(){
				facebook.share(app.url);
			}, function(){
				if(c.system.isMobile) { 
                    ga('send', 'event', 'Game', 'CTA - Share Facebook URL');
                    app.openLink(app.url + 'api/share/facebook', 'share', e);
                } else {
                    v.get('connect').transitionIn(function(){
                        facebook.share(app.url);
                        ga('send', 'event', 'Game', 'CTA - Share Facebook Native');
                    }); 
                }
			}, false);
		});
		
		/*this.children.settings_btn.hitArea = new PIXI.Rectangle(-10, -10, 50, 50);
		this.settings_btn = new button(this.children.settings_btn, { y: 15 }, function(){
			au.play('button'+m.randomInt(1,2), 1);
            v.get('settings').transitionIn();
		});*/
		
		var _t = this;

        var yUp = this.children.homeButton.position.y - 2.5;
        this.homeButton = new button(this.children.homeButton, {y: yUp }, function(){
            
            au.play('button'+m.randomInt(1,2), 1);
            if(currentView == 'congratulations'){v.get('congratulations').transitionOut();}
            if(currentView == 'scoreboard'){v.get('scoreboard').transitionOut();}
            //v.get.('congratulations').transitionOut();
            v.get('mainmenu').transitionIn();
            
        });

        var yUp = this.children.settingsButton.position.y - 2.5;
        this.settingsButton = new button(this.children.settingsButton, {y: yUp }, function(){
            au.play('button'+m.randomInt(1,2), 1);
            v.get('settings').transitionIn();
            
        });

        var yUp = this.children.backButton.position.y - 2.5;
        this.backButton = new button(this.children.backButton, {y: yUp }, function(){
            au.play('button'+m.randomInt(1,2), 1);
            if(currentView == 'congratulations'){v.get('congratulations').transitionOut();}
            if(currentView == 'scoreboard'){v.get('scoreboard').transitionOut();}
            //v.get('charselect').transitionOut();
            v.get('mainmenu').transitionIn();
            
        });

        this.virginlogo_btn = new button(this.children.virginlogo, { s: 0.99 }, function(e){
            au.play('button'+m.randomInt(1,2), 1);
		});
		
		this.virgincardswebsite_btn = new button(this.children.virgincardswebsite, null, function(e){
			au.play('button'+m.randomInt(1,2), 1);
            ga('send', 'event', 'Game', 'CTA - Visit Virgin Cards');
            app.openLink(app.url + 'api/link/client2', 'link', e);
		});
	},
    resize:function(){
        var vp = app.viewPort;
        
        this.children.virginlogo.position.x = vp.left;
        this.children.virgincardswebsite.position.x = vp.left + 10;
        this.children.right_cont.position.x = vp.right;
    },

    switchToNoIcons: function(){
		this.children.pamcoLogo.visible = false;
		this.children.settingsButton.visible = false;
		this.children.homeButton.visible = false;
		this.children.backButton.visible = false;
        
	},

	switchToPreloader: function(){
		this.children.pamcoLogo.visible = true;
		this.children.settingsButton.visible = false;
		this.children.homeButton.visible = false;
		this.children.backButton.visible = false;
        
	},

	switchToMainMenu: function(){
		this.children.pamcoLogo.visible = true;
		this.children.settingsButton.visible = true;
		this.children.homeButton.visible = false;
		this.children.backButton.visible = false;
        
	},

	switchToCharacterSelect: function(){
		this.children.pamcoLogo.visible = false;
		this.children.settingsButton.visible = true;
		this.children.homeButton.visible = false;
		this.children.backButton.visible = true;
        
	},

	switchToCongratulations: function(){
		this.children.pamcoLogo.visible = false;
		this.children.settingsButton.visible = true;
		this.children.homeButton.visible = true;
		this.children.backButton.visible = false;
        
	},

	gameOver: function(){
		this.children.pamcoLogo.visible = false;
		this.children.settingsButton.visible = true;
		this.children.homeButton.visible = false;
		this.children.backButton.visible = true;
        
	},


	transitionIn: function(){
		

		if(_.size(this.children) == 0) this.buildView();
		
        this.switchToMainMenu();
        this.show();
		this.enable();
        
        var _t = this 
        app.addResizeListener('menuhud', function(){
           _t.resize();
        });


	}, 
	transitionOut: function(){
		this.hide();
		this.disable();
	},
	enable: function(){
		this.virginlogo_btn.enable();
		this.virgincardswebsite_btn.enable();
		this.challengefriends_btn.enable();
		//this.settings_btn.enable();

		this.homeButton.enable();
		this.settingsButton.enable();
		this.backButton.enable();
	},
	disable: function(){
		this.virginlogo_btn.disable();
		this.virgincardswebsite_btn.disable();
		this.challengefriends_btn.disable();
		//this.settings_btn.disable();
	},
	toggleCardVisibility: function(visibility){
		this.children.virgincardswebsite.visible = visibility;	
	}
});