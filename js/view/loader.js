// JavaScript Document
var LoaderView = koko.view({
	assets: [
		{ id:'preloader_json', src:'img/preloaderIcons.json' }
	],
	buildView: function(){
		var g = app.guiSize, vp = app.viewPort;
		this.children = d.buildLayout([
            { name:'loader', type:'container', regX:173, regY:51, x: 405, y: 420, visible:true },
			{ name:'loading_back', type:'sprite', id:'loading_pink.png', parent:'loader', x: 15, y:40, visible:false }, //black bar
			{ name:'loading_front', type:'sprite', id:'loading_bar.png', parent:'loader', visible:true }, //fancy box to "frame" the loading bar
            { name:'loading_bar', type:'sprite', id:'loading_pink.png', parent:'loader', x: 0, y:0, sX:0, visible:true },//red bar
			{ name:'loadingText', type:'sprite', id:'loading_title.png', parent:'loader', x: 0, y:0, visible:true }
		], this.container);

		var relativeUnit = app.viewPort.height/100;
	//this.children.title.scale.x = 0.5;
	//this.children.title.scale.y = 0.5;
	this.children.loading_front.scale.x = 0.5;
	this.children.loading_front.scale.y = 0.5;
	this.children.loading_bar.scale.x = 0;
	this.children.loading_bar.scale.y = 0.5;
	this.children.loadingText.scale.x = 0.5;
	this.children.loadingText.scale.y = 0.5;

	this.children.loader.anchor = new PIXI.Point(0.5, 0.5);
	this.children.loading_front.anchor = new PIXI.Point(0.5, 0.5);
	this.children.loadingText.anchor = new PIXI.Point(0.5, 0.5);


	this.children.loader.position.y = app.viewPort.centerY ;
	this.children.loadingText.position.y -=  relativeUnit*0.5;
	this.children.loading_bar.position.y -=  relativeUnit*3.4;
	this.children.loading_bar.position.x -=  relativeUnit*15;
	//this.children.loadingText.position.x = app.viewPort.centerX;*/
    //this.children.mainMenuTitle.position.y = app.viewPort.centerY - relativeUnit*15;
    //this.children.mainMenuTitle.position.x = app.viewPort.centerX;
	},
	transitionIn: function(assets, complete){
		v.get('menuhud').switchToNoIcons();
		if(_.size(this.children) == 0) this.buildView();
		
		var _t = this;
		
		a.clearManifest();
		a.addToManifest(assets);
		
        
        this.container.alpha = 0;
        d.animateObject(this.container, .3, { alpha:1, onComplete: function(){
            a.loadManifest(function(perc) {
                //_t.children.loading_bar.scale.x += perc;
                
                console.log("enter load function");
             
            	var loadingBarFitsInsideLoadingFront = _t.children.loading_bar.width < _t.children.loading_front.width*0.9;
			
				_t.children.loading_bar.visible = true;

	            if(loadingBarFitsInsideLoadingFront){

		            _t.children.loading_bar.scale.x = perc/2;
		            
		            if(_t.children.loading_bar.scale.x >1){
		            	_t.children.loading_bar.scale.x = 1;
		            }
		            console.log("loading frame width:", _t.children.loading_front.width);
		            console.log("loading bar width:", _t.children.loading_bar.width);
		            console.log("");
	            }

            }, function(){
                setTimeout(function(){
                    _t.transitionOut(complete);
                }, 200);
            });
        }});
		
        this.show();
	},
	transitionOut: function(complete){
        var _t = this;
        d.animateObject(this.container, .2, {alpha:0, onComplete:function(){
		  v.get('menubg').transitionOut();
            _t.hide();
            complete();
        }});
	}
});	