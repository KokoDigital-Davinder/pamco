var CharacterSelectView = koko.view({
    assets: [ { id:'char_select_json', src:'img/character_select.json' },
            { id:'char_select_json', src:'img/characterSelectIcons.json' },
            { id:'ingame2_json', src:'img/howToPlayIcons.json?v=4' } 
            ],

    chosenCharacter: 'male',

    buildView: function(){
        var vp = app.viewPort;
        
        this.children = d.buildLayout([
            { name:'title', type:'sprite', id:'title_selectplayer.png', regX: 141, x: vp.centerX, y: 100 },
            { name:'femaleSilouette', type:'sprite', id:'female_selectedNew.png', regX:179, x: vp.centerX-50, y:520, regY:336, visible:false },
            { name:'female', type:'sprite', id:'female_not_selectedNew.png', regX:179, x: vp.centerX-50, y:520, regY:323},
            { name:'maleSilouette', type:'sprite', id:'male_selectedNew.png', x:vp.centerX+50, y: 520, regY:323, visible:true },
            { name:'male', type:'sprite', id:'male_not_selectedNew.png', x:vp.centerX+50, y: 520, regY:323 },
            { name:'continue', type:'sprite', id:'confirm_button.png', regX: 141, x: vp.centerX, y: 100 }
            ], this.container);

        var relativeUnit = app.viewPort.height/100;

        this.children.title.anchor = new PIXI.Point(0.5, 0.5);
        this.children.female.anchor = new PIXI.Point(0.5, 0.5);
        this.children.femaleSilouette.anchor = new PIXI.Point(0.5, 0.5);
        this.children.male.anchor = new PIXI.Point(0.5, 0.5);
        this.children.maleSilouette.anchor = new PIXI.Point(0.5, 0.5);
        this.children.continue.anchor = new PIXI.Point(0.5, 0.5);

        this.children.title.scale.x = 0.6;
        this.children.title.scale.y = 0.6;
        this.children.female.scale.x = 0.45;
        this.children.female.scale.y = 0.45;
        this.children.femaleSilouette.scale.x = 0.45;
        this.children.femaleSilouette.scale.y = 0.45;
        this.children.male.scale.x = 0.45;
        this.children.male.scale.y = 0.45;
        this.children.maleSilouette.scale.x = 0.45;
        this.children.maleSilouette.scale.y = 0.45;
        this.children.continue.scale.x = 0.5;
        this.children.continue.scale.y = 0.5;

        this.children.title.position.y -=  relativeUnit*0;
        this.children.title.position.x +=  relativeUnit*12;
        this.children.continue.position.y +=  relativeUnit*76;
        this.children.continue.position.x +=  relativeUnit*11.5;
        this.children.male.position.y +=  relativeUnit*0;
        this.children.male.position.x -=  relativeUnit*24;
        this.children.maleSilouette.position.y +=  relativeUnit*0;
        this.children.maleSilouette.position.x -=  relativeUnit*24;
        this.children.female.position.y +=  relativeUnit*0;
        this.children.female.position.x +=  relativeUnit*38;
        this.children.femaleSilouette.position.y +=  relativeUnit*1;
        this.children.femaleSilouette.position.x +=  relativeUnit*38;


        
        var _t = this;
        var yUp = this.children.female.position.y - 2.5;
        this.female_btn = new button(this.children.female, {}, function(){
            //_t.select('female'); 
            //ga('send', 'event', 'Game', 'Choose Female');
            au.play('button'+m.randomInt(1,2), 1);
            user.gender = 'female';
            _t.children.maleSilouette.visible = false;
            _t.children.femaleSilouette.visible = true;
            
        });
        
        yUp = this.children.male.position.y - 2.5;
        this.male_btn = new button(this.children.male, {}, function(){
            //_t.select('male'); 
            //ga('send', 'event', 'Game', 'Choose Male');
            au.play('button'+m.randomInt(1,2), 1);

            user.gender = 'male';
            _t.children.maleSilouette.visible = true;
            _t.children.femaleSilouette.visible = false;
            
        });
        
        yUp = this.children.continue.position.y - 2.5;
        this.continue_btn = new button(this.children.continue, {y:yUp}, function(){
            //_t.select('male'); 
            //ga('send', 'event', 'Game', 'Choose Male');
            au.play('button'+m.randomInt(1,2), 1);
            _t.select(_t.chosenCharacter);
        });
    },
    select: function(g) {
        //user.gender = g;
        user.save();
        this.transitionOut();
        game.setLevel(0);

        v.get('ingame').transitionIn();
    },
    transitionIn: function(){
        //only show this if the gender had not been selected
        
        //DAVINDER access character select here 
        currentView = 'charselect';
        user.gender = 'unknown';
        if(user.gender == 'unknown') {
            if(_.size(this.children)==0) this.buildView();   
            v.get('menubg').switchToBg2();
            v.get('menuhud').switchToCharacterSelect();
            this.show();
            this.enable();
            user.gender = 'male';
        } else {
            //v.get('levelselect').transitionIn(); 

            game.setLevel(0);
            v.get('ingame').transitionIn();
        }
    },
    transitionOut: function(){
        
        this.hide();
        v.get('menubg').switchToBg();
        this.disable();
    },
    enable: function(){
        this.male_btn.enable();
        this.female_btn.enable();
        this.continue_btn.enable();
    },
    disable: function(){
       this.male_btn.disable();
       this.female_btn.disable(); 
    }
});