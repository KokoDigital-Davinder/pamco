// GAME
var CONST = {
	FLOOR: 600,
	GRAVITY: 35,
	JUMP_SPEED: -25,
    LEAP_SPEED: -32,
	BOOST_SPEED: -60,
	RECYCLE_BOUNDARY: 300,
	GENERATE_BOUNDARY: 300,
	CAM_TOP_PADDING: 300, //610-charHeight(130) = 480*0.5
	CAM_BOTTOM_PADDING: 180,
	PIX_M_MULT: 0.02,
	COLLECTABLE_X_DISTANCE: 200,
	COLLECTABLE_Y_DISTANCE: 90,
	COLLECTABLE_X_SPEED: 0,
	COLLECTABLE_Y_SPEED: 1,
	X_SPEED: 300,
	JUMP_AT_BOOST: 100,
	DIE_HEIGHT: -1000
};
var LVL_SETTINGS = [
    {
        COLLECTABLE_DISTANCE_X: { START: 140, HEIGHT_M: 1/35, INC: 5, MAX: 400 },
        COLLECTABLE_SPEED_X: { START: -0.9, HEIGHT_M: 1/50, INC: 0.3, MAX: 8 },
        BOOK_DISTANCE: {  START: 84, HEIGHT_M: 1/55, INC:5, MAX: 220 }
    },
    {
        COLLECTABLE_DISTANCE_X: { START: 180, HEIGHT_M: 1/34, INC: 5, MAX:400 },
        COLLECTABLE_SPEED_X: { START: -0.3, HEIGHT_M: 1/50, INC: 0.3, MAX: 8 },
        BOOK_DISTANCE: {  START: 86, HEIGHT_M: 1/50, INC:5, MAX: 220 }
    },
    {
        COLLECTABLE_DISTANCE_X: { START: 220, HEIGHT_M: 1/33, INC: 5, MAX:400 },
        COLLECTABLE_SPEED_X: { START: 0.3, HEIGHT_M: 1/50, INC: 0.3, MAX: 8 },
        BOOK_DISTANCE: {  START: 88, HEIGHT_M: 1/45, INC:5, MAX: 220 }
    },
    {
        COLLECTABLE_DISTANCE_X: { START: 260, HEIGHT_M: 1/32, INC: 5, MAX:400 },
        COLLECTABLE_SPEED_X: { START: 0.9, HEIGHT_M: 1/50, INC: 0.3, MAX: 8 },
        BOOK_DISTANCE: {  START: 90, HEIGHT_M: 1/40, INC:5, MAX: 220 }
    }
];
koko.class('gameController', null, {
	level:null,
	setLevel: function(level) { 
		this.level = levels[level];
	},
	vp: null,
	container: null,
	player: null,
	cameraFollowPlayer: true,
	isDead: false,
	collectStreak: 0,
	boostStreak: 0,
	boost:0,
	forcedCollectableX: 0,
	highestHeight: 0,
	highestHeightActual: 0,
	collectables: { 
        active: [], 
        pool: {
            standard: [],
            lounge_coll: [],
            train_coll: [],
            ball_coll: [],
            atlantic_coll: [],
            coffee: [],
            news: [],
            chocolate: [],
            binoculars: [],
            case: [],
            ticket: []
        }
    },
	input: null,
	controlType: 'move',
	mouseX: 0,
	background: null,
	gameController: function() {
		//viewport
		this.vp = new PIXI.Rectangle(0, 0, 660, 610);
		//container
        this.container = new PIXI.DisplayObjectContainer();
        
	},
    collected: {},
    collectSpecial: function(type) {
        if(this.collected[type] == null) {
            this.collected[type] = 1; 
        } else {
            this.collected[type]++;
        }
    },
	reset: function(){
		this.boost = 0;
		this.boostStreak = 0;
		this.collectStreak = 0;
		this.cameraFollowPlayer = true;
		this.isDead = false;
		this.highestHeight = 0;
		CONST.DIE_HEIGHT = -1000;
		this.forcedCollectableX = 0;
        this.collected = {};
        this.lastSpecialHeight = 0;
        
        var CD = LVL_SETTINGS[game.level.id].COLLECTABLE_DISTANCE_X;
		var CS = LVL_SETTINGS[game.level.id].COLLECTABLE_SPEED_X;
		var B = LVL_SETTINGS[game.level.id].BOOK_DISTANCE;
		
		CONST.COLLECTABLE_X_DISTANCE = CD.START;
		CONST.COLLECTABLE_X_SPEED = CS.START;
		CONST.COLLECTABLE_Y_DISTANCE = B.START;
	},
    lastSpecialHeight: 0,
	hasInitialisedInterface: false,
	init: function(){
		var g = app.guiSize, vp = app.viewPort;
        
        
		
		if(!this.hasInitialisedInterface) {
			//control input calc
			this.controlType = koko.client.system.isMobile ? 'tap' : 'keys';
			//create bg
			var c = this.children = d.buildLayout([
				{ name:'mouse_event', type:'shape', w:1, h:1, alpha:0, x:0 },
				
                { name:'collect_cont' },
				{ name:'character_cont' },	
                
				{ name:'mobile_left', type:'sprite', id:'mobile_left', regY:134, x:10, y: vp.bottom-10 },
                { name:'mobile_right', type:'sprite', id:'mobile_right', regY:134, regX:132, x:650, y: vp.bottom-10 },
			], this.container);
			
			var mm = this.children.mouse_event, l = this.children.mobile_left, r = this.children.mobile_right, _t = this;
            //hitareas and events
            if(this.controlType == 'tap') {
                mm.visible = false;
                
                l.hitArea = new PIXI.Rectangle(-100, -100, 332, 334);
                r.hitArea = new PIXI.Rectangle(-100, -100, 332, 334);

                l.interactive = true;
                l.touchstart = l.mousedown = function(){ _t.input.onKeydown(KEYS.LEFT); }
                l.touchend = l.touchendoutside = l.mouseup = l.mouseupoutside = function(){  _t.input.onKeyup(KEYS.LEFT); }

                r.interactive = true;
                r.touchstart = r.mousedown = function(){ _t.input.onKeydown(KEYS.RIGHT); }
                r.touchend = r.touchendoutside = r.mouseup = r.mouseupoutside = function(){ _t.input.onKeyup(KEYS.RIGHT); }

			} else {
                l.visible = r.visible = false;

				/*
                mm.hitArea = new PIXI.Rectangle(0, 0, g.width, g.height);
                mm.interactive = true;
                mm.mousemove = function(e) {
                    _t.mouseX = ((e.originalEvent.clientX - (app.viewPort.x > 0 ? app.viewPort.x : 0)) / game.scale / app.viewPort.s);
				};
				*/
            }
            this.hasInitialisedInterface = true;
        }
        
        //player
		this.player = new character();
        this.children.character_cont.addChild( this.player.img.movieClip );
		//background
		this.background = v.get('ingamebg');
		this.background.transitionIn();
	},
	start: function() {
		var _t = this;
		//update listener
		app.addUpdateListener('ingame', function(){
			_t.update();
		});
		
		//controls
		this.reset();
		this.setupControls();
        
        //create new collectable
		var c;
        switch( game.level.id ) {
            case 0:
                c = new standardLoungeCollectable();
            break;
            case 1:
                c = new standardTrainCollectable();
            break;
            case 2:
                c = new standardBalloonCollectable();
            break;
            case 3:
                c = new standardAtlanticCollectable();
            break;
        }
		//DAVINDER change collectable size
		c.setPosition((this.vp.width*0.5)-20, 400);
		this.children.collect_cont.addChild(c.img);
		c.img.scale.x = 0.7;
		c.img.scale.y = 0.7;
		this.collectables.active.push(c);
                
                
		//position player
		this.player.setPosition( this.vp.width * 0.5, CONST.FLOOR );
	},
	stop: function() {
		app.removeUpdateListener('ingame');
		
        //remove active collectables
		var c = this.collectables;
		for( var i = 0, l = c.active.length; i < l; i++ ){
			var itm = c.active[0];
			this.children.collect_cont.removeChild(itm.img);
			c.active.splice(0, 1);
		}
        //empty all pools
        for( var key in c.pool ) {
            for( i = 0, l = c.pool[key].length; i < l; i++ ){
                var itm = c.pool[key][0];
                this.children.collect_cont.removeChild(itm.img);
                c.pool[key].splice(0, 1);
            }
        }
		this.player.destroy();
		this.player = null;
       
        //achievemtns
        switch(this.level.id) {
			case 0:
				if(this.collected.coffee && this.collected.coffee >= 4) {
                    this.level.specialCollectableTypes.splice(this.level.specialCollectableTypes.indexOf('coffee'), 1);
                    this.level.achievements[1].unlock();
                }
			break;
			case 1:
				if(this.collected.news && this.collected.news >= 2) {
                    this.level.specialCollectableTypes.splice(this.level.specialCollectableTypes.indexOf('news'), 1);
                    this.level.achievements[0].unlock();
                }
                if(this.collected.chocolate && this.collected.chocolate >= 6) {
                    this.level.specialCollectableTypes.splice(this.level.specialCollectableTypes.indexOf('chocolate'), 1);
                    this.level.achievements[2].unlock();
                }
			break;
			case 2:
				if(this.collected.binoculars && this.collected.binoculars >= 6) {
                    this.level.specialCollectableTypes.splice(this.level.specialCollectableTypes.indexOf('binoculars'), 1);
                    this.level.achievements[1].unlock()
                }
            break;
			case 3:
				if(this.collected.ticket && this.collected.ticket >= 4) {
                    this.level.specialCollectableTypes.splice(this.level.specialCollectableTypes.indexOf('ticket'), 1);
                    this.level.achievements[0].unlock();
                }
				if(this.collected.case && this.collected.case >= 8) {
                    this.level.specialCollectableTypes.splice(this.level.specialCollectableTypes.indexOf('case'), 1);
                    this.level.achievements[1].unlock();
                }
			break;
		}
	},
	setupControls: function() {
		this.input = new inputController();
		if (this.controlType == 'keys') {
			this.input.enableKeyboard();
		}
	},
	time: 0,
	delta: 0,
    allowMovement: false,
	die: function() {
        au.play('gameover', 1);
		this.cameraFollowPlayer = false;
		this.isDead = true;
        
        setTimeout(function(){
		  v.get('ingame').transitionOut();
        }, 1000);
	},
	update: function() {
        CONST.PIX_M_MULT = 0.02;
		//delta time
		var time = new Date().getTime()*0.001;
		this.delta = time-this.time;
		this.time = time;
		if(this.delta > 1) this.delta = 0;
		//only contorl if hes not dead init
		if(!this.isDead && this.allowMovement) {
			//controls
			/*
			if(this.controlType == 'move') {
				var xSpeed = (this.mouseX - this.player.box.x - (this.player.box.width*0.5)) * -3;
				xSpeed = m.clamp(xSpeed, -400, 400);
				this.player.speed.x = xSpeed * this.delta * -1;
			} else {
				*/
			if (this.controlType == 'keys') {
				//left de right?
				
				if(this.input.isDown(KEYS.LEFT)) {
					this.player.speed.x = CONST.X_SPEED * this.delta * -1;
				} else if(this.input.isDown(KEYS.RIGHT)) {
				
					this.player.speed.x = CONST.X_SPEED  * this.delta;	
				} else {
					this.player.speed.x = 0;	
				}
			}
			
			if (this.player.speed.x >0) {
				this.player.scaleX = 1;
			} else {
				this.player.scaleX = -1;
			}
		
			//player input
			if(this.player.isGrounded) this.player.jump(true);
		}
		//character
		this.player.update(this.delta);
		
		if(this.player.speed.y > 0) {
			this.collectStreak = 0;	
			this.boostStreak = 0;
		}
		
		//position
		var height = (this.player.box.y + this.player.box.height - CONST.FLOOR) * -1;
		var heightM = height * CONST.PIX_M_MULT;
		
		
		var CD = LVL_SETTINGS[game.level.id].COLLECTABLE_DISTANCE_X;
		var CS = LVL_SETTINGS[game.level.id].COLLECTABLE_SPEED_X;
		var B = LVL_SETTINGS[game.level.id].BOOK_DISTANCE;
        
        
		
		CONST.COLLECTABLE_X_DISTANCE = CD.START + Math.abs(Math.floor(heightM * CD.HEIGHT_M)*CD.INC);   //0.01 = 100m increase speed by 0.3
		CONST.COLLECTABLE_X_SPEED = CS.START + Math.abs(Math.floor(heightM * CS.HEIGHT_M)*CS.INC);   //0.01 = 100m increase speed by 0.3
		CONST.COLLECTABLE_Y_DISTANCE = B.START + Math.abs(Math.floor(heightM * B.HEIGHT_M)*B.INC); //0.01 = 100m increase distance by 60 * 0.075 == 4.5?
        
        if(CONST.COLLECTABLE_X_SPEED < 0) CONST.COLLECTABLE_X_SPEED = 0;
		
        //ENFORCE MAXIMUMS
        var forceMax = false;
        if(CONST.COLLECTABLE_X_DISTANCE > CD.MAX || forceMax) CONST.COLLECTABLE_X_DISTANCE = CD.MAX;
        if(CONST.COLLECTABLE_X_SPEED > CS.MAX || forceMax) CONST.COLLECTABLE_X_SPEED = CS.MAX;
        if(CONST.COLLECTABLE_Y_DISTANCE > B.MAX || forceMax) CONST.COLLECTABLE_Y_DISTANCE = B.MAX;


        
		//all items appear below ya head
		if(this.isBoosting) CONST.COLLECTABLE_X_DISTANCE = 0;
		
		//height based achievements
		switch(this.level.id) {
			case 0:
				if(heightM >= 100) {
                    //if(!this.level.achievements[0].completed) v.get('ingamebg').unlockDistAch(this.level.achievements[0].id);
					//this.level.achievements[0].unlock();	
				}
				if(heightM >= 500) {
                    //if(!this.level.achievements[2].completed) v.get('ingamebg').unlockDistAch(this.level.achievements[2].id);
					//this.level.achievements[2].unlock();	
				}
			break;
			case 1:
				if(heightM >= 300) {
                    //if(!this.level.achievements[1].completed) v.get('ingamebg').unlockDistAch(this.level.achievements[1].id);
					//this.level.achievements[1].unlock();	
				}
			break;
			case 2:
				if(heightM >= 300) {
                    //if(!this.level.achievements[0].completed) v.get('ingamebg').unlockDistAch(this.level.achievements[0].id);
					//this.level.achievements[0].unlock();	
				}
				if(heightM >= 600) {
                    //if(!this.level.achievements[2].completed) v.get('ingamebg').unlockDistAch(this.level.achievements[2].id);
					//this.level.achievements[2].unlock();	
				}
			break;
			case 3:
				if(heightM >= 750) {
                    //if(!this.level.achievements[1].completed) v.get('ingamebg').unlockDistAch(this.level.achievements[1].id);
					//this.level.achievements[1].unlock();	
				}
			break;
		}
		
		//reaching the highest of heights
		if(height > this.highestHeight) {
			this.highestHeight = height;
			CONST.DIE_HEIGHT = this.highestHeight - 650;
		}
		
		//fell to death
		if(height > 0 && height < CONST.DIE_HEIGHT) {
			if(!this.isDead) this.die();
		}
				
		//update the hud
        var hightestM = this.highestHeight * CONST.PIX_M_MULT;
        this.highestHeightActual = hightestM;
		v.get('ingame').updateHUD( hightestM, this.collectStreak, this.boost / CONST.JUMP_AT_BOOST);
		
        v.get('ingame').updateStars( hightestM, this.collected);
        
		//move the camera around this geeza
		if(this.cameraFollowPlayer) {
			if(this.player.box.y < this.vp.y + CONST.CAM_TOP_PADDING) {
				this.vp.y = this.player.box.y - CONST.CAM_TOP_PADDING;	
			} 
			else if ((this.player.box.y + this.player.box.height) > (this.vp.y + this.vp.height - CONST.CAM_BOTTOM_PADDING)) {
				this.vp.y = (this.player.box.y + this.player.box.height) - this.vp.height + CONST.CAM_BOTTOM_PADDING;
				if(this.vp.y > 0) this.vp.y = 0;	
			}
		}
		
		//update background
		this.background.update(this.vp.y);
		//position player image
		
		this.player.img.movieClip.position.x = this.player.box.x + ( this.player.box.width*0.5);
		this.player.img.movieClip.position.y = this.player.box.y - this.vp.y + this.player.animYOffset;
		
		//collectables
        var ceilingHeight = 610 - ( 610 / game.scale );
        
		for( var i = 0; i < this.collectables.active.length; i++ ){
			var c = this.collectables.active[i];
			//update it
			c.update();
			//if within the viewport
			if(c.box.y > (this.vp.y + ceilingHeight - 50) && c.box.y < (this.vp.y + this.vp.height)) {
				//check collisions
				if(!this.player.isGrounded && !c.collected) c.collisionCheck(this.player);
				//position the image
				c.img.position.x = c.box.x;
				c.img.position.y = c.box.y - this.vp.y;
				//add to the container if the img doenst have a container
				//if(!c.img.parent) this.gameCont.addChild(c.img);
				c.img.visible = true;
			} else {
				//if(c.img.parent) this.gameCont.removeChild(c.img);
				c.img.visible = false;
			}
			//has been collected or out of range
			if((c.collected && c.canBeRecycled) || c.box.y > (this.vp.y + this.vp.height + CONST.RECYCLE_BOUNDARY) || c.box.y > (CONST.FLOOR-150)) {
				//if(c.img.parent) this.gameCont.removeChild(c.img);
				c.img.visible = false;
                var itm = (this.collectables.active.splice(i, 1))[0];
				this.collectables.pool[itm.type].push(itm);
				i--;
			}
		}
		
		//generate the next set of collectables        
		var lastCollectable = this.collectables.active[ this.collectables.active.length - 1];
        
        
		if(!lastCollectable || lastCollectable.box.y > ((this.vp.y + ceilingHeight)- CONST.GENERATE_BOUNDARY)) {
			var x, y, newCollectable, lastX, diffX, dir, type;
            
            //yo yo 
            if(lastCollectable && lastCollectable.cat == 'special') lastCollectable = this.collectables.active[ this.collectables.active.length - 2];

            
			lastX = lastCollectable ? lastCollectable.box.x : this.vp.width*0.5;
            
            type = this.level.collectableTypes[m.randomInt(0, this.level.collectableTypes.length-1)];
            
            var addSpecialCollectable = this.lastSpecialHeight + 25 < heightM;
            //special
            if(addSpecialCollectable) {
                
                this.lastSpecialHeight += 25;
                if(this.level.specialCollectableTypes.length > 0 && Math.random() < 0.5) {
                    type = this.level.specialCollectableTypes[m.randomInt(0, this.level.specialCollectableTypes.length-1)]; 
                    if(this.lastSpecialHeight <= 150) {
                        type = this.level.specialCollectableTypes[0];   
                    }
                }
            }
			do {
				if(this.collectables.pool[type].length > 0) {
					newCollectable = (this.collectables.pool[type].splice(0, 1))[0];
					newCollectable.reset();
				} else {
                    switch( type ) {
                        case 'lounge_coll':
                            newCollectable = new standardLoungeCollectable();
                        break;
                        case 'train_coll':
                            newCollectable = new standardTrainCollectable();
                        break;
                        case 'ball_coll':
                            newCollectable = new standardBalloonCollectable();
                        break;
                        case 'atlantic_coll':
                            newCollectable = new standardAtlanticCollectable();
                        break;
                        case 'coffee':
                            newCollectable = new coffeeCollectable();
                        break;
                        case 'news':
                            newCollectable = new newsCollectable();
                        break;
                        case 'chocolate':
                            newCollectable = new chocolateCollectable();
                        break;
                        case 'binoculars':
                            newCollectable = new binocularsCollectable();
                        break;
                        case 'case':
                            newCollectable = new caseCollectable();
                        break;
                        case 'ticket':
                            newCollectable = new ticketCollectable();
                        break;
                    }
                    //DAVINDER change size of collectable
					newCollectable.img.scale.x = 0.7;
					newCollectable.img.scale.y = 0.7;
					this.children.collect_cont.addChild(newCollectable.img);
				}
				//position
				if(this.forcedCollectableX == 0)
				{
					diffX = m.randomInt(0,  CONST.COLLECTABLE_X_DISTANCE);
	
					dir = (Math.random() < 0.5 ? 1 : -1);
					if(lastX <= 5) dir = 1;
					if(lastX >= this.vp.width-45) dir = -1;
					
					x = m.clamp(lastX + (diffX * dir), 5, this.vp.width-40-5);
				} else {
					x = this.forcedCollectableX;	
				}
                
                if(newCollectable.cat == 'special') x = m.randomInt(10, this.vp.width-79-10);
				
				y = !lastCollectable ? 400 : lastCollectable.box.y - CONST.COLLECTABLE_Y_DISTANCE;
				//update last X position
				lastX = x;
				//set the position
				newCollectable.setPosition(x, y);
				//push into active
				this.collectables.active.push(newCollectable);
				//set reference for while loop check
				lastCollectable = newCollectable;
				
			} while( lastCollectable.box.y > ((this.vp.y + ceilingHeight) - CONST.GENERATE_BOUNDARY) );
		}
		
		
	},
	increaseCollectStreak: function(increase){
		//increase boost
        if(increase == undefined) {
		    this.boost += (this.boostStreak > 10 ? 10 : this.boostStreak); //max boost increase 10 per collection 
        } else {
            this.boost += increase;   
        }
		//increase the collect streak
		this.collectStreak++;
		this.boostStreak++;
        
        au.play('collect_' + (this.boostStreak > vm.collectSoundCount ? vm.collectSoundCount : this.boostStreak), 1);
		//check if enough boost
		if(this.boost >= CONST.JUMP_AT_BOOST) {
			this.boost = 0;
			this.boostStreak = 0;
			this.forcedCollectableX = this.player.box.x;
			
			//reset is boosting
			var _t = this;
			setTimeout(function(){
				_t.forcedCollectableX = 0;
			}, 600);
			
			this.player.boost();
		}
	},
    showMultiplier: function(x, y) {
        var multi = (this.boostStreak > 10 ? 10 : this.boostStreak);
        if(multi < 2) return;

        //DAVINDER load in graphics for points here
        
        var mult = d.buildLayout([
            { name:'itm', type:'sprite', regX:43, regY:24, x:x, y:y-this.vp.y, s:0.2, alpha:0, id:'multiNew_' +multi }
        ], this.children.character_cont);
        
        var _t = this;
        d.animateObject(mult.itm, .1, { s:0.7, a:1, ease:Back.easeOut, onComplete: function(){
           d.animateObject(mult.itm, .5, { x:0, y:0, alpha:0, ease: Quad.easeIn, onComplete:function(){
                _t.children.character_cont.removeChild(mult.itm);
                mult.itm = null;
           }}); 
        }});
    }
});
//update listener
var game = new gameController();