// JavaScript Document
koko.class('character', null, { 
	box: null,
	img: null,
	speed: null,
	isGrounded: false,
	scaleX: 1,
	animYOffset: -60,
	character: function(){
		//box
		this.box = new PIXI.Rectangle(0, 0, 40, 70);
		this.speed = new PIXI.Point(0, 0);
		
		
		//frames
		var assPrefix = user.gender.substr(0,1) + "_" + game.level.assetRef;
		
		//create frames
		var frames = [new PIXI.Texture.fromFrame('idle.png')];
		for( var i =0; i < 26; i++ ) {
			frames.push( new PIXI.Texture.fromFrame('jump_' + (i+1) + '.png') );
		}
		
		//clip manager
		this.img = new clipManager(new PIXI.MovieClip(frames), {
			//old values

			idle: [false, 0, 0],
			jumpFromFloor: [false, 1, 14],
			jumpPop: [false, 1, 14],
			jumpFall: [false, 15, 26]

			/*idle: [false, 0, 0],
			jumpFromFloor: [false, 6, 8],
			jumpPop: [false, 1, 8],
			jumpFall: [false, 9, 14]*/	
		});
		
		//widths f=104 m=97
		//height f=174 m=167
		console.log("this.img.movieClip",this.img.movieClip);
		//DAVINDER change size
		this.img.movieClip.scale.x = 0.75;
		this.img.movieClip.scale.y = 0.75;
		this.img.movieClip.pivot.x = user.gender == 'male' ? 48 : 52;
		this.animYOffset = user.gender == 'male' ?  70-167 : 70-174;
	},
	setPosition: function(x, y) {
		this.box.x = x - (this.box.width * 0.5);
		this.box.y = y - this.box.height;	
		
	},
	update: function(delta) {
		//gravity
		this.speed.y += CONST.GRAVITY * delta;
		
		//animation
		if(this.speed.y > 0 && this.img.active != 'jumpFall') {
			this.img.gotoAndPlay('jumpFall');	
		}
		//apply speed to the position
		if(!this.isGrounded) this.box.x += this.speed.x;
		this.box.y += this.speed.y;
		
		//flip reverse it? no - currently stops at edge
		if(this.box.x < 0){
			this.box.x = 0;	
		} else if(this.box.x +this.box.width > game.vp.width) {
			this.box.x = game.vp.width - this.box.width;
		}
		
		//floor		
		if(this.box.y > CONST.FLOOR - this.box.height) {
			this.speed.y = 0;
			this.box.y = CONST.FLOOR - this.box.height;
			this.isGrounded = true;
			this.img.gotoAndStop('idle');
		} else {
			this.isGrounded = false;
		}
		
		//this.img.movieClip.scale.x = this.speed.x < 0 ? -1 : 1;
	},
	jump: function(fromFloor) {
		if(this.speed.y > CONST.JUMP_SPEED) {
			this.img.gotoAndPlay(fromFloor ? 'jumpFromFloor' : 'jumpPop');
			this.speed.y = CONST.JUMP_SPEED;
		}
	},
    leap: function() {
		if(this.speed.y > CONST.LEAP_SPEED) {
			this.img.gotoAndPlay('jumpPop');
			this.speed.y = CONST.LEAP_SPEED;
		}
	},
	boost: function() {
        au.play('boost', 1);
		this.img.gotoAndPlay('jumpPop');
		this.speed.y = CONST.BOOST_SPEED;	
	},
	destroy: function(){
		this.img.movieClip.parent.removeChild(this.img.movieClip);
		this.img = null;
	}
});