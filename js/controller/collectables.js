//standard platform
koko.class('collectable', null, {
    type:'standard',
    cat: 'normal',
    frames: null,
	collectable: function() {
		this.box = new PIXI.Rectangle();
        
		this.frames = [];
		this.createFrames();
        
		this.img = new PIXI.MovieClip(this.frames);
		this.img.loop = false;
		this.img.gotoAndStop(0);	
		this.img.visible = false;
		
		this.box.width = 40;
		this.box.height = 40;
        this.reset();
	},
    createFrames: function(){},
	collide: function(player) {
		if(this.collected) return;
		//var
		var _t = this;
		//this has been collected
		this.collected = true;
		//jump the player
        this.react(player);
		//animation 
		this.img.play();
		this.img.onComplete = function(){
			_t.canBeRecycled = true;
		}
	},
    react: function(player){
        player.jump();
        game.increaseCollectStreak();
        game.showMultiplier(this.box.x+(this.box.width*0.5), this.box.y+(this.box.height*0.5));
    },
	update: function() {
		this.box.y += CONST.COLLECTABLE_Y_SPEED;

        if(CONST.COLLECTABLE_X_SPEED > 0) {
			this.box.x += CONST.COLLECTABLE_X_SPEED * this.speed * this.direction;	
			
			if(this.box.x < 0) {
				this.box.x = 0;
				this.direction = 1;	
			} else if((this.box.x+this.box.width)>game.vp.width) {
				this.box.x = game.vp.width - this.box.width;
				this.direction = -1;
			}
		}
	},
	reset: function(){
		this.canBeRecycled = false;
		this.collected = false;
		this.direction = 1 * (Math.random() < .5 ? 1 : -1); //random direction (left or right)
		this.speed = .5 + (Math.random() * .5); //random value between .5 and 1 (speed mult)
		
		this.img.gotoAndStop(0);
	},
    setPosition: function(x, y){
		this.box.x = x;
		this.box.y = y;
	},
	collisionCheck: function(player) {
		if(this.box.intersectsRect(player.box)) {
			this.collide(player);
		}
	},
});

koko.class('standardLoungeCollectable', 'collectable', {
    type:'lounge_coll',
    standardLoungeCollectable: function(){ this.super(); },
    createFrames: function(){
        for( var i = 0; i < 9; i++ ){
            var coinTexture = new PIXI.Texture.fromFrame('Coin_' + (i+1) + '.png');
            coinTexture.height = coinTexture.height*0.2;
            this.frames.push(coinTexture);
        } 	
    }
});
koko.class('standardTrainCollectable', 'collectable', {
    type:'train_coll',
    standardTrainCollectable: function(){ this.super(); },
    createFrames: function(){
        for( var i = 0; i < 5; i++ ) this.frames.push(new PIXI.Texture.fromFrame('mdisc_' + (i+1) + '.png'));	 
    }
});
koko.class('standardBalloonCollectable', 'collectable', {
    type:'ball_coll',
    standardBalloonCollectable: function(){ this.super(); },
    createFrames: function(){
        for( var i = 0; i < 5; i++ ) this.frames.push(new PIXI.Texture.fromFrame('bdisc_' + (i+1) + '.png'));	
    }
});
koko.class('standardAtlanticCollectable', 'collectable', {
    type:'atlantic_coll',
    standardAtlanticCollectable: function(){ this.super(); },
    createFrames: function(){
        for( var i = 0; i < 5; i++ ) this.frames.push(new PIXI.Texture.fromFrame('jdisc_' + (i+1) + '.png'));	
    }
});

//['coffee', 'news', 'chocolate', 'binoculars', 'case', 'ticket' ]
koko.class('coffeeCollectable', 'collectable', {
    type:'coffee',
    cat :'special',
    coffeeCollectable: function(){
        this.super(); 
        this.box.width = 79;
		this.box.height = 79;
    },
    createFrames: function(){
        for (var i = 1; i < 6; i++ ){
            this.frames.push(new PIXI.Texture.fromFrame('c_iPadCollectable'+i+'.png'));
        }
    },
    react: function(player){
        au.play('collectable', 1);
        player.leap();
        game.increaseCollectStreak(10);
        game.collectSpecial('coffee');
        game.showMultiplier(this.box.x+(this.box.width*0.5), this.box.y+(this.box.height*0.5));
    }
});
koko.class('newsCollectable', 'collectable', {
    type:'news',
    cat :'special',
    newsCollectable: function(){
        this.super(); 
        this.box.width = 79;
		this.box.height = 79;
    },
    createFrames: function(){
        for (var i = 1; i < 10; i++ ){
            this.frames.push(new PIXI.Texture.fromFrame('iPadCollectable'+i+'.png'));
        }
    },
    react: function(player){
        au.play('collectable', 1);
        player.leap();
        game.increaseCollectStreak(10);
        game.collectSpecial('news');
        game.showMultiplier(this.box.x+(this.box.width*0.5), this.box.y+(this.box.height*0.5));
    }
});
koko.class('chocolateCollectable', 'collectable', {
    type:'chocolate',
    cat :'special',
    chocolateCollectable: function(){
        this.super(); 
        this.box.width = 79;
		this.box.height = 79;
    },
    createFrames: function(){
        for (var i = 1; i < 6; i++ ){
            this.frames.push(new PIXI.Texture.fromFrame('c_membership_card_'+i+'.png'));
        }
    },
    react: function(player){
        au.play('collectable', 1);
        player.leap();
        game.increaseCollectStreak(10);
        game.collectSpecial('chocolate');
        game.showMultiplier(this.box.x+(this.box.width*0.5), this.box.y+(this.box.height*0.5));
    }
});
koko.class('binocularsCollectable', 'collectable', {
    type:'binoculars',
    cat :'special',
    binocularsCollectable: function(){
        this.super(); 
        this.box.width = 79;
		this.box.height = 79;
    },
    createFrames: function(){
        for (var i = 1; i < 6; i++ ){
            this.frames.push(new PIXI.Texture.fromFrame('c_cassette_card_'+i+'.png'));
        }
    },
    react: function(player){
        au.play('collectable', 1);
        player.leap();
        game.increaseCollectStreak(10);
        game.collectSpecial('binoculars');
        game.showMultiplier(this.box.x+(this.box.width*0.5), this.box.y+(this.box.height*0.5));
    }
});
koko.class('caseCollectable', 'collectable', {
    type:'case',
    cat :'special',
    caseCollectable: function(){
        this.super(); 
        this.box.width = 79;
		this.box.height = 79;
    },
    createFrames: function(){
        for (var i = 1; i < 6; i++ ){
            this.frames.push(new PIXI.Texture.fromFrame('c_membership_card_'+i+'.png'));
        }
    },
    react: function(player){
        au.play('collectable', 1);
        player.leap();
        game.increaseCollectStreak(10);
        game.collectSpecial('case');
        game.showMultiplier(this.box.x+(this.box.width*0.5), this.box.y+(this.box.height*0.5));
    }
});
koko.class('ticketCollectable', 'collectable', {
    type:'ticket',
    cat :'special',
    ticketCollectable: function(){
        this.super(); 
        this.box.width = 79;
		this.box.height = 79;
    },
    createFrames: function(){
        for (var i = 1; i < 6; i++ ){
            this.frames.push(new PIXI.Texture.fromFrame('c_spray_card_'+i+'.png'));
        }
    },
    react: function(player){
        au.play('collectable', 1);
        player.leap();
        game.increaseCollectStreak(10);
        game.collectSpecial('ticket');
        game.showMultiplier(this.box.x+(this.box.width*0.5), this.box.y+(this.box.height*0.5));
    }
});