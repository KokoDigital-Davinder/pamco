koko.views = {
	pool: {},
	add: function(name, view, parent) {
		view.name = name;
        koko.views.pool[name] = view;
		//add to parent container
		if(parent != null) {
			if (typeof parent == 'string' || parent instanceof String) {
				koko.views.pool[parent].container.addChild( view.container );
			} else {
				parent.addChild(view.container);	
			}
		}
	},
	get: function(name) {
		return koko.views.pool[name];	
	},
	destroy: function(name) {
		var view = koko.views.pool[name];
		if(view.container.parent != null) view.container.parent.removeChild(view.container);
		delete koko.views.pool[name];
	},
	each: function(views, method) {	
	for( var i = 0; i < views.length; i++ ) {
			var view = koko.views.get(views[i]);
			if (typeof method == 'string' || method instanceof String) {
				if(method in view) view[method]();	
			} else {
				method(view);
			}
		}
	},
	all: function(method) {
		var names = [];
		for( var key in koko.views.pool ) names.push(key);
		koko.views.each(names, method);
	},
	allBut: function(ignoreArray, method) {
		var names = [], ignore = false;
		for( var key in koko.views.pool ) {
			ignore = false;
			for( var i = 0; i < ignoreArray.length; i++ ) {
				if(ignoreArray[i] == key) ignore = true;
			}
			if(!ignore) names.push(key);
		}
		koko.views.each(names, method);
	},
	filter: function(_func, method){
		var names = [];
		for( var key in koko.views.pool ) {
			if(_func(koko.views.pool[key], key)) names.push(key);
		}
		koko.views.each(names, method);
	},
	allVisible: function(method) {
		koko.views.filter(function(v, k){
			return v.container.visible && _.size(v.children) > 0;
		}, method);
	}
}
koko.view = function(args) {
	var defaults = {
		container: null,
		view: function(){ },
		assets: [],
		children: [],
		queueAssets: function(){
			koko.assets.addToManifest(this.assets);	
		},
		buildView: function(){},
		transitionIn: function(){}, 
		transitionOut: function(){},
		enable: function(){},
		disable: function(){},
		show: function() {
			this.container.visible = true;
		},
		hide: function(){
			this.container.visible = false;
		},
		resize: function(){},
		destroyContainer: function(){
			for( var i = 0, l = this.container.children.length; i < l; i++ ){
				this.container.removeChildAt(0);	
			}
			this.container.parent.removeChild(this.container);
			this.container = null;	
		}
	}
	var view = function() {
		this.container = new PIXI.DisplayObjectContainer();
		this.hide();
	};
	_.extend(view.prototype, defaults, args);
	return view;
};
koko.class = function(name, extend, args) {
	if(args.hasOwnProperty(name)) { 
		//create function
		window[name] = args[name];
		delete args[name];
		//create base extension first
		if(extend && extend.length > 0) _.extend(window[name].prototype, window[extend].prototype);
		//add args to class
		_.extend(window[name].prototype, args);
		//add extensions constructor to this
		if(extend && extend.length > 0) window[name].prototype.super = window[extend].prototype.constructor;
		//return it
		return window[name];
	} else {
		console.log('ERROR: class ' + name + ' needs a constructor!');
	}
};