//intersectsRect
PIXI.Rectangle.prototype.intersectsRect = function(r){
	return !(r.x > (this.x+this.width) || (r.x+r.width) < this.x || r.y > (this.y+this.height) || (r.y+r.height) < this.y);
}
//bitmap text update
PIXI.BitmapText.prototype.setText = function(text)
{
    this.text = text || ' ';
    this.dirty = true;
    return this;
};

PIXI.BitmapText.prototype.centerText = function(xAxis, yAxis){
   return this.pivotText(xAxis ? 0.5 : 0, yAxis ? 0.5 : 0);
}

PIXI.BitmapText.prototype.pivotText = function(xAxis, yAxis){
    this.updateText();
    if(xAxis) this.pivot.x = this.textWidth * xAxis;
    if(yAxis) this.pivot.y = this.textHeight * yAxis;
    return this;
}