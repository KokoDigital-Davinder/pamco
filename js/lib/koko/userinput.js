var KEYS = {
	LEFT: 37,
	UP: 38,
	RIGHT: 39,
	DOWN: 40,	
}
koko.class('inputController', null, {
	pressed: {},
	keyuplistener: null,
	keydownlistener: null,
	inputController: function(){ 
		//reference 
		var _t = this;
		this.keyuplistener = function(e) { _t.onKeyup(e.keyCode); };
		this.keydownlistener =  function(e) { _t.onKeydown(e.keyCode); };
	},
	isDown: function(keyCode) { 
		return this.pressed[keyCode]; 
	},
	onKeydown: function(keyCode) {
		this.pressed[keyCode] = true;
	},
	onKeyup: function(keyCode) {
		delete this.pressed[keyCode];
 	},
	enableKeyboard: function(){
		//add the key listeners
		window.addEventListener('keyup', this.keyuplistener, false);
		window.addEventListener('keydown', this.keydownlistener, false);
	},
	disableKeyboard: function(){
		window.removeEventListener('keyup', this.keyuplistener, false);
		window.removeEventListener('keydown', this.keydownlistener, false);
	}
});
