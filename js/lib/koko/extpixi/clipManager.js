// JavaScript Document
/*
	new clipManager(PIXI.MoveClip(),
	{
		animName: [doesLoop, frameStart, frameEnd],
		idle: [false, 0, 0],
		jumpStart: [false, 1, 2],
		jumpTip: [true, 3, 5]	
	});
*/
koko.class('clipManager', null, {
	id: 0,
	anims: null,
	movieClip: null,
	active: null,
	clipManager: function(mc, anims){
		this.id = Math.ceil(Math.random()*10000);
		this.movieClip = mc;
		this.anims = anims;

		var _t = this;
		app.addUpdateListener('clip'+this.id, function(){
			_t.update();
		});
	},
	gotoAndStop: function(anim) {
		this.active = anim;
		this.movieClip.gotoAndStop(this.anims[anim][1]);
	},
	gotoAndPlay: function(anim) {
		this.active = anim;
		this.movieClip.gotoAndPlay(this.anims[anim][1]);	
	},
	update: function(){
		if(!this.movieClip.playing) return;
		
		var a = this.anims[this.active];
		//shouldnt loop
		if(!a[0] && this.movieClip.currentFrame == a[2]) {
			this.movieClip.stop();
		}
		//should loop
		else if(a[0] && this.moveClip.currentFrame == a[2]) {
			this.movieClip.movieClip(a[1]);	
		}
	}
});