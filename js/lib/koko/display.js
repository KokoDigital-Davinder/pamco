// JavaScript Document
koko.display = {
	def_props: {
		position: { 
			x: ['position.x','x'], 
			y: ['position.y','y']
		},
		pivot: { 
			x: ['pivot.x', 'regX'], 
			y: ['pivot.y', 'regY'] 
		},
		anchor: { 
			x: ['anchor.x', 'anchorX', 'aX'], 
			y: ['anchor.y', 'anchorY', 'aY'] 
		},
		scale: { 
			x: ['scale.x', 'scaleX', 'sX', 'scale', 's'], 
			y: ['scale.y', 'scaleY', 'sY', 'scale', 's'], 
		},
		alpha: ['alpha', 'a'], 
		visible: ['visible', 'v'],
		rotation: ['rotation', 'r'], 
		name: ['name', 'n'],
		font: ['font'],
		fill: ['fill', 'color'],
		tint: ['tint'],
		align: ['align'],
		stroke: ['stroke', 'strokeColor', 'sColor'],
		strokeThickness: ['strokeThickness','sThickness', 'sWidth', 'strokeWidth', 'st'],
		wordWrapWidth: ['wordWrapWidth', 'linewidth', 'lineWidth', 'lw'],
	},
	convertProps: function(props, ignoreProps){
		//this returns values passes to it in a way that PIXI is expecting.
		//so can just pass this for example
		//{ x: 100, position: { y: 10 }, scaleX: 2, alpha:0.2, r: 1 }
		//and it will return
		//{ position: { x: 100, y: 10 }, scale: { x: 2 }, alpha: 0.2, rotation: 1 }
		var def_props = koko.display.def_props;
		var setProperValues = function(structure, values) {
			var o = {};
			for( var key in structure ){
				var prop = structure[key];
				if(Object.prototype.toString.call(prop) === '[object Array]') {
					for( var i = 0; i < prop.length; i++ ){
						//prop split at . so we can process deepObjects
						var p = prop[i].split('.');
						var val = values[p[0]];
						if(p.length == 2 && values[p[0]] !== undefined) val = values[p[0]][p[1]];
						//set value
						if(val !== undefined) o[key] = val;	
					}
				} else {
					var deepObj = setProperValues(prop, values, key); 
					if(_.size(deepObj) > 0) {
						o[key] = setProperValues(prop, values);
					}
				}
			}
			return o;
		}
		var t = setProperValues(def_props, props);
		//used when this function is cleaning tweening props
		//so it doesnt remove ease, onComplete etc...
		if(ignoreProps) {
			for( var i = 0; i < ignoreProps.length; i++ ){
				var prop = props[ignoreProps[i]];
				if(prop === undefined) continue;
				t[ignoreProps[i]] = prop;
			}
		}
		//if wordWrapWidth is set (text only) then wordWrap is set to true else wordWrapWidth does nothing
		if(t.hasOwnProperty('wordWrapWidth')) t.wordWrap = true;
		return t;
	},
	applyProps: function(itm, props){
		for( var key in props ){
			switch(key) {
				case 'scale':
				case 'position':
				case 'pivot':
				case 'anchor':
					//this has to be applied like this otherwise the deepObject would be replace
					//ie: if you passed { scale: { x: 10 } }, rather than scale being replaced
					//with that (losing the y property) the values have to be set independntly
					if(!itm.hasOwnProperty(key)) return;
					if(props[key].x != null) itm[key].x = props[key].x;
					if(props[key].y != null) itm[key].y = props[key].y;
				break;
				case 'rotation':
					itm[key] = koko.math.angleToRadians(props[key]); //pixijs expects angles in radians
				break;
				default: 
					itm[key] = props[key];
				break;	
			}
		}
		return itm;
	},
	animationSpecificProperties: ['delay', 'ease', 'onComplete', 'onCompleteParams', 'onCompleteScope', 'useFrames',
		'immediateRender', 'onStart', 'onStartParams', 'onStartScope', 'onUpdate', 'onUpdateParams', 'onUpdateScope', 'onReverseComplete',
		'onReverseCompleteParams', 'onReverseCompleteScope', 'paused', 'overwrite', 'yoyo', 'repeat', 'repeatDelay', 'reversed', 'paused'
	],
	animateObject: function(obj, duration, props) {
		//this is needed as the deepObject properties in DisplayObjectContainer,
		//need to be animated seperatly for this reason
		props = koko.display.convertProps(props, koko.display.animationSpecificProperties);
		var anims = [];
		var deepObjects = ['position', 'pivot', 'scale'];
		for( var i = 0; i < deepObjects.length; i++ ){
			var prop = props[deepObjects[i]];
			//skip if not found
			if(prop === undefined) continue;
			//if(duration == 0.655) console.log(prop);
			anims.push({ obj: obj[deepObjects[i]], props: prop });
			//remove from main props;
			delete props[deepObjects[i]];
		}
		//push main obj anim properties
		anims.push({ obj: obj, props: props });
		//loop all anims and do 'em
		var persistantProperties = ['ease', 'delay', 'repeat', 'yoyo'];
		var tweens = [];
		for( var i = 0; i < anims.length; i++ ){
			var anim = anims[i];
			for( var j = 0; j < persistantProperties.length; j++) {
				var p = persistantProperties[j];
				if(props.hasOwnProperty(p)) anim.props[p] = props[p]; //copy ease to all anims objects
			}
			//tween it
			tweens.push(TweenMax.to(anim.obj, duration, anim.props));
		}
		return tweens;
	},
	killAnimations: function(object) {
		var deepObjects = ['position', 'pivot', 'scale'];
		var objects = Object.prototype.toString.call( object ) === '[object Array]' ? object : [object];
		for( var i = 0; i < objects.length; i++ ){
			var o = objects[i];
			TweenMax.killTweensOf(o);
			for( var j = 0; j < deepObjects.length; j++ ){
				TweenMax.killTweensOf(o[deepObjects[j]]);
			}
		}

	},
	buildLayout: function(objs, container) {
		var itms = {};
		var calculateProperties = koko.display.convertProps;
		for( var i = 0; i < objs.length; i++ ) {
			var obj = objs[i];
			var type = obj.type || 'container';
			var itm,  props = null;
			
			switch(obj.type || 'container') {
				case 'container':
					itm = new PIXI.DisplayObjectContainer();
					props = calculateProperties(obj);
				break;
				case 'bitmap':
					itm = new PIXI.Sprite(PIXI.Texture.fromImage(koko.assets.getAssetURL(obj.id), true));
					props = calculateProperties(obj);
				break;
				case 'sprite':
					var spriteId = obj.id;

					if(PIXI.TextureCache[spriteId] == null) spriteId = obj.id + '.png';
					if(PIXI.TextureCache[spriteId] == null) spriteId = obj.id + '.jpg';
					if(PIXI.TextureCache[spriteId] == null) alert('no texture found for name ' + obj.id);
					
					itm = new PIXI.Sprite.fromFrame(spriteId);
					props = calculateProperties(obj);
				break;
				case 'shape':
				case 'mask':
				//case 'hitarea':
					itm = new PIXI.Graphics();
					itm.beginFill( obj.fill || '#000000' );
					itm.drawRect(0, 0, obj.width || obj.w || 100, obj.height || obj.h || 100);
					props = calculateProperties(obj);
				break;
				case 'text':
					props = calculateProperties(obj);
					itm = new PIXI.Text( obj.text || "", props );
				break;
				case 'bmptext':
					props = calculateProperties(obj);
					itm = new PIXI.BitmapText( obj.text || "", props );
				break;
			}
			//parent will be main container passed through unless item has parent's name (or parent object) passed through must have already been created
			var parent = container;
			if(obj.hasOwnProperty('parent')){
				if (typeof obj.parent == 'string' || obj.parent instanceof String) {
					parent = itms[obj.parent];
				} else {
					parent = obj.parent;	
				}
			}
			//what to do?
			switch(obj.type || 'container') {
				case 'mask':
					parent.parent.addChildAt(itm, 0);
					
					itm.position.x = parent.position.x;
					itm.position.y = parent.position.y;

					parent.mask = itm;
				break;
				default:
					parent.addChild(itm);
				break;
			}
			//extend properties to itm
            koko.display.applyProps(itm, props);  
            
			//push into itms
			//parent[obj.name] = itm;
			itms[obj.name] = itm;
		}
		//return the itms
		return itms;
	}	
}