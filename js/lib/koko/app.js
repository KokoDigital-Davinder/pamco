// JavaScript Document
var app = {
	//environment variables
	is_cocoon_js: false,
	use_cocoon_screen_canvas: true,
	//pixi
	game_container: null,
	stage: null,
	renderer: null,
	game_stage: null,
	token: null,
	tokenRetries: 0,
	canvas: null,
	setupComplete: function(){},
	// guiSize: { width: 810, height:610 },
	// minSize: { width: 660, height:610 },
	// viewPort: { x:0, y:0, left:0, right:0, top:0, bottom:0, width:810, height:610, centerX:405, centerY:305 },
	guiSize: { width: 460, height:650 },
	minSize: { width: 460, height:650 },
	viewPort: { x:0, y:0, left:0, right:0, top:0, bottom:0, width:460, height:610, centerX:230, centerY:305 },
	scale:1,
	hasLoaded:false,
	rotate_screen: null,
	url: 'https://www.loungeleap.co.uk/',
	local: document.location.hostname == "localhost" || document.location.hostname.substr(0,3) == "192",
	link: null,
    leaderboardAlwaysOpen: window.location.href.indexOf('tab') != -1,
	init: function() {
		app.is_cocoon_js = !!navigator.isCocoonJS;
		app.link = document.getElementById('link');
	},
	getToken: function(complete){
		//if(!facebook.enabled || app.tokenRetries >= 3) return;
		if(app.tokenRetries >= 3) return;
        $.ajax({
			dataType:'json', 
			type: 'GET',
			url: app.url + "api/token", 
            crossDomain: true,
            xhrFields: { withCredentials: true },
			success: function( data ) {
				app.token = data.response.token; 
                if(complete) complete(); 
			}
		});
	},
    userLoaded: false,
    loadUser: function(complete) {
         $.ajax({
			dataType:'json', 
			type: 'POST',
			url: app.url + "api/user/scores", 
            crossDomain: true,
            xhrFields: { withCredentials: true },
             data: JSON.stringify({
				// "token": md5(app.token.substr(8,15)+facebook.userID+app.token.substr(3,7)+facebook.userID+app.token.substr(5,3)),
				"token": md5(app.token.substr(8,15)+app.token.substr(3,7)+app.token.substr(5,3)),
				//"player_fb_uid": facebook.userID,
			}), 
			success: function( data ) {
                user.loadFromString(data.response.player_extra);
                app.userLoaded = true;
                if(complete) complete(); 
			}
		});  
    },

    
    submitAllScores: function(){
        if(la.highscore == 0 && lb.highscore == 0 && lc.highscore == 0 && ld.highscore == 0) return;
        
        if(app.token == null) {
            app.getToken(function(){
                app.submitAllScores();
            });
            return;
        }

        $.ajax({
			dataType:'json', 
			type: 'POST',
			url: app.url + "api/scores/connect", 
            crossDomain: true,
            xhrFields: { withCredentials: true },
			data: JSON.stringify({
				"token": md5(app.token.substr(8,15)+app.token.substr(3,7)+app.token.substr(5,3)),
                "player_extra": user.getSaveString(),
				"player_full_name": facebook.userName,
				"player_fb_uid": facebook.userID,
				"player_fb_token": facebook.token,
                "player_level_1_score": Math.floor(la.highscore * CONST.PIX_M_MULT).toString(),
                "player_level_2_score": Math.floor(lb.highscore * CONST.PIX_M_MULT).toString(),
                "player_level_3_score": Math.floor(lc.highscore * CONST.PIX_M_MULT).toString(),
                "player_level_4_score": Math.floor(ld.highscore * CONST.PIX_M_MULT).toString()
			}), 
			success: function( data ) {
				app.tokenRetries = 0;
                if(data.response[0].player_extra.length > 0) {
                    user.loadFromString(data.response[0].player_extra); 
                }  
			},
			error: function( data ) {
				app.tokenRetries++;
				if(data.responseJSON && data.responseJSON.error && data.responseJSON.error.code == '401') {
					app.getToken(function(){
						app.submitAllScores();
					});
				}
			}
		});
        
        
    },
	submitScore: function(complete){
		// if(!facebook.enabled || !app.token) {
		// 	if(complete) complete();
		// 	return;	
		// }
		if(!app.token) {
			if(complete) complete();
			return;	
		}
		var s = Math.floor(game.highestHeight * CONST.PIX_M_MULT).toString();
        $.ajax({
			dataType:'json', 
			type: 'POST',
			url: app.url + "api/scores", 
            crossDomain: true,
            xhrFields: { withCredentials: true },
			data: JSON.stringify({
				"token": md5(app.token.substr(8,15)+s+app.token.substr(3,7)+s+app.token.substr(5,3)),
				"player_score": s,
                "player_extra": user.getSaveString(),
				"player_full_name": "form-name",
				"player_fb_uid": "form-??",
				"player_fb_token": "form-token",
                "player_level": (game.level.id+1).toString()
                //DAVINDER - FORM SCORE SUBMISSION
			}), 
			success: function( data ) {
				app.tokenRetries = 0;
				if(complete) complete();
			},
			error: function( data ) {
				app.tokenRetries++;
				if(data.responseJSON && data.responseJSON.error && data.responseJSON.error.code == '401') {
					app.getToken(function(){
						app.submitScore(complete);
					});
				}
			}
		});
	},
	loaded: function() {
		//client
		koko.client.init();
		
		//size
		// app.guiSize = { width: 810, height:610 };
		// app.minSize = { width: 660, height:610 };
  //       app.minSize.width = 460;

  		app.guiSize = { width: 460, height:650 };
		app.minSize = { width: 460, height:650 };
        app.minSize.width = 460;
        
		//create pixi stage
		app.stage = new PIXI.Stage(0xFFFFFF, true);
		var width = app.guiSize.width, height = app.guiSize.height;
		//if cocoon
		if(app.is_cocoon_js){
			//create canvas
			console.log('write canvas');
			var canvas = document.createElement(app.use_cocoon_screen_canvas ? 'screencanvas' : 'canvas');
			//kinect js scale property
			canvas.style.cssText = "idtkscale:ScaleAspectFill;"; 
			//set width and height
			_.extend(canvas, {width:width, height:height });
			//add it to the body
			document.body.appendChild(canvas);
			//create the renderer
			app.renderer = new PIXI.CanvasRenderer(width, height, canvas);
			
		} else {
			//auto detect renderer (webGL or Canvas);
			app.renderer = PIXI.autoDetectRenderer(width, height, null, true);
			//app.renderer = PIXI.CanvasRenderer(width, height)
			app.renderer.view.style.cssText = "idtkscale:ScaleAspectFill;"; 
			//create wrap div
			var div = document.createElement("div");
			div.id = "game_container";
			app.game_container = div;
			app.game_container.className = "DOMscaleable";
			//add wrap to body and renderer to wrap
			document.body.insertBefore(div, document.body.firstChild);
			app.game_container.appendChild(app.renderer.view);
			console.log('write outer div');
		}
		
		app.rotate_screen = document.getElementById('rotate_msg');
		//stage
		app.background_layer = new PIXI.DisplayObjectContainer();
		app.game_stage = new PIXI.DisplayObjectContainer();
		app.stage.addChild(app.background_layer);
		app.stage.addChild(app.game_stage);
		
		
		if(!koko.client.system.isMobile  && koko.client.engine.ie == 0) {
			app.renderer.view.addEventListener(koko.client.eventName('down'), function(e){
				if(document.activeElement) document.activeElement.blur();
			});
		}
		
		//events
		window.addEventListener("resize", app.fireResize);
		window.addEventListener("scroll", app.fireResize);
        if(koko.client.system.isMobile) {
			window.addEventListener("orientationchange", function(){
				app.checkOrientation();
			});
		}
		app.resize();
		
		//start e'rythin
		//requestAnimFrame(app.update);
		//alert('request frame');
		
		//We've detected that you may be using an older native android browser. 
		//This app may perform better using the latest Chrome browser.
		
		if(koko.client.system.android && koko.client.engine.webkit < 535){
			alert('We\'ve detected you are using a mobile, hit OK to enable fullscreen mode.');
		}
		
		//requestAnimFrame(app.update);
		setInterval(function(){ app.update(); }, 1000/30);

		
		app.setupComplete();
		app.hasLoaded = true;
		app.resize();
		if(koko.client.system.isMobile) {
			setTimeout(function(){
				app.checkOrientation();
			}, 1000);
		}
	},
    checkOrientation: function(){
		koko.client.calculateOrientation();
		if(koko.client.browser.orientation == 'landscape' && koko.client.system.isMobile) {
			app.rotate_screen.style.display = 'block';
		} else {
			app.rotate_screen.style.display = 'none';
		}
	},
	updateListeners: {},
	addUpdateListener: function(name, func){
		app.updateListeners[name] = func;
	},
	removeUpdateListener:function(name) {
		app.updateListeners[name] = null;
		delete app.updateListeners[name];
	},
	update: function(){
		//update listeners
		for( var key in app.updateListeners ) app.updateListeners[key]();
		//render stage
		app.renderer.render(app.stage);
		//next interval
		//requestAnimFrame(app.update);
	},
	resizeTimer: null,
	fireResize: function(){
		if(app.resizeTimer) clearTimeout(app.resizeTimer);
		app.resizeTimer = setTimeout(function(){
			app.resize();
		}, 100);
	},
	resize: function(){
		if(!app.hasLoaded) {
			setTimeout(function(){
				window.dispatchEvent(new Event('resize'));
			}, 1000);
			return;
		}
		//variables
		var gui = app.guiSize, vp = app.viewPort, min = app.minSize, width = window.innerWidth, height = window.innerHeight;
		
        if(app.leaderboardAlwaysOpen) {	//not being used for pamco
            vp.width = 660;
            vp.height = 610;
            
            vp.centerX = 330;
            vp.centerY = 305;
            
            vp.top = 0;
            vp.bottom = 610;
            vp.right = 660;
            vp.left = 0;
            
            vp.x = 0;
            vp.y = 0;
            
            vp.s = 1;
        } else {
            //reset the vp vars
            vp.width = gui.width;
            vp.height = gui.height
            vp.left = 0;
            vp.right = vp.width;
            vp.top = 0;
            vp.bottom = vp.height;
            vp.x = 0;
            vp.y = 0;
            vp.s = 1;

            //scale to fit height
            vp.s = height / vp.height;
            //if the new scaled width is wider than the width of screen
            if((min.width * vp.s) > width) {
                vp.s = width / min.width;
            }

            vp.x = (width-(vp.width*vp.s))*0.5; //center
            vp.y = (height-(vp.height*vp.s))*0.5; //center

            if(vp.x < 0) vp.left = vp.x * -1 / vp.s;
            if(vp.y < 0) vp.top = vp.y * -1;

            var visibleWidth = width / vp.s;
            if(visibleWidth > gui.width) visibleWidth = gui.width;

            vp.right = vp.left + visibleWidth;

            vp.width = visibleWidth;

            vp.centerX = vp.left + (vp.right - vp.left) * 0.5;
            vp.centerY = vp.top + (vp.bottom - vp.top) * 0.5;
        }
		
		//scrol to top
		if(app.rotate_screen != null) {
			koko.client.calculateOrientation();
			if(koko.client.browser.orientation == 'landscape' && koko.client.system.isMobile) {
				app.rotate_screen.style.display = 'block';
			} else {
				app.rotate_screen.style.display = 'none';
			}
		}
        
		//scale and position
		var trans = "scale("+vp.s+", "+vp.s+") translate("+(vp.x/vp.s)+"px,"+(vp.y/vp.s)+"px)";
		app.renderer.view.style[koko.client.cssprops.transform] = trans;
		
		//height
		app.game_container.style.height = Math.round(height * vp.s) + "px";
		document.body.height = (Math.round(height * vp.s)) + "px";
		
		//woo
		app.scale = vp.s;
		app.renderer.render(app.stage);
		
		//resize listeners
		for( var key in app.resizeListeners ) app.resizeListeners[key]();
	},
	resizeListeners: {},
	addResizeListener: function(name, func){
		app.resizeListeners[name] = func;
		func();
	},
	removeResizeListener:function(name) {
		app.resizeListeners[name] = null;
		delete app.resizeListeners[name];
	},
	openLink: function(url, type) {
		switch(type) {
			case 'link':
			case 'share':
				if(koko.client.browser.firefox > 0) {
					var popup = window.open(url);
					setTimeout( function() {
						if(!popup || popup.outerHeight === 0) {
							alert("We have detected you may have a popup blocker enabled. Please add this site to your exception list.");
						}
					}, 500);
				} else {
					
					window.open(url)
					/*app.link.setAttribute("href", url);
					app.link.setAttribute("target","_blank");
					
					var dispatch = document.createEvent("HTMLEvents");
					dispatch.initEvent("click", true, true);
					
					app.link.dispatchEvent(dispatch);*/
				}
			break;	
		}
	}
}