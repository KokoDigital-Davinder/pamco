// JavaScript Document
koko.facebook = {
	enabled: false,
	token: "",
	userID: null,
	userName: null,
	scope: 'user_friends',
    loggedin: false,
    gotfriends: false,
	friends: [{ id: 0, name: 0, score:0, installed:true }],
	images: {},
	installedFriendCount:0,
    success: [],
    addSuccessFunc: function(success) {
        this.success.push(success);  
    },
	getFriends: function(complete){
		var _t = koko.facebook;

		if(_t.gotfriends) {
			if(complete) complete();
			return;
        }
		
		FB.api('/me/friends?access_token='+_t.token+'&fields=installed,name', function(response) {
			for( var i = 0; i < response.data.length; i++ ) {
				var r =  response.data[i];
				_t.friends.push({ id: r.id, name:r.name, installed:true, score:0});
			}
			_t.installedFriendCount = _t.friends.length;
            //load invitable friends
            FB.api('/me/invitable_friends?access_token='+_t.token, function(response) {
                for( var i = 0; i < response.data.length; i++ ) {
                    var r =  response.data[i];
                    var img = 'none';
                    if(!r.picture.data.is_silhouette) img = response.data[i].picture.data.url.replace('https://','http://');
                    _t.images[ r.id ] = img;
                    _t.friends.push({ id: r.id, name:r.name, img:img, installed:false});
                }
                _t.gotfriends = true;
                if(complete) complete();
            });
		});
	},
	getFriendIds: function(){
		var ids = [], _t = koko.facebook;
		for( var i = 0; i < _t.installedFriendCount; i++ ) {
            if(_t.friends[i].id.length > 64) break;
			ids.push(_t.friends[i].id);	
		}
		return ids;
	},
    completeLogin: function(){
        if(this.loggedin) return;
        
       $("#facebook_notconnected_like").hide();  
       $("#facebook_connected_like").removeClass('hide');

        for( var i = 0; i < this.success.length; i++) this.success[i]();  
    },
	checkLogin: function(success, fail, loginOnFail, onlyLoginOnFailIfUnathorized){
		var _t = koko.facebook;
		if(!_t.enabled) {
			success();
			return;	
		}
        
        if(window.location.hash) {
            var h = window.location.hash; 
            _t.token = h.substring(h.indexOf('#access_token=')+(('#access_token=').length), h.indexOf('&expires_in'));            
            koko.facebook.getUserData(function(){
                _t.completeLogin();
                _t.loggedin = true;
                success();
            });
        }   
        else {

            //check login
            FB.getLoginStatus(function(response) {
                
                if(response.status!='unknown') {
                   // $("#facebook_notconnected_like").hide();  
                   // $("#facebook_connected_like").removeClass('hide');     
                }
                
                if (response.status == 'connected') {
                    _t.token = response.authResponse.accessToken;
                    _t.userID = response.authResponse.userID;				
                    koko.facebook.getUserData(success);
                } else{ 
                    if(loginOnFail) {
                        if(onlyLoginOnFailIfUnathorized && response.status != 'not_authorized') {
                            if(fail) fail();
                            return;
                        }
                                                
                        if(c.system.isMobile) {
                            var uri = encodeURI('http://www.loungeleap.co.uk/');
                            window.location = encodeURI("https://www.facebook.com/dialog/oauth?client_id=1606787219590251&redirect_uri="+uri+"&response_type=token&scope="+_t.scope);
                        } else {
                            FB.login(function(response) {
                                if (response.authResponse) {
                                    _t.token = response.authResponse.accessToken;
                                    _t.userID = response.authResponse.userID;
                                    koko.facebook.getUserData(success);
                                } else {
                                    fail();   
                                }
                            }, { scope: _t.scope, return_scopes: true});
                        }
                    } else {
                        if(fail) fail();	
                    }
                }
            });
        }
	},
	share: function(url) {
		FB.ui({
		  method: 'share',
		  href: url,
		}, function(response){});
	},
	inviteFriends: function(title){
		FB.ui({method: 'apprequests',
			message: (title ? title : "Check out Lounge Leap from Virgin Money! Play now for your chance to win amazing prizes!")
		}, function(){
		});	
		
	},
	inviteFriend:function(id, title){
		FB.ui({
			method: 'apprequests',
			message: (title ? title : "Check out Lounge Leap from Virgin Money! Play now for your chance to win amazing prizes!"),
			to: id
			}, function(){
		});	 
	},
	getUserData: function(success) {
        var _t = koko.facebook;
		FB.api('/me?access_token='+this.token, {fields: 'name'}, function(response) {
            koko.facebook.userID = response.id;
			koko.facebook.userName = response.name;
			koko.facebook.friends[0].name = response.name;
			koko.facebook.friends[0].id = response.id;
            _t.completeLogin();
            _t.loggedin = true;
            if(success) success();
		});
	}
}