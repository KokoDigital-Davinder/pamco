// JavaScript Document
koko.audio = {
	manifest: [],
	groups: {},
	sounds: {},
	instances: [],
	meta: false,
	loaded: 0,
	hasLoaded: false,
	volume: 1,
	masterVolume: 1,
	loadedHandler: null,
	setMute: function(mute) {
		koko.audio.mute = mute;
		createjs.Sound.setMute(mute);
	},
	setVolume: function(volume) {
		var t = koko.audio;
		t.volume = volume;
		//update all group volumes
		for( var key in t.soundGroups ) {
			t.setSoundGroupVolume(key, t.groups[key].volume);
		}
	},
	toggleMute: function() {
		var t = koko.audio;
		var mute = !t.mute;
		t.setMute(mute);
		return mute;
	},
	addSoundGroup: function(name, volume) {
		var t = koko.audio;
		t.groups[name] = {
			enabled: true,
			volume: volume == null ? 1 : volume,
			instances: []
		};
	},
	disableSoundGroup: function(name) {
		var t = koko.audio;
		if(!t.groups.hasOwnProperty(name)) return;
		t.groups[name].enabled = false;
	},
	
	setSoundGroupVolume: function(name, volume) {
		var t = koko.audio;

		if(!t.groups.hasOwnProperty(name)) return;
		t.groups[name].volume = volume;
		
		//loop over sounds and update them
		var instances = t.groups[name].instances;
		for( var i = 0; i < instances.length; i++ ){
			var sound = t.instances[instances[i]];
			sound.s.setVolume(sound.volume * volume * t.sounds[sound.name].volume * t.masterVolume);
		}
	},
	
	add: function(name, src, volume, group) {
		var t = koko.audio;
		t.sounds[name] = {
			id: name,
			group: group,
			volume: volume || 1
		}
		t.manifest.push({id: name, src: src});
	},
	
	play: function(name, volume, loop) {
		var t = koko.audio;
		//find sound group
		var volume = volume || 1;
		var loop = loop || false;
		
		if(!t.sounds.hasOwnProperty(name)) return;
		//found sound now play?
		var sound = t.sounds[name];
		var soundGroup = t.groups[sound.group];
		if(soundGroup.enabled) {
			var s = createjs.Sound.createInstance(name);
			var playVolume = volume * soundGroup.volume * sound.volume * t.volume;
			//console.log(volume);
			s.play(createjs.Sound.INTERRUPT_ANY, 0, 0, loop ? -1 : 0, playVolume, 0);
			t.instances.push({
				name: name,
				s: s,
				volume: volume
			});
			var id = t.instances.length-1;
			soundGroup.instances.push(id);
			return s;
		}
		return false;
	},
	loadSounds: function() {
		var _t = koko.audio;
		if(_t.manifest.length == 0) _t.loadedHandler();
		//extenstions
		createjs.Sound.alternateExtensions = ["mp3", "ogg"];	// add other extensions to try loading if the src file extension is not supported
        createjs.Sound.addEventListener("fileload", function(event){
			_t.loaded++;
            v.get('preloader').updateSoundPerc(_t.loaded / _t.manifest.length);
			if(_t.loaded == _t.manifest.length) {
				_t.hasLoaded = true;
				if(_t.loadedHandler) _t.loadedHandler();	
			}
		}); // add an event listener for when load is completed
        createjs.Sound.registerSounds(_t.manifest, _t.dir);
	}
}