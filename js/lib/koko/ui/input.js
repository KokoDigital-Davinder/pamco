// JavaScript Document
/*
	input improvements
	desktop improvment (float input over the top of the container and hid container, add domScalable and custom class per instance)
		create input with code document.createElement(input) etc...
	for reference: http://www.askbriantherobot.co.uk/js/views/HUDView.js?v=1.04
*/
var inputCount = 0;
koko.class('input', null, {
	inputID: 0,
	container:null,
	question: null,
	defaultText: "",
	val: "",
	isSet: function(){
		return this.val != this.defaultText;
	},
	displayLength: 50, 
	displayExt: "...",
	setDisplayLength: function(length, ext) {
		this.displayLength = length;
		this.displayExt = ext;
	},
	desktopInput: null,
	desktopOffset: { x:0, y:0 },
	desktopIdleClass: "",
	desktopActiveClass: "",
	setDesktopClasses: function(defClass, idleClass, activeClass) {
		if(this.desktopInput) this.desktopInput.className += " " + defClass + " " + idleClass;	
		this.desktopIdleClass = idleClass;
		this.desktopActiveClass = activeClass;
        return this;
	},
	input: function(container, width, height, xOffset, yOffset){ 
		this.container = container;
		
		var _t = this;
		
		inputCount++;
		this.inputID = inputCount;
		
		if(koko.client.system.isMobile) {
			this.hit = new PIXI.Graphics();
			
			this.hit.beginFill('#000000');
			this.hit.alpha = 0;
			this.hit.drawRect(0, 0, 1, 1);
			this.hit.hitArea = new PIXI.Rectangle(0, 0, width + (xOffset*-2), height+ (yOffset*-2));
			this.hit.buttonMode = true;
			this.hit.position.x = this.container.position.x + xOffset;
			this.hit.position.y = this.container.position.y + yOffset;
			
			this.container.parent.addChild(this.hit);
		}
		else
		{
			this.container.visible = false;
			
			this.desktopOffset.x = xOffset;
			this.desktopOffset.y = yOffset;
			
			this.desktopInput = document.createElement('input');
			this.desktopInput.className = "DOMscaleable";
			this.desktopInput.style.display = "none";
			//set text
			this.desktopInput.type = "text";
			this.desktopInput.value = this.container.text;
			//dimension
			this.desktopInput.style.width = width + "px";
			this.desktopInput.style.height = height + "px";
			
			document.body.appendChild(this.desktopInput);
			
			this.desktopInput.addEventListener('change', function(e){
				if(_t.desktopIdleClass != "")e.target.className = e.target.className.replace(_t.desktopIdleClass, "");
				if(_t.desktopActiveClass != "") e.target.className = e.target.className.replace(_t.desktopActiveClass, "");	
				
				if(e.target.value == _t.defaultText) {
					if(_t.desktopIdleClass != "") {
						e.target.className += " " + _t.desktopIdleClass;
					}
				} else {
					if(_t.desktopActiveClass != "") {
						e.target.className += " " + _t.desktopActiveClass;
					}
				}
			});
			
			this.desktopInput.addEventListener('focus', function(e){
				console.log('FOCUS BABY')
				if(e.target.value == _t.defaultText) {
					if(_t.desktopIdleClass != "")e.target.className = e.target.className.replace(_t.desktopIdleClass, "");
					if(_t.desktopActiveClass != "") e.target.className = e.target.className.replace(_t.desktopActiveClass, "");	
					
					e.target.className += " " + _t.desktopActiveClass;
					
					e.target.value = "";
				}
			});
			
			this.desktopInput.addEventListener('blur', function(e){
				if(e.target.value == "") {
					if(_t.desktopIdleClass != "")e.target.className = e.target.className.replace(_t.desktopIdleClass, "");
					if(_t.desktopActiveClass != "") e.target.className = e.target.className.replace(_t.desktopActiveClass, "");	
					
					e.target.className += " " + _t.desktopIdleClass;
					
					e.target.value = _t.defaultText;
				}
				_t.val = e.target.value == _t.defaultText ? "" : e.target.value;
			});
		}
		
		this.defaultText = container.text;
		
		return this;
	},
	idleColor: null,
	activeColor: null,
	setColors: function(idle, active) {
		this.idleColor = idle;
		this.activeColor = active;
        return this;
	},
	callback: function(callback) {
		this.clickCallback = callback;
		return this;	
	},
	enable: function(resize) {
		var _t = this;
		
		if(koko.client.system.isMobile) {
			//mouse up
			this.hit.interactive = true;
			this.hit.touchstart = function(e){
				var c = prompt(this.question || 'Please fill in this field', _t.isSet() ? _t.val : "");
				if(c) {
					_t.val = c;
					
					var dis = c;
					if(c.length > _t.displayLength) dis = c.substr(0, _t.displayLength) + _t.displayExt;
					
					_t.container.setText(dis);
					
					if(_t.val == _t.defaultText) {
						//if(_t.desktopIdleClass != null) _t.container.tint = _t.idleColor;
					} else {
						//if(_t.desktopActiveClass != null) _t.container.tint = _t.activeColor;
					}
					
				}
				return false;
			}
		}
		else
		{
			//desktop	
			this.desktopInput.style.display = "block";
			
			var _t = this;
			app.addResizeListener('input' + this.inputID, function(){
				_t.resize();
			});
		}
        
        if(resize) this.resize();
        
		return this;	
	},
	disable: function(){
		if(koko.client.system.isMobile) {
			this.hit.interactive = false;
			//remove all event listeners
			var arr = ['touchstart', 'mousedown'];
			for( var i = 0; i < arr.length; i++ ){
				this.hit[arr[i]] = null;	
			}
		}
		else
		{
			//desktop
			this.desktopInput.style.display = "none";
			app.removeResizeListener('input' + this.inputID);
		}
		return this;
	},
	resize: function() {
		if(!koko.client.system.isMobile) {
			//desktop
			var vp = app.viewPort, left = 0, top = 0, _t = this;
			
			
			left = vp.x + (this.container.parent.worldTransform.tx + this.container.position.x + this.desktopOffset.x) * vp.s;
			top = vp.y + (this.container.parent.worldTransform.ty + this.container.position.y + this.desktopOffset.y) * vp.s;
					
			this.desktopInput.style.left = Math.round(left) + "px";
			this.desktopInput.style.top = Math.round(top) + "px";
			
			//weird bug when reszieing, the lineheight pushes text to bottom of the textfeidl until you focus (only when vp.s is 1) WEIRD
			this.desktopInput.style.lineHeight = Math.round(38*vp.s)+"px";
			this.desktopInput.style[koko.client.cssprops.transform] = "scale("+vp.s+")";
		}
	}
});