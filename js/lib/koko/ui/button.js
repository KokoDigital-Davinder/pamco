// JavaScript Document
koko.class('button', null, {
	button: function(container, over, callback, data){
		this.overAnims = {};
		this.outAnims = {};

		if(container) this.setContent(container);
		if(over) this.over(over);
		if(callback) this.callback(callback);
        this.data = data ? data : {};
        
		return this;
	},
	triggerCallbackOnMouseDown: false,
	data: null,
	overAnims: null,
	outAnims: null,
	isOver: false,
	enabled: false,
	blockClick: false,
	setContent:function(container) {
		this.container = container;
		this.container.buttonMode = true;
		this.clickTime = 0;
		return this;
	},
	over: function(overParams){
		this.overAnims = {};
		if(overParams.hasOwnProperty('children')){
			for(var i = 0; i < overParams.children.length; i++ ){
				var c = overParams.children[i];
				this.overAnims['child'+(c.index||c.i)] = koko.display.convertProps(overParams.children[i]);
			}
			delete overParams.children;
		}
		this.overAnims['main'] = koko.display.convertProps(overParams);
		//look objs;
		this.out(_.clone(this.overAnims));
		return this;
	},
	out: function(outParams){
		this.outAnims = {};
		for( var key in outParams){
			var container = key == 'main' ? this.container : this.container.getChildAt(key.replace('child',''));
			this.outAnims[key] = this.reverseParams(outParams[key], container);
		}
	},
	reverseParams: function(params, c) {
		var newParams = koko.display.convertProps(params);
		var getDefaultValues = function(params, cont){
			var out = {};
			for(var key in params) {
				if(Object.prototype.toString.call(params[key]) === '[object Object]') {
					out[key] = getDefaultValues(params[key], cont[key]);
				} else {
					out[key] = cont[key]; 
				}
			}
			return out;
		}
		return getDefaultValues(newParams, c);
	},
	enable: function() {
		var _t = this;
		if(this.enabled) return;
		this.enabled = true;
		this.container.interactive = true;
		//mouseoverlistener
		this.container.mouseover = this.container.touchstart = function(e){
			if(e && e.originalEvent && e.originalEvent.type == 'touchstart') {
				if(_t.downCallback) _t.downCallback(e);
			}
			//document.body.style.cursor = 'pointer';
			_t.isOver = true;
			for( var key in _t.overAnims ) {
				var container = key == 'main' ? _t.container : _t.container.getChildAt(key.replace('child',''));
				koko.display.animateObject(container, 0.2, _t.overAnims[key] || {});
			}
		};
		//mouse out
		this.mouseDown = function(e){
			if(_t.downCallback) _t.downCallback(e);
		}
		this.container.mousedown = function(e){
			
			_t.mouseDown(e);
		}
		
		this.mouseOut = function() {
			//document.body.style.cursor = 'auto';
			_t.isOver = false;
			for( var key in _t.outAnims ) {
				var container = key == 'main' ? _t.container : _t.container.getChildAt(key.replace('child',''));
				koko.display.animateObject(container, 0.2, _t.outAnims[key] || {});
				//koko.display.applyProps(container, _t.outAnims[key] || {});
			}
		};
		this.container.mouseout = function(){
			_t.mouseOut();
		}
		//mouse up
		//this.container.mouseup = this.container.mouseupoutside = this.container.touchend = this.container.touchendoutside = function(e){
		this.container.mouseup = this.container.touchend = function(e){
			//work out if click able by time
            if(_t.upCallback) _t.upCallback(e);
            
            if(!_t.isOver) return;
			if(_t.blockClick) {
				_t.blockClick = false;
				return;
			}
            
			var ct = new Date().getTime();
			if(ct > (_t.clickTime + buttonSettings.timeBetweenClicks)) {
				_t.clickTime = ct;
				//this click callback is in a setimeout to put it in its own namespace, as if a window is opened or alert is fired, the event doesnt return properly and it errors in firefox.
                if(_t.clickCallback) {
                   //setTimeout(function(){ _t.clickCallback(e); }, 0);
                   _t.clickCallback(e);
                }
			}
			//return all
			return false;
		}
		return this;
	},
	disable: function(){
		this.container.interactive = false;
		this.enabled = false;
		//mouseout
		if(this.isOver) this.mouseOut();
		//remove all event listeners
		var arr = ['touchendoutside', 'touchend', 'mouseupoutside', 'mouseup', 'mouseout', 'mouseover', 'mousedown'];
		for( var i = 0; i < arr.length; i++ ){
			this.container[arr[i]] = null;	
		}
		return this;
	},
	callback: function(callback) {
		this.clickCallback = callback;
		return this;	
	},
	down: function(callback) {
		this.downCallback = callback;
		return this;	
	},
	up: function(callback) {
		this.upCallback = callback;
		return this;	
	}
});

var buttonSettings = {
	timeBetweenClicks: 300,
};
if(koko.client.system.android && koko.client.engine.webkit < 535) buttonSettings.timeBetweenClicks = 1000;