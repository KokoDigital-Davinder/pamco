// JavaScript Document
var panview = koko.view({
	isDragging: false,
	blockDrag: false,
	setContent: function(container) {
		this.panContainer = new PIXI.DisplayObjectContainer();
		this.panContainer.addChild(container);
		this.container.addChild(this.panContainer);
	},
	setBoundaries: function(width, height){
		this.width = width*-1;
		this.height = height*-1;
	},
	pressMoveHandler: function(e, _t){
		if(!_t.isDragging || _t.blockDrag) return;
		
		if(_t.lastX == -100000) _t.lastX = e.global.x;
		if(_t.lastY == -100000) _t.lastY = e.global.y;

		var diffX = e.global.x - _t.lastX;
		var diffY = e.global.y - _t.lastY;

		var newX = _t.panContainer.x + diffX;
		var newY = _t.panContainer.y + diffY;

		if(newX < _t.width) newX = _t.width;
		if(newX > 0) newX = 0;

		if(newY < _t.height) newY = _t.height;
		if(newY > 0) newY = 0;
		
		_t.panContainer.x = newX;
		_t.panContainer.y = newY;

		_t.lastX = e.global.x;
		_t.lastY = e.global.y;
		
		if(_t.changeCallback) _t.changeCallback();
	},
	touchStartHandler: function(_t){
		//reset lastX
		_t.lastX = -100000;
		_t.lastY = -100000;
		_t.isDragging = true;
	},
	touchEndHandler: function(_t){
		//reset lastX
		_t.lastX = -100000;
		_t.lastY = -100000;
		_t.isDragging = false;
	},
	enable: function(eventContainer){
		var _t = this;
		this.eventContainer = eventContainer || this.container;
		this.eventContainer.mousedown = this.eventContainer.touchstart = function(){
			_t.touchStartHandler(_t);	
		}
		this.eventContainer.mouseup = this.eventContainer.mouseupoutside = this.eventContainer.touchend = this.eventContainer.touchendoutside = function(){
			_t.touchEndHandler(_t);
		}
		this.eventContainer.mousemove = this.eventContainer.touchmove = function(e){
			_t.pressMoveHandler(e, _t);
		}
		this.eventContainer.interactive = true;
		return this;
	}, 
	disable: function(){
		var arr = ['touchendoutside', 'touchend', 'touchmove', 'mouseupoutside', 'mouseup', 'mouseout', 'mouseover','mousemove'];
		for( var i = 0; i < arr.length; i++ ){
			this.eventContainer[arr[i]] = null;	
		}
		this.eventContainer.interactive = false;
		return this;
	},
	setupChangeHandler: function(func) {
		this.changeCallback = func;
	},
	gotoPerc: function(x, y, duration) {
		if( duration && duration > 0 ) {
			var _t = this;
			_t.blockDrag = true;	
			koko.display.animateObject(this.panContainer, duration, {x:this.width*x*-1, y:this.height*y*-1, 
				onUpdate: function(){if(_t.changeCallback) _t.changeCallback();}, 
				onComplete:function(){
				_t.blockDrag = false;	
			}});
		} else {
			this.panContainer.x = this.width*x*-1;
			this.panContainer.y = this.height*y*-1;
		}
	},
	gotoLocation: function(x, y, duration) {
		if( duration && duration > 0 ) {
			var _t = this;
			_t.blockDrag = true;	
			koko.display.animateObject(this.panContainer, duration, {x:x*-1, y:y*-1, 
				onUpdate: function(){if(_t.changeCallback) _t.changeCallback();}, 
				onComplete:function(){
				_t.blockDrag = false;	
			}});
		} else {
			this.panContainer.x = x*-1;
			this.panContainer.y = y*-1;
		}
	}
});