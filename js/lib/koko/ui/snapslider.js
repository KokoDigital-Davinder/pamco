koko.class('snapslider', '', {
	isDragging: false,
    axis:'x',
    snapslider: function(container, axis) {
        if(container) this.setContent(container);
        if(axis) this.axis = axis;
    },
	setContent: function(container) {
		this.slideContainer = container;
		return this;
	},
	pressMoveHandler: function(e){
		if(!this.isDragging) return;
		//first move handler?
		if(this.lastX == -100000) this.lastX = e.global[this.axis];
		//diff
		var diff = e.global[this.axis] - this.lastX;
		
		//new position
		var newX = m.clamp(this.slideContainer[this.axis] + diff, this.farX, 0 );
		//update vars
		this.direction = diff;
		this.slideContainer[this.axis] = newX;
		this.lastX = e.global[this.axis];
		
		//changed
		if(this.changeCallback) this.changeCallback();
	},
	touchStartHandler: function(){
		//reset lastX
		this.lastX = -100000;
		this.isDragging = true;
	},
	touchEndHandler: function(){
		//snap
		var slide = Math.abs(this.slideContainer.position[this.axis])/this.snapWidth;
		slide = this.direction < 0 ? Math.ceil(slide) : Math.floor(slide);	
		//set slide
		this.currentSlideIndex = slide;
		//goto slide
		this.gotoSlide(this.currentSlideIndex, .2);
		//stop dragging
		this.lastX = -100000;
		this.isDragging = false;
	},
	enable: function(eventContainer){
		var _t = this;
		this.currentSlideIndex = 0;
		this.eventContainer = eventContainer || this.slideContainer;
		this.eventContainer.mousedown = this.eventContainer.touchstart = function(e){
			_t.touchStartHandler();	
		}
		this.eventContainer.mouseup = this.eventContainer.mouseupoutside = this.eventContainer.touchend = this.eventContainer.touchendoutside = function(){
			_t.touchEndHandler();
		}
		this.eventContainer.mousemove = this.eventContainer.touchmove = function(e){
			_t.pressMoveHandler(e);
		}
		this.eventContainer.interactive = true;
		return this;
	}, 
	disable: function(){
		var arr = ['touchendoutside', 'touchend', 'touchmove', 'mouseupoutside', 'mouseup', 'mouseout', 'mouseover','mousemove'];
		for( var i = 0; i < arr.length; i++ ){
			this.eventContainer[arr[i]] = null;	
		}
		this.eventContainer.interactive = false;
		return this;
	},
	setSnapping: function(width, count){
        if(count < 1) count = 1;
        
		this.snapWidth = width;
		this.totalSlides = count-1;
		this.farX = (width*(count-1)*-1);
        
		if(this.changeCallback) this.changeCallback();
		if(this.changeCompleteCallback) this.changeCompleteCallback();
		return this;
	},
	gotoSlide: function(index, speed) {
		
		if(index < 0){
			index = 0;
		}
		if(index > this.totalSlides){
			index = this.totalSlides;
		}
		
		this.currentSlideIndex = index;
		var xPos = ( this.snapWidth * index * -1 );
		
		TweenLite.killTweensOf(this.slideContainer.position);
		
		if(speed) {
			var _t = this;
            var animVars = { ease: Power2.easeIn, onUpdate:function(){
				if(_t.changeCallback) _t.changeCallback();
			}, onComplete: function(){
				if(_t.changeCompleteCallback) _t.changeCompleteCallback();	
			}};
            animVars[this.axis] = xPos;
			TweenLite.to(this.slideContainer.position, speed, animVars);
		} else {
			this.slideContainer[this.axis] = xPos;
			if(this.changeCallback) this.changeCallback();
			if(this.changeCompleteCallback) this.changeCompleteCallback();	
		}
		return this;
	},
	gotoNextSlide: function(speed) {
		if(this.currentSlideIndex > 0) {
			this.currentSlideIndex-=1;
			this.gotoSlide(this.currentSlideIndex, speed);
		}
		return this;
	},
	gotoPreviousSlide: function(speed){
		if(this.currentSlideIndex < this.totalSlides) {
			this.currentSlideIndex+=1;
			this.gotoSlide(this.currentSlideIndex, speed);
		}
		return this;
	},
	atStart: function(){
		return this.currentSlideIndex == 0;
	}, 
	atEnd: function(){
		return this.currentSlideIndex == this.totalSlides;
	},
	setupChangeHandler: function(func) {
		this.changeCallback = func;
	},
	setupChangeCompleteHandler: function(func) {
		this.changeCompleteCallback = func;	
	}
});