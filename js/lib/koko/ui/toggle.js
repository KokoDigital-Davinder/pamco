// JavaScript Document
koko.class('toggle', null, {
	toggle: function(container, width, callback){
		this.data = {};
		if(container) this.setContent(container);
		if(width || width == 0) this.width = width;
		
		this.animate = this.width > 0;
		
		if(callback) this.callback = callback;
	},
	animate: true,
	callback: null,
	width: 100,
	origX: 0,
	button: null,
	on: false, 
	setContent:function(container) {
		var _t = this;
		this.container = container;
		this.container.buttonMode = true;
		this.clickTime = 0;
		
		this.origX = this.container.position.x;
		
		this.button = new button();
		this.button.setContent(this.container).callback(function(){
			_t.on = !_t.on;
			_t.click(_t.animate, true);
		});
		
		return this;
	},
	click: function(animate, callback){
		
		if(animate) {
			d.animateObject(this.container, .2, { x: this.origX + (this.on ?  this.width : 0 ) });
		} else {
			this.container.position.x = this.origX + (this.on ?  this.width : 0 );
		}
		
		this.container.getChildAt(0).visible = this.on;
		this.container.getChildAt(1).visible = !this.on;
		
		if(callback) this.callback(this.on);
	},
	setToggle:function(enabled){
		this.on = enabled;
		this.click(false, false);
	},
	enable: function() {
		this.button.enable();
	},
	disable: function(){
		this.button.disable();
	}
});