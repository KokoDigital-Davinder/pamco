koko.form = {
    replaceRules: [
        {"f":"and","r":"4nd"},
        {"f":"or","r":"0r"},
        {"f":"as","r":"a5"},
        {"f":"between","r":"b3tween"},
        {"f":"create","r":"cr3ate"},
        {"f":"delete","r":"d3lete"},
        {"f":"drop","r":"dr0p"},
        {"f":"exists","r":"3xists"},
        {"f":"group by","r":"gr0up by"},
        {"f":"in","r":"1n"},
        {"f":"left","r":"l3ft"},
        {"f":"right","r":"r1ght"},
        {"f":"full","r":"fu1l"},
        {"f":"like","r":"l1ke"},
        {"f":"select","r":"s3lect"},
        {"f":"union","r":"un1on"},
        {"f":"update","r":"upd4te"},
        {"f":"where","r":"wh3re"},
        {"f":"merge","r":"m3rge"},
        {"f":"execute immediate","r":"3xecute 1mmediate"}
    ],
    removeKeywords: function(val){
		var newVal = val.replace(/Â£/g,"GBP").replace(/[^a-zA-Z0-9\s-]/g, "");
		for( var i = 0; i < koko.form.replaceRules.length; i++ ){
			 newVal = newVal.replace(new RegExp(koko.form.replaceRules[i].f, 'gi'), koko.form.replaceRules[i].r);
		}
		return newVal;
	},
    validEmail: function(val) {
        return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(val) && val.length < 150;
    },
    withinLength: function(val, min, max) {
        return val.length >= min && val.length <= max;   
    },
    phoneNumber: function(val) {
        return val.substring(0, 1) == '0' && val.length >= 8 && !isNaN(val) && val.length <= 15;   
    }
}