koko.assets = {
	queue: null,
	pool: {},
	manifest: [],
	init: function(){ },
	loadManifest: function(progress, callback) {
        if(koko.assets.manifest.length == 0)
		{
			callback();
		} 
		else 
		{
            var crossorigin = true;
            if(koko.client.engine.ie > 0 && koko.client.engine.ie < 11) crossorigin = false;
            koko.assets.queue = new PIXI.AssetLoader(koko.assets.manifest, crossorigin);

            koko.assets.queue.addEventListener('onComplete', function(){
                koko.assets.clearManifest();
                callback();	
                koko.assets.queue = null;	
            });


            koko.assets.queue.addEventListener('onProgress', function(e){
                var perc = 1 - (e.content.content.loadCount / e.content.content.assetURLs.length); 
                progress(perc);
                
            });
		
			koko.assets.queue.load();
		}
	},
	getAssetURL:function(id){
		return koko.assets.pool[id];
	},
	destroyAsset: function(id) {
		if(!koko.assets.pool.hasOwnProperty(id)) return;
		PIXI.Texture.removeTextureFromCache(koko.assets.getAssetURL(id));
		delete koko.assets.pool[id];
	},
	clearManifest: function(){
		koko.assets.manifest = [];	
	},
	addToManifest: function(manifest){
		for( var i = 0; i < manifest.length; i++ ){
			var src = manifest[i].src;
			koko.assets.manifest.push(src);
			koko.assets.pool[ manifest[i].id ] = src;
		}
	}
}
