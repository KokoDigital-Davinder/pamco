// JavaScript Document
var APP_STATE = {
	MENUS: 0,
	GAME: 1
}
var vm = {
	currentState: APP_STATE.MENUS,
	init: function(){
		vm.setupViews();
		vm.setupAudio();
		vm.setupComplete();
	},
	setupViews: function(){
		
		v.add('background', 		new BackgroundView(), 		app.game_stage);
		v.add('ingame', 			new InGameView(), 			app.game_stage);
		v.add('ingamehud', 			new InGameHUDView(), 		app.game_stage);
		v.add('mainmenu', 			new MainMenuView(), 		app.game_stage);
		v.add('endthrow', 			new EndThrowView(), 		app.game_stage);
		v.add('endgame', 			new EndGameView(), 			app.game_stage);
		v.add('characterselect',	new CharacterSelectView(),	app.game_stage);
		v.add('congratulations',	new CongratulationsView(),	app.game_stage);
		
		
		v.add('leaderboard', 		new LeaderboardView(), 		app.game_stage);
		v.add('settings',			new SettingsView(),			app.game_stage);
		v.add('winprizes',			new WinPrizesView(),		app.game_stage);
		v.add('preloader', 			new PreloaderView(), 		app.game_stage);
	},
	setupAudio: function() {
		//audio folder
		au.dir = 'audio/';
		//create groups
		//au.createGroup('music');
		//au.createGroup('sfx');
		au.addSoundGroup('music', game.music);
		au.addSoundGroup('sfx', game.sfx);

		//au.add(name, url, volume, multiInstance, group);
		var fileExt = '.mp3';
		//if(koko.client.system.macMobile || koko.client.system.android) fileExt = '.ogg';
		//music
		au.add("music_sound", 		'music', 	0.75, 		"Beach_Party"+ fileExt);
		au.add("swoosh_1", 			'sfx', 		1, 		"swoosh_four"+ fileExt);
		au.add("swoosh_2", 			'sfx', 		1, 		"swoosh_two"+ fileExt);
		au.add("bounce", 			'sfx', 		0.7,	"Boing"+ fileExt);
		au.add("pop_1", 			'sfx', 		1, 		"pop_1"+ fileExt);
		au.add("pop_2", 			'sfx', 		1, 		"pop_2"+ fileExt);
		au.add("foul_throw", 		'sfx', 		0.5, 		"foul_throw"+ fileExt);
		au.add("new_pb", 			'sfx', 		1, 		"newpb"+ fileExt);
		
		au.add("beachball_sfx", 	'sfx', 		1, 		"beachball"+ fileExt);
		au.add("hammock_sfx", 		'sfx', 		1, 		"hammock"+ fileExt);
		au.add("lilo_sfx", 			'sfx', 		1, 		"lilo"+ fileExt);
		au.add("parasol_sfx", 		'sfx', 		1, 		"parasol"+ fileExt);
		au.add("rockpool_sfx", 		'sfx', 		1, 		"rockpool"+ fileExt);
		au.add("sandcastle_sfx", 	'sfx', 		1, 		"sandcastle"+ fileExt);
		//au.add("sunbed_sfx", 		'sfx', 		1, 	"sunbed"+ fileExt);
	},
	setupComplete: function(){
		//load the preloader graphics, before showing anything
		v.get('preloader').queueAssets();		
		
		a.loadManifest(function(p){
			//LOADING PROGRESS EVENT
		}, function() {			
		
			//queue assets from all other menus
			v.each([
				'background',
				'ingame',
				'ingamehud',
				'mainmenu',
				'characterselect',
				'settings',
				'winprizes',
				'leaderboard'
			], 'queueAssets');
			
			//show the preloader
			v.get('preloader').transitionIn();
			
			//load them sounds yo
			au.loadSounds();
		});
	}
}
