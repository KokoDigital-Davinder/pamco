var PannableView = (function() {
	//extends view
	PannableView.prototype = new View();
    //constructor
    function PannableView(content, width, height) { 
		this.container = new createjs.Container();
		this.content = content;
		this.container.addChild(this.content);
		this.setupMask(width, height);
		this.bmpWidth  = this.content.getBounds().width;
		this.bmpHeight = this.content.getBounds().height;
		this.setCenter();
	};
	
	PannableView.prototype.setupMask = function(width, height) {
		this.width = width;	
		this.height = height;
		
		if(!this.shapeMask) {
			this.shapeMask = new createjs.Shape();
		} else {
			this.shapeMask.graphics.clear();	
		}
		this.shapeMask.graphics = new createjs.Graphics().beginFill( 'rgba(0,0,0,1)' ).rect(0,0,width,height);
		this.content.mask = this.shapeMask
		//this.container.addChild(this.shapeMask);
	}
	
	PannableView.prototype.setCenter = function() {
		this.updatePosition(0.5, 0.5);
	}
	
	PannableView.prototype.updatePosition = function(x, y) {
		this.content.x = -(this.bmpWidth-this.width)*x;
		this.content.y = -(this.bmpHeight-this.height)*y;
	}
	
	PannableView.prototype.updatePositionPixels = function(x, y) {
		this.content.x = x;
		this.content.y = y;
	}
	
	//return constructor
    return PannableView;
})();