// JavaScript Document
/// x, y, gravity, spawn angle, angle offset, limit
/// angles in radians
function Emitter(opts) {

    var me = this,
        particles = [];
	
    this.limit = opts.limit || 1000;
    this.x = opts.x || 0;
    this.y = opts.y || 0;
    this.gravity = opts.gravity || 0;
    this.angle = (parseInt(opts.angle, 10) / 180 * Math.PI) || 2 * Math.PI;
    this.angleOffset = (parseInt(opts.angleOffset, 10) / 180 * Math.PI) || 0;
	
    this.count = 0;
	this.bitmapID = opts.bitmapID;
	this.container = opts.container;
    
    this.add = function(v, w, life) {

        var p = new Particle(me.bitmapID, me.container);
		p.bit.x = p.x = me.x;
		p.bit.y = p.y = me.y;
        p.life = life || -1;
        p.velocity = v;
        p.weight = w * (me.gravity / 1000);
        p.angle = getAngle();
        p.init();
        
        particles.push(p);

        if (particles.length > me.limit) {
            particles.splice(0, (particles.length - me.limit));
        }
        me.count = particles.length;
    }
	
	this.destroy = function() {
		particles = [];
	}
    
    this.update = function() {
        
        var i = 0, p,
            live = [];
        
        for(; p = particles[i]; i++) {
            p.update();
			p.bit.y = p.y;
			p.bit.x = p.x;
			if (p.life !== 0) {
				live.push(p);
			} else {
				p.destroy();	
			}
        }

        particles = live;
        me.count = particles.length;
    }

    function getAngle() {
        return me.angle * Math.random() + me.angleOffset;
    }
}

/// velocity, weight
function Particle(id, cont) {
	this.bit = new createjs.Bitmap( AssetManager.assets.getResult(id) );
	this.cont = cont;
	cont.addChild(this.bit);
}    
Particle.prototype.update = function() {
    this.stepY += this.weight;
    this.x += this.stepX;
    this.y += this.stepY;
	//console.log(this.x, this.y);
    if (this.life > -1) this.life--;
}
Particle.prototype.init = function() {
    this.stepX = this.velocity * Math.cos(this.angle);
	//console.log(this.stepX);
    this.stepY = this.velocity * Math.sin(this.angle);
}

Particle.prototype.destroy = function() {
  	this.cont.removeChild(this.bit);
	this.bit = null;
}
Particle.prototype.life = 1000;
Particle.prototype.x = 0;
Particle.prototype.y = 0;
Particle.prototype.stepX = 0;
Particle.prototype.stepY = 0;
Particle.prototype.velocity = 0;
Particle.prototype.weight = 0;
Particle.prototype.angle = 0;


function getRandom(min, max) {
    min = parseInt(min, 10);
    max = parseInt(max, 10);
 
    if (min > max) min = max;
    return (max - min) * Math.random() + min;
}