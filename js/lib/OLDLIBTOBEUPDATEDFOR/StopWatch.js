// JavaScript Document
var StopWatch = function() {
	this.timeElapsed = 0;
	this.startTime = 0;
	this.active = false;
	
	this.totalPauseTime = 0;
	this.pausedAt = 0;
	this.isPaused = true;

	this.reset = function(){
		this.totalPauseTime = 0;
		this.pausedAt = 0;
		this.isPaused = true;
		this.timeElapsed = 0;
		this.startTime = 0;
		this.active = false;
	}
	
	this.start = function(){
		this.startTime = new Date().getTime();
		this.active = true;
		this.isPaused = false;
	}
	
	this.pause = function(){
		this.isPaused = true;
		this.pausedAt = new Date().getTime();
	}
	
	this.resume = function(){
		this.isPaused = false;
		this.totalPauseTime += new Date().getTime() - this.pausedAt;
	}
	
	this.stop = function(){
		this.update();
		this.active = false;
	}
	
	this.update = function(){
		if(!this.active || this.isPaused) return;
		this.time = new Date().getTime() - this.startTime;
		this.timeElapsed = this.time - this.totalPauseTime;
	}
	
	this.getFormattedString = function(){
		return this.formatTime(this.timeElapsed);
	}
	
	this.formatTime = function(time) {
		var string = (Math.floor( time / 10 ) / 100) + '';
		if(string.indexOf('.') == -1) {
			string += '.00';
		} else {
			var ms = (string.split('.'))[1];
			if(ms.length == 1) string += '0';
		}
		return string;	
	}
}