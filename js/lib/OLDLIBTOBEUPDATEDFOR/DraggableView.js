var DraggableView = (function() {
	//extends view
	DraggableView.prototype = new View();
    //constructor
    function DraggableView() { 
		this.container = new createjs.Container();
		this.defaultX = 0;
		this.defaultY = 0;	
		this.accuracy = 30;
		this.mouseOffset = { x:0, y:0 };
		this.snapPoints = [];
		this.enabled = true;
		this.data = {};
		this.holding = false;
		this.includeMouseOffsetInFinalCalc = true;
		
		(function(_t) {
			_t.container.on('mousedown', function(evt) {
				if(!_t.enabled) return;
				_t.select(evt);
				_t.holding = true;
			});
			_t.container.on("pressmove", function(evt) {
				if(!_t.enabled) return;
				_t.drag(evt);
			});
			CanvasViewport.canvas.addEventListener(Client.touch() ? 'touchend' : 'mouseup', function(e){
				if(!_t.enabled || !_t.holding) return;
				_t.drop(e);		
				_t.holding = false;									  
			});
			CanvasViewport.canvas.addEventListener(Client.touch() ? 'touchleave' : 'mouseout', function(e){
				if(!_t.enabled || !_t.holding) return;
				_t.drop(e);
				_t.holding = false;
				
				/*setTimeout(function(){
					_t.holding = true;
					_t.drop(e);
					_t.holding = false;
				}, 1000);*/
			});
			//ie fix for no mouse up event on canvas!
			if(Modernizr.ie) {
				CanvasViewport.canvas.attachEvent("onclick", function(e){
					if((!_t.enabled || _t.done) && !_t.holding) return;
					_t.drop(e);			
					_t.holding = false;										  
				});
			}
		})(this);
	};
	
	DraggableView.prototype.select = function(e) {
		this.mouseOffset.x = 0;
		this.mouseOffset.y = 0;
	
		if(this.selectCallback) this.selectCallback(this);
		
		this.container.x = e.stageX + this.mouseOffset.x;
		this.container.y = e.stageY + this.mouseOffset.y
		
		
	}
	
	DraggableView.prototype.drag = function(e) {
		this.container.x = e.stageX+this.mouseOffset.x;
		this.container.y = e.stageY+this.mouseOffset.y;
		if(this.draggingCallback) this.draggingCallback(this);
	}
	
	
	DraggableView.prototype.getClosestPointWithinRange = function() {
		var pinsWithinRange = [];
		for( var i = 0; i < this.points.length; i++ ) {
			var p = this.points[i];
			var point = { x: this.container.x, y: this.container.y };
			if(this.includeMouseOffsetInFinalCalc) {
				point.x -= this.mouseOffset.x;
				point.y -= this.mouseOffset.y;
			}
			var dist = MathUtils.pointDistance(point, p );
			if(dist < this.accuracy) {
				//if they'vew matched
				pinsWithinRange.push([i, dist]);
			}
		}
		
		if(pinsWithinRange.length > 0) {
			//look for closest
			var closest = [0, 100000];
			for( var i = 0; i < pinsWithinRange.length; i++ ) {
				if(pinsWithinRange[i][1] < closest[1]) closest = pinsWithinRange[i];
			}
			return closest[0];
		} else {
			return null;
		}
	}
	
	DraggableView.prototype.drop = function(e) {
		//console.log(this.container.x, this.container.y);
		if(!this.holding) return;			
		var closest = this.getClosestPointWithinRange();

		if(closest != null) {
			this.enabled = false;
			this.callback( closest, this );	
		} else {
			this.resetPosition();
		}
	}
	
	DraggableView.prototype.addContainer = function(container) {
		this.container.addChild(container);
	}
	
	DraggableView.prototype.setupPoints = function(points) {
		this.points = points;
	}
	
	DraggableView.prototype.addCallback = function(callback) {
		this.callback = callback;
	}
	
	DraggableView.prototype.addDraggingCallback = function(callback) {
		this.draggingCallback = callback;
	}
	
	DraggableView.prototype.setAccuracy = function(pixels) {
		this.accuracy = pixels;	
	}

	DraggableView.prototype.hideDisable = function(){
		this.enabled = false;
		this.container.visible = false;
	}
	
	DraggableView.prototype.disable = function(){
		this.enabled = false;
	}
	
	DraggableView.prototype.enable = function(){
		this.enabled = true;
	}
	
	DraggableView.prototype.showEnable = function(){
		this.enabled = true;
		this.container.visible = true;	
	}
	
	DraggableView.prototype.setDefaultPosition = function(x, y) {
		this.defaultX = x;
		this.defaultY = y;
		this.container.x = x;
		this.container.y = y;
	}
	
	
	DraggableView.prototype.setPosition = function(x, y) {
		this.container.x = x;
		this.container.y = y;
	}
	
	DraggableView.prototype.resetPosition = function() {
		if(this.resetCallback) this.resetCallback(this);
		this.container.x = this.defaultX;
		this.container.y = this.defaultY;
	}
	
	//return constructor
    return DraggableView;
})();