/***************************************************************************************************
KOKO USER CLASS
***************************************************************************************************/
koko.class('WebAPI', '', {
    WebAPI: function(){
        this.getToken()
    },
    
    token:'',
    tokenRetries:0,
    apiDomian:'http://pamco.kokogames.com/api',
    scoreList:[],
  
    getToken: function(complete){
      if(app.tokenRetries >= 3) return;
        $.ajax({
			dataType:'json', 
			type: 'GET',
			url: this.apiDomian + "/token", 
            crossDomain: true,
            xhrFields: { withCredentials: true },
			success: function( data ) {
				WebAPICall.token = data.response.token; 
                
                //console.log('TOKEN', WebAPICall.token);
                if(!user.guid  || user.guid ==''){
                    user.guid = data.response.guid;
                    //user.save();
                }
                
                if(complete) complete(); 
			}
		});
	},
    getScores: function(complete, error){
    	console.log('GET SCORES')
        $.ajax({
			dataType:'json', 
			type: 'GET',
			url: WebAPICall.apiDomian + "/scores/alltime", 
            crossDomain: true,
            xhrFields: { withCredentials: true },
			success: function( data ) {
                
                WebAPICall.scoreList = data.response.scores;
                if(complete) complete(); 
			},
			error: function( data ) {
				if(error) error();
			}
		});
	},
	submitScore: function(complete,name,score, company, email){
         console.log("ATEMPT SUBMIT", this.token);
		if(WebAPICall.token == null || WebAPICall.token.length < 4) return;
        console.log("SUBMITTING");
		var s = Math.floor(user.score).toString();
               console.log('SUBMIT SCORE', name, score, company, email)
		$.ajax({
			dataType:'json', 
			type: 'POST',
			url: WebAPICall.apiDomian + "/scores", 
            crossDomain: true,
            xhrFields: { withCredentials: true },
			data: JSON.stringify({
				"token": md5(WebAPICall.token.substr(8,15)+s+WebAPICall.token.substr(3,7)+s+WebAPICall.token.substr(5,3)),
				"player_score": score,
				"player_full_name": name,
				"player_email": email,
				"player_company": company,
                "player_type": 1
			}), 
			success: function( data ) {
				WebAPICall.tokenRetries = 0;
				if(complete) complete();
			},
			error: function( data ) {
				WebAPICall.tokenRetries++;
				if(data.responseJSON.error.code == '401') 
                {
				    WebAPICall.getToken(function()
                    {
				        WebAPICall.submitScore(complete);
					});
				}
			}
		});
	},
});
var WebAPICall = new WebAPI();