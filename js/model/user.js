// JavaScript Document
koko.class('userModel', null, {
	cookieName: 'loungeLeap0.1',
	gender:'unknown',
    music: 1,
    sfx:1, 
	userModel: function(){
	},
    getSaveString: function(){
        var str = "";
		for( var i = 0; i < levels.length; i++ ) {
			var l = levels[i];
			str += Math.round(l.highscore) + '-';
			for( var j = 0; j < l.achievements.length; j++ ){
				var a = l.achievements[j];
				if(a.completed) str += a.id + ",";	
			}
			str = str.substring(0, str.length-1);
			str += '||';
		}
		str = str.substring(0, str.length-2);
        str = str + "##" + this.gender;
        str = str + "##" + this.music + "##" + this.sfx;
        return str;
    },
	save: function(){
		koko.cookies.set(this.cookieName, this.getSaveString());
	},
    loadFromString: function(c){
        if(!c) {
			levels[0].locked = false;
			return;
		}
        
        var data = c.split('##');
        
		var lData = data[0].split('||');
		var shouldOpen = true;
		for( var i =0; i < lData.length; i++ ){
			if(shouldOpen) {
				levels[i].locked = false;
				shouldOpen = false;	
			}
			var l = lData[i];
			if(l == '0') continue;
			var d = l.split('-');
			levels[i].highscore = d[0] > levels[i].highscore ? d[0] : levels[i].highscore;
			if(d.length == 2) {
				for( var j = 0; j < levels[i].achievements.length; j++ ){
					var a = levels[i].achievements[j];
					if(d[1].indexOf(a.id) > -1) {
						shouldOpen = true;	
						a.completed = true;
					}
				}
			}
		}
        //level special collectable updates
        if(levels[0].achievements[1].completed && levels[0].specialCollectableTypes.indexOf('coffee') > -1) levels[0].specialCollectableTypes.splice(levels[0].specialCollectableTypes.indexOf('coffee'), 1);
        if(levels[1].achievements[0].completed && levels[1].specialCollectableTypes.indexOf('news') > -1) levels[1].specialCollectableTypes.splice(levels[1].specialCollectableTypes.indexOf('news'), 1);
        if(levels[1].achievements[2].completed && levels[1].specialCollectableTypes.indexOf('chocolate') > -1) levels[1].specialCollectableTypes.splice(levels[1].specialCollectableTypes.indexOf('chocolate'), 1);
        if(levels[2].achievements[1].completed && levels[2].specialCollectableTypes.indexOf('binoculars') > -1) levels[2].specialCollectableTypes.splice(levels[2].specialCollectableTypes.indexOf('binoculars'), 1);
        if(levels[3].achievements[0].completed && levels[3].specialCollectableTypes.indexOf('ticket') > -1) levels[3].specialCollectableTypes.splice(levels[3].specialCollectableTypes.indexOf('ticket'), 1);
        if(levels[3].achievements[1].completed && levels[3].specialCollectableTypes.indexOf('case') > -1) levels[3].specialCollectableTypes.splice(levels[3].specialCollectableTypes.indexOf('case'), 1);
        //gender
        this.gender = data[1];
        this.music = data[2];
        this.sfx = data[3];
    },
	load: function(){
		var c = koko.cookies.get(this.cookieName);
		this.loadFromString(c);
	}
});
var user = new userModel();
var currentView = 'not defined';
var recentScore = 'nnnnnnnnot defined';