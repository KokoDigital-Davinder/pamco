// JavaScript Document
koko.class('levelModel', null, {
	id: 0,
	assetRef: '',
	name: '',
    bg: { ss:'', bottom: '', itms: [] },
    setBG: function(ss, bottom, itms) {
        this.bg = {
            ss: ss,
            bottom: bottom,
            itms: itms
        };
    },
	achievements: null,
    collectableTypes: null,
    specialCollectableTypes: null,
	createAchievement: function(id, title, data) {
		this.achievements.push(new achievement(id, title, data));
	},
	locked: true,
	highscore: 0,
	levelModel: function(id, name, assetRef) {
		this.achievements = [];
        this.collectableTypes = [];
		this.id = id;
		this.name = name;
		this.assetRef = assetRef;
	},
    setCollectables: function(standard, special) {
        this.collectableTypes = standard;   
        this.specialCollectableTypes = special;
    }
});

//levels
var la = new levelModel(0, 'Level 1', 'l'); //lounge
la.createAchievement('a', "Reach 100m", { type:'height', value:100 });
la.createAchievement('b', "Collect 4 x ", { type:'collect', value:4, icon: 'nmtb_card', id:'coffee' }); //DAVINDER try to replace textures here
la.createAchievement('c', "Reach 500m", { type:'height', value:500 });
la.setCollectables(['lounge_coll'], ['coffee']);

var lb = new levelModel(1, 'Level 2', 't'); //train
lb.createAchievement('d', "Collect 2 x ", { type:'collect', value:2, icon: 'iPadCollectable', id:'news' });
lb.createAchievement('e', "Reach 300m", { type:'height', value:300 });
lb.createAchievement('f', "Collect 6 x ", { type:'collect', value:6, icon: 'membership_card', id:'chocolate' });
lb.setCollectables(['train_coll'], ['news', 'chocolate']);

var lc = new levelModel(2, 'Level 3', 'b'); //balloons
lc.createAchievement('g', "Reach 300m", { type:'height', value:300 });
lc.createAchievement('h', "Collect 6 x ", { type:'collect', value:6, icon: 'cassette_card', id:'binoculars' });
lc.createAchievement('i', "Reach 600m", { type:'height', value:600 });
lc.setCollectables(['ball_coll'], ['binoculars']);

var ld = new levelModel(3, 'Level 4', 'a'); //atlantic
ld.createAchievement('j', "Collect 4 x ", { type:'collect', value:4, icon: 'spray_card', id:'ticket' });
ld.createAchievement('l', "Collect 8 x ", { type:'collect', value:8, icon: 'membership_card', id:'case' });
ld.createAchievement('k', "Reach 750m", { type:'height', value:750 });

ld.setCollectables(['atlantic_coll'], ['case', 'ticket']);

var levels = [la, lb, lc, ld];

la.locked = false;
/*lb.locked = false;
lc.locked = false;
ld.locked = false;*/

la.setBG('backgroundSky', 'Menus_backgroundstatic', ['backgroundClouds1', 'backgroundClouds2', 'backgroundClouds3']);
lb.setBG('background_train', 'bottom_trainstation', ['train_lights', 'train_map.jpg', 'train_times.jpg', 'train_welcome.jpg', 'train_windows']);
lc.setBG('background_balloon', 'bottom_balloon', ['section_balloons_one', 'section_balloons_three', 'section_balloons_two', 'section_fou']);
ld.setBG('background_airport', 'bottom_airport', ['airport_section_one', 'airport_section_three', 'airport_section_two', 'airport_section_four']);