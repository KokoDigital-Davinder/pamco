// JavaScript Document
koko.class('achievement', null, {
	id: 0,
	title: '',
	completed: false,
	justUnlocked: false,
    data: null,
	achievement: function(id, title, data){
		this.id = id;
		this.title = title;
        this.data = data;
	},
	unlock: function(){
		if(!this.completed) this.justUnlocked = true;
		this.completed = true;
	}
});