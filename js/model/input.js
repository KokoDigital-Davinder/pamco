koko.class('InputText', null, {
	content: null,
	defaultVal: '',
	inp: null,
	enabled: false,
	blinker: null,
	parent: null,
	view: null,
	origViewY:0,
	focused: false,
	width:0,
	mobileMove:0,
	val:'',
	clearOnFocus: false,
	InputText: function(field, view, defaultVal, mobileMove, events){ 
		this.content = field;

		this.container = new PIXI.Container();
		this.container.x = this.content.x;
		this.container.y = this.content.y;

		this.mobileMove = mobileMove;
		this.origViewY = view.container.y;

		this.content.x = this.content.y = 0;
		this.content.parent.addChild(this.container);
		this.container.addChild(this.content);
		
		this.defaultVal = defaultVal;
		this.events = events;
		
		this.view = view;
		var h = this.content.hitArea;
		this.width = h.width;
		koko.display.mask(this.container, h.x, h.y, h.width, h.height);
		
		this.blinker = new PIXI.Graphics();
		this.drawBlinker();
		
		this.inp = document.getElementById('input');
	
		this.parent = this.container.parent;
	},
	cleanText: function(txt) {
		return txt;
		//return txt.replace(/([\uE000-\uF8FF]|\uD83C[\uDF00-\uDFFF]|\uD83D[\uDC00-\uDDFF])/g, '');	
		//return txt.replace(/[\u1f600-\u1f64f]/g, '');
	},
	drawBlinker: function(){
		this.blinker.clear();
		this.blinker.beginFill(this.content.tint);
		this.blinker.drawRect(0, 0, 2, 30);
		this.blinker.endFill();
	},
	setValue: function(value){
		this.val = value;
		this.content.text = value;
		this.content.updateText();
	},
	lastClickTime: 0,
	enable: function(){
		if(this.enabled) return;
		this.enabled = true;
		
		var boundDown = function(e) {
			
			var thisClickTime = new Date().getTime();
			if(thisClickTime - this.lastClickTime < 500 && koko.client.engine.webkit < 535 && koko.client.system.isMobile) return;
			
			this.lastClickTime = thisClickTime;
			
			if(koko.client.engine.webkit < 535) { 
				koko.timer(this.focus, 350, this, e);
			} else {
				this.focus(e);
			}
		}.bind(this);
				
		this.content.on('mousedown', boundDown);
		this.content.on('touchstart', boundDown);
	
		this.content.interactive = true;
	},
	disable: function(){
		if(!this.enabled) return;
		this.enabled = false;
		
		this.content.off('mousedown');
		this.content.off('touchstart');
	},
	setBlinkerPosition: function(pos){
		pos /= app.viewPort.s;
		for( var i = 1; i < this.content.children.length; i++ ){
			if(this.content.children[i].position.x + this.content.children[i].width > pos) {
				if(this.content.children[i].position.x + (this.content.children[i].width*0.5) > pos) i--;
				this.setBlinkerIndex(i, true);
				return;
			}
		}
		this.setBlinkerIndex(i, true);
	},
	setBlinkerIndex: function(index, set){
		
		if(index <= 0) {
			this.blinker.x = 1;
		} else {
			if(index >= this.content.children.length) index = this.content.children.length-1;
			this.blinker.x = this.content.children[index].x + this.content.children[index].width + 1;
		}
		var pad = 20;
		if(this.blinker.x > (this.width - this.content.x - pad)) {
			this.content.x = this.width - this.blinker.x - pad;
		} 
		else if((this.blinker.x - pad) < Math.abs(this.content.x))
		{
			this.content.x = (this.blinker.x * -1 ) + pad;
		}
		if(this.content.x > 0 || this.content.width < this.width) this.content.x = 0;

		if(set) {
			this.inp.setSelectionRange(index, index);
		}
	},
	focus: function(e){
		if(this.events && this.events.focus) this.events.focus.call(this, this);
		
		if(this.content.text == this.defaultVal || (this.clearOnFocus && !this.focused)) {
			this.content.text = '';
			this.content.updateText();
		}
		this.drawBlinker();
		
		//set blinker position
		this.content.addChildAt(this.blinker, 0);
		var localX = 0;
		if(e) { 
			localX = e.data.global.x - this.content.worldTransform.tx;
		}
		this.setBlinkerPosition(localX);
		
		if(this.focused) return;
		this.focused = true;
		
		app.blockResize = true;

		//set value
		this.inp.value = this.content.text == ' ' ? '' : this.content.text;		
		this.inp.focus();
		
		//add blinker
		if(koko.client.system.isMobile) {	
			this.view.container.y = this.origViewY + this.mobileMove;
		}
	
		this.inp.onkeyup = function(e){
			this.update();
		}.bind(this);
		
		this.inp.onkeydown = function(e){
			if(e.keyCode == 13) {
				this.blur();
				if(this.events && this.events.return) this.events.return.call(this, this);
				e.preventDefault();
			} else if(e.keyCode == 9){
				this.blur();
				if(this.events && this.events.tab) this.events.tab.call(this, this);
				e.preventDefault();
			} else {
				this.update();
			}
		}.bind(this);
		
		this.inp.onblur = function(e){
			this.blur();
		}.bind(this);
		
		this.blinkIndex = 0;
		app.addUpdateListener('input', this.blink.bind(this));
		
		app.stage.interactive = true;
		app.stage.touchstart = app.stage.mousedown = function(r){
			this.checkClick(r);
		}.bind(this)
	},
	checkClick: function(e){
		var h = this.content.hitArea;
		var o = this.container.worldTransform;
		var area = new PIXI.Rectangle(o.tx + h.x, o.ty + h.y, h.width*app.scale, h.height*app.scale);
		
		if(!area.contains(e.data.global.x, e.data.global.y)) this.blur();
	},
	update:function(){
		this.val = this.cleanText(this.inp.value);
		this.content.text = this.val;
		this.content.updateText();
		this.content.hitArea.width = this.content.width < this.width ? this.width : this.content.width;
		this.setBlinkerIndex(this.inp.selectionStart, false);	
		if(this.events && this.events.update) this.events.update.call(this, this);
	},
	blur: function(e){
		if(!this.focused) return;
		
		app.stage.touchstart = app.stage.mousedown = null;		
		app.blockResize = false;
		
		this.focused = false;
		
		this.inp.blur();
		this.inp.onkeyup = null;
		
		//clear?
		if(this.content.text == '' || this.content.text == ' ') {
			this.content.text = this.defaultVal;
			this.content.updateText();
		}
		
		//events
		if(this.events && this.events.change) this.events.change.call(this, this);
		if(this.events && this.events.blur) this.events.blur.call(this, this);
		
		//position
		if(koko.client.system.isMobile) this.view.container.y = this.origViewY;
		
		//add blinker
		this.content.removeChild(this.blinker);
		app.removeUpdateListener('input');
	},
	blinkIndex: 0,
	blink: function(){
		this.blinkIndex++;
		this.blinker.visible = this.blinkIndex % 30 < 20;
	}
});

