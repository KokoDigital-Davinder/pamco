// JavaScript Document
var APP_STATE = {
	MENUS: 0,
	GAME: 1
}
var vm = {
    music: null,
    level_music: null,
	currentState: APP_STATE.MENUS,
	init: function(){
		vm.setupViews();
		vm.setupAudio();
		vm.setupComplete();
	},
    playMusic: function(){
        if(vm.music) {
            if(vm.level_music != null) {
                vm.level_music.stop();
                vm.level_music = null;
            }
            vm.music.play(createjs.Sound.INTERRUPT_ANY, 0, 0, -1, 0.5*user.music, 0);
        } else {
            vm.music = au.play('music', 1, true);   
        }
    },
    playLevelMusic: function(level){
        vm.music.stop();
        vm.level_music = au.play('level_music'+level, 1, true);   
    },
	setupViews: function(){
        //app.leaderboardAlwaysOpen = !koko.client.system.isMobile;
        app.leaderboardAlwaysOpen = 0;
		
        v.add('menubg',				new MenuBGView(),				app.game_stage);
        
        v.add('ingamebg',			new InGameBackgroundView(),		app.game_stage);
		v.add('ingame', 			new InGameView(), 				app.game_stage);
		v.add('ingamecountdown', 	new InGameCountdownView(), 		app.game_stage);
        v.add('howtoplay',          new HowToPlayView(),            app.game_stage);
        
		
		
		v.add('mainmenu',			new MainMenuView(),				app.game_stage);
        v.add('charselect',         new CharacterSelectView(),      app.game_stage);
        v.add('scoreboard',         new ScoreboardView(),      app.game_stage);
		v.add('levelselect', 		new LevelSelectView(), 			app.game_stage);
		v.add('gameover', 			new GameOverView(),				app.game_stage);
        v.add('congratulations',           new CongratulationsView(),             app.game_stage);
		v.add('gamecomplete', 			new GameCompleteView(),		app.game_stage);
		
		v.add('menuhud',			new MenuHUD(),					app.game_stage);
    
        v.add('leaderboard', 		new LeaderboardView(),	 		app.game_stage);
        
        
        v.add('settings',           new SettingsView(),             app.game_stage);
        v.add('winprizes',          new WinPrizesView(),	 		app.game_stage);
        v.add('connect',            new ConnectView(),              app.game_stage);
        v.add('connect2',            new ConnectConnectView(),              app.game_stage);
		
        
		v.add('loader', 			new LoaderView(), 				app.game_stage);
		v.add('loaduser', 			new LoadUserView(), 			app.game_stage);
		v.add('preloader', 			new PreloaderView(), 			app.game_stage);
    
    },
	collectSoundCount: 5,
	setupAudio: function() {
		//audio folder
		au.dir = 'audio/';
		//create groups
		au.addSoundGroup('music', user.music);
		au.addSoundGroup('sfx', user.sfx);

		//au.add(name, url, volume, group);
		var fileExt = '.mp3';
        
        au.add('music',          'music_loop' + fileExt,       0.5, 'music');
        
        au.add('level_music0',         'music_loop' + fileExt,       0.5, 'music');
        au.add('level_music1',         'music_loop' + fileExt,       0.5, 'music');
        au.add('level_music2',         'Balloons' + fileExt,       0.5, 'music');
        au.add('level_music3',         'Airplanes' + fileExt,       0.5, 'music');
        
        au.add('boost',         'boost' + fileExt,            1, 'sfx'); //
        
        au.add('button1',       'button_1' + fileExt,         1, 'sfx'); //
        au.add('button2',       'button_2' + fileExt,         1, 'sfx'); //
        
		
		var isIE = koko.client.engine.ie > 0 && koko.client.engine.ie < 11;
        
       au.add('collectable',   'collectible' + fileExt,      1, 'sfx'); //
        au.add('gameover',      'gameover' + fileExt,         1, 'sfx'); //
        au.add('newpb',         'newpersonalbest' + fileExt,  1, 'sfx'); //
        
        au.add('star1',         'star-1' + fileExt,           1, 'sfx'); //
        au.add('star2',         'star-2' + fileExt,           1, 'sfx'); //
        au.add('star3',         'star-3' + fileExt,           1, 'sfx'); //
        
        au.add('collect_1',     'collect_1' + fileExt,        1, 'sfx');
        au.add('collect_2',     'collect_2' + fileExt,        1, 'sfx');
        au.add('collect_3',     'collect_3' + fileExt,        1, 'sfx');
        au.add('collect_4',     'collect_4' + fileExt,        1, 'sfx');
        au.add('collect_5',     'collect_5' + fileExt,        1, 'sfx');
		
		this.collectSoundCount = 5;
		
		if(!isIE) {
			this.collectSoundCount = 10;
			au.add('collect_6',     'collect_6' + fileExt,        1, 'sfx');
			au.add('collect_7',     'collect_7' + fileExt,        1, 'sfx');
			au.add('collect_8',     'collect_8' + fileExt,        1, 'sfx');
			au.add('collect_9',     'collect_9' + fileExt,        1, 'sfx');
			au.add('collect_10',    'collect_10' + fileExt,        1, 'sfx');
		}
	},
	setupComplete: function(){
        //setup two facebook login callback
        // facebook.addSuccessFunc(function() {
        // //     facebook.getFriends(function(){
        // //         v.get('leaderboard').loadScores(0);
        // //     });
            
        //     app.getToken(function(){
        //         app.submitAllScores();
        //     });
            
        //     if(v.get('levelselect').container.visible) {
        //         v.get('levelselect').transitionOut(function(){
        //             v.get('charselect').transitionIn(); 
        //         });
        //     } else if(v.get('gameover').container.visible) {
        //         v.get('gameover').transitionOut(function(){
        //             v.get('charselect').transitionIn(); 
        //         });
        //     }
        //     else if(!v.get('mainmenu').container.visible && !v.get('preloader').container.visible && !v.get('mainmenu').container.visible) {
        //         window.location = window.location;   
        //     }
        // });
        
        
    
		//load the preloader graphics, before showing anything
		v.each(['preloader', 'menubg'], 'queueAssets');		
		//load manifest
		a.loadManifest(function(p){
			//LOADING PROGRESS EVENT
		}, function() {			
		
			//queue assets from all other menus
			v.each([
				'mainmenu',
				'menuhud',
				'leaderboard',
				'levelselect',
				'ingame',
				'gameover',
                'congratulations',
                'charselect',
                'scoreboard',
                'settings',
                'winprizes',
                'howtoplay',
                'ingamecountdown',
				'gamecomplete'
			], 'queueAssets');
			
			//show the preloader
			v.each(['preloader', 'menubg'], 'transitionIn');
			
			//load them sounds yo
			au.loadSounds();
		});
	}
}
