<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>C:/Users/Koko User/Desktop/pamcoTexture/congratulationsIcons.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>json</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../../../xampp/htdocs/pamco/img/congratulationsIcons.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">//koko-nas/WIP/PAMCO/03_BUILD/01_ASSETS/GRAPHICS/Menus/Submit_panel_background.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>169,234,339,468</rect>
                <key>scale9Paddings</key>
                <rect>169,234,339,468</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">//koko-nas/WIP/PAMCO/03_BUILD/01_ASSETS/GRAPHICS/Menus/enter_button.png</key>
            <key type="filename">//koko-nas/WIP/PAMCO/03_BUILD/01_ASSETS/GRAPHICS/Menus/submit_button.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>116,25,231,49</rect>
                <key>scale9Paddings</key>
                <rect>116,25,231,49</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">//koko-nas/WIP/PAMCO/03_BUILD/01_ASSETS/GRAPHICS/Menus/hud_home.png</key>
            <key type="filename">//koko-nas/WIP/PAMCO/03_BUILD/01_ASSETS/GRAPHICS/Menus/hud_settings.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>18,18,36,36</rect>
                <key>scale9Paddings</key>
                <rect>18,18,36,36</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">//koko-nas/WIP/PAMCO/03_BUILD/01_ASSETS/GRAPHICS/Menus/pamco_logo.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>52,14,104,28</rect>
                <key>scale9Paddings</key>
                <rect>52,14,104,28</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">//koko-nas/WIP/PAMCO/03_BUILD/01_ASSETS/GRAPHICS/Menus/replay_button.png</key>
            <key type="filename">//koko-nas/WIP/PAMCO/03_BUILD/01_ASSETS/GRAPHICS/Menus/top10_button.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>75,28,149,57</rect>
                <key>scale9Paddings</key>
                <rect>75,28,149,57</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">//koko-nas/WIP/PAMCO/03_BUILD/01_ASSETS/GRAPHICS/Menus/share_facebook_button.png</key>
            <key type="filename">//koko-nas/WIP/PAMCO/03_BUILD/01_ASSETS/GRAPHICS/Menus/share_twitter_button.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>31,25,61,49</rect>
                <key>scale9Paddings</key>
                <rect>31,25,61,49</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">//koko-nas/WIP/PAMCO/03_BUILD/01_ASSETS/GRAPHICS/Menus/submitform_textfield.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>137,18,275,35</rect>
                <key>scale9Paddings</key>
                <rect>137,18,275,35</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">//koko-nas/WIP/PAMCO/03_BUILD/01_ASSETS/GRAPHICS/Menus/title_congratulations.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>154,18,308,37</rect>
                <key>scale9Paddings</key>
                <rect>154,18,308,37</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">//koko-nas/WIP/PAMCO/03_BUILD/01_ASSETS/GRAPHICS/Menus/title_win50.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>102,19,205,38</rect>
                <key>scale9Paddings</key>
                <rect>102,19,205,38</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">//koko-nas/WIP/PAMCO/03_BUILD/01_ASSETS/GRAPHICS/Menus/titls_submitscore.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>80,7,160,13</rect>
                <key>scale9Paddings</key>
                <rect>80,7,160,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>//koko-nas/WIP/PAMCO/03_BUILD/01_ASSETS/GRAPHICS/Menus/title_congratulations.png</filename>
            <filename>//koko-nas/WIP/PAMCO/03_BUILD/01_ASSETS/GRAPHICS/Menus/Submit_panel_background.png</filename>
            <filename>//koko-nas/WIP/PAMCO/03_BUILD/01_ASSETS/GRAPHICS/Menus/submit_button.png</filename>
            <filename>//koko-nas/WIP/PAMCO/03_BUILD/01_ASSETS/GRAPHICS/Menus/share_twitter_button.png</filename>
            <filename>//koko-nas/WIP/PAMCO/03_BUILD/01_ASSETS/GRAPHICS/Menus/share_facebook_button.png</filename>
            <filename>//koko-nas/WIP/PAMCO/03_BUILD/01_ASSETS/GRAPHICS/Menus/hud_home.png</filename>
            <filename>//koko-nas/WIP/PAMCO/03_BUILD/01_ASSETS/GRAPHICS/Menus/enter_button.png</filename>
            <filename>//koko-nas/WIP/PAMCO/03_BUILD/01_ASSETS/GRAPHICS/Menus/submitform_textfield.png</filename>
            <filename>//koko-nas/WIP/PAMCO/03_BUILD/01_ASSETS/GRAPHICS/Menus/title_win50.png</filename>
            <filename>//koko-nas/WIP/PAMCO/03_BUILD/01_ASSETS/GRAPHICS/Menus/titls_submitscore.png</filename>
            <filename>//koko-nas/WIP/PAMCO/03_BUILD/01_ASSETS/GRAPHICS/Menus/top10_button.png</filename>
            <filename>//koko-nas/WIP/PAMCO/03_BUILD/01_ASSETS/GRAPHICS/Menus/replay_button.png</filename>
            <filename>//koko-nas/WIP/PAMCO/03_BUILD/01_ASSETS/GRAPHICS/Menus/pamco_logo.png</filename>
            <filename>//koko-nas/WIP/PAMCO/03_BUILD/01_ASSETS/GRAPHICS/Menus/hud_settings.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
