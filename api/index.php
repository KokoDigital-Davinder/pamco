<?php

 // ### CORS headers Allow from any origin
   if (isset($_SERVER['HTTP_ORIGIN'])) {
   $http_origin = $_SERVER['HTTP_ORIGIN'];
    $ip = ($_SERVER['HTTP_CF_CONNECTING_IP']) ? $_SERVER['HTTP_CF_CONNECTING_IP'] : $_SERVER['REMOTE_ADDR']; 

   if ( preg_match( "/launchquest.kokogames.com/i", $http_origin) || preg_match( "/launchquest.co.uk/i", $http_origin) || preg_match("/loungeleap.co.uk/i", $http_origin) || $ip == '82.69.49.244' ){
   
			header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
			header('Access-Control-Allow-Credentials: true');
			header('Access-Control-Max-Age: 86400');    // cache for 1 day
		}
  }
  
 // ## Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
		exit(0);
}

session_start();
date_default_timezone_set("Europe/London");
require '../config/newrelic.php'; // ## new relic set app name
require '../config/connstr.php'; //## database conn string
require '../config/memcache.php'; //## memcache stuff
require 'logging.php'; // ### slim custom logging class
require 'Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim(
array(
    'mode' => 'development'
));


// ### API Wide Variables ############################################################
$app->APPCache = ($memcache_stat) ? 'memcache' : 'json'; //if memcache server available use it for cache else fall back on json
$app->APPMemcacheHash = 'LoungeLeapApp'; //memcache hash make sure its unique to the project
$app->APPName = 'Lounge Leap'; // name of campaign
$app->APPTitle = 'Lounge Leap'; // share title of campaign

$app->APPScoreboardFile = 'scores_weekly.json'; // json cache file for weekyly scores
$app->APPScoreboardDefaultLength = 50; // amount of records for normal scoreboard
$app->APPFBScoreboardFile = 'scores_facebook.json'; // json cache file for all fb scores
$app->APPWorldRankScoreboardFile = 'scores_worldrank.json'; // json cache file for world rank
$app->APPShareurl = 'http://www.loungeleap.co.uk/_index.php';

// score/comp submisson restrictions below
$app->APPBannedIps = ''; // comma delimited with pipes(|1.1.1.1|,|8.8.8.8|) list of ips to not allow post of scores /comp entrees;
$app->APPBannedEmails = ''; // comma delimited with pipes(|joebloggs@jeans.com|,|mickymouse@disney.com|) list of Emails to not allow post of scores /comp entrees;
$app->APPMinScore = 0; // limit min score allowed 
$app->APPMaxScore = 2000; // limit max score allowed 
$app->APPMinTimeSubmit  = 5; // add minium time in seconds allowed between score submissions - session based

// ##################################################################################


// ### Slim Settings ################################################################

// Only invoked if mode is "production"
$app->configureMode('production', function () use ($app) {
    $app->config(array(
        'log.enable' => false,
        'debug' => false
    ));
	$app->contentType('application/json;charset=utf-8');
});

// Only invoked if mode is "development"
$app->configureMode('development', function () use ($app) {
    $app->config(array(
        'log.enable' => true,
        'debug' => true,
		'log.writer' => new FileLogWriter($app)
    ));
	$app->contentType('application/json;charset=utf-8');
});
// ##################################################################################


// ### Global Functions / Auth Function ###################################
require 'globalfunctions.php';
require 'authfunction.php';
// ##################################################################################


// ### Test for quick test of parts of code #################################################################
$app->get('/test', function () use ($app) {
global $memcache,$memcache_stat;
	$data = $app->request(); // get data send with request
	$token = $data->get('token');
	$checktoken = md5(substr($token,8,15).substr($token,3,7).substr($token,5,3));
	echo $checktoken;
});

$app->get('/kill', function () use ($app) {
	$_SESSION['token'] ='##DEAD##';
	unset($_SESSION['token']);
	die('Token Dead! (chewing gum)');
	
});




$app->get('/deletecache', function () use ($app ){
	global $memcache,$memcache_stat,$mememarker;
	
	$myFile = $app->APPScoreboardFile;
	unlink('json/'.$myFile);
	$memcache->delete(str_replace('.json',$app->APPMemcacheHash,$myFile));
	
	$facebookFile = $app->APPFBScoreboardFile;
	unlink('json/'.$facebookFile);
	$memcache->delete(str_replace('.json',$app->APPMemcacheHash,$facebookFile));

	$worldrankFile = $app->APPWorldRankScoreboardFile;
	unlink('json/'.$worldrankFile);
	$memcache->delete(str_replace('.json',$app->APPMemcacheHash,$worldrankFile));
	

	
	$myFile = 'v2_'.$app->APPScoreboardFile;
	unlink('json/'.$myFile);
	$memcache->delete(str_replace('.json',$app->APPMemcacheHash,$myFile));
	
	$facebookFile = 'v2_'.$app->APPFBScoreboardFile;
	unlink('json/'.$facebookFile);
	$memcache->delete(str_replace('.json',$app->APPMemcacheHash,$facebookFile));

	$worldrankFile = 'v2_'.$app->APPWorldRankScoreboardFile;
	unlink('json/'.$worldrankFile);
	$memcache->delete(str_replace('.json',$app->APPMemcacheHash,$worldrankFile));

	echo 'deleted cache';

});

// ##################################################################################

// ### Client Links #################################################################
include 'clientlinks.php';
// ##################################################################################

// ### Social Shares ################################################################
include 'socialshares.php';
// ##################################################################################

// ### Token/ Login Security ########################################################
include 'tokenlogin.php';
// ##################################################################################

// ### Scoreboards ##################################################################
include 'scoreboardsv2.php';
include 'scoreboards.php';
// ##################################################################################

// ### Post Scores ##################################################################
include 'postscore.php';
// ##################################################################################
$app->notFound(function (){
	$app = \Slim\Slim::getInstance();
	//$response = $app->response();
    //$response->header("Content-Type", "text/html");
	$app->contentType('text/html');
	
	$app->halt('404');	
});



$app->run();
// Run Slim App

?>
