<?


$app->post('/OLD/competition', function() use ($app) {
	$app->getLog()->info('### POST /competition');
	// CURL post to form recorder
	
	$request = $app->request(); // get data send with request
    $player = koko_json_decode($request->getBody());

	//$url = 'https://apps.virginmoney.com/form-recorder/record.htm';
	
	$url = 'https://apps.staging.virginmoney.com/form-recorder/record.htm';
	
	
	$redirection = 'https://uk.virginmoney.com/virgin/forms/custStories-thanks.jsp';
	
	//'formName' => urlencode('facebookGame01'),
	
	//'formName' => urlencode('launchquestGame'),
	
	$fields = array(
					'formName' => urlencode('facebookGame02'),
					'redirect' => urlencode('https://uk.virginmoney.com/virgin/forms/custStories-thanks.jsp'),
					'formSubmitted' => urlencode('Y'),
					'deviceInfo' => urlencode(''),
					'browserInfo' => urlencode(''),
					'title' => urlencode(''),
					'forename' => urlencode($player->player_forename),
					'surname' => urlencode($player->player_surname),
					'addLine1' => urlencode($player->player_address1),
					'addLine2' => urlencode($player->player_address2),
					'city' => urlencode($player->player_city),
					'postcode' => urlencode($player->player_postcode),
					'email' => urlencode($player->player_email),
					'telephone' => urlencode($player->player_telephone)
			);
			
	foreach($fields as $key=>$value) { 
	$fields_string .= $key.'='.$value.'&';
	 }
	rtrim($fields_string, '&');		
			
	$curl = curl_init();
	curl_setopt_array($curl, array( CURLOPT_RETURNTRANSFER => 1,CURLOPT_URL => $url,CURLOPT_POST => count($fields),CURLOPT_POSTFIELDS => $fields_string, CURLOPT_HEADER =>1,CURLINFO_HEADER_OUT =>true,CURLOPT_FOLLOWLOCATION =>false ));

	$comp_response = curl_exec($curl);
	$info = curl_getinfo($curl);
	
	if(preg_match('#Location: (.*)#', $comp_response, $r))
	$locationfollowed = trim($r[1]);
	
		
	$app->getLog()->info('### FIELDS '.$fields_string);
	$app->getLog()->info('### RESPONSE '.$comp_response);
	$app->getLog()->info("### RESPONSE INFO " . print_r($info, true));
	$app->getLog()->info("### REDIRECTION:	$locationfollowed");
	
	curl_close($curl);
	
	
	if($locationfollowed==$redirection){
		// success
	
			echo '{"response": [{"code":"200","message":"success"}]}';
		
	} else{
		// fail send email
		
			require('../config/class.phpmailer.php'); // EMAIL FUNCTIONS
			$strSubject = '#FAIL# Comp Entry - Virgin Suitcase Sling';
			
			$strBody = '### Virgin Contacts: '.chr(13).'Barry Brosnihan : 01603 215 624/Barry.Brosnihan@virginmoney.com'.chr(13).' Keith Talbot: 01603 215 583/Keith.Talbot@virginmoney.com'.chr(13).chr(13);
			$strBody .= '### FIELDS: '.chr(13).$fields_string.chr(13).chr(13);
			$strBody .= '### RESPONSE: '.chr(13).$comp_response.chr(13).chr(13);
			$strBody .= '### RESPONSE INFO: '.chr(13).print_r($info, true).chr(13).chr(13);
			$strBody .= "### REDIRECTION:	$locationfollowed".chr(13);
			
			$mail = new PHPMailer();
			$mail->IsHTML(false); 
			$mail->From = 'no-reply@suitcasesling.kokogames.com';
			$mail->FromName = 'Suitcase Sling';
			$mail->AddAddress('brett@kokodigital.co.uk','brett@kokodigital.co.uk');
			$mail->AddAddress('rory@kokodigital.co.uk','rory@kokodigital.co.uk');
			$mail->AddAddress('chris@kokodigital.co.uk','chris@kokodigital.co.uk');
			$mail->Subject = $strSubject;
			$mail->Body = $strBody;
			
		   	if(!$mail->Send()){
				 $app->getLog()->info('MAIL ERROR: '.$mail->ErrorInfo);
			}
		
	
		$app->halt('500','{"error":{"code":"500","message":"error"}}');	
	}
});


$app->post('/competition', function() use ($app) {
	$app->getLog()->info('### POST /competition');
	// CURL post to form recorder
	
	$request = $app->request(); // get data send with request
    $player = koko_json_decode($request->getBody());

	$url = 'https://apps.virginmoney.com/form-recorder/record.htm';
	
	///	$url = 'https://apps.staging.virginmoney.com/form-recorder/record.htm';
	
	$redirection = 'https://uk.virginmoney.com/virgin/forms/custStories-thanks.jsp';
	
	//'formName' => urlencode('facebookGame01'),
	//customerStories
	
	$fields = array(
					'formName' => urlencode('facebookGame02'),
					'redirect' => urlencode('https://uk.virginmoney.com/virgin/forms/custStories-thanks.jsp'),
					'formSubmitted' => urlencode('Y'),
					'TCConfirm' => urlencode('true'),
					'deviceInfo' => urlencode(''),
					'browserInfo' => urlencode(''),
					'title' => urlencode(''),
					'forename' => urlencode($player->player_forename),
					'surname' => urlencode($player->player_surname),
					'addLine1' => urlencode($player->player_address1),
					'addLine2' => urlencode($player->player_address2),
					'townCity' => urlencode($player->player_city),
					'postcode' => urlencode($player->player_postcode),
					'email' => urlencode($player->player_email),
					'telephone' => urlencode($player->player_telephone)
			);
			
	foreach($fields as $key=>$value) { 
	$fields_string .= $key.'='.$value.'&';
	 }
	rtrim($fields_string, '&');		
			
	$curl = curl_init();
	curl_setopt_array($curl, array( CURLOPT_RETURNTRANSFER => 1,CURLOPT_URL => $url,CURLOPT_POST => count($fields),CURLOPT_POSTFIELDS => $fields_string, CURLOPT_HEADER =>1,CURLINFO_HEADER_OUT =>true,CURLOPT_FOLLOWLOCATION =>false ));

	$comp_response = curl_exec($curl);
	$info = curl_getinfo($curl);
	
	if(preg_match('#Location: (.*)#', $comp_response, $r))
	$locationfollowed = trim($r[1]);
	
		
	$app->getLog()->info('### FIELDS '.$fields_string);
	$app->getLog()->info('### RESPONSE '.$comp_response);
	$app->getLog()->info("### RESPONSE INFO " . print_r($info, true));
	$app->getLog()->info("### REDIRECTION:	$locationfollowed");
	
	curl_close($curl);
	
	
	if($locationfollowed==$redirection){
		// success
		echo '{"response": [{"code":"200","message":"success"}]}';
	} else{
		// fail send email
			require('../config/class.phpmailer.php'); // EMAIL FUNCTIONS
			$strSubject = '#FAIL# Comp Entry - Virgin Suitcase Sling';
			
			$strBody = '### Virgin Contacts: '.chr(13).'Barry Brosnihan : 01603 215 624/Barry.Brosnihan@virginmoney.com'.chr(13).' Keith Talbot: 01603 215 583/Keith.Talbot@virginmoney.com'.chr(13).chr(13);
			$strBody .= '### FIELDS: '.chr(13).$fields_string.chr(13).chr(13);
			$strBody .= '### RESPONSE: '.chr(13).$comp_response.chr(13).chr(13);
			$strBody .= '### RESPONSE INFO: '.chr(13).print_r($info, true).chr(13).chr(13);
			$strBody .= "### REDIRECTION:	$locationfollowed".chr(13);
			
			$mail = new PHPMailer();
			$mail->IsHTML(false); 
			$mail->From = 'no-reply@suitcasesling.kokogames.com';
			$mail->FromName = 'Suitcase Sling';
			$mail->AddAddress('brett@kokodigital.co.uk','brett@kokodigital.co.uk');
			$mail->AddAddress('rory@kokodigital.co.uk','rory@kokodigital.co.uk');
			$mail->AddAddress('chris@kokodigital.co.uk','chris@kokodigital.co.uk');
			$mail->Subject = $strSubject;
			$mail->Body = $strBody;
			
		   	if(!$mail->Send()){
				 $app->getLog()->info('MAIL ERROR: '.$mail->ErrorInfo);
			}
		
		$app->halt('500','{"error":{"code":"500","message":"error"}}');	
	}
});



$app->post('/user/scores','authenticate', function () use ($app) {
	global $memcache;
	$app->getLog()->info('### post /user/scores');
	$request = $app->request(); // get data send with request
	// json data : {"token":"31123123","player_fb_uid":"1232465879"}	
    $player = koko_json_decode($request->getBody());
	
	$sql = 'CALL spGetUser(:fbid)';
	
	try {
		$app->getLog()->info('call mysql: '.$sql.' data:' .$request->getBody());
		$db = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->bindValue(':fbid', strval($player->player_fb_uid),PDO::PARAM_STR);
		$stmt->execute();
		$response = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		$app->getLog()->info('mysql response: '.koko_json_encode($response));
	
		if($response[0]->code =='200'){
			echo '{"response": '.koko_json_encode($response[0]).'}';
		}
	
		
	 } catch(PDOException $e) {
		// mysql error 500
		$app->getLog()->info(' DB ERROR: '.$e->getMessage());
		$app->halt(500,'{"error":{"code":"500","message":"DB ERROR - '. $e->getMessage() .'"}}');
    }
	
	
	
});


$app->post('/scores','authenticate', function () use ($app) {
	global $memcache;
	$app->getLog()->info('### POST /scores');
	// json data : {"token":"31123123",player_full_name":"Joe Bloggs","player_score":"123456","player_fb_uid":"1232465879"}
	
	$request = $app->request(); // get data send with request
    $player = koko_json_decode($request->getBody());
	$player_ip = ($_SERVER['CF-Connecting-IP']) ? $_SERVER['CF-Connecting-IP'] : $_SERVER['REMOTE_ADDR'];
	$player_country = ($_SERVER["HTTP_CF_IPCOUNTRY"]) ? $_SERVER["HTTP_CF_IPCOUNTRY"] : '??';
	// get players ip address
	
	/// ### stop spamming below #######
	$stopsubmission = false;
	$stopreason = '';
	
		if(!$_SESSION['LAST_SUB_TIME']){
			$_SESSION['LAST_SUB_TIME'] = strtotime("now");
			
			// record time of last submission
		} else if(abs(strtotime("now")-intval($_SESSION['LAST_SUB_TIME'])) < intval($app->APPMinTimeSubmit) ){
			// time between submission to short stop score submission
			$stopsubmission = true;
			$stopreason .= '[time between submission to short '.abs(strtotime("now")-intval($_SESSION['LAST_SUB_TIME'])).']';
		}
		
		if (is_numeric(strpos($app->APPBannedIps,'|'.$playersIP.'|'))===true && is_numeric(strpos($app->APPBannedEmails ,'|'.$player->player_email.'|'))===true ){
			// if either ban list stop score submission
			$stopsubmission = true;
			$stopreason .= '[user on banned list]';
			
		}

		if(intval($player->player_score) > intval($app->APPMaxScore) && intval($player->player_score) < intval($app->APPMinScore) ){
			// limit min score allowed 
			$stopsubmission = true;
			$stopreason .= '[score caught with min max code]';
		}
		if($stopsubmission){
			// stop code here 
			$app->getLog()->info('424 stopped spam submission - '.$stopreason);
			$app->halt('424','{"error":{"code":"424","message":"error"}}');	
		}
		
	
		#########################

	
	$sql = 'CALL spAddScore(:name,:score,:fbid,:glevel,:extra)';
	
	try {
		$app->getLog()->info('call mysql: '.$sql.' data:' .$request->getBody());
		$db = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->bindValue(':name', strval(ucwords($player->player_full_name)),PDO::PARAM_STR);
		$stmt->bindValue(':score', intval($player->player_score),PDO::PARAM_INT);
		$stmt->bindValue(':fbid', strval($player->player_fb_uid),PDO::PARAM_STR);
		$stmt->bindValue(':glevel', strval($player->player_level),PDO::PARAM_STR);
		$stmt->bindValue(':extra', strval($player->player_extra),PDO::PARAM_STR);
		$stmt->execute();
		$response = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		$app->getLog()->info('mysql response: '.koko_json_encode($response));
		
		$app->getLog()->info('LEVEL###########################: '.$player->player_level);

		if($response[0]->code ==200){
			$_SESSION['LAST_SUB_TIME'] = strtotime("now");
			echo '{"response": '.koko_json_encode($response).'}';
			
			$player->player_all_score  = intval($response[0]->totalscores);
			
			//check scoreboards cache
			$myFile = $app->APPScoreboardFile;
			if($app->APPCache == 'json'){
				if(file_exists('json/'.$myFile)){
					$response = koko_json_decode(file_get_contents('json/'.$myFile, FILE_USE_INCLUDE_PATH));
					if(intval($player->player_all_score) > intval(end($response->scores)->player_score) || count($response->scores) < intval($app->APPScoreboardDefaultLength)){
						//if score better than last in scoreboard then delete scoreboard so it gets recreated
						unlink('json/'.$myFile);
					}
				}
			} else if(($response = getCache(str_replace('.json',$app->APPMemcacheHash,$myFile))) !== false) {
				
				if(intval($player->player_all_score) > intval(end($response->scores)->player_score) || count($response->scores) < intval($app->APPScoreboardDefaultLength)){
						//if score better than last in scoreboard then delete scoreboard so it gets recreated
						$memcache->delete(str_replace('.json',$app->APPMemcacheHash,$myFile));
				}
			}
			
			$facebookFile = $app->APPFBScoreboardFile;
			
			if($facebookFile){
				if($app->APPCache == 'json'){
					if(file_exists('json/'.$facebookFile)){
						$response = koko_json_decode(file_get_contents('json/'.$facebookFile, FILE_USE_INCLUDE_PATH));
						if(intval($player->player_score) >  intval(end($response->scores)->{('player_score_level'.$player->player_level)}) || count($response->scores) < intval($app->APPScoreboardDefaultLength)){
							//if score better than last in scoreboard then delete scoreboard so it gets recreated
							unlink('json/'.$myFile);
						}
					}
					
				} else if(($response = getCache(str_replace('.json',$app->APPMemcacheHash,$facebookFile))) !== false) {
						
					if((intval($player->player_all_score) > intval(end($response->scores)->player_score) || intval($player->player_score) >  intval(end($response->scores)->{('player_score_level'.$player->player_level)})) || count($response->scores) < intval($app->APPScoreboardDefaultLength)){
							//if score better than last in scoreboard then delete scoreboard so it gets recreated
							$memcache->delete(str_replace('.json',$app->APPMemcacheHash,$facebookFile));
						}
						
					} else if(($response = getCache(str_replace('.json',$app->APPMemcacheHash,'v2_'.$facebookFile))) !== false) {
						
				if((intval($player->player_all_score) > intval(end($response->scores)->player_score) || intval($player->player_score) >  intval(end($response->scores)->{('player_score_level'.$player->player_level)})) || count($response->scores) < intval($app->APPScoreboardDefaultLength)){
							//if score better than last in scoreboard then delete scoreboard so it gets recreated
							$memcache->delete(str_replace('.json',$app->APPMemcacheHash,'v2_'.$facebookFile));
						}
					}
			}
			
			$worldrankFile = $app->APPWorldRankScoreboardFile;
			if($worldrankFile){
				if($app->APPCache == 'json'){
					if(file_exists('json/'.$worldrankFile)){
						$response = koko_json_decode(file_get_contents('json/'.$worldrankFile, FILE_USE_INCLUDE_PATH));
						if(intval($player->player_score) > intval(end($response->scores)->player_score) || count($response->scores) < 144 ){
						//if score better than last in world rank then delete json so it gets recreated
						// or worldrank json has less records than the first : fibonacci sequence after a 100 records (144)
						unlink('json/'.$worldrankFile);
						}
					}
				} else if(($response = getCache(str_replace('.json',$app->APPMemcacheHash,$worldrankFile))) !== false) {
						
					if(intval($player->player_score) > intval(end($response->scores)->player_score) || count($response->scores) < 144 ){
						//if score better than last in scoreboard then delete scoreboard so it gets recreated
						$memcache->delete(str_replace('.json',$app->APPMemcacheHash,$worldrankFile));
					}
				}
			}
			
			
			// success 200
		} else {
			$app->halt($response[0]->code,'{"error":'.koko_json_encode($response).'}');
			// coded error from sql
		}
		
	 } catch(PDOException $e) {
		// mysql error 500
		$app->getLog()->info(' DB ERROR: '.$e->getMessage());
		$app->halt(500,'{"error":{"code":"500","message":"DB ERROR - '. $e->getMessage() .'"}}');
    }
	
});

$app->post('/scores/connect','authenticate', function () use ($app) {
	global $memcache;
	$app->getLog()->info('### POST /scores/connect');
	// json data : {"token":"31123123",player_full_name":"Joe Bloggs","player_score":"123456","player_fb_uid":"1232465879"}
	
	$request = $app->request(); // get data send with request
	
    $player = koko_json_decode($request->getBody());
	$player_ip = ($_SERVER['CF-Connecting-IP']) ? $_SERVER['CF-Connecting-IP'] : $_SERVER['REMOTE_ADDR'];
	$player_country = ($_SERVER["HTTP_CF_IPCOUNTRY"]) ? $_SERVER["HTTP_CF_IPCOUNTRY"] : '??';
	// get players ip address
	
	/// ### stop spamming below #######
	$stopsubmission = false;
	$stopreason = '';
	
		if(!$_SESSION['LAST_SUB_TIME']){
			$_SESSION['LAST_SUB_TIME'] = strtotime("now");
			
			// record time of last submission
		} else if(abs(strtotime("now")-intval($_SESSION['LAST_SUB_TIME'])) < intval($app->APPMinTimeSubmit) ){
			// time between submission to short stop score submission
			$stopsubmission = true;
			$stopreason .= '[time between submission to short '.abs(strtotime("now")-intval($_SESSION['LAST_SUB_TIME'])).']';
		}
		
		if (is_numeric(strpos($app->APPBannedIps,'|'.$playersIP.'|'))===true && is_numeric(strpos($app->APPBannedEmails ,'|'.$player->player_email.'|'))===true ){
			// if either ban list stop score submission
			$stopsubmission = true;
			$stopreason .= '[user on banned list]';
			
		}
    
        $one_over = intval($player->player_level_1_score) > intval($app->APPMaxScore) && intval($player->player_level_1_score) < intval($app->APPMinScore);
        $two_over = intval($player->player_level_2_score) > intval($app->APPMaxScore) && intval($player->player_level_2_score) < intval($app->APPMinScore);
        $three_over = intval($player->player_level_3_score) > intval($app->APPMaxScore) && intval($player->player_level_3_score) < intval($app->APPMinScore);
        $four_over = intval($player->player_level_4_score) > intval($app->APPMaxScore) && intval($player->player_level_4_score) < intval($app->APPMinScore);

		if($one_over || $two_over || $three_over || $four_over){
			// limit min score allowed 
			$stopsubmission = true;
			$stopreason .= '[score caught with min max code]';
		}
		if($stopsubmission){
			// stop code here 
			$app->getLog()->info('424 stopped spam submission - '.$stopreason);
			$app->halt('424','{"error":{"code":"424","message":"error"}}');	
		}
		
	
		#########################

	
	$sql = 'CALL spAddScoreConnect(:name,:score1,:score2,:score3,:score4,:fbid,:extra)';
	
	try {
		$app->getLog()->info('call mysql: '.$sql.' data:' .$request->getBody());
		$db = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->bindValue(':name', strval(ucwords($player->player_full_name)),PDO::PARAM_STR);
		$stmt->bindValue(':score1', intval($player->player_level_1_score),PDO::PARAM_INT);
		$stmt->bindValue(':score2', intval($player->player_level_2_score),PDO::PARAM_INT);
		$stmt->bindValue(':score3', intval($player->player_level_3_score),PDO::PARAM_INT);
		$stmt->bindValue(':score4', intval($player->player_level_4_score),PDO::PARAM_INT);
		$stmt->bindValue(':fbid', strval($player->player_fb_uid),PDO::PARAM_STR);
		$stmt->bindValue(':extra', strval($player->player_extra),PDO::PARAM_STR);
		$stmt->execute();
		$response = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		$app->getLog()->info('mysql response: '.koko_json_encode($response));
		
		$app->getLog()->info('LEVEL###########################: '.$player->player_level);

		if($response[0]->code ==200){
			$_SESSION['LAST_SUB_TIME'] = strtotime("now");
			echo '{"response": '.koko_json_encode($response).'}';
			
			$player->player_all_score  = intval($response[0]->totalscores);
			
			//check scoreboards cache
			$myFile = $app->APPScoreboardFile;
			if($app->APPCache == 'json'){
				if(file_exists('json/'.$myFile)){
					$response = koko_json_decode(file_get_contents('json/'.$myFile, FILE_USE_INCLUDE_PATH));
					
					if(intval($player->player_all_score) > intval(end($response->scores)->player_score) || count($response->scores) < intval($app->APPScoreboardDefaultLength)){
						//if score better than last in scoreboard then delete scoreboard so it gets recreated
						unlink('json/'.$myFile);
					}
					
					if(intval($player->player_level_1_score) > intval(end($response->scores)->player_score_level1) || count($response->scores) < intval($app->APPScoreboardDefaultLength)){
						//if score better than last in scoreboard then delete scoreboard so it gets recreated
						unlink('json/'.$myFile);
					}
					
					if(intval($player->player_level_2_score) > intval(end($response->scores)->player_score_level2) || count($response->scores) < intval($app->APPScoreboardDefaultLength)){
						//if score better than last in scoreboard then delete scoreboard so it gets recreated
						unlink('json/'.$myFile);
					}
					
					if(intval($player->player_level_3_score) > intval(end($response->scores)->player_score_level3) || count($response->scores) < intval($app->APPScoreboardDefaultLength)){
						//if score better than last in scoreboard then delete scoreboard so it gets recreated
						unlink('json/'.$myFile);
					}
					
					if(intval($player->player_level_4_score) > intval(end($response->scores)->player_score_level4) || count($response->scores) < intval($app->APPScoreboardDefaultLength)){
						//if score better than last in scoreboard then delete scoreboard so it gets recreated
						unlink('json/'.$myFile);
					}
					
					
					
					
				}
			} else if(($response = getCache(str_replace('.json',$app->APPMemcacheHash,$myFile))) !== false) {
				
			
					if(intval($player->player_all_score) > intval(end($response->scores)->player_score) || count($response->scores) < intval($app->APPScoreboardDefaultLength)){
						//if score better than last in scoreboard then delete scoreboard so it gets recreated
						$memcache->delete(str_replace('.json',$app->APPMemcacheHash,$myFile));
					}
					
					if(intval($player->player_level_1_score) > intval(end($response->scores)->player_score_level1) || count($response->scores) < intval($app->APPScoreboardDefaultLength)){
						//if score better than last in scoreboard then delete scoreboard so it gets recreated
						$memcache->delete(str_replace('.json',$app->APPMemcacheHash,$myFile));
					}
					
					if(intval($player->player_level_2_score) > intval(end($response->scores)->player_score_level2) || count($response->scores) < intval($app->APPScoreboardDefaultLength)){
						//if score better than last in scoreboard then delete scoreboard so it gets recreated
						$memcache->delete(str_replace('.json',$app->APPMemcacheHash,$myFile));
					}
					
					if(intval($player->player_level_3_score) > intval(end($response->scores)->player_score_level3) || count($response->scores) < intval($app->APPScoreboardDefaultLength)){
						//if score better than last in scoreboard then delete scoreboard so it gets recreated
						$memcache->delete(str_replace('.json',$app->APPMemcacheHash,$myFile));
					}
					
					if(intval($player->player_level_4_score) > intval(end($response->scores)->player_score_level4) || count($response->scores) < intval($app->APPScoreboardDefaultLength)){
						//if score better than last in scoreboard then delete scoreboard so it gets recreated
						$memcache->delete(str_replace('.json',$app->APPMemcacheHash,$myFile));
					}
				
				
			}
			
			$facebookFile = $app->APPFBScoreboardFile;
			
			if($facebookFile){
				if($app->APPCache == 'json'){
					if(file_exists('json/'.$facebookFile)){
						$response = koko_json_decode(file_get_contents('json/'.$facebookFile, FILE_USE_INCLUDE_PATH));
					
					
							if(intval($player->player_all_score) > intval(end($response->scores)->player_score) || count($response->scores) < intval($app->APPScoreboardDefaultLength)){
								//if score better than last in scoreboard then delete scoreboard so it gets recreated
									unlink('json/'.$facebookFile);
							}
							
							if(intval($player->player_level_1_score) > intval(end($response->scores)->player_score_level1) || count($response->scores) < intval($app->APPScoreboardDefaultLength)){
								//if score better than last in scoreboard then delete scoreboard so it gets recreated
									unlink('json/'.$facebookFile);
							}
							
							if(intval($player->player_level_2_score) > intval(end($response->scores)->player_score_level2) || count($response->scores) < intval($app->APPScoreboardDefaultLength)){
								//if score better than last in scoreboard then delete scoreboard so it gets recreated
									unlink('json/'.$facebookFile);
							}
							
							if(intval($player->player_level_3_score) > intval(end($response->scores)->player_score_level3) || count($response->scores) < intval($app->APPScoreboardDefaultLength)){
								//if score better than last in scoreboard then delete scoreboard so it gets recreated
									unlink('json/'.$facebookFile);
							}
							
							if(intval($player->player_level_4_score) > intval(end($response->scores)->player_score_level4) || count($response->scores) < intval($app->APPScoreboardDefaultLength)){
								//if score better than last in scoreboard then delete scoreboard so it gets recreated
									unlink('json/'.$facebookFile);
							}
						
						
						
					}
					
				} else if(($response = getCache(str_replace('.json',$app->APPMemcacheHash,$facebookFile))) !== false) {
					
				
						if(intval($player->player_all_score) > intval(end($response->scores)->player_score) || count($response->scores) < intval($app->APPScoreboardDefaultLength)){
							//if score better than last in scoreboard then delete scoreboard so it gets recreated
							$memcache->delete(str_replace('.json',$app->APPMemcacheHash,$facebookFile));
						}
						
						if(intval($player->player_level_1_score) > intval(end($response->scores)->player_score_level1) || count($response->scores) < intval($app->APPScoreboardDefaultLength)){
							//if score better than last in scoreboard then delete scoreboard so it gets recreated
							$memcache->delete(str_replace('.json',$app->APPMemcacheHash,$facebookFile));
						}
						
						if(intval($player->player_level_2_score) > intval(end($response->scores)->player_score_level2) || count($response->scores) < intval($app->APPScoreboardDefaultLength)){
							//if score better than last in scoreboard then delete scoreboard so it gets recreated
							$memcache->delete(str_replace('.json',$app->APPMemcacheHash,$facebookFile));
						}
						
						if(intval($player->player_level_3_score) > intval(end($response->scores)->player_score_level3) || count($response->scores) < intval($app->APPScoreboardDefaultLength)){
							//if score better than last in scoreboard then delete scoreboard so it gets recreated
							$memcache->delete(str_replace('.json',$app->APPMemcacheHash,$facebookFile));
						}
						
						if(intval($player->player_level_4_score) > intval(end($response->scores)->player_score_level4) || count($response->scores) < intval($app->APPScoreboardDefaultLength)){
							//if score better than last in scoreboard then delete scoreboard so it gets recreated
							$memcache->delete(str_replace('.json',$app->APPMemcacheHash,$facebookFile));
						}
						
					
						
					} else if(($response = getCache(str_replace('.json',$app->APPMemcacheHash,'v2_'.$facebookFile))) !== false) {
						
					
						if(intval($player->player_all_score) > intval(end($response->scores)->player_score) || count($response->scores) < intval($app->APPScoreboardDefaultLength)){
							//if score better than last in scoreboard then delete scoreboard so it gets recreated
							$memcache->delete(str_replace('.json',$app->APPMemcacheHash,'v2_'.$facebookFile));
						}
						
						if(intval($player->player_level_1_score) > intval(end($response->scores)->player_score_level1) || count($response->scores) < intval($app->APPScoreboardDefaultLength)){
							//if score better than last in scoreboard then delete scoreboard so it gets recreated
							$memcache->delete(str_replace('.json',$app->APPMemcacheHash,'v2_'.$facebookFile));
						}
						
						if(intval($player->player_level_2_score) > intval(end($response->scores)->player_score_level2) || count($response->scores) < intval($app->APPScoreboardDefaultLength)){
							//if score better than last in scoreboard then delete scoreboard so it gets recreated
						$memcache->delete(str_replace('.json',$app->APPMemcacheHash,'v2_'.$facebookFile));
						}
						
						if(intval($player->player_level_3_score) > intval(end($response->scores)->player_score_level3) || count($response->scores) < intval($app->APPScoreboardDefaultLength)){
							//if score better than last in scoreboard then delete scoreboard so it gets recreated
							$memcache->delete(str_replace('.json',$app->APPMemcacheHash,'v2_'.$facebookFile));
						}
						
						if(intval($player->player_level_4_score) > intval(end($response->scores)->player_score_level4) || count($response->scores) < intval($app->APPScoreboardDefaultLength)){
							//if score better than last in scoreboard then delete scoreboard so it gets recreated
							$memcache->delete(str_replace('.json',$app->APPMemcacheHash,'v2_'.$facebookFile));
						}
						
						
						
					}
			}
			
			$worldrankFile = $app->APPWorldRankScoreboardFile;
			if($worldrankFile){
				if($app->APPCache == 'json'){
					if(file_exists('json/'.$worldrankFile)){
						$response = koko_json_decode(file_get_contents('json/'.$worldrankFile, FILE_USE_INCLUDE_PATH));
						if(intval($player->player_score) > intval(end($response->scores)->player_score) || count($response->scores) < 144 ){
						//if score better than last in world rank then delete json so it gets recreated
						// or worldrank json has less records than the first : fibonacci sequence after a 100 records (144)
						unlink('json/'.$worldrankFile);
						}
					}
				} else if(($response = getCache(str_replace('.json',$app->APPMemcacheHash,$worldrankFile))) !== false) {
						
					if(intval($player->player_score) > intval(end($response->scores)->player_score) || count($response->scores) < 144 ){
						//if score better than last in scoreboard then delete scoreboard so it gets recreated
						$memcache->delete(str_replace('.json',$app->APPMemcacheHash,$worldrankFile));
					}
				}
			}
			
			
			// success 200
		} else {
			$app->halt($response[0]->code,'{"error":'.koko_json_encode($response).'}');
			// coded error from sql
		}
		
	 } catch(PDOException $e) {
		// mysql error 500
		$app->getLog()->info(' DB ERROR: '.$e->getMessage());
		$app->halt(500,'{"error":{"code":"500","message":"DB ERROR - '. $e->getMessage() .'"}}');
    }
	
});


?>