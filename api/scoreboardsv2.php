<?

$app->get('/scores/v2/:type', function ($type) use ($app) {
	// ### get scores
	$data = $app->request();
    $from = ($data->get('from'))? $data->get('from') : 0;
	$to = ($data->get('to')) ? $data->get('to') : intval($app->APPScoreboardDefaultLength)-1;
	$friends = ($data->get('friends')) ? $data->get('friends') : '';
	
	$app->getLog()->info('### GET /scores/'.$type);

	$sql = 'CALL spGetScores(:type,:from,:to,:friends)'; // sql string to call	
	try {
		 $generateCache = false;
		
		if($type=='facebook'){
			$myFile = $app->APPFBScoreboardFile;
			
			if(strlen($friends)>0){
				$myFile = 'dontcache.json';
				$generateCache  = true;
				// dont cache one to one fb scoreboard
				}
		} else{
			$myFile = $app->APPScoreboardFile;
		}
		// set correct file for caching scorboard
		
	if($app->APPCache == 'json'){
		if(file_exists('json/'.$myFile)){
		// check json file exists before reading from DB
            $date = filemtime('json/'.$myFile);
            $dateDiff    = date() - $date;  
            $fullHours   = floor($dateDiff/(60*60));
			if($fullHours>168){
				$generateCache  = true;
			}
		} else{
			$generateCache = true;
		}
	} else{
	
		if(($response = getCache(str_replace('.json',$app->APPMemcacheHash,'v2_'.$myFile))) === false) { 
			 $generateCache = true;
		}
		// check if memcache exists
	}

	$app->getLog()->info('### Generate new cache'.$generateCache);
	
	
		if($generateCache){
			
			
			$app->getLog()->info('call mysql: '.$sql.' data:'.$type.','.$from.','.$to.','.$friends);
			
			$db = getConnection();
			$stmt = $db->prepare($sql);
			$stmt->bindValue(':type', strval($type),PDO::PARAM_STR);
			$stmt->bindValue(':from', intval($from),PDO::PARAM_INT);
			$stmt->bindValue(':to', intval($to),PDO::PARAM_INT);
			$stmt->bindValue(':friends', strval($friends),PDO::PARAM_STR);
			$stmt->execute();
			$scores = $stmt->fetchAll(PDO::FETCH_OBJ);
			// get live score form database write to json file
			
			if(count($scores) > 0){
					$arrayFbBatch = array();
					
					foreach ($scores as $row) {
						$row->player_full_name =  funcNameFormat($row->player_full_name);
						$strURL = $row->player_fb_uid.'/picture?redirect=0&height=60&type=normal&width=60';
						array_push($arrayFbBatch, array("method"=> "GET","relative_url" => $strURL));
					}
					
					$postData = 'access_token=1537041603204831|ZQ-6N7RYQvCuIlcAGEczoPP2Pt8&batch='.urlencode(json_encode($arrayFbBatch));
					$app->getLog()->info('FB POST DATA BATCH: '.$postData);
					
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, 'https://graph.facebook.com');
					curl_setopt($ch, CURLOPT_HEADER, 0);
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			
					$output = koko_json_decode(curl_exec($ch));
					//$app->getLog()->info('FB POST output: '.$output);
					curl_close($ch);
					$images = array();
					
					for($i=0;$i<count($output);$i++){
						$code = $output[$i]->code;
						$body = koko_json_decode($output[$i]->body);
						$images[$i] = 'none';
						
						if($code == '200'){
							if(intval($body->data->is_silhouette)==0){
								$images[$i] = $body->data->url;
							}
						}
					}
					
					for($i=0;$i<count($scores);$i++){	
							
						$scores[$i]->player_image = $images[$i];
						
						if (is_numeric(strpos($friends,$scores[$i]->player_fb_uid))===false){
							$scores[$i]->player_fb_uid = '#NULL#';
						}
						
					}
			}
			
			
			$response->code = '200';
			$response->scores = $scores;
			
			
			if($myFile!='dontcache.json'){
				if($app->APPCache == 'json'){
					file_put_contents('json/v2_'.$myFile,koko_json_encode($response). PHP_EOL, FILE_APPEND);
					// write json file;
				} else{
					setCache(str_replace('.json',$app->APPMemcacheHash,'v2_'.$myFile),$response,172800);
					//write memcache entry
				}
			}
		
		} else {
			if($app->APPCache == 'json'){
				$response = koko_json_decode(file_get_contents('json/v2_'.$myFile, FILE_USE_INCLUDE_PATH));
				//read json file
			} 	
		}
		
	
		
		echo '{"response": '.koko_json_encode($response).'}';
		// success 200
		
	} catch(PDOException $e) {
		// mysql error 500
		$app->getLog()->info(' DB ERROR: '.$e->getMessage());
		$app->halt(500,'{"error":{"code":"500","message":"DB ERROR - '. $e->getMessage() .'"}}');
    }	
});
?>