<?
$app->get('/token', function () use ($app) {
	// ### get token and extra game info 
	// data ?app_version=1.0"
	$data = $app->request(); // get data send with request
	$app->getLog()->info('### GET /token');
	$app->getLog()->info('### ORIGIN: '.$_SESSION['OriginBB']);
	
	try {
		$token = guid();
		$_SESSION['token'] = $token;
		
		$response->code = '200';
		$response->token = $token;
		
		$app->getLog()->info('response: '.koko_json_encode($response));
	

		echo '{"response": '.koko_json_encode($response).'}';
		// success 200
	 } catch(Exception $e) {
		// error 500
		$app->getLog()->info(' DB ERROR: '.$e->getMessage());
		$app->halt(500,'{"error":{"code":"500","message":"'. $e->getMessage() .'"}}');
    }
});
?>