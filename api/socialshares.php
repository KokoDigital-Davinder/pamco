<?
$app->get('/share/:shareref', function ($shareref) use ($app) {	
	$app->getLog()->info('### GET /share/'.$shareref);
	// share on social media
	
	$bitly = 'http://www.loungeleap.co.uk';
	
	
	$data = $app->request();
	
	$score = filter_var($data->get('player_score'), FILTER_SANITIZE_NUMBER_INT);
	// only allow int to stop XSS hacks
	
	
	
	switch ($shareref){
		case 'facebook':
		
			$url = $app->APPShareurl.'?player_score='.$score.'&player_share=1';		
			header('Location:http://www.facebook.com/sharer.php?u='.urlencode($url));
				
		break;
		
		case 'twitter':

			$twitter_share_title = 'Play #LoungeLeap from @VirginMoney for a chance to win prizes. www.loungeleap.co.uk';
			
			if(strlen($data->get('player_score')) > 0 ){
				
				$twitter_share_title = 'I just scored '.$score.' playing #LoungeLeap from @VirginMoney! Play now for a chance to win prizes. www.loungeleap.co.uk';			
			}
			
			header('Location:http://twitter.com/intent/tweet?text='.rawurlencode($twitter_share_title));
		break;
		
		case 'googleplus':
			$url = $app->APPShareurl.'?player_score='.$score.'&player_share=1';
			
			header('Location:https://plus.google.com/share?url='.urlencode($url));
		break;

	 	case 'linkedin':
		
			$url = $app->APPShareurl.'?player_score='.$score.'&player_share=1';
				
			header('Location:http://www.linkedin.com/shareArticle?mini=true&url='.urlencode($url).'&title=' . str_replace('%2527', "'", str_replace('%2B', '+', urlencode($app->APPTitle))));

		break;
		
	}
});
?>