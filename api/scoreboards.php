<?

$app->get('/scores/:type', function ($type) use ($app) {
	// ### get scores
	$data = $app->request();
    $from = ($data->get('from'))? $data->get('from') : 0;
	$to = ($data->get('to')) ? $data->get('to') : intval($app->APPScoreboardDefaultLength)-1;
	$friends = ($data->get('friends')) ? $data->get('friends') : '';
	$app->getLog()->info('### GET /scores/'.$type);

	$sql = 'CALL spGetScores(:type,:from,:to,:friends)'; // sql string to call	
	try {
		 $generateCache = false;
		
		if($type=='facebook'){
			$myFile = $app->APPFBScoreboardFile;
			
			if(strlen($friends)>0){
				$myFile = 'dontcache.json';
				$generateCache  = true;
				// dont cache one to one fb scoreboard
				}
		} else{
			$myFile = $app->APPScoreboardFile;
		}
		// set correct file for caching scorboard
		
	if($app->APPCache == 'json'){
		if(file_exists('json/'.$myFile)){
		// check json file exists before reading from DB
            $date = filemtime('json/'.$myFile);
            $dateDiff    = date() - $date;  
            $fullHours   = floor($dateDiff/(60*60));
			if($fullHours>168){
				$generateCache  = true;
			}
		} else{
			$generateCache = true;
		}
	} else{
	
		if(($response = getCache(str_replace('.json',$app->APPMemcacheHash,$myFile))) === false) { 
			 $generateCache = true;
		}
		// check if memcache exists
	}

	$app->getLog()->info('### Generate new cache'.$generateCache);
	
	
		if($generateCache){
			
			
			$app->getLog()->info('call mysql: '.$sql.' data:'.$type.','.$from.','.$to.','.$friends);
			
			$db = getConnection();
			$stmt = $db->prepare($sql);
			$stmt->bindValue(':type', strval($type),PDO::PARAM_STR);
			$stmt->bindValue(':from', intval($from),PDO::PARAM_INT);
			$stmt->bindValue(':to', intval($to),PDO::PARAM_INT);
			$stmt->bindValue(':friends', strval($friends),PDO::PARAM_STR);
			$stmt->execute();
			$scores = $stmt->fetchAll(PDO::FETCH_OBJ);
			// get live score form database write to json file
			
			foreach ($scores as $row) {
				$row->player_full_name =  funcNameFormat($row->player_full_name);
			}
			
			
			$response->code = '200';
			$response->scores = $scores;
			
			
			
			
			if($myFile!='dontcache.json'){
				if($app->APPCache == 'json'){
					file_put_contents('json/'.$myFile,koko_json_encode($response). PHP_EOL, FILE_APPEND);
					// write json file;
				} else{
					setCache(str_replace('.json',$app->APPMemcacheHash,$myFile),$response,172800);
					//write memcache entry
				}
			}
			
		
		} else {
			if($app->APPCache == 'json'){
				$response = koko_json_decode(file_get_contents('json/'.$myFile, FILE_USE_INCLUDE_PATH));
				//read json file
			} 	
		}
		
	
		
		echo '{"response": '.koko_json_encode($response).'}';
		// success 200
		
	} catch(PDOException $e) {
		// mysql error 500
		$app->getLog()->info(' DB ERROR: '.$e->getMessage());
		$app->halt(500,'{"error":{"code":"500","message":"DB ERROR - '. $e->getMessage() .'"}}');
    }	
});
?>